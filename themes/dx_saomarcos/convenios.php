<?php include "inc/header.php" ?>
<!-- Our team Section -->
<section id="team" class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>CONVÊNIOS</h2>
                <h3 class="caption color-black-100 width50 centertable">
                    Agendamento de Consultas: (86) 2106-8262<br>
                    Horário: 14h às 17h</h3>
            </div><!-- /.col-md-12 -->

            <div class="clear20"></div>
            <div class="clear20"></div>


            <div class="container">
                <div class="row">

                    <?php
                    $read = new Read;
                    $where = Admx::Trash();
                    $read->ExeRead(DB_CONVENIOS, "WHERE status =:st {$where} ORDER BY convenio_image ASC, convenio_title ASC ", "st=1");
                    foreach ($read->getResult() as $row):
                        extract($row);
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 convenios">
                            <?php
                            if ($convenio_image && $convenio_image != 'zzzz'):
                                ?>
                                <div class="convenio_image">
                                    <img src="<?= BASE ?>/tim.php?src=uploads/<?= $convenio_image ?>&w=300&h=200">
                                </div>
                                <?php
                            endif;
                            ?>
                            <h3 class="post-it-note onfade4 bg-red-300 text-bold"
                                style="font-size:20px;margin-top: 0px; height: 80px !important">
                                <?= $convenio_title ?>
                            </h3>
                            <div class="clear50"></div>
                        </div>
                        <?php
                    endforeach;
                    ?>


                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear40"></div>

</section><!-- /.our-team -->
<?php include "inc/footer.php" ?>
