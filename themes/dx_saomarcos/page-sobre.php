<?php
extract($DataResult);
$missao = Admx::getMeta(DB_PAGES, '1', 'missao');
$visao = Admx::getMeta(DB_PAGES, '1', 'visao');
$valores = Admx::getMeta(DB_PAGES, '1', 'valores');
include "inc/header.php"
?>
    <style>
        .texto, .texto p {
            text-align: justify
        }
    </style>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2><?= $page_title ?></h2>
                    <h3 class="caption color-black-100 width50 centertable">
                        <?= $page_subtitle ?>
                    </h3>
                    <div class="clear20"></div>
                </div><!-- /.col-md-12 -->
                <div class="clear20"></div>
                <div class="col-md-12">
                    <div class="apcc-box">
                        <div class="clear10"></div>
                        <div style="display:table; width:100%;">
                            <div class="col-md-6">
                                <iframe width="560" style="width:100%; margin-top:10px;" height="315"
                                        src="https://www.youtube.com/embed/qKsJEb8lZJc" frameborder="0"
                                        allowfullscreen></iframe>
                            </div>
                            <div class="col-md-6">
                                <div class="texto" style="text-align:justify">
                                    <?= $page_content ?>
                                </div>
                            </div>
                            <div class="clear20"></div>
                            <div class="col-md-12">
                                <h3><strong>Missão</strong></h3>
                                <p><?= $missao ?></p>
                            </div>
                            <div class="clear20"></div>
                            <div class="col-md-12">
                                <h3><strong>Visão</strong></h3>
                                <p><?= $visao ?></p>
                            </div>
                            <div class="clear20"></div>
                            <div class="col-md-12">
                                <h3><strong>Valores</strong></h3>
                                <p><?= nl2br($valores) ?></p>
                            </div>
                        </div><!-- hack table -->
                    </div>
                </div>
                <!--
            <div class="col-md-6">
                <div class="apcc-box">
                    <h3><strong>Missão</strong></h3>
                    <p><?= $missao ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="apcc-box">
                    <h3><strong>Visão</strong></h3>
                    <p><?= $visao ?></p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="apcc-box">
                    <h3><strong>Valores</strong></h3>
                    <p><?= nl2br($valores) ?></p>
                </div>
            </div>
            <div class="clear20"></div>
            -->

            </div>
        </div>
    </section><!-- /.our-team -->
<?php include "inc/footer.php" ?>