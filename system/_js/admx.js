var base = '';
$(function () {
    base = $('link[rel="base"]').attr('href');
    //############## GET CEP
    $('.getCep').change(function () {
        var cep = $(this).val().replace('-', '').replace('.', '');
        if (cep.length === 8) {
            $.get("https://viacep.com.br/ws/" + cep + "/json", function (data) {
                if (!data.erro) {
                    $('.wc_bairro').val(data.bairro);
                    $('.wc_complemento').val(data.complemento);
                    $('.wc_localidade').val(data.localidade);
                    $('.wc_logradouro').val(data.logradouro);
                    $('.wc_uf').val(data.uf);
                }
            }, 'json');
        }
    });
    //IMAGE ERROR
    $('img').error(function () {
        var s, w, h;
        s = $(this).attr('src');
        w = s.split('&')[1].replace('w=', '');
        h = s.split('&')[2].replace('h=', '');
        $(this).attr('src', '../tim.php?src=system/_img/no_image.jpg&w=' + w + "&h=" + h);
    });
    $('.inputDatepicker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_3",
        format: 'DD/MM/YYYY',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1
        }
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    //NEW LINE ACTION
    $('textarea.work_mce').keypress(function (event) {
        if (event.which === 13) {
            var s = $(this).val();
            $(this).val(s + "\n");
        }
    });


    //AUTOSAVE ACTION
    $('form.auto_save').change(function () {
        var form = $(this);
        var callback = form.find('input[name="callback"]').val();
        var callback_action = form.find('input[name="callback_action"]').val();
        form.ajaxSubmit({
            url: '_ajax/' + callback + '.ajax.php',
            data: {callback_action: callback_action},
            dataType: 'json',
            uploadProgress: function (evento, posicao, total, completo) {
                var porcento = completo + '%';
                $('.workcontrol_upload_progrees').text(porcento);
                if (completo <= '80') {
                    $('.workcontrol_upload').fadeIn('fast');
                }
                if (completo >= '99') {
                    $('.workcontrol_upload').fadeOut('slow', function () {
                        $('.workcontrol_upload_progrees').text('0%');
                    });
                }
                //PREVENT TO RESUBMIT IMAGES GALLERY
                form.find('input[name="image[]"]').replaceWith($('input[name="image[]"]').clone());
            },
            success: function (data) {

                if (data.gallery) {
                    SetGallery(data.gallery);
//                    form.find('.gallery').fadeTo('300', '0.5', function () {
//                        $(this).html($(this).html() + data.gallery).fadeTo('300', '1');
//                    });
                }

                if (data.view) {
                    $('.wc_view').attr('href', data.view);
                }

                //CLEAR INPUT FILE
                if (!data.error) {
                    form.find('input[type="file"]').val('');
                }

                if (data.error) {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }
            }
        });
    });
    //Coloca todos os formulários em AJAX mode e inicia LOAD ao submeter!
    $('form').not('.ajax_off').submit(function () {
        var form = $(this);
        var callback = form.find('input[name="callback"]').val();
        var callback_action = form.find('input[name="callback_action"]').val();
        form.ajaxSubmit({
            url: '_ajax/' + callback + '.ajax.php',
            data: {callback_action: callback_action},
            dataType: 'json',
            beforeSubmit: function () {
                $('.loading_form').fadeIn('fast');
            },
            uploadProgress: function (evento, posicao, total, completo) {
                var porcento = completo + '%';
                $('.workcontrol_upload_progrees').text(porcento);
                if (completo <= '80') {
                    $('.workcontrol_upload').fadeIn('fast');
                }
                if (completo >= '99') {
                    $('.workcontrol_upload').fadeOut('slow', function () {
                        $('.workcontrol_upload_progrees').text('0%');
                    });
                }
                //PREVENT TO RESUBMIT IMAGES GALLERY
                form.find('input[name="image[]"]').replaceWith($('input[name="image[]"]').clone());
            },
            success: function (data) {
                form.find('input[type="file"]').val('');
                //REMOVE LOAD
                if (data.success) {
                    $('.loading_form').fadeOut();
                    trigger('info', 'Sucesso!', 'Tarefa realizada com sucesso!', 'dark');
                }

                if (data.image) {
                    $(".image").hide();
                    $(".image").attr("src", data.image.src);
                    $(".image").fadeIn();
                }

                if (data.tinyMCE) {
                    $('.loading_form').fadeOut();
                    tinyMCE.activeEditor.insertContent(data.tinyMCE);
                    $('.workcontrol_imageupload').fadeOut('slow', function () {
                        $('.workcontrol_imageupload .image_default').attr('src', '../tim.php?src=admin/_img/no_image.jpg&w=500&h=300');
                    });
                }

                if (data.error) {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }

                if (data.password) {
                    form.find('input[type="password"]').val('');
                }

                //REDIRECIONA
                if (data.redirect) {
                    window.setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 500);
                }


            }
        });
        return false;
    });


    //Chama as funcoes da galeria
    Gallery();

    // confirm delete
    $(".delete").click(function () {
        if ($(this).hasClass('trash')) {
            callback = $(this).attr('callback');
            callback_action = $(this).attr('callback_action');
            rel = $(this).attr('rel');
            pai = $(this).parents('.' + rel);
            id = $(this).attr('key_id');
            token = $(this).attr('formToken');
            $.post(base + "/_ajax/" + callback + ".ajax.php", {
                formToken: token,
                callback: callback,
                callback_action: callback_action,
                key_id: id
            }, function (data) {
                if (data.success) {
                    $(pai).fadeOut('300');
                } else {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }
            }, 'json');
        } else {
            $(this).css('display', 'none');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-warning');
            $(this).html('<i class="fa fa-exclamation-triangle"></i> Deseja excluir?');
            $(this).addClass('trash');
            $(this).fadeIn();
        }
    });


    $(".status").click(function () {
        callback = $(this).attr('callback');
        callback_action = $(this).attr('callback_action');
        rel = $(this).attr('rel');
        pai = $(this).parents('.' + rel);
        id = $(pai).attr('rel');
        filho = $(this);
        $.post(base + "/_ajax/" + callback + ".ajax.php", {
            callback: callback,
            callback_action: callback_action,
            key_id: id
        }, function (data) {
            if (data.success) {
                if ($(pai).hasClass('rascunho')) {
                    $(filho).removeClass('btn-default');
                    $(filho).addClass('btn-primary');
                    $(filho).html('<i class="fa fa-check"></i>');
                    $(pai).removeClass('rascunho');
                } else {
                    $(filho).removeClass('btn-primary');
                    $(filho).addClass('btn-default');
                    $(filho).html('<i class="fa fa-exclamation-triangle"></i> Ativar');
                    $(pai).addClass('rascunho');
                }
                if (data.cover) {
                    img = $(pai).find('.cover');
                    $(img).attr("src", "../tim.php?src=" + data.cover + "&w=200&h=200");
                }
            } else {
                trigger('error', 'Error!', 'Tente novamente mais tarde!');
            }
        }, 'json');
    });

    //ACTIONS GERAL
    $(".actions_status").click(function () {
        table = $(this).attr('table');
        key_name = $(this).attr('key_name');
        key_id = $(this).attr('key_id');
        value = $(this).attr('value');
        bt = $(this);

        $.post(base + "/_ajax/Actions.ajax.php", {
            callback: 'callback',
            callback_action: 'status',
            table: table,
            key_name: key_name,
            key_id: key_id,
            value: value
        }, function (data) {
            if (data.success) {
                if (data.success !== 'ativo') {
                    $(bt).removeClass("btn-primary");
                    $(bt).addClass("btn-default");
                    $(bt).html('<i class="fa fa-exclamation-triangle"></i> Ativar');
                    $(bt).attr('value', '1');
                } else {
                    $(bt).removeClass("btn-default");
                    $(bt).addClass("btn-primary");
                    $(bt).html('<i class="fa fa-check"></i>');
                    $(bt).attr('value', '0');
                }

            } else {
                trigger('error', 'Error!', 'Tente novamente mais tarde!');
            }
        }, 'json');
    });

    $(".actions_trash").click(function () {
        table = $(this).attr('table');
        key_name = $(this).attr('key_name');
        key_id = $(this).attr('key_id');
        value = $(this).attr('value');
        parent = $(this).attr('parent');
        bt = $(this);

        $.post(base + "/_ajax/Actions.ajax.php", {
            callback: 'callback',
            callback_action: 'trash',
            table: table,
            key_name: key_name,
            key_id: key_id,
            value: value
        }, function (data) {
            if (data.success) {
                pai = $(bt).parents("#" + parent);
                $(pai).remove();
                trigger('info', 'Sucesso!', 'Tarefa realizada com sucesso!', 'dark');
            } else {
                trigger('error', 'Error!', 'Tente novamente mais tarde!');
            }
        }, 'json');
    });


    $(".actions_delete").click(function () {
        if ($(this).hasClass('trash')) {
            table = $(this).attr('table');
            key_name = $(this).attr('key_name');
            key_id = $(this).attr('key_id');
            parent = $(this).attr('parent');
            bt = $(this);

            formToken = $(this).attr('formToken');
            $.post(base + "/_ajax/Actions.ajax.php", {
                callback: 'callback',
                callback_action: 'delete',
                table: table,
                key_name: key_name,
                key_id: key_id,
                formToken: formToken
            }, function (data) {
                if (data.success) {
                    parent = $(bt).parents("#" + parent);
                    $(parent).remove();
                    trigger('info', 'Sucesso!', 'Tarefa realizada com sucesso!', 'dark');
                } else {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }
            }, 'json');
        } else {
            $(this).css('display', 'none');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-warning');
            $(this).html('<i class="fa fa-exclamation-triangle"></i> Deseja excluir?');
            $(this).addClass('trash');
            $(this).fadeIn();
        }
    });


    $(".btn_search_top").click(function () {
        ;
        gotoSearch();
    });


    // confirm delete
    $(".categories_delete").click(function () {
        if ($(this).hasClass('trash')) {
            callback = $(this).attr('callback');
            callback_action = $(this).attr('callback_action');
            token = $(this).attr('formToken');
            key_id = $(this).attr('key_id');
            $.post(base + "/_ajax/" + callback + ".ajax.php", {
                formToken: token,
                callback: callback,
                callback_action: callback_action,
                key_id: key_id
            }, function (data) {
                if (data.success) {
                    trigger('info', 'Sucesso!', 'Tarefa realizada com sucesso!', 'dark');
                } else {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }

                if (data.redirect) {
                    window.setTimeout(function () {
                        window.location.href = data.redirect;
                    }, 500);
                }
            }, 'json');
        } else {
            $(this).css('display', 'none');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-warning');
            $(this).html('<i class="fa fa-exclamation-triangle"></i> Deseja excluir?');
            $(this).addClass('trash');
            $(this).fadeIn();
        }
    });

});

function enterpressSearch(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        gotoSearch();
    }
}

function gotoSearch() {
    urlSearch = $(".btn_search_top").attr("rel");
    param = $("#campo_search_top").val();
    location.href = urlSearch + "&s=" + param;
}

function trigger(type, title, text, style) {
    if (style === 'dark') {
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3',
            addclass: 'dark'
        });
    } else {
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3'
        });
    }
}

function SetGallery(json) {
    for (var x in json) {
        div = '<div class="col-md-55 gallery_item item_' + json[x].key + '" id="' + json[x].key + '">';
        div += '<div class="thumbnail">';
        div += '<div class="image">';
        div += '<img class="animate" src="../tim.php?src=uploads/' + json[x].src + '&w=250&h=150" alt="' + json[x].name + '" />';
        div += '</div>';
        div += '<div class="caption">';
        div += '<p class="file_name">' + json[x].name + '</p>';
        div += '<div class="clear5"></div>';
        div += '<button class="btn btn-sm btn-info "><i class="fa fa-pencil"></i></button>';
        div += '<button class="btn btn-sm btn-danger image_delete" rel="item_' + json[x].key + '" callback="Midias" callback_action="delete" key_id="' + json[x].key + '"><i class="fa fa-trash"></i></button>';
        div += '</div>';
        div += '</div>';
        div += '</div>';
        $(".gallery").prepend(div);
    }
    //Chama as funcoes da galeria
    Gallery();
}

function Gallery() {

    //Gallery
    $(".image_delete").click(function () {
        if ($(this).hasClass('trash')) {
            callback = $(this).attr('callback');
            callback_action = $(this).attr('callback_action');
            rel = $(this).attr('rel');
            pai = $(this).parents('.' + rel);
            id = $(this).attr('key_id');
            token = $(this).attr('formToken');
            $.post(base + "/_ajax/" + callback + ".ajax.php", {
                formToken: token,
                callback: callback,
                callback_action: callback_action,
                key_id: id
            }, function (data) {
                if (data.result == 'success') {
                    $(pai).fadeOut('300');
                } else {
                    trigger('error', 'Error!', 'Tente novamente mais tarde!');
                }
            }, 'json');
        } else {
            $(this).css('display', 'none');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-warning');
            $(this).html('<i class="fa fa-exclamation-triangle"></i> Deseja excluir?');
            $(this).addClass('trash');
            $(this).fadeIn();
        }
    });
    //bottam edit
    $(".gallery .btn-info").click(function () {
        gallery_item = $(this).parents('.gallery_item');
        src = $(gallery_item).find('img').attr("src");
        file_name = $(gallery_item).find('.file_name').text();
        key_id = $(gallery_item).attr('id');
        $(".key_id").val(key_id);
        $("#modal_gallery").find('img').attr('src', src);
        $("#modal_gallery").find('.file_name').val(file_name);
        $("#modal_gallery").modal();
    });

    $(".modal_gallery_form").submit(function () {
        return false;
    });
    $(".modal_gallery_save_name").click(function () {
        var form = $('.modal_gallery_form');
        var callback = form.find('input[name="callback"]').val();
        var callback_action = form.find('input[name="callback_action"]').val();
        var key_id = form.find('input[name="key_id"]').val();
        var file_name = form.find('input[name="file_name"]').val();
        $.post(base + "/_ajax/" + callback + ".ajax.php", {
            callback: callback,
            callback_action: callback_action,
            key_id: key_id,
            file_name: file_name
        }, function (data) {
            console.log(data.result);
            if (data.result === 'success') {
                gallery_item = $('#' + key_id);
                $(gallery_item).find('img').attr("src", data.src);
                $(gallery_item).find('.file_name').text(data.text);
                $("#modal_gallery").modal('hide');
                trigger('info', 'Sucesso', 'Tarefa realizada com sucesso', 'dark');
            } else {
                $("#modal_gallery").modal('hide');
                trigger('error', 'Error!', 'Tente novamente mais tarde!');
            }
        }, 'json');
    });
}
