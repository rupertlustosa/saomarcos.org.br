<?php include("inc/header.php") ?>
<!-- Our team Section -->
<section class="content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>FALE CONOSCO</h2>
                <h3 class="caption color-black-100 width50 centertable">Utilize este espaço para enviar sugestões,
                    dúvidas, elogios ou reclamações.</h3>
            </div><!-- /.col-md-12 -->

            <div class="clear40"></div>
        </div>

        <body>
        <div class="container">
            <div class="innerwrap">
                <section class="section2 clearfix">
                    <div class="col2 column1 first">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3974.0817129328907!2d-42.80449118572071!3d-5.09048865303796!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e375e28097ba3%3A0xecb20b737393ff3b!2sHospital+S%C3%A3o+Marcos!5e0!3m2!1spt-BR!2sbr!4v1473964534509"
                                width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col2 column2 last">
                        <div class="sec2innercont">
                            <div class="sec2addr">
                                <p>Associação Piauiense de Combate ao Câncer</p>
                                <p><span class="collig">Endereço :</span>Rua Olavo Bilac, 2300 - Centro, Teresina- PI
                                </p>
                                <p><span class="collig">Telefone :</span> 86 2106-8000 | 86 2106-8262</p>
                                <p><span class="collig">Cep :</span> 64001-280</p>
                            </div>
                        </div>
                        <div class="sec2contactform">
                            <form action="javascript:;" id="formContato">
                                <div class="clearfix">
                                    <input type="text" required="required" name="contactNome" maxlength="200"
                                           class="col2 first" placeholder="Nome">
                                    <input type="email" required="required" name="contactMail" maxlength="200"
                                           class="col2 last" placeholder="Email">
                                </div>
                                <div class="clear10"></div>
                                <div class="clearfix">
                                    <label>EU SOU:</label>
                                    <div style="padding-left: 20px;">
                                        <label class="checkbox"><input type="checkbox" value="Acompanhante"
                                                                       name="contactSOU[]">Acompanhante</label>
                                        <label class="checkbox"><input type="checkbox" value="Fornecedor"
                                                                       name="contactSOU[]">Fornecedor</label>
                                        <label class="checkbox"><input type="checkbox" value="Paciente"
                                                                       name="contactSOU[]">Paciente</label>
                                        <label class="checkbox"><input type="checkbox" value="Estudante"
                                                                       name="contactSOU[]">Estudante</label>
                                        <label class="checkbox"><input type="checkbox" value="Funcionário"
                                                                       name="contactSOU[]">Funcionário</label>
                                        <label class="checkbox"><input type="checkbox" value="Profissional de Saúde"
                                                                       name="contactSOU[]">Profissional de Saúde</label>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <input type="text" class="col2 first phone" maxlength="200" name="contactTelCasa"
                                           placeholder="Telefone Residencial">
                                    <input type="text" class="col2 last phone" maxlength="200"
                                           name="contactTelComercial" placeholder="Telefone Comercial">
                                </div>
                                <div class="clearfix">
                                    <textarea id="" cols="30" required="required" maxlength="500" name="contactMensagem"
                                              rows="3" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="clearfix">
                                    <div class="response"></div>
                                    <input type="submit" class="btn-default-hsm bg-red-300" value="Enviar">
                                </div>
                            </form>
                        </div>

                    </div>
                </section>

            </div>
        </div>
        </body>

</section>
<script>
    $(function () {
        $(".phone").mask("(99) 9.9999-9999");
        $(".response").click(function () {
            $(this).html('');
        })
        base = $('link[rel="base"]').attr('href');
        $("#formContato").submit(function () {
            data = $(this).serialize();
            $.post(base + "/sendmail.php", {data: data}, function (response) {
                if (response == 1) {
                    $(".response").html('<div class="alert alert-success" role="alert">Formulário enviado com sucesso!</div>');
                } else {
                    $(".response").html('<div class="alert alert-success" role="alert">' + response + '</div>');
                }
                console.log(response);
            });
            this.reset();
        })

    })


</script>
<?php
include("inc/footer.php")
?>
