<?php
ob_start();
include "../_app/autoload.php";
$hoje = date('Y-m-d');
$host = BASE;

header("Content-Type: application/xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';

$noticias = new Read;
$noticias->ExeRead("dx_posts"," WHERE post_date <= NOW() and status = 1");

?>

<urlset
xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<url>
  <loc><?=$host?></loc>
  <lastmod><?php echo $hoje;?></lastmod>
  <priority>0.9</priority>
  <changefreq>daily</changefreq>
</url>


<?php
    foreach ($noticias->getResult() as $post):
    echo "<url>
  		<loc>".$host."/artigo/".$post['post_name']."</loc>
  		<lastmod>".$hoje."</lastmod>
  		<changefreq>daily</changefreq>
  		<priority>0.8</priority>
  	</url>";
    endforeach;
?>


</urlset>
