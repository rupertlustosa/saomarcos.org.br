<?php 
include "header.html";
?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            
            <div class="col-md-12">
                
                <h2>TITULO DA ESPECIALIDADE</h2>
                <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999"> Profissionais</h3>
            </div><!-- /.col-md-12 -->

            <div class="clear40"></div>

            <div class="container">

                <div class="row">

                    <?php
                    //LISTAGEM DOS MEDICOS
                    foreach ($medisoc as $medico):
                        
                        ?>
                        <div class="col-lg-3">
                            <article class="SvItem">
                                <div class="anchorSv onfade4 zd100" href="" title="">
                                    <div class="icoContentSv zd50">
                                        <div class="insetBlk centralizar">
                                            <span class="fa fa-user-md fontsize2"></span>
                                        </div>
                                    </div>
                                    <div class="brdContentSv zd45">
                                        <div class="insetSlk centralizar">
                                            <h1 class="font15px al-center">
                                                <a style="font-size:15px;" href="<?=BASE?>/profissional/nome-do-medico">
                                                    NOME DO MÉIDCO                                                    
                                                </a>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php
                    endforeach;
                    ?>
                </div>

            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>
    <div style="display:table; margin: 20px auto">
        <!-- PAGINAÇÃO -->
        <ul class="pagination">
            <li><a title="&lt;&lt;" href="pagina-1">&lt;&lt;</a></li>
            <li class="active"><a href="javascript:;">1 <span class="sr-only">(current)</span></a></li>
            <li><a title="Página 2" href="pagina-2">2</a></li>
            <li><a title="Página 3" href="pagina-3">3</a></li>
            <li><a title="Página 4" href="pagina-4">4</a></li>
            <li><a title="Página 5" href="pagina-5">5</a></li>
            <li><a title="Página 6" href="pagina-6">6</a></li>
            <li><a title="&gt;&gt;" href="pagina-104">&gt;&gt;</a></li>
        </ul>
    </div>
</section><!-- /.our-team -->

<?php include "footer.html" ?>