<?php
Users::AutorArtigoLogout();
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$msg = null;
$step = 1;

if ($PostData) {
    $cpf = $PostData['cliente_cpf'];

    if ($PostData['step'] == 1) {
        $Read->FullRead("SELECT id,email FROM dx_clientes WHERE cpf=:cpf ", "cpf={$cpf}");
        if ($Read->getResult()) {
            list($email, $provedor) = explode("@", $Read->getResult()[0]['email']);
            $step = 2;
            $msg = '<div class="alert alert-info " role="alert">Digite o seu email *******@' . $provedor . '!</div>';
        } else {
            $step == 1;
            $msg = '<div class="alert alert-danger " role="alert">O CPF <strong>' . $cpf . '</strong> não está cadastrado!</div>';
        }
    }

    if ($PostData['step'] == 2) {
        $cpf = $PostData['cliente_cpf'];
        $email = $PostData['cliente_email'];
        $Read->FullRead("SELECT id,nome,email FROM dx_clientes WHERE cpf=:cpf and email=:em ", "cpf={$cpf}&em={$email}");
        require('_app/Library/PHPMailer/class.phpmailer.php');
        if ($Read->getResult()) {
            $id = $Read->getResult()[0]['id'];
            $email = $Read->getResult()[0]['email'];
            $nome = $Read->getResult()[0]['nome'];
            $senha = cc_rand(rand(6, 12));
            $step = 1;
            $HTML = "olá {$nome}, sua nova senha de acesso é {$senha}.<br> Para enviar seus artigos acesse <a href='" . BASE . "/{$URL[0]}/login'>aqui</a> ";
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
            $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
            $mail->Password = 'apcchsm$69';                           // SMTP password
            $mail->Port = 25;
            $mail->FromName = "WebMaster";
            $mail->AddAddress($email, $nome);
            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->Subject = "|| NOVA SENHA HSM ||"; // Assunto da mensagem
            $mail->Body = $HTML;
            $enviado = $mail->Send();
            if ($enviado) {
                $Update = new Update;
                $data['senha'] = Admx::encripta($senha);
                $Update->ExeUpdate("dx_clientes", $data, " WHERE id=:id", "id={$id}");
                $msg = '<div class="alert alert-info " role="alert">' . $nome . ', Sua nova senha foi enviada para seu email!</div>';
            } else {
                $msg = '<div class="alert alert-danger " role="alert">' . $nome . ', Não conseguimos enviar a senha para seu email, tente novamente mais tarde!</div>';
            }


        } else {
            $step = 2;
            $msg = '<div class="alert alert-danger " role="alert">O email não conrresponde!</div>';
        }
    }

}

?>


<div class="clear20"></div>

<div class="well" style="max-width:300px; margin:0px auto;">
    <form id="envia_artigo" method="post">
        <div style="display:table; width:100%; text-align:left;">
            <div class="response">
                <?php
                if ($msg) {
                    echo $msg;
                }
                ?>
            </div>
            <?php
            if ($step == 1) {
                ?>
                <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" name="cliente_cpf" class="form-control cpf" id="cpf" placeholder="CPF">
                </div>
                <?php
            } else {
                ?>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="cliente_email" class="form-control" id="email" placeholder="email">
                </div>
                <input type="hidden" name="cliente_cpf" value="<?= $cpf ?>">
                <?php
            }
            ?>
            <input type="hidden" name="step" value="<?= $step ?>">


            <button type="submit" class="btn btn-default pull-left">Enviar</button>

            <a href="<?= BASE . "/{$URL[0]}/cadastre-se" ?>" class="pull-right">Cadastre-se</a>

        </div>
    </form>
</div>
<div class="clear20"></div>