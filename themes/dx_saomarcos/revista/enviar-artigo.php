<?php

$jSON = null;
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$params = $PostData;
$types = array(
    'application/x-compressed',
    'application/x-zip-compressed',
    'application/zip',
    'multipart/x-zip',
    'application/pdf',
    'application/x-7z-compressed',
    'application/octet-stream',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'application/vnd.ms-word.document.macroEnabled.12',
    'application/vnd.ms-word.template.macroEnabled.12',
);
$maxSize = 1024 * 1024 * 2; // 2Mb
$File = null;

if (!empty($_FILES['arquivo'])):
    $File = $_FILES['arquivo'];
endif;
$ext = substr($File['name'], -4);
$nome_final = md5(time()) . $ext;
$msg = null;
$autor = Users::GetAutorArtigo(array('cliente_id', 'cliente_nome'));

if ($File) {

    if (in_array($File['type'], $types)) {
        if ($File['size'] <= $maxSize) {
            $src = 'uploads/revista/' . $nome_final;
            if (move_uploaded_file($File['tmp_name'], $src)) {
                $id = $autor['cliente_id'];
                $data['artigo_autor_id'] = $id;
                $data['artigo_title'] = $PostData['title'];
                $data['artigo_src'] = $src;

                $Create = new Create;
                $Create->ExeCreate("dx_artigos_revista", $data);
                $msg = '<div class="alert alert-success " role="alert">Artigo enviado com sucesso!</div>';
            }
        } else {
            $msg = '<div class="alert alert-danger " role="alert">Arquivo muito grande máximo 2mb</div>';
        }
    } else {
        $msg = '<div class="alert alert-danger " role="alert">Arquivo inválido</div>';
    }

}
?>


<div class="clear20"></div>

<div class="well" style="max-width:500px; margin:0px auto;">
    <form id="envia_artigo" enctype="multipart/form-data" method="post">
        <div style="display:table; width:100%; text-align:left;">
            <div class="response">
                <?php
                if ($msg) {
                    echo $msg;
                }
                ?>
            </div>
            <div class="clear20"></div>

            <div class="form-group">
                <label for="title">Titulo</label>
                <input type="text" class="form-control" name="title" id="title">
            </div>
            <div class="form-group">
                <label for="cliente_nome">Artigo</label>
                <input type="file" name="arquivo" id="cliente_nome">
                <p class="help-block">extesão: *.doc | *.docx</p>
            </div>

            <button type="submit" class="btn btn-info pull-right" style="margin-top:20px;">Envia</button>
        </div>
    </form>
</div>
<div class="clear20"></div>