<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'corpo-docente';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack && $PostData['formToken'] == $formToke):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            $Read->ExeRead(DB_CORPO_DOCENTE, "WHERE id = :id", "id={$KeyId}");
            if (!$Read->getResult()):


            else:

                $nome = !empty($PostData['nome']) ? Check::Name($PostData['nome']) : $post['id'];

                if (!empty($File)):
                    $Upload->Image($File, "{$nome}-" . time(), 200);

                    if ($Upload->getResult()):
                        $image = $Upload->getResult();
                        $PostData['image'] = $image;
                        $jSON['image'] = ["src" => "../tim.php?src=uploads/{$image}&w=200"];
                    endif;
                endif;

                $ReadName = new $Read;
                $ReadName->FullRead("SELECT nome FROM " . DB_CORPO_DOCENTE . " WHERE nome= :nm and id != :id ", "nm={$nome}&id={$KeyId}");

                $PostData['status'] = ($rascunho) ? 0 : 1;

                //print_r($PostData);

                $post = $Read->getResult()[0];

                $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE id = :id", "id={$KeyId}");
                $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$nome}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];

            endif;
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
