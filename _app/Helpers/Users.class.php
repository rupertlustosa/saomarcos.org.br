<?php

class Users
{
    public static function GetUserImage($user_id, $subdir = '../')
    {
        $read = new Read();
        $array = array();
        $avatar_homem = ADM_PATH . "/img/avatar-homem.jpg";
        $avatar_mulher = ADM_PATH . "/_img/avatar-mulher.jpg";
        $avatar_no_avatar = ADM_PATH . "/_img/no_avatar.jpg";
        $sql = "SELECT user_id,user_image, user_genero,user_status FROM " . DB_USERS . " WHERE user_id=" . $user_id;
        $read->FullRead($sql);
        if ($read->getRowCount()):
            $image = $read->getResult()[0]['user_image'];
            if ($read->getResult()[0]['user_status'] == 0) {
                return $avatar_no_avatar;
            } else {

                if (file_exists($subdir . "uploads/" . $image) && !is_dir($subdir . "uploads/" . $image)) {
                    return "uploads/" . $image;
                } else {
                    if ($read->getResult()[0]['user_genero'] == 'M') {
                        return $avatar_homem;
                    }
                    if ($read->getResult()[0]['user_genero'] == 'F') {
                        return $avatar_mulher;
                    } else {
                        return $avatar_no_avatar;
                    }
                }
            }

        else:
            return null;
        endif;
    }

    public static function CheckLoginAutor()
    {
        if ($_SESSION['user']) {
            $token = $_SESSION['user'];
            $Read = new Read;
            $Read->FullRead("SELECT * FROM dx_clientes WHERE token = :tk", "tk={$token}");
            if ($Read->getResult()) {
                $diff = time() - $Read->getResult()[0]['time'];
                if ($diff > 7200) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public static function GetAutorArtigo($fields = null)
    {
        if ($_SESSION['user']) {
            $token = $_SESSION['user'];
            $Read = new Read;
            $Read->FullRead("SELECT * FROM dx_clientes WHERE token = :tk", "tk={$token}");
            if ($Read->getResult()) {
                $diff = time() - $Read->getResult()[0]['time'];
                if ($diff > 7200) {
                    return false;
                } else {
                    $dados = $Read->getResult()[0];
                    $user = array();
                    unset($dados['token'], $dados['time'], $dados['created'], $dados['senha']);
                    foreach ($dados as $key => $value) {
                        if ($fields) {
                            if (in_array('cliente_' . $key, $fields)) {
                                $user['cliente_' . $key] = $value;
                            }
                        } else {
                            $user['cliente_' . $key] = $value;
                        }
                    }
                    return $user;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public static function AutorArtigoLogout()
    {
        unset($_SESSION['user']);
    }

    public static function getAutorToken()
    {
        if ($_SESSION['user']) {
            return $_SESSION['user'];
        }
    }

}