<div class="response">
    <?php
    if ($msg) {
        echo $msg;
    }
    ?>
</div>

<div class="form-group col-xs-12 col-sm-5">
    <label for="cliente_nome">Nome</label>
    <input type="text" name="cliente_nome" value="<?= isset($cliente_nome) ? $cliente_nome : '' ?>" class="form-control"
           id="cliente_nome" placeholder="Nome">
</div>

<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_email">E-mail</label>
    <input type="email" name="cliente_email" value="<?= isset($cliente_email) ? $cliente_email : '' ?>"
           class="form-control" id="cliente_email" placeholder="Email">
</div>

<div class="form-group col-xs-12 col-sm-3">
    <label for="cliente_cpf">CPF</label>
    <input type="text" name="cliente_cpf" value="<?= isset($cliente_cpf) ? $cliente_cpf : '' ?>"
           class="form-control cpf" id="cliente_cpf" placeholder="CPF">
</div>

<div class="clear10"></div>

<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_rg">RG</label>
    <input type="text" name="cliente_rg" value="<?= isset($cliente_rg) ? $cliente_rg : '' ?>" class="form-control"
           id="cliente_rg" placeholder="RG">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_senha">Senha</label>
    <input type="password" name="cliente_senha" class="form-control" id="cliente_senha" placeholder="Senha">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_confirm_senha">Confirme a senha</label>
    <input type="password" class="form-control" name="cliente_confirm_senha" id="cliente_confirm_senha"
           placeholder="Confirme a senha">
</div>

<div class="clear10"></div>

<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_telefone">Telefone</label>
    <input type="text" class="form-control fone" value="<?= isset($cliente_telefone) ? $cliente_telefone : '' ?>"
           name="cliente_telefone" id="cliente_telefone" placeholder="Telefone">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_telefone_contato">Telefone Contato</label>
    <input type="email" class="form-control fone"
           value="<?= isset($cliente_telefone_contato) ? $cliente_telefone_contato : '' ?>"
           name="cliente_telefone_contato" id="cliente_telefone_contato" placeholder="Telefone Contato">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_celular">Celular</label>
    <input type="text" class="form-control cel" value="<?= isset($cliente_celular) ? $cliente_celular : '' ?>"
           name="cliente_celular" id="cliente_celular" placeholder="Celular">
</div>

<div class="clear10"></div>

<div class="form-group col-xs-12 col-sm-6">
    <label for="cliente_instituicao_ensino">Instituição de Ensino</label>
    <input type="text" class="form-control"
           value="<?= isset($cliente_instituicao_ensino) ? $cliente_instituicao_ensino : '' ?>"
           name="cliente_instituicao_ensino" id="cliente_instituicao_ensino" placeholder="Instituição de Ensino">
</div>

<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_graduacao">Graduação</label>
    <?php
    $cliente_graduacao = (isset($cliente_graduacao)) ? $cliente_graduacao : null
    ?>
    <select id="inscricao_graduacao" name="cliente_graduacao" id="cliente_graduacao" class="form-control"
            required="required">
        <option value=""></option>
        <option value="Graduando" <?= ($cliente_graduacao == 'Graduando') ? 'selected="selected"' : '' ?> >Graduando
        </option>
        <option value="Graduado" <?= ($cliente_graduacao == 'Graduado') ? 'selected="selected"' : '' ?> >Graduado
        </option>
        <option value="Residente" <?= ($cliente_graduacao == 'Residente') ? 'selected="selected"' : '' ?> >Residente
        </option>
        <option value="Pós-graduando" <?= ($cliente_graduacao == 'Pós-graduando') ? 'selected="selected"' : '' ?> >
            Pós-graduando
        </option>
        <option value="Pós-graduado" <?= ($cliente_graduacao == 'Pós-graduado') ? 'selected="selected"' : '' ?> >
            Pós-graduado
        </option>
    </select>
</div>

<div class="clear10"></div>
<div class="form-group col-xs-12 col-sm-6">
    <label for="cliente_endereco">Endereço</label>
    <input type="text" class="form-control" value="<?= isset($cliente_endereco) ? $cliente_endereco : '' ?>"
           name="cliente_endereco" id="cliente_endereco" placeholder="Endereço">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_cep">CEP</label>
    <input type="text" class="form-control  cep" value="<?= isset($cliente_cep) ? $cliente_cep : '' ?>" id="cliente_cep"
           id="cliente_cep" placeholder="CEP">
</div>
<div class="form-group col-xs-12 col-sm-2">
    <label for="cliente_numero">Numero</label>
    <input type="text" class="form-control" value="<?= isset($cliente_numero) ? $cliente_numero : '' ?>"
           id="cliente_numero" name="cliente_numero" placeholder="numero">
</div>

<div class="clear10"></div>
<div class="form-group col-xs-12 col-sm-6">
    <label for="cliente_complemento">Complemento</label>
    <input type="text" class="form-control" value="<?= isset($cliente_complemento) ? $cliente_complemento : '' ?>"
           id="cliente_complemento" name="cliente_complemento" placeholder="Complemento">
</div>
<div class="form-group col-xs-12 col-sm-4">
    <label for="cliente_cidade">Cidade</label>
    <input type="text" class="form-control" value="<?= isset($cliente_cidade) ? $cliente_cidade : '' ?>"
           id="cliente_cidade" name="cliente_cidade" placeholder="Cidade">
</div>
<div class="form-group col-xs-12 col-sm-2">
    <label for="cliente_uf">UF</label>
    <select class="form-control" name="cliente_uf">
        <option></option>
        <?php
        $cliente_uf = (isset($cliente_uf)) ? $cliente_uf : null;
        foreach (getListUF() as $key => $val) {
            $selected = ($val == $cliente_uf) ? 'selected="selected"' : '';
            echo '<option value="' . $val . '" ' . $selected . ' >' . $val . '</option>';
        }
        ?>
    </select>
</div>

<div class="clear10"></div>
<div class="form-group col-xs-12 col-sm-4">
    <button type="submit" class="btn btn-default">Salvar</button>
</div>