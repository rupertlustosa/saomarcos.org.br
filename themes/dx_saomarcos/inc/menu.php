<nav class="navbar navbar-default nomargin nopadding" style="border: none; margin-top: 60px;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?= BASE ?>">HOME <span class="sr-only">(atual)</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu tuppercase">
                    <li><a href="<?= BASE ?>/sobre">SOBRE O HOSPITAL</a></li>
                    <!--<li><a href="<?= BASE ?>/contato">LOCALIZAÇÃO</a></li>-->
                    <li><a href="<?= BASE ?>/diretores">DIRETORIA</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">ESPECIALIDADES</a>
                        <ul class="dropdown-menu tuppercase">
                            <?php
                            foreach (getEspecialidadesSecao() as $sec) {
                                echo '<li><a href="' . BASE . '/especialidades/' . $sec[1] . '">' . $sec[0] . '</a></li>';
                            }
                            ?>

                        </ul>
                    </li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">PACIENTES <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu tuppercase">
                    <li><a href="<?= BASE ?>/convenios">Convênios do Hospital</a></li>
                    <li><a href="<?= BASE ?>/profissionais">ENCONTRE SEU MÉDICO</a></li>
                    <li><a href="<?= Admx::getMeta('dx_configs', 1, 'url_exames') ?>">RESULTADOS DE EXAMES</a></li>
                    <li><a href="<?= BASE ?>/acomodacoes">ACOMODAÇÕES</a></li>

                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">SERVIÇOS <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu tuppercase">
                    <li><a href="<?= BASE ?>/fisioterapia">FISIOTERAPIA</a></li>
                    <li><a href="<?= BASE ?>/nuttem">NUTTEM</a></li>
                    <li><a href="<?= BASE ?>/psicologia">PSICOLOGIA</a></li>
                    <li><a href="<?= BASE ?>/servico-social">SERVIÇO SOCIAL</a></li>
                    <li><a href="<?= BASE ?>/cartao-hsm+">CARTÃO HSM+</a></li>
                    <li><a href="<?= BASE ?>/uploads/termo-consentimento-informado.pdf" target="_blank">TERMO DE CONSENTIMENTO INFORMADO</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            RESPONSABILIDADE SOCIAL
                        </a>
                        <ul class="dropdown-menu tuppercase">
                            <?php
                            $responsabilidade = Site::getResponsabilidadeSocial();
                            foreach ($responsabilidade as $row) {
                                echo '<li><a href="' . BASE . '/responsabilidade-social/' . $row['social_name'] . '">' . $row['social_title'] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">ENSINO E PESQUISA
                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu tuppercase">

                    <li><a href="<?= BASE ?>/comite-de-etica">Comitê de ética</a></li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            Periódico Científico
                        </a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="<?= BASE ?>/revista-medica">Revista Médica</a></li>
                            <li><a href="http://ojs.saomarcos.org.br/ojs/index.php/cientifica" target="_blank">Revista
                                    Multiprofissional</a></li>
                            <!--li><a href="<?= BASE ?>/anais">Anais</a></li-->
                        </ul>
                    </li>

                    <li>
                        <a href="http://www.saomarcos.org.br/web/xhtml/residenciaMedica/listarAtualizacoes.xhtml"
                           target="_blank">Residencia Médica
                        </a>
                    </li>

                    <li class="dropdown-submenu">
                        <a href="<?= BASE ?>/cursos" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            Cursos
                        </a>
                        <ul class="dropdown-menu tuppercase">
                            <?php
                            $tipo_curso = Site::getTipoCurso();
                            foreach ($tipo_curso as $row) {
                                echo '<li><a href="' . BASE . '/cursos/' . $row['id'] . '">' . $row['tipo'] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </li>


            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">NOVIDADES <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu tuppercase">
                    <li><a href="<?= BASE ?>/noticias">Notícias</a></li>
                    <li><a href="<?= BASE ?>/videos">Vídeos</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">CONTATO <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu tuppercase">
                    <li><a href="<?= BASE ?>/contato">FALE CONOSCO</a></li>
                    <li><a href="<?= BASE ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
                </ul>
            </li>

        </ul>
    </div>

</nav>
