<IfModule mod_rewrite.c>
RewriteEngine On
Options All -Indexes

RewriteCond %{SCRIPT_FILENAME} !-f
RewriteCond %{SCRIPT_FILENAME} !-d
RewriteRule ^(.*)/(.*)$ response.php?action=$1&id=$2
</IfModule>
