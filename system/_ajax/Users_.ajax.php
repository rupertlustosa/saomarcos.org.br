<?php

require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'users';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            $Read->ExeRead(DB_USERS, "WHERE user_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):
                $jSON['error'] = 'ok';
            else:
                $post = $Read->getResult()[0];
                $name = !empty($PostData['user_name']) ? $PostData['user_name'] : $post['user_id'];
                if (!empty($File)):
                    $Upload->Image($File, "{$name}-" . time(), 1000);
                    if ($Upload->getResult()):
                        $image = $Upload->getResult();
                        $PostData['user_image'] = $image;
                        $jSON['image'] = ["src" => "../tim.php?src=uploads/{$image}&w=350&h=350"];
                    endif;
                endif;

                $permissoes = !empty($PostData['permissoes']) ? $PostData['permissoes'] : null;

                if ($permissoes):
                    $Delete->ExeDelete(DB_PERMISSION, " WHERE user_id=:id", "id={$KeyId}");
                    if ($Delete->getResult()):
                        foreach ($permissoes as $p):
                            $Dados['user_id'] = $KeyId;
                            $Dados['table_id'] = $p;
                            $Create->ExeCreate(DB_PERMISSION, $Dados);
                        endforeach;
                    endif;
                endif;

                unset($PostData['permissoes']);


                if (!empty($PostData['user_password'])) {
                    $PostData['user_password'] = md5($PostData['user_password'] . HASH_KEY);
                } else {
                    unset($PostData['user_password']);
                }

                $PostData['user_status'] = ($rascunho) ? 0 : 1;

                $Update->ExeUpdate(DB_USERS, $PostData, "WHERE user_id = :id", "id={$KeyId}");
                $jSON["success"] = 'ok';
                $jSON["password"] = "empty";
            endif;
            break;

        case 'delete':
            $Read->FullRead("SELECT user_id,user_name FROM " . DB_USERS . " WHERE user_id=:id", "id={$KeyId}");
            if ($Read->getResult()):
                if ($Read->getResult()[0]['user_id'] == 1):
                    $jSON['error'] = 'ok';
                else:
                    $Delete->ExeDelete(DB_USERS, " WHERE user_id = :id ", "id={$KeyId}");
                    $jSON["success"] = 'ok';
                endif;

            else:
                $jSON['error'] = 'ok';
            endif;
            break;

        case 'status':
            $Read->FullRead("SELECT user_id,user_name,user_status FROM " . DB_USERS . " WHERE user_id=:id", "id={$KeyId}");
            if ($Read->getResult()):
                $name = $Read->getResult()[0]['user_name'];
                if ($Read->getResult()[0]['user_status'] == 1):
                    $Dados['user_status'] = 0;
                else:
                    $Dados['user_status'] = 1;
                endif;
                $Update->ExeUpdate(DB_USERS, $Dados, " WHERE user_id =:id ", "id={$KeyId}");
                usleep(10000);
                $jSON["cover"] = Users::GetUserImage($KeyId, '../../');
                $jSON["success"] = 'ok';
            else:
                $jSON['error'] = 'ok';
            endif;
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
