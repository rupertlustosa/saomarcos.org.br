<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['status'] = 0;
    $create->ExeCreate("dx_videos", $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=videos/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM dx_videos WHERE video_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/videos/videos.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?>
                    <small> <?= $Post['video_title'] ?></small>
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <?= Admx::formSearch($TABLE['VIEW']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Título do Post</label>
                                    <input type="text" name="video_title" value="<?= $Post['video_title'] ?>"
                                           placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea name="video_desc"
                                              placeholder="Digite a descrição do <?= $TABLE['TITLE_SINGLE'] ?> "
                                              class="form-control"><?= $Post['video_desc'] ?></textarea>

                                </div>

                                <div class="form-group">
                                    <label>Url youtube</label>
                                    <input type="text" name="video_url" value="<?= $Post['video_url'] ?>"
                                           placeholder="Digite a url do <?= $TABLE['TITLE_SINGLE'] ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left:20px">
                                <div class="xdisplay_inputx form-group has-feedback" style="width:100%">
                                    <label>Data</label>
                                    <input type="text" name="video_date"
                                           value="<?= ($Post['video_date'] == '0000-00-00 00:00:00' || empty($Post['video_date'])) ? date('d/m/Y') : Check::DateDecode($Post['video_date']) ?>"
                                           style="padding-left:55px;" placeholder="Data" id="single_cal3"
                                           class="inputDatepicker form-control has-feedback-left active">
                                    <span aria-hidden="true" style="margin-top:5px;"
                                          class="fa fa-calendar-o form-control-feedback left"></span>
                                    <span class="sr-only" id="inputSuccess2Status3">(success)</span>
                                </div>
                                <div class="clear10"></div>
                                <div class="form-group">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                        </label>
                                    </div>
                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                    <img src="_img/load.gif" class="loading_form pull-right">

                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->
                <div class="clear20"></div>

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php
//include './_inc/gallery.php';
include './_inc/footer.php';
?>
