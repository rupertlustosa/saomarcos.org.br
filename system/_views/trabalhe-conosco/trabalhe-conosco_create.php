<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['vaga'] = '';
    $dados['image'] = '';
    $dados['informacoes'] = '';
    $dados['trash'] = 0;
    $dados['status'] = 0;
    $create->ExeCreate(DB_VAGAS, $dados);

    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=trabalhe-conosco/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM " . DB_VAGAS . " WHERE id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/banners/banners.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?>
                    <small> <?= $Post['vaga'] ?></small>
                </h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar Vagas do <?= $TABLE['TITLE_SINGLE'] ?></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>
                            <div class="col-md-8">
                                <div>
                                    <?php
                                    if (file_exists("../uploads/" . $Post['image']) && !is_dir("../uploads/" . $Post['image'])) {
                                        echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $Post['image'] . '&w=900" />';
                                    } else {
                                        echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=600&h=190" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                    }
                                    ?>

                                </div>
                                <div class="clear10"></div>

                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="vaga" value="<?= $Post['vaga'] ?>"
                                           placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>Conteudo</label>
                                    <textarea class="dx_editor"
                                              name="informacoes"><?= $Post['informacoes'] ?></textarea>
                                </div>

                            </div>
                            <div class="col-md-4" style="padding-left:20px">
                                <div class="form-group">
                                    <label>Imagem</label>
                                    <div class="upload_file" style="border:1px solid #ccc"><input class="loadimage"
                                                                                                  type="file"
                                                                                                  name="image"/></div>
                                </div>

                                <div class="clear20"></div>
                                <div class="form-group">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                        </label>
                                    </div>
                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                    <img src="_img/load.gif" class="loading_form pull-right">

                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar Vaga
                                    </button>&nbsp;

                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>
