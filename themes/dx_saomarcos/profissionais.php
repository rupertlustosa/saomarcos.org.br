<?php
include "inc/header.php";
?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>PROFISSIONAIS</h2>
                <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">
                    Encontre um profissional
                </h3>
            </div><!-- /.col-md-12 -->
            <div class="clear30"></div>

            <div class="atomwrapper">
                <form style="text-align: left !important">
                    <div class="row" style="margin: 0px; padding: 0px;">
                        <div class="col-md-2">&nbsp;</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="clinica">Pesquisar por clinica</label>
                                <select id="clinica" onchange="get_medicos('clinica')" class="form-control ">
                                    <option value="0"></option>
                                    <?php
                                    $read = new Read();
                                    $read->FullRead("
                                    SELECT c.* FROM dx_clinicas as c
                                    WHERE c.status = 1 and c.trash = 0
                                    ORDER BY clinica_title ASC");
                                    foreach ($read->getResult() as $clinica) {
                                        echo "<option value='{$clinica['clinica_id']}'>{$clinica['clinica_title']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="especialidade">Pesquisar por especialidade</label>
                                <select id="especialidade" onchange="get_medicos('especialidade')"
                                        class="form-control ">
                                    <option value="0"></option>
                                    <?php
                                    $read = new Read();
                                    $where = Admx::Trash();
                                    $read->FullRead("SELECT s.* FROM dx_especialidades as s
                                    WHERE s.status = 1 and s.trash = 0
                                    ORDER BY especialidade_title ASC");
                                    foreach ($read->getResult() as $row) {
                                        echo "<option value='{$row['especialidade_id']}'>{$row['especialidade_title']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">&nbsp;</div>
                    </div>
                    <div class="row" style="margin: 0px; padding: 0px;">
                        <div class="col-md-2">&nbsp;</div>

                        <div class="col-md-8">
                            <div class="form-group" id="responseMedicos">
                                <label for="profissionais">
                                    Profissionais
                                    <button id="btn_ver_todos" type="button" onclick="get_all_medicos()"
                                            class="btn btn-xs btn-info" style="display:none">ver todos
                                    </button>
                                </label>
                                <div class="clear"></div>
                                <select id="profissionais" class="select" style="width: 100%"
                                        onchange="goToMedico(this.value)" class="form-control">
                                    <option value="0"></option>
                                    <?php
                                    $read = new Read();
                                    $where = Admx::Trash();
                                    $read->FullRead("SELECT id_medico,Nm_Medico FROM hsm_medicos ORDER BY Nm_Medico ASC");
                                    foreach ($read->getResult() as $row) {
                                        $data = $row;
                                        $data['slug'] = Vendor::Name($row['Nm_Medico']);

                                        echo "<option value='" . json_encode($data) . "'>{$row['Nm_Medico']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clear40"></div>


            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>

</section><!-- /.our-team -->
<style type="text/css">
    .loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        z-index: 99999;
        background-color: rgba(0, 0, 0, 0.6);
        display: none;
    }

    .loading div {
        width: 100px;
        color: #fff;
        height: 30px;
        display: table;
        top: 50%;
        position: relative;
        margin-top: -15px;
        margin-left: -50px;
        left: 50%;
    }
</style>
<div class="loading">
    <div>Aguarde...</div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select').select2();
        $('#clinica').select2();
        $('#especialidade').select2();
    })

    function goToMedico(medico) {
        med = JSON.parse(medico);
        location.href = '<?=BASE?>/profissional/' + med.slug + '/' + med.id_medico;
    }

    function get_all_medicos() {
        $(".loading").show();
        $("#aguarde").css("display", "inline-block");
        $("#select2-especialidade-container").html('');
        $("#select2-clinica-container").html('');
        $.post("http://www.saomarcos.org.br/api/response.php?action=all-medicos", {api: 'get_medicos'}, function (data) {

            if (data) {
                $("#profissionais").html("<option value='0'> ----- </option>");
                for (var i = 0; i < data.length; i++) {
                    nome = data[i].Nm_Medico;
                    medico = JSON.stringify(data[i]);
                    $("#profissionais").append("<option value='" + medico + "'>" + nome + "</option>");
                }
                $("#responseMedicos").show();
                $(".loading").hide();
            } else {
                $(".loading").hide();
                $("#profissionais").html("<option value='0'> ----- </option>");
            }
        }, 'json')
    }

    function get_medicos(action) {
        query = null;
        $(".loading").show();
        $("#responseMedicos").hide();
        $("#btn_ver_todos").show();

        if (action == 'clinica') {
            $("#select2-especialidade-container").html('');
            clin = $('#clinica').val();
            query = "clinica=" + clin;
        } else {
            $("#select2-clinica-container").html('');
            esp = $('#especialidade').val();
            query = "especialidade=" + esp;
        }

        $("#aguarde").css("display", "inline-block");
        $.post("http://www.saomarcos.org.br/api/response.php?action=busca&tipo=" + action + "&" + query, {api: 'get_medicos'}, function (data) {

            if (data) {
                $("#profissionais").html("<option value='0'> ----- </option>");
                for (var i = 0; i < data.length; i++) {
                    nome = data[i].Nm_Medico;
                    medico = JSON.stringify(data[i]);
                    $("#profissionais").append("<option value='" + medico + "'>" + nome + "</option>");
                }
                $("#responseMedicos").show();
                $(".loading").hide();
            } else {
                $(".loading").hide();
                $("#profissionais").html("<option value='0'> ----- </option>");
            }
        }, 'json')
    }
</script>
<?php include "inc/footer.php" ?>
