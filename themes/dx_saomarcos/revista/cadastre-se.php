<form method="post">
    <?php
    $msg = null;
    $erro = 0;
    $PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if ($PostData) {
        $cpf = trim($PostData['cliente_cpf']);
        $email = trim($PostData['cliente_email']);

        if (Check::CheckCPF($cpf)) {
            $Read->FullRead("SELECT id FROM dx_clientes WHERE cpf=:cpf", "cpf={$cpf}");
            if ($Read->getResult()) {
                $msg .= '<div class="alert alert-danger">Este cpf <strong>' . $cpf . '</strong> ja está cadastrado, para recuperar sua senha cliqui <a href="' . BASE . '/' . $URL[0] . '/recuperar">aqui</a></div>';
                $erro++;
            }
        } else {
            $msg .= '<div class="alert alert-danger">Este cpf  <strong>' . $cpf . '</strong> não é válido!</div>';
            $erro++;
        }


        $Read->FullRead("SELECT id FROM dx_clientes WHERE email=:email", "email={$email}");
        if ($Read->getResult()) {
            $msg .= '<div class="alert alert-danger">Este email ja está cadastrado!</div>';
            $erro++;
        }

        $confirme_senha = $PostData['cliente_confirm_senha'];
        unset($PostData['cliente_confirm_senha']);
        if (!empty($PostData['cliente_senha'])) {
            if ($PostData['cliente_senha'] != $confirme_senha) {
                $msg .= '<div class="alert alert-danger">As senhas não são iguais!</div>';
                $erro++;
            }
        } else {
            $msg .= '<div class="alert alert-danger">Digite sua senha!</div>';
            $erro++;
        }

        if ($erro == 0) {
            $data = null;
            $PostData['cliente_cpf'] = trim($PostData['cliente_cpf']);
            $PostData['cliente_email'] = trim($PostData['cliente_email']);
            foreach ($PostData as $key => $value) {
                $fil = str_replace("cliente_", "", $key);
                $data[$fil] = $value;
            }
            $data['senha'] = Admx::encripta($data['senha']);
            $Create = new Create;
            $Create->ExeCreate("dx_clientes", $data);
            $msg = '<div class="alert alert-success " role="alert">Perfil atualizado com sucesso! Faça login para enviar seu artigo. <a href="' . BASE . '/' . $URL[0] . '/login">Logar</a></div>';

        } else {
            extract($PostData);
        }

    }
    include("form_cliente.php");
    ?>

</form>