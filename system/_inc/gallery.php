<div class="x_panel">
    <div class="x_title">
        <h2>Galeria de Fotos</h2>

        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form class="form-horizontal form-label-left auto_save" method="post" enctype="multipart/form-data">
            <input type="hidden" name="callback" value="Midias">
            <input type="hidden" name="callback_action" value="gbGalery">
            <input type="hidden" name="view" value="<?= $TABLE['VIEW'] ?>">
            <input type="hidden" name="key_id" value="<?= $KeyId ?>">
            <div class="upload_file" style="border-top:solid 1px #ddd">
                <input class="loadimage" name="galery[]" type="file" multiple="multiple">
            </div>
        </form>
        <div class="clear20"></div>
        <div class="row gallery">
            <?php
            //$Read->FullRead("SELECT * FROM " . DB_MIDIAS . " WHERE file_key_id=:id ORDER BY file_date DESC ", "id={$KeyId}");
            $galery = Site::getGalley($TABLE['KEY'], $KeyId);
            if ($galery):
                $i = 0;
                foreach ($galery as $file) {
                    $i++;
                    echo '<div class="col-md-55 gallery_item item_' . $file['file_id'] . '" id="' . $file['file_id'] . ' ">';
                    echo '<div class="thumbnail">';
                    echo '<div class="image">';
                    echo '<img class="animate" src="../tim.php?src=uploads/' . $file['file_src'] . '&w=250&h=150" alt="' . $file['file_name'] . '" />';
                    echo '</div>';
                    echo '<div class="caption">';
                    echo '<p class="file_name">' . $file['file_name'] . '</p>';
                    echo '<div class="clear5"></div>';
                    echo '<button class="btn btn-sm btn-info "><i class="fa fa-pencil"></i></button>';
                    echo '<button class="btn btn-sm btn-danger image_delete" rel="item_' . $file['file_id'] . '" callback="Midias" callback_action="delete" key_id="' . $file['file_id'] . '"><i class="fa fa-trash"></i></button>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    if ($i % 5 == 0) {
                        echo '<div class="clear"></div>';
                    }
                }
            endif;
            ?>
        </div>
    </div>
</div><!-- panel galery -->

<div class="modal fade modal_gallery" id="modal_gallery" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form class="modal_gallery_form ajax_off" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Galeria de Foto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <img class="file_src" src="">
                    </div>
                    <div class="form-group">
                        <input type="text" name="file_name" placeholder="Digite a descrição da imagem"
                               class="file_name form-control">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="callback" value="Midias">
                    <input type="hidden" name="callback_action" value="edit_name">
                    <input type="hidden" name="key_id" class="key_id">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary modal_gallery_save_name">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /modals -->
