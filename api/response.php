<?php
include("../_app/autoload.php");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

$Post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$getAction = filter_input(INPUT_GET,'action');
$action = Vendor::String($getAction);

$getTipo = filter_input(INPUT_GET,'tipo');
$tipo = Vendor::String($getTipo);

$getClinica = filter_input(INPUT_GET,'clinica');
$clinica = Vendor::String($getClinica);

$getEspecialidade = filter_input(INPUT_GET,'especialidade');
$especialidade = Vendor::String($getEspecialidade);

$getMedico = filter_input(INPUT_GET,'medico');
$medico = Vendor::String($getMedico);



$JSON = null;
$Read = new Read;

if($Post['api'] == 'get_medicos'):
  switch ($action) {
    case 'busca':
        if($tipo == 'clinica' ){
          //$Read->ExeRead('hsm_medicos','WHERE id_clinica = :id',"id={$clinica}");
          $Read->FullRead('SELECT id_medico,Nm_Medico FROM hsm_medicos WHERE id_clinica =:id ORDER BY Nm_Medico ',"id={$clinica}");

        }else{
              //$Read->ExeRead('hsm_medicos','WHERE (id_especialidade1 =:id OR id_especialidade2 =:id OR id_especialidade3 =:id ',"id={$especialidade}");
          $Read->FullRead('SELECT  id_medico,Nm_Medico FROM hsm_medicos WHERE (id_especialidade1 =:id OR id_especialidade2 =:id OR id_especialidade3 =:id)  ORDER BY Nm_Medico ',"id={$especialidade}");
        }

        foreach ($Read->getResult() as $data) {
			      $data['debug'] = "axdesousa";
           $data['slug'] = Vendor::Name($data['Nm_Medico']);
           $JSON[] = $data;
        }
        break;

    case 'show':
      //$Read->ExeRead('hsm_medicos','WHERE id_medico =:id',"id={$medico}");
      $Read->FullRead('
        SELECT m.*,h.dia,h.hr_inicio,hr_fim,d.* FROM hsm_medicos m
        INNER JOIN hsm_horarios h ON m.Id_Medico=h.Id_Medico
        LEFT JOIN hsm_dados d ON m.Id_Medico=d.id_medico
        WHERE m.Id_Medico =:id',"id={$medico}"
      );

      $data = $Read->getResult();
      $data['horario'] = '';


      $JSON[] = $data;



    case 'all-medicos':
      $Read->FullRead("SELECT id_medico,Nm_Medico FROM hsm_medicos ORDER BY Nm_Medico ASC");
      foreach ($Read->getResult() as $data) {
			    $data['debug'] = "axdesousa";
          $data['slug'] = Vendor::Name($data['Nm_Medico']);
          $JSON[] = $data;
      }

    break;
    default:
      header('HTTP/1.0 403 Forbidden');
    break;
  }

  echo json_encode($JSON);
else:
  header('HTTP/1.0 403 Forbidden');
endif;





?>
