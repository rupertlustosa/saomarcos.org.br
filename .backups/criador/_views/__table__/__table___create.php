<?php

$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {

    $create = new Create();
__CREATE_VAZIO__
    $dados['trash'] = 0;
    $dados['status'] = 0;
    $create->ExeCreate(__DB_TABLE__, $dados);

    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=".__DB_TABLE__."/create&id=" . $newId);
} else {

    $Read->FullRead("SELECT * FROM " . __DB_TABLE__ . " WHERE id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {

        $itemDb = $Read->getResult()[0];

    } else {

        die('REGISTRO NÃO ENCONTRADO!');
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/banners/banners.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>

                            <div class="col-md-8">

__FORM_CREATE__

                            </div>


                            <div class="col-md-4" style="padding-left:20px">
                                <div class="clear20"></div>
                                <div class="form-group">
                                    <div class="checkbox pull-left" style="display: none">
                                        <label>
                                            <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                        </label>
                                    </div>

                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                    <img src="_img/load.gif" class="loading_form pull-right">

                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar
                                    </button>&nbsp;

                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>
