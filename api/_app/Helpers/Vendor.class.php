<?php
class Vendor{
	private static $Format;
	private static $Data;

	static function String($str){
		$str = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '', $str);
		$str = trim($str);//limpa espaços vazio
		$str = strip_tags($str);//tira tags html e php
		$str = addslashes($str);//Adiciona barras invertidas a uma string
		return $str;
	}

	public static function Name($Name) {
			$Format = array();
			$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
			$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

			$Data = strtr(utf8_decode($Name), utf8_decode($Format['a']), $Format['b']);
			$Data = strip_tags(trim($Data));
			$Data = str_replace(' ', '-', $Data);
			$Data = str_replace(array('-----', '----', '---', '--', '--'), '-', $Data);

			return strtolower(utf8_encode($Data));
	}

}
?>
