<?php
session_destroy();
$front = new Front();
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_js/login.js');
$front->set_css(ADM_FILE . '/_css/login.css');
include '_inc/header.php';
?>
    <div class="login_form">
        <div class="lock"><i class="fa fa-code" aria-hidden="true"></i></div>
        <h1>Admx</h1>
        <form class="ajax_off" method="post">
            <div class="control-campo">
                <i class="fa fa-user"></i>
                <input type="text" required="required" name="user_login" placeholder="Login" class="form-control">
            </div>
            <div class="clear20"></div>
            <div class="control-campo">
                <i class="fa fa-lock"></i>
                <input type="password" required="required" name="user_password" placeholder="Senha"
                       class="form-control">
            </div>
            <div class="clear20"></div>
            <input type="hidden" name="callback" value="Login"/>
            <input type="hidden" name="callback_action" value="logar"/>
            <button class="btn btn-success pull-right">Entrar</button>
        </form>
        <div class="clear"></div>
    </div>
<?php

include '_inc/footer.php';
?>