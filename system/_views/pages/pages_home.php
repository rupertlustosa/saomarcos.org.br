<?php
if (empty($Read)) {
    $Read = new Read;
}

//Auto delete
$Delete = new Delete;
$Delete->ExeDelete(DB_PAGES, "WHERE page_title IS NULL AND page_content IS NULL and status = :st", "st=0");


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><?= $TABLE['TITLE'] ?></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= (!empty($_GET['s'])) ? "Buscar por '{$_GET['s']}'" : "Gerenciar {$TABLE['TITLE']}" ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th>#Título</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $where = " AND page_has_content = 1 ";
                                $where .= Admx::Trash();
                                $Read->ExeRead(DB_PAGES, " WHERE 1=1 {$where} ORDER BY status DESC, page_title ASC ");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <tr id="row_<?= $ROW['page_id'] ?>" class="class_<?= $ROW['page_id'] ?>">
                                        <td><?= str_pad($page_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=pages/create&id=<?= $page_id ?>"
                                               title="Editar: <?= $page_title ?>"><?= $page_title ?></a>
                                            <br>
                                            <small class="label label-primary"><?= $views ?> Views</small>
                                        </td>
                                        <td>
                                            <?= Admx::BtnStatus(DB_PAGES, 'page_id', $page_id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=pages/create&id=<?= $page_id ?>"
                                               title="Editar: <?= $page_title ?>" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Edit </a>
                                            <?= Admx::BtnTrash(DB_PAGES, 'page_id', $page_id, "row_{$ROW['page_id']}", 1) ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php


include './_inc/footer.php';
?>