<?php

class Session
{

    var $user = null;

    static function Logount()
    {
        session_destroy();
    }

    static function Login($userDados)
    {
        $_SESSION[SESSION_KEY] = $userDados;
        unset($_SESSION[SESSION_KEY]['user_password']);
    }

    /** funções para check de usuarios **/
    static function chekLogin()
    {
        if (!empty($_SESSION[SESSION_KEY])) {
            return true;
        } else {
            return false;
        }
    }

    static function checkAutorPost()
    {
        $userId = $_SESSION[SESSION_KEY]['user_id'];
        if ($_SESSION[SESSION_KEY]['user_level'] > 9):
            return null;
        else:
            return " AND post_user_id = {$userId} ";
        endif;

    }

    static function checkPermission($tableId)
    {
        $userId = $_SESSION[SESSION_KEY]['user_id'];
        if ($_SESSION[SESSION_KEY]['user_level'] >= 9):
            return true;
        else:
            $Read = new Read;
            $Read->FullRead("SELECT permission_id FROM " . DB_PERMISSION . " WHERE user_id=:us AND table_id=:tb", "us={$userId}&tb={$tableId}");
            if ($Read->getResult()):
                return true;
            else:
                return false;
            endif;
        endif;
    }

    /** get infos do user online **/
    static function UserLevel($level = 6)
    {
        if ($_SESSION[SESSION_KEY]['user_level'] >= $level) {
            return true;
        } else {
            return false;
        }
    }

    static function getUserId()
    {
        return $_SESSION[SESSION_KEY]['user_id'];
    }

    static function getUserName()
    {
        return $_SESSION[SESSION_KEY]['user_name'];
    }

    static function getUserEmail()
    {
        return $_SESSION[SESSION_KEY]['user_email'];
    }

    static function getUserLevel()
    {
        return $_SESSION[SESSION_KEY]['user_level'];
    }


}
