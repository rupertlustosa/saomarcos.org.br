<?php

require '../../_app/autoload.php';

usleep(50000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$CallBack = 'Default';
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):

    $Case = $PostData['callback_action'];
    unset($PostData['callback'], $PostData['callback_action']);

    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;

    //SELECIONA AÇÃO
    switch ($Case):
        case 'delete':


            $Table = $PostData['table'];
            $KeyName = $PostData['key_name'];
            $KeyValue = $PostData['key_valeu'];

            $Read->FullRead("SELECT * FROM " . DB_USERS . " WHERE user_id=:id", "id={$UserId}");
            if ($Read->getResult()):
                if ($Read->getResult()[0]['user_id'] == 1):
                    $name = (!empty($Read->getResult()[0]['user_name'])) ? $Read->getResult()[0]['user_name'] : $Read->getResult()[0]['user_id'];
                    $jSON['error'] = AdxAlert("<b>Erro: </b>O usuário {$name} não pode ser excluído do sistema!", DX_ERROR);
                else:
                    $Delete->ExeDelete(DB_USERS, " WHERE user_id = :id ", "id={$UserId}");
                    $name = (!empty($Read->getResult()[0]['user_name'])) ? $Read->getResult()[0]['user_name'] : $Read->getResult()[0]['user_id'];
                    $jSON['success'] = AdxAlert("O usuário <b>{$name}</b>, foi excluido com sucesso!", DX_SUCCESS);
                endif;

            else:
                $jSON['error'] = AdxAlert("Erro ao escluir!", DX_ERROR);
            endif;
            break;

        case 'status':
            $UserId = $PostData['key_id'];
            unset($PostData['key_id']);
            $Read->FullRead("SELECT user_id,user_name,user_status FROM " . DB_USERS . " WHERE user_id=:id", "id={$UserId}");
            if ($Read->getResult()):
                $name = $Read->getResult()[0]['user_name'];
                if ($Read->getResult()[0]['user_status'] == 1):
                    $Dados['user_status'] = 0;
                    $msg = "O usuário <b>{$name}</b>, foi desativado!";
                else:
                    $Dados['user_status'] = 1;
                    $msg = "O usuário <b>{$name}</b>, foi Ativado!";
                endif;
                $Update->ExeUpdate(DB_USERS, $Dados, " WHERE user_id =:id ", "id={$UserId}");
                $jSON['success'] = AdxAlert($msg, DX_SUCCESS);
            else:
                $jSON['error'] = AdxAlert("<b>Erro: </b> Usuário não encontrado!", DX_ERROR);
            endif;
            break;
    endswitch;

endif;