<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>ESPECIALIDADES</h2>
                    <!--<h3 class="caption color-black-100 width50 centertable"> xx </h3>-->
                </div><!-- /.col-md-12 -->

                <div class="clear40"></div>

                <div class="container">

                    <div class="row">
                        <div class="col-md-3" style="height:535px; background-color:#fff ">
                            <div class="manuaisCategorias">
                                <ul>
                                    <?php
                                    $titleSessao = null;
                                    $catWhere = null;
                                    foreach (getEspecialidadesSecao() as $key => $sec) {
                                        if ($sec[1] == $URL[1]) {
                                            $class = "currentManual";
                                            $titleSessao = $sec[0];
                                            $catWhere = $key;
                                        } else {
                                            $class = "";
                                        }

                                        echo "<li ><a class='onfade {$class}' href='" . BASE . "/especialidades/{$sec[1]}' title='{$sec[0]}'>{$sec[0]}</a></li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 style="margin-top: 0px; text-align: left;"><?= $titleSessao ?></h3>
                                </div>
                            </div>
                            <div class="row">
                                <?php

                                $getPage = (!empty($URL[3])) ? $URL[3] : null;
                                $Page = ($getPage ? $getPage : 1);
                                $BaseUrl = BASE . "/especialidades/{$URL[1]}/pg/";
                                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                                $Paginator->ExePager($Page, 12);
                                $where = Admx::Trash();
                                if ($catWhere) {
                                    $where .= " AND especialidade_secao = '{$catWhere}' ";
                                }

                                $Read->ExeRead(DB_ESPECIALIDADES, " WHERE status=:st {$where} ORDER BY especialidade_title ASC LIMIT :limit OFFSET :offset", "st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <div class="col-lg-3">
                                        <article class="SvItem especialidade-item">
                                            <div class="anchorSv onfade4 zd100" href="" title="">
                                                <a class="brdContentSv zd45 centralizar font12px al-center animate200"
                                                   style="height:80px; min-height: 80px;"
                                                   href="<?= BASE ?>/especialidade/<?= $especialidade_name ?>">
                                                    <div style="padding-top:0px;">
                                                        <?= $especialidade_title ?>
                                                    </div>
                                                </a>
                                            </div>
                                        </article>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                        </div>

                    </div><!-- /.container -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="clear20"></div>
            <div style="display:table; margin: 20px auto">
                <?php
                $Paginator->ExePaginator(DB_ESPECIALIDADES, " WHERE  status=:st {$where} ", "&st=1");
                echo $Paginator->getPaginator();
                ?>
            </div>
    </section><!-- /.our-team -->

<?php include "inc/footer.php" ?>