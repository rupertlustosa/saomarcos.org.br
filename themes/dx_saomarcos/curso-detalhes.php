<?php
include "inc/header.php";
extract($DataResult);
$read = new Read();

?>

    <section class="content-section ">
        <div class="container-fluid" style="background-color: #fff">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="col-md-12">

                            <ul id="ul-listagem">
                                <li class="no-padding-left no-border">
                                    CURSOS >>
                                </li>

                                <li>
                                    <a href="<?= BASE . '/cursos/' ?>">Todos</a>
                                </li>

                                <?php
                                $tipo_curso = Site::getTipoCurso();
                                foreach ($tipo_curso as $in => $row) {

                                    $cl = '';
                                    if ($in == (count($row) - 1)) {

                                        $cl = 'class="no-border"';
                                    }
                                    echo '<li ' . $cl . '><a href="' . BASE . '/cursos/' . $row['id'] . '">' . $row['tipo'] . '</a></li>';
                                }
                                ?>
                            </ul>

                        </div>
                        <div class="col-md-12">
                            <h2 class="fontsize2 cavbold  tuppercase ">
                                <b><?= $curso_title ?></b>
                            </h2>

                        </div>


                        <div class="clear20"></div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-9">

                            <div id="escopo-imagem">
                                <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $image ?>&w=1000&h=600"
                                     alt="<?= $curso_title ?>"
                                     title="<?= $curso_title ?>"/>

                                <div class="legenda">

                                    <?php
                                    if (isset($image) && !empty($image)) {

                                        $readTc = new Read();
                                        $readTc->ExeRead(DB_TIPOS_CURSOS, " WHERE id = " . $tipo_curso_id);
                                        $tipo_curso = $readTc->getResult();

                                        echo mb_strtoupper($tipo_curso[0]['tipo']);
                                    }
                                    ?>

                                </div>

                            </div>
                        </div>

                        <div class="col-lg-3 no-padding-left">

                            <div id="direita-cursos">

                                <?php
                                if (!empty($publico_alvo)) {
                                    ?>
                                    <h3>PÚBLICO ALVO</h3>
                                    <span>
                                       <?= $publico_alvo ?>
                                    </span>
                                    <?php
                                }

                                if (!empty($carga_horaria)) {
                                    ?>
                                    <h3>CARGA HORÁRIA</h3>
                                    <span>
                                       <?= $carga_horaria ?>
                                    </span>
                                    <?php
                                }

                                if (!empty($periodo_curso)) {
                                    ?>
                                    <h3>PERÍODO</h3>
                                    <span>
                                        <?= $periodo_curso ?>
                                    </span>
                                    <?php
                                } ?>


                                <h3>INVESTIMENTO</h3>
                                <span>
                                   <?= ((empty($curso_valor) || $curso_valor == 0.00) && empty($valor_extenso)) ? "GRATUITO" : (!empty($valor_extenso) ? $valor_extenso : ("R$ " .$curso_valor)); ?>
                                </span>

                                <a class="btn btn-danger btn-large" id="btn-inscricao" href="#inscricao">INSCRIÇÃO</a>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="contentMsg width90">
                            <?= $curso_content ?>
                        </div>
                        <div class="clear20"></div>
                    </div>


                    <div class="col-md-12">
                        <div class="section2" style="width:auto">
                            <div class="row ">
                                <div class="col-md-12">
                                    <h3 class="text-bold divisao"><span class="bg-red-300">COORDENAÇÃO</span></h3>
                                </div>
                                <div class="col-md-12">
                                    <?= $coordenacao ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php

                    $read->FullRead("SELECT rl_corpo_docente.* FROM rl_corpo_docente
                                            JOIN rl_curso_corpo_docente ON rl_curso_corpo_docente.corpo_docente_id = rl_corpo_docente.id
                                            WHERE rl_curso_corpo_docente.curso_id = " . $curso_id);

                    if ($read->getResult()) {
                    ?>
                    <div class="col-md-12">
                        <div class="section2" style="width:auto">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="text-bold divisao"><span class="bg-red-300">CORPO DOCENTE</span></h3>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion">
                                        <?php

                                        foreach ($read->getResult() as $indice => $cd) {

                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion"
                                                           href="#collapse<?= $indice ?>">
                                                            <strong><?php echo $cd['nome'] ?></strong><!-- [ + ]-->
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse<?= $indice ?>"
                                                     class="panel-collapse collapse <?= ($indice == 0) ? 'in' : '' ?>">
                                                    <div class="panel-body"><?php echo $cd['minicurriculo'] ?></div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>

                        <a name="inscricao"></a>
                        <div class="col-md-12">
                            <div class="section2" style="width:auto">
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <h3>FAÇA SUA INSCRIÇÃO</h3>
                                    </div>

                                    <div class="clear40"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <div class="atomwrapper">
                                            <form id="form_inscricao" enctype="multipart/form-data"
                                                  style="text-align: left !important"
                                                  method="post">

                                                <input id="inscricao_curso_id" name="inscricao_curso_id"
                                                       value="<?php echo $curso_id ?>" type="hidden">
                                                <div class="col-md-12">
                                                    <div class="curso-info">
                                                        <div class="curso-content"></div>
                                                    </div>
                                                </div>

                                                <div class="response col-md-12">
                                                    <div class="alert alert-warning " style="display:none" role="alert">
                                                        Enviando ...
                                                    </div>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="inscricao_cpf">CPF:</label>
                                                        <div class="clear5"></div>
                                                        <input class="form-control cpf "
                                                               style="max-width: 159px;float: left;"
                                                               name="inscricao_cpf" required="required">
                                                        <button type="button" class="btn btn-default"
                                                                onclick="getUser()">Verificar
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label for="inscricao_nome">Nome:</label>
                                                        <input class="form-control" id="nome" name="inscricao_nome"
                                                               disabled="disabled"
                                                               required="required">
                                                    </div>
                                                </div>

                                                <div class="clear5"></div>


                                                <?php if ($aceita_pagseguro == 'S' && $curso_valor > 0.00) {

                                                    ?>
                                                    <div class="col-md-12" style="display: none">
                                                        <div class="form-group">
                                                            <label for="inscricao_nome">Pagamento:</label>
                                                            Após realizar sua inscrição, você receberá um link em seu
                                                            email para pagamento através do Pagseguro.
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="comprovante" name="comprovante" value="">
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="col-md-12"
                                                         id="comprovante" <?= (empty($curso_valor) || $curso_valor == 0.00) ? 'style="display:none"' : '' ?>>
                                                        <div class="form-group">
                                                            <label for="inscricao_nome">Comprovante:</label>
                                                            <input name="arquivo" type="file">
                                                            <small>O comprovante deve ser comprimido no formato zip
                                                            </small>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="clear5"></div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="inscricao_email">E-Mail:</label>
                                                        <input class="form-control" id="email" name="inscricao_email"
                                                               disabled="disabled"
                                                               required="required">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="inscricao_fone ">Telefone:</label>
                                                        <input class="form-control phone" id="fone"
                                                               name="inscricao_fone"
                                                               disabled="disabled" required="required">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="inscricao_graduacao">Graduação:</label>
                                                        <select id="inscricao_graduacao" name="inscricao_graduacao"
                                                                disabled="disabled"
                                                                class="form-control" required="required">
                                                            <option value=""></option>
                                                            <option value="Graduando">Graduando</option>
                                                            <option value="Graduado">Graduado</option>
                                                            <option value="Residente">Residente</option>
                                                            <option value="Pós-graduando">Pós-graduando</option>
                                                            <option value="Pós-graduado">Pós-graduado</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <?php /*if ($aceita_pagseguro == 'S' && $curso_valor > 0.00) { ?>

                                                    <div class="clear5"></div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="postal_code">CEP:</label>
                                                            <input class="form-control" id="postal_code" name="postal_code"
                                                                   required="required" maxlength="9">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="street">Endereço:</label>
                                                            <input class="form-control" id="street" name="street"
                                                                   required="required" maxlength="100">
                                                        </div>
                                                    </div>
                                                    <div class="clear5"></div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="number">Número:</label>
                                                            <input class="form-control" id="number" name="number"
                                                                   required="required" maxlength="9">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="district">Bairro:</label>
                                                            <input class="form-control" id="district" name="district"
                                                                   required="required" maxlength="50">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="city">Cidade:</label>
                                                            <input class="form-control" id="city" name="city"
                                                                   required="required" maxlength="50">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="state">Estado:</label>
                                                            <input class="form-control" id="state" name="state"
                                                                   required="required" maxlength="2">
                                                        </div>
                                                    </div>
                                                <?php }*/ ?>

                                                <div class="clear5"></div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" checked="checked" value="1"
                                                                       name="inscricao_aceito">
                                                                Desejo receber informações do Hospital São Marcos
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear5"></div>


                                                <div class="col-md-offset-4 col-md-4">
                                                    <div class="form-group">
                                                        <input type="hidden" name="action" value="inscricao">
                                                        <button type="submit" class="btn btn-danger btn-large"
                                                                style="width: 100%; display: table; margin: 0px auto;">
                                                            ENVIAR
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>
                                            <script>

                                                function getUser() {
                                                    cpf = $('.cpf').val();
                                                    if (cpf.length > 10) {
                                                        $.post("<?=BASE?>/ajax/curso.php", {
                                                            cpf: cpf,
                                                            action: 'get-user'
                                                        }, function (response) {
                                                            result = response.result;
                                                            $("#nome").removeAttr('disabled');
                                                            $("#email").removeAttr('disabled');
                                                            $("#fone").removeAttr('disabled');
                                                            $("#inscricao_graduacao").removeAttr('disabled');


                                                            if (result.status == 'success') {
                                                                $("#nome").val(result.data.nome);
                                                                $("#email").val(result.data.email);
                                                                $("#fone").val(result.data.telefone);
                                                                $('#inscricao_graduacao option[value=' + result.data.graduacao + ']').attr('selected', 'selected');

                                                            } else {
                                                                alert(result.data);
                                                                $("#nome").focus();
                                                            }
                                                            console.log(result.data);
                                                            console.log(result.status);
                                                        }, 'json');
                                                    } else {
                                                        alert("Digite seu CPF");
                                                    }
                                                }

                                                $(function () {
                                                    $(".phone").mask("(99) 9.9999-9999");
                                                    $("#postal_code").mask("99999-999");
                                                    $(".cpf").mask("999.999.999-99");
                                                    $(".response").click(function () {
                                                        $(this).html('');
                                                    })
                                                    base = $('link[rel="base"]').attr('href');

                                                    $("#form_inscricao").submit(function () {
                                                        var form = $(this);
                                                        var form_this = this;

                                                        form.ajaxSubmit({
                                                            url: base + "/ajax/curso.php",
                                                            beforeSubmit: function () {
                                                                enviando();
                                                            },
                                                            dataType: 'json',
                                                            success: function (rsp) {
                                                                console.log(rsp);
                                                                console.log(rsp.msg);
                                                                if (rsp.status == 'success') {
                                                                    $(".curso-content").html('');
                                                                    $(".curso-info").hide();
                                                                    $(".response .alert").removeClass("alert-warning");
                                                                    $(".response .alert").removeClass("alert-danger");
                                                                    $(".response .alert").addClass("alert-success");
                                                                    $(".response .alert").html(rsp.msg);
                                                                    form_this.reset();
                                                                } else {
                                                                    $(".response .alert").removeClass("alert-warning");
                                                                    $(".response .alert").addClass("alert-danger");
                                                                    $(".response .alert").html(rsp.msg);
                                                                }
                                                            }
                                                        })


                                                        return false;
                                                    })
                                                })


                                            </script>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php

                        $read3 = new Read();
                        $read->FullRead("SELECT * FROM dx_cursos WHERE dx_cursos.`status` = 1 AND dx_cursos.trash = 0;");

                        if ($read->getResult()) {
                        ?>
                        <div class="col-md-12">
                            <div class="section2" style="width:auto">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-bold divisao"><span class="bg-red-300">OUTROS CURSOS</span></h3>
                                    </div>


                                    <div class="col-md-12 ">

                                        <?php

                                        foreach ($read->getResult() as $indice => $cd) {

                                            $read3->ExeRead(DB_TIPOS_CURSOS, " WHERE id = " . $cd['tipo_curso_id']);
                                            $tipo_curso = $read3->getResult();

                                            if (!empty($cd['link_externo'])) {

                                                $urlCurso = $cd['link_externo'];
                                                $targetCurso = '_blank';
                                            } else {

                                                $urlCurso = BASE . '/curso-detalhes/' . $cd['curso_id'];
                                                $targetCurso = '_self';
                                            }

                                            ?>
                                            <div class="col-md-6 no-padding-left padding-bottom-10">
                                                <a href="<?= $urlCurso ?>" target="<?= $targetCurso ?>"
                                                   title="<?= $cd['curso_title'] ?>">

                                                    <div class="col-md-12 no-padding">
                                                        <div id="escopo-imagem">
                                                            <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $cd['image'] ?>&w=500&h=300"/>
                                                            <div class="legenda"><?= mb_strtoupper($tipo_curso[0]['tipo']); ?>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 no-padding-left">
                                                        <h3 class="color-black-300 ">
                                                            <b><?= $cd['curso_title'] ?></b>
                                                        </h3>

                                                        <h4 class="color-red-300 fontsize1b">CARGA
                                                            HORÁRIA: <?= $cd['carga_horaria'] ?></h4>
                                                    </div>

                                                </a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>


                                </div>
                            </div>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
    </section>
<?php
include "inc/footer.php";
?>