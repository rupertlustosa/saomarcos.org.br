<?php include "inc/header.php" ?>

<div class="features cycle-slideshow" data-cycle-slides="> .item-feature" data-cycle-prev=".prevControl" data-cycle-next=".nextControl" data-cycle-timeout="8000" data-cycle-swipe-fx="scrollHorz" data-cycle-swipe="true">
    <?php
    foreach (Site::getBanners() as $banner):
        extract($banner);
        $getUrl = (!empty($banner_link)) ? "href='{$banner_link}'" : "";
        ?>
        <a <?= $getUrl ?> title="<?= $banner_title ?>" class="item-feature no-decoration" style="background-image: url('<?= BASE ?>/uploads/<?= $banner_image ?>')">
            <div class="atomwrapper">
                <div class="item-desc">
                    <h2 class="call-action color-white text-bold" style="text-transform: uppercase"><?= $banner_title ?></h2>
                    <p class="tagline color-white"><?= $banner_subtitle ?></p>
                </div>
            </div>
        </a>
        <?php
    endforeach;
    ?>

    <div id="progress"></div>
    <div class="onTable hidden-xs hidden-sm">
        <a href="" title="" class="prevControl onfade4 btn-default-hsm-reverse"><i class="fa fa-angle-double-left"></i></a>
        <a href="" title="" class="nextControl onfade4 btn-default-hsm"><i class="fa fa-angle-double-right"></i></a>
    </div>
    <div class="cycle-pager hidden-xs hidden-sm"></div>
</div>

<div class="about-hsm container-fluid">


    <div class="atomwrapper">
        <?php
        $page = Site::getPage(1);
        $galery = Site::getGalley(5, 1);
        foreach ($galery as $imgs) {
            $sobreImages[] = $imgs['file_src'];
        }
        shuffle($sobreImages);
        ?>
        <h3 class="al-center centertable fontsize2 text-bold"><?= $page['page_subtitle'] ?></h3>
        <div class="clear10"></div>
        <p class="tagline al-center centertable font20px width50 lineh090 color-black-300">
            <?= $page['page_chamada'] ?>
        </p>

        <div class="clear40"></div>

        <div class="row no-gutters">
            <div class="col-lg-4">
                <div class="item-grid-first">
                    <div class="igftext">
                        <div class="centralizar">
                            <div>
                                <h1 class="al-center font17px text-bold">INFRA-ESTRUTURA</h1>
                                <p class="al-center">
                                    <?= Admx::getMeta(DB_PAGES, 1, 'infraestrutura') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="igfimg" style="background-image: url('<?= BASE; ?>/uploads/<?= $sobreImages[0] ?>')"></div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="item-grid-second" style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[1] ?>') no-repeat;background-size:cover;"></div>
            </div>
            <div class="col-lg-5">
                <div class="item-grid-three" style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[2] ?>');no-repeat;background-size:cover;"></div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-3">
                <div class="item-grid-four" style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[3] ?>')no-repeat;background-size:cover;"></div>
            </div>
            <div class="col-lg-4">
                <div class="item-grid-five">
                    <div class="centralizar">
                        <div class="atomwrapper">
                            <h1 class="al-center font17px text-bold">EQUIPAMENTOS MODERNOS</h1>
                            <p class="al-center">
                                <?= Admx::getMeta(DB_PAGES, 1, 'equipamentos-medernos') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="item-grid-six" style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[4] ?>');no-repeat;background-size:cover;"></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="atomwrapper">
        <h3 class="al-center centertable fontsize2 text-bold">NOSSAS ESPECIALIDADES</h3>
        <div class="clear10"></div>
        <p class="tagline al-center centertable font20px width50 lineh090 color-black-300">
            Duis non condimentum nunc. Nunc quis turpis eu est tincidunt rutrum. Cras a purus quis sem tincidunt egestas vel id lacus.
        </p>

        <div class="clear40"></div>
        <div class="row MainCarousel">

            <?php
            $especialidades = Site::getEspecialidades();
            foreach ($especialidades as $esp):
                extract($esp)
                ?>
                <div class="col-lg-2">
                    <article class="SvItem">
                        <div class="anchorSv onfade4 zd100" href="" title="">
                            <div class="icoContentSv zd50">
                                <div class="insetBlk centralizar">
                                    <span class="fa fa-heart-o fontsize2"></span>
                                </div>
                            </div>
                            <div class="brdContentSv zd45">
                                <div class="insetSlk centralizar">
                                    <h1 class="font12px al-center">
                                        <a href="<?=BASE?>/especialidade/<?=$especialidade_name?>"><?=$especialidade_title?></a>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php
            endforeach;
            ?>

        </div>
    </div>
</div>


<div class="clear40"></div>

<div class="container-fluid bg-light-gray-100">
    <div class="atomwrapper content-page">
        <h3 class="al-center centertable fontsize2 text-bold">NOTÍCIAS</h3>
        <div class="clear10"></div>
        <p class="tagline al-center centertable font20px width50 lineh090 color-black-300">
            Duis non condimentum nunc. Nunc quis turpis eu est tincidunt rutrum. Cras a purus quis sem tincidunt egestas vel id lacus.
        </p>
        <div class="clear40"></div>
        
        <?php
        
        $noticiasDestaque = Site::getNoticias(1);
        $noticia = $noticiasDestaque[0];
        extract($noticia);
        $post_not_id[] = $noticia['post_id'];
        $noticiaData = new Date($post_date);
        ?>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="on-thumb-news" style="background-image: url('<?=BASE?>/uploads/<?=$noticia['post_image']?>');">
                    <div class="bot-n"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="set-txt-desc">
                    <span><?="{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></span>
                </div>
                <div class="clear30"></div>
                <h3 class="nomargin nopadding"><?=$noticia['post_title']?></h3>
                <div class="clear10"></div>
                <p class="font17px">
                    <?=$noticia['post_chamada']?>
                </p>
                <div class="clear20"></div>
                <a href="<?=BASE?>/artigo/<?=$noticia['post_name']?>" title="<?=$noticia['post_title']?>" class="onfade4 btn-default-hsm bg-red-300 no-decoration">CONTINUAR LENDO&nbsp;<i class="fa fa-angle-double-right"></i></a>
            </div>
        </div>
        <div class="clear40"></div>
        <div class="row MainNews">
            <?php
            $noticias = Site::getNoticias(9, $post_not_id);
            foreach ($noticias as $news):
                extract($news);
                $data = new Date($post_date);
            ?>
            <div class="col-lg-4">
                <div class="bg-other-n">
                    <div class="centralizar nopadding">
                        <div class="atomwrapper">
                            <div  style="margin: 0px !important;padding: 0px !important;">
                                <div class="set-txt-desc" style="margin: 0px !important;padding: 0px !important;">
                                    <span class="font18px"><?="{$data->getDia()} de {$data->getMesName()} de {$data->getAno()} "?></span>
                                </div>
                                <div class="clear10"></div>
                                <h3 class="nomargin nopadding font18px"><?=$post_title?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>
<section class="MainAtendimento nycPhone hero" data-parallax-speed="1">
    <div class="hero-content">
        <div class="atomwrapper content-page">
            <h1 class="al-center color-white titleBorderW fontzero">TELEFONES<br>PARA CONTATO</h1>
            <div class="centralizar">
                <img style="max-width: 200px" src="<?= BASE; ?>/uploads/midias/slider/bg-shapePhone.png" alt="" title=""/>
            </div>
        </div>
    </div>
</section>
</div>

<?php include "inc/footer.php" ?>
        