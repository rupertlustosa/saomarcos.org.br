<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = 1;

$Read->FullRead("SELECT * FROM dx_revista WHERE revista_id=:id ", "id=1");
if ($Read->getResult()) {
    $Post = $Read->getResult()[0];
} else {
    //header("Location: " . ADM_URL . "?dx=error/404");
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/pages/pages.js');

include '_inc/header.php';
include '_inc/nav.php';


?>


    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <?= $TABLE['TITLE_SINGLE'] ?></h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                <?php Admx::setFormToken() ?>
                                <div class="col-md-8">
                                    <div class="form-group title">
                                        <label>Título </label>
                                        <input type="text" name="revista_title" value="<?= $Post['revista_title'] ?>"
                                               placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Guia do Autor</label>
                                        <textarea class="dx_editor" id="revista_autor"
                                                  name="revista_autor"><?= $Post['revista_autor'] ?></textarea>
                                    </div>
                                    <div class="clear30"></div>

                                    <div class="form-group">
                                        <label>Editorial</label>
                                        <textarea class="dx_editor" id="revista_editorial"
                                                  name="revista_editorial"><?= $Post['revista_editorial'] ?></textarea>
                                    </div>
                                    <div class="clear30"></div>
                                    <div class="form-group">
                                        <label>Submissao dos Artigos</label>
                                        <textarea class="dx_editor" id="revista_submissao"
                                                  name="revista_submissao"><?= $Post['revista_submissao'] ?></textarea>
                                    </div>
                                    <div class="clear30"></div>
                                    <div class="form-group">
                                        <label>Aceite dos Artigos</label>
                                        <textarea class="dx_editor" id="revista_aceite"
                                                  name="revista_aceite"><?= $Post['revista_aceite'] ?></textarea>
                                    </div>
                                    <div class="clear30"></div>
                                    <div class="form-group">
                                        <label>Estrutura do Artigo</label>
                                        <textarea class="dx_editor" id="revista_estrutura"
                                                  name="revista_estrutura"><?= $Post['revista_estrutura'] ?></textarea>
                                    </div>

                                </div>
                                <div class="col-md-4 sidebar" style="padding-left:20px">

                                    <div class="upload_progress none"
                                         style="padding: 5px; background: #00B594; color: #fff; width: 0%; text-align: center; max-width: 100%;">
                                        0%
                                    </div>
                                    <div style="overflow: auto; max-height: 400px;">
                                        <?php
                                        if (file_exists("../uploads/" . $Post['revista_image']) && !is_dir("../uploads/" . $Post['revista_image'])) {
                                            echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $Post['revista_image'] . '&w=350&h=350" />';
                                        } else {
                                            echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=350&h=350" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                        }
                                        ?>

                                    </div>
                                    <div class="upload_file"><input class="loadimage" type="file" name="image"/></div>
                                    <div class="clear20"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                            </label>
                                        </div>
                                        <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                        <input type="hidden" name="callback_action" value="manager">
                                        <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                        <img src="_img/load.gif" class="loading_form pull-right">

                                        <button class="btn btn-success pull-right" type="submit">
                                            Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                    </div>

                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div><!-- content do formulario -->
                    </div><!-- painel do formulario -->
                    <div class="clear20"></div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php

include './_inc/footer.php';
?>