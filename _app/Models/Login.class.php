<?php

/**
 * Login.class [MODEL]
 * Responsável por autentica, validar e checar os usuários dos sistema de login
 * @copyright (c) 2015, João Manoel
 */
class Login
{

    private $Level;
    private $Email;
    private $Senha;
    private $Error;
    private $Result;

    function __construct($Level)
    {
        $this->Level = (int)$Level;
    }

    public function ExeLogin(array $UserData)
    {
        $this->Email = (string)strip_tags(trim($UserData['user']));
        $this->Senha = (string)strip_tags(trim($UserData['pass']));
        $this->setLogin();
    }

    public function getResult()
    {
        return $this->Result;
    }

    public function getError()
    {
        return $this->Error;
    }

    public function CheckLogin()
    {
        if (empty($_SESSION['userlogin']) || $_SESSION['userlogin']['user_level'] < $this->Level):
            unset($_SESSION['userlogin']);
            return false;
        else:
            return true;
        endif;
    }

    //Verifica as permissoes do usuário
    public function CheckPermission()
    {
        if ($_SESSION['userlogin']['user_level'] < $this->Level):
            return false;
        else:
            return true;
        endif;
    }

    //Métodos privados
    private function setLogin()
    {
        if (!$this->Email || !$this->Senha || !Check::Email($this->Email)):
            $this->Error = array('Informe seu Email e senha para efetuar o login', DX_INFOR);
            $this->Result = false;
        elseif (!$this->getUser()):
            $this->Error = array('Os dados informados não são compatíveis', DX_ALERT);
            $this->Result = false;
        elseif ($this->Result['user_level'] < $this->Level):
            $this->Error = array("{$this->Result['user_name']}, você não tem permissão para acessar essa área", DX_ERROR);
            $this->Result = false;
        else:
            $this->Execute();
        endif;
    }

    //Verifica o usuário e senha no banco de dados
    private function getUser()
    {
        $this->Senha = md5($this->Senha);
        $read = new read;
        $read->ExeRead("cj_users", "WHERE user_email = :e AND user_password = :p", "e={$this->Email}&p={$this->Senha}");
        if ($read->getResult()):
            $r = $read->getResult();
            $this->Result = $r[0];
            return true;
        else:
            return false;
        endif;
    }

    private function Execute()
    {
        $data = array();
        if (!session_id()):
            session_start();
        endif;

        $data['last_user_id'] = $this->Result['user_id'];
        $data['last_date_start'] = date("Y-m-d H:i:s");

//        $create = new Create;
//        $create->ExeCreate("cj_lasts_logins",$data);

        $_SESSION['userlogin'] = $this->Result;
        $_SESSION['userlogin']['user_idsession'] = $create->getResult();

        $this->Error = array("Olá, {$this->Result['user_name']}, seja bem vindo(a).", CJ_ACCEPT);
        $this->Result = true;
    }

}
