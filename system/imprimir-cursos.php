<?php
include("../_app/autoload.php");
if (!Session::chekLogin()){
    include_once './login.php';
}else{
$id = filter_input(INPUT_GET, 'id', FILTER_DEFAULT);
$view = filter_input(INPUT_GET, 'view', FILTER_DEFAULT);


if (is_numeric($id)):


/*
SELECT * FROM dx_cursos_inscricao as ci  
INNER JOIN dx_cursos ON curso_id = inscricao_curso_id
INNER JOIN dx_clientes cl ON cl.cpf = inscricao_cliente_cpf
WHERE inscricao_curso_id = :id  ORDER BY ci.nome ASC, ci.status ASC, ci.created DESC ","id={$id}
*/

$Read = new Read;
$Read->FullRead("
SELECT dxc.curso_title, i.inscricao_id,i.created as data_inscricao,i.inscricao_cliente_cpf, i.inscricao_curso_id,i.status,i.inscricao_comprovante, c.* 
FROM dx_cursos_inscricao as i
INNER JOIN dx_cursos as dxc ON curso_id = inscricao_curso_id
INNER JOIN dx_clientes as c ON c.cpf = i.inscricao_cliente_cpf
WHERE inscricao_curso_id = :id  ORDER BY status ASC, created DESC ", "id={$id}
");

if ($Read->getResult()):
$curso = $Read->getResult()[0]['curso_title'];
$cpfs = array();
if ($view == 'xls'):
    header("Content-type: application/x-msexcel");
    header("Content-Disposition: attachment; filename=\"{$curso}.xls\"");
    header("Content-Description: PHP Generated Data");
else:;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="icon" href="http://www.saomarcos.org.br/system/_img/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admx</title>
    <!-- Bootstrap -->
    <link href="http://www.saomarcos.org.br/_cdn/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        tr, td, th {
            padding: 5px;
        }
    </style>
</head>

<body>
<?php
endif;
?>
<div class="container">
    <table>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table>
        <tr>
            <th><?= $curso ?></th>
        </tr>
    </table>
    <table>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <th>CPF</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>Graduação</th>
            <th>Data da Inscr.</th>
        </tr>
        <?php
        foreach ($Read->getResult() as $inscricao):

            list($date, $hora) = explode(" ", $inscricao['data_inscricao']);
            list($ano, $mes, $dia) = explode("-", $date);
            $data = "{$dia}/{$mes}/{$ano}";
            if (!in_array($inscricao['cpf'], $cpfs)) {
                ?>
                <tr>
                    <td><?= $inscricao['inscricao_cliente_cpf'] ?></td>
                    <td><?= strtoupper($inscricao['nome']) ?></td>
                    <td><?= $inscricao['email'] ?></td>
                    <td><?= $inscricao['telefone'] ?></td>
                    <td><?= $inscricao['graduacao'] ?></td>
                    <td><?= $data ?></td>
                </tr>
                <?php
                $cpfs[] = $inscricao['cpf'];
            }

        endforeach;
        ?>
    </table>

    <?php

    if ($view == 'xls'):
        echo '
    </div>
    </body>
</html>';

    endif;

    else:
        echo "Esse curso não tem inscritos!";
    endif;

    else:
        echo "Id não é valido";
    endif;

    } //Check Session;
    ?>
