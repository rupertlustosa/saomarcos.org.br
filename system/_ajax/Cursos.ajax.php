<?php
require '../../_app/autoload.php';
require('../../_app/Library/PHPMailer/class.phpmailer.php');

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'cursos';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
//VALIDA AÇÃO

$Delete = new Delete();
$Delete->ExeDelete(DB_CURSOS_CORPO_DOCENTE, " WHERE curso_id = :curso_id", "curso_id={$KeyId}");

if(isset($PostData['corpo_docente_id'])){

    $corpo_docente_id = $PostData['corpo_docente_id'];
    unset($PostData['corpo_docente_id']);

    if($corpo_docente_id){

        foreach ($corpo_docente_id as $item){

            $create = new Create();
            $dados['curso_id'] = $KeyId;
            $dados['corpo_docente_id'] = $item;
            $create->ExeCreate(DB_CURSOS_CORPO_DOCENTE, $dados);

        }
    }
}


if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack && $PostData['formToken'] == $formToke):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            $Read->ExeRead(DB_CURSOS, "WHERE curso_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):


            else:

                $name = !empty($PostData['curso_title']) ? Check::Name($PostData['curso_title']) : $post['curso_id'];

                if (!empty($File)):

                    $Upload->Image($File, "{$name}-" . time(), 900);

                    if ($Upload->getResult()):
                        $image = $Upload->getResult();
                        $PostData['image'] = $image;
                        $jSON['image'] = ["src" => "../tim.php?src=uploads/{$image}&w=900"];
                    endif;
                endif;

                $ReadName = new $Read;
                $ReadName->FullRead("SELECT curso_name FROM " . DB_CURSOS . " WHERE curso_name= :nm and curso_id != :id ", "nm={$name}&id={$KeyId}");
                if ($ReadName->getResult()):
                    $PostData['curso_name'] = $name . time();
                else:
                    $PostData['curso_name'] = $name;
                endif;

                $PostData['curso_valor'] = trim($PostData['curso_valor']);
                $PostData['curso_valor'] = str_replace('.', '', $PostData['curso_valor']);
                $PostData['curso_valor'] = str_replace(',', '.', $PostData['curso_valor']);

                $PostData['curso_limite'] = (isset($PostData['curso_limite'])) ? $PostData['curso_limite'] : 170;
                $PostData['curso_valor'] = (!empty($PostData['curso_valor'])) ? $PostData['curso_valor'] : 0.00;
                $PostData['aceita_pagseguro'] = (!empty($PostData['aceita_pagseguro'])) ? $PostData['aceita_pagseguro'] : 'N';
                $PostData['status'] = ($rascunho) ? 0 : 1;
                $post = $Read->getResult()[0];
                $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE curso_id = :id", "id={$KeyId}");

                $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
            endif;
            break;
        case "manager_inscricao":
            $Read->FullRead("
                    SELECT curso_title, i.*,i.status as inscricao_status, c.*
                    FROM dx_cursos_inscricao as i
                    INNER JOIN dx_clientes as c ON c.cpf = i.inscricao_cliente_cpf
                    INNER JOIN dx_cursos as curso ON curso_id=inscricao_curso_id
                    WHERE inscricao_id = :id", "id={$KeyId}
                ");
            $inscricao = $Read->getResult()[0];
            $nome = $inscricao['nome'];
            $email = $inscricao['email'];
            $status = $PostData['status'];
            $Data['status'] = $status;
            $hash = md5(uniqid(rand(), true));
            $Data['hash'] = $hash;
            if ($status != 0) {

                $mail = new PHPMailer();
                $mail->isSMTP();
                $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
                $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
                $mail->Password = 'apcchsm$69';                           // SMTP password
                $mail->Port = 25;
                $mail->FromName = 'Hospital Sao Marcos';
                $mail->AddAddress(trim($email), $nome);
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                if ($status == 1) {
                    $texto = $PostData['texto'];
                    $Data['inscricao_comprovante'] = null;
                    $mail->Subject = "|| PRÉ-INSCRIÇÃO ||"; // Assunto da mensagem
                    $title = "PRÉ-INSCRIÇÃO: {$inscricao['curso_title']} ";
                    $url = BASE . "/cursos/{$hash}";
                    $texto = $texto . " <br>clique <a href='{$url}'>aqui</a> para enviar um novo comprovante.";
                } else {
                    $mail->Subject = "|| INSCRIÇÃO CONFIRMADA ||"; // Assunto da mensagem
                    $title = "INSCRIÇÃO: {$inscricao['curso_title']} ";
                    $texto = "<b>{$nome}</b>, Sua inscrição foi confirmada. <br>Seu numero de inscrição é <b>{$inscricao['inscricao_numero']}</b>";
                }
                include("../../templates/email-inscricao.php");
                $mail->Body = $msg;
                $mail->Send();
            }

            $Update->ExeUpdate("dx_cursos_inscricao", $Data, " WHERE inscricao_id = :id ", "id={$KeyId}");

            $jSON["response"] = "success";
            break;

        case "enviar_certificado":
            $types = array(
                'image/jpeg',
                'image/png',
                'application/pdf',
            );
            $maxSize = 1024 * 1024 * 2; // 2Mb
            $File = null;

            if (!empty($_FILES['arquivo'])):
                $File = $_FILES['arquivo'];
            endif;
            $ext = substr($File['name'], -4);
            $nome_final = md5(time()) . $ext;
            if ($File) {
                if (in_array($File['type'], $types)) {
                    if ($File['size'] <= $maxSize) {
                        if (move_uploaded_file($File['tmp_name'], '../../uploads/files/' . $nome_final)) {
                            $certficado = $nome_final;
                        }
                    } else {
                        $jSON['status'] = 'error';
                        $jSON['msg'] = "Arquivo muito grande máximo 2mb";
                        echo json_encode($jSON);
                        die();
                    }
                } else {
                    $jSON['status'] = 'error';
                    $jSON['msg'] = "arquivo inválido";
                    echo json_encode($jSON);
                    die();
                }
            } else {
                $jSON['status'] = 'error';
                $jSON['msg'] = "Envie o comprovante de pagamento!";
                echo json_encode($jSON);
                die();
            }
            $Read->FullRead("
                    SELECT curso_title, i.*,i.status as inscricao_status, c.*
                    FROM dx_cursos_inscricao as i
                    INNER JOIN dx_clientes as c ON c.cpf = i.inscricao_cliente_cpf
                    INNER JOIN dx_cursos as curso ON curso_id=inscricao_curso_id
                    WHERE inscricao_id = :id", "id={$KeyId}
                ");
            $inscricao = $Read->getResult()[0];
            $nome = $inscricao['nome'];
            $email = $inscricao['email'];

            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
            $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
            $mail->Password = 'apcchsm$69';                           // SMTP password
            $mail->Port = 25;
            $mail->FromName = 'Hospital Sao Marcos';
            $mail->AddAddress(trim($email), $nome);
            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->Subject = "|| CERTIFICADO HSM ||"; // Assunto da mensagem
            $title = "CERTIFICADO: {$inscricao['curso_title']}";
            $hash = md5(uniqid(rand(), true));

            $texto = 'Olá, ' . $nome . ', segue o <a href="' . BASE . '/certificado.php?token=' . $hash . '" >link</a> para baixar seu certificado do curso ' . $inscricao['curso_title'];

            include("../../templates/email-inscricao.php");
            $mail->Body = $msg;
            if ($mail->Send()) {
                $Data['hash'] = $hash;
                $Data['inscricao_certificado_url'] = $certficado;
                $Update->ExeUpdate("dx_cursos_inscricao", $Data, " WHERE inscricao_id = :id ", "id={$KeyId}");
                $jSON['status'] = 'success';
                $jSON['msg'] = "Certificado enviado para {$email}";

            } else {
                $jSON['status'] = 'error';
                $jSON['msg'] = "Error: tente novamente mais tarde";
                echo json_encode($jSON);
                die();
            }

            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
