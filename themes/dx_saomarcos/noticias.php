<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>NOTÍCIAS</h2>
                </div><!-- /.col-md-12 -->
            </div>
            <section id="postIndex" class="widthWrapper">
                <?php

                $getPage = (!empty($URL[2])) ? $URL[2] : null;
                $Page = ($getPage ? $getPage : 1);
                $BaseUrl = BASE . "/noticias/pg/";
                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                $Paginator->ExePager($Page, 10);
                $where = Admx::Trash();

                $Read->ExeRead(DB_POSTS, " WHERE status=:st {$where} ORDER BY post_date DESC LIMIT :limit OFFSET :offset", "st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                foreach ($Read->getResult() as $ROW):
                    extract($ROW);
                    $noticiaData = new Date($post_date);
                    ?>
                    <article class="postInner">
                        <a href="<?= BASE ?>/artigo/<?= $post_name ?>" title="<?= $post_title ?>">
                            <div class="postImg">
                                <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $post_image ?>&w=800&h600"/>
                            </div>
                            <h2 class="color-red-300 fontsize1b"><?= $post_title ?></h2>
                            <span class="tuppercase">Postada em: <?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></span>
                            <p><?= $post_chamada ?></p>
                        </a>
                    </article>
                    <?php
                endforeach;
                ?>
            </section>
            <div style="display:table; margin: 20px auto">
                <?php
                $Paginator->ExePaginator(DB_POSTS, " WHERE status=:st {$where} ", "&st=1");
                echo $Paginator->getPaginator();
                ?>
            </div>
        </div>
        <div class="clear40"></div>
    </section>

<?php include "inc/footer.php" ?>