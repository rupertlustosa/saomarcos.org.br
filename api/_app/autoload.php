<?php
/**
 * Name: Admx - Sistema e Gerenciador de conteudo
 * Autor: Ax de sousa
 * Ano: 2016
 * Version: 1.0.1
 */

include_once("Config/define.inc.php");
include_once 'Config/functions.inc.php';
//Auto load de Classes  ####################


function __autoload($Class) {
    $Class = ucwords($Class);
    $cDir = array('Conn', 'Helpers', 'Models');
    $iDir = null;

    foreach ($cDir as $dirName):
        if (!$iDir && file_exists(__DIR__ . DIRECTORY_SEPARATOR . "{$dirName}" . DIRECTORY_SEPARATOR . "{$Class}.class.php") && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . "{$dirName}" . DIRECTORY_SEPARATOR . "{$Class}.class.php")):
            include_once (__DIR__ . DIRECTORY_SEPARATOR . "{$dirName}" . DIRECTORY_SEPARATOR . "{$Class}.class.php");
            $iDir = true;
        endif;
    endforeach;

    if (!$iDir):
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    endif;
}

set_error_handler('PHPErro');
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set("America/Sao_Paulo");
