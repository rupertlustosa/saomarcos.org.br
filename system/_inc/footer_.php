<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?= CDN_FILE ?>/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?= CDN_FILE ?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= ADM_FILE ?>/_js/jquery.form.js"></script>
<script src="<?= ADM_FILE ?>/_js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?= ADM_FILE ?>/_js/plugins.js"></script>

<!-- FastClick -->
<script src="<?= CDN_FILE ?>/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?= CDN_FILE ?>/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?= CDN_FILE ?>/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?= CDN_FILE ?>/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?= CDN_FILE ?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?= CDN_FILE ?>/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?= CDN_FILE ?>/vendors/skycons/skycons.js"></script>

<!-- PNotify -->
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="<?= CDN_FILE ?>/js/moment/moment.min.js"></script>
<script src="<?= CDN_FILE ?>/js/datepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?= CDN_FILE ?>/build/js/custom.min.js"></script>

<?php
$front->JS();
?>

</body>
</html>
