<?php

Class Site
{

    static function getBanners()
    {
        $read = new Read();
        $read->ExeRead(DB_BANNERS, " WHERE status = 1 
                                    AND trash = 0
                                    AND banner_dataentrada <= NOW() 
                                    AND (banner_datasaida >= NOW() OR banner_datasaida IS NULL) 
                                    ORDER BY banner_dataentrada DESC");
        return $read->getResult();
    }

    static function getPage($page_id)
    {
        $read = new Read();
        $read->ExeRead(DB_PAGES, " WHERE status = 1 AND page_id =:id LIMIT 1", "id={$page_id}");
        $r = $read->getResult();
        return $r[0];
    }

    static function getGalley($table_id, $parent_id)
    {
        $read = new Read();
        $read->ExeRead(DB_MIDIAS, " WHERE file_table_id =:tbId AND file_key_id =:keyId ", "tbId={$table_id}&keyId={$parent_id}");
        if ($read->getResult()) {
            return $read->getResult();
        } else {
            return null;
        }
    }

    static function getEspecialidades()
    {
        $read = new Read();
        $read->ExeRead(DB_ESPECIALIDADES, " WHERE status = :st ORDER BY especialidade_title ", "st=1");
        return $read->getResult();
    }

    static function getResponsabilidadeSocial()
    {
        $read = new Read();
        $read->ExeRead(DB_SOCIAIS, " WHERE status = :st ", "st=1");
        return $read->getResult();
    }

    static function getTipoCurso()
    {
        $read = new Read();
        $read->ExeRead(DB_TIPOS_CURSOS, " WHERE status = :st ", "st=1");
        return $read->getResult();
    }

    static function getNoticias($qnt = 10, $not_id = null, $search = null)
    {
        $read = new Read();
        $where = null;

        if ($not_id != null) {
            $exclude = implode(",", $not_id);
            $where = " AND post_id not in ({$exclude})";
        }
        $searchAnt = Anti::String($search);
        if ($search != null) {
            $where .= " AND (post_content LIKE '%{$searchAnt}%' ) ";
        }
        $read->ExeRead(DB_POSTS, " WHERE status =:st {$where}  ORDER BY post_date DESC LIMIT {$qnt}", "st=1");
        if ($read->getResult()) {
            return $read->getResult();
        } else {
            return null;
        }
    }

    static function getImageYoutube($url)
    {
        preg_match_all('/v=([^\&]+)[\&]?/', $url, $matches);
        $id_video = $matches[1][0];
        return 'https://i.ytimg.com/vi/' . $id_video . '/hqdefault.jpg';
    }

    static function getFrameYoutube($url, $width = '100%', $heigh = '480', $id = "video_youtube")
    {
        preg_match_all('/v=([^\&]+)[\&]?/', $url, $matches);
        $id_video = $matches[1][0];
        return '<iframe width="' . $width . '" height="' . $heigh . '" id="' . $id . '" src="//www.youtube.com/embed/' . $id_video . '?enablejsapi=1&html5=1" frameborder="0" allowfullscreen></iframe>';
    }

}
