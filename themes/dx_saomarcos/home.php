<?php include "inc/header.php" ?>

<div class="bannerDestaque features cycle-slideshow" data-cycle-slides="> .item-feature" data-cycle-prev=".prevControl"
     data-cycle-next=".nextControl" data-cycle-timeout="8000" data-cycle-swipe-fx="scrollHorz" data-cycle-swipe="true"
     style="z-index: 5">
    <?php
    foreach (Site::getBanners() as $banner):
        extract($banner);
        $getUrl = (!empty($banner_link)) ? "href='{$banner_link}'" : "";
        ?>
        <a <?= $getUrl ?> title="<?= $banner_title ?>" class="item-feature no-decoration"
                          style="background-image: url('<?= BASE ?>/uploads/<?= $banner_image ?>')">
            <div class="atomwrapper">
                <div class="item-desc">

                </div>
            </div>
        </a>
        <?php
    endforeach;
    ?>

    <div id="progress"></div>
    <div class="onTable hidden-xs hidden-sm">
        <a href="" title="" class="prevControl onfade4 btn-default-hsm-reverse"><i class="fa fa-angle-double-left"></i></a>
        <a href="" title="" class="nextControl onfade4 btn-default-hsm"><i class="fa fa-angle-double-right"></i></a>
    </div>
    <div class="cycle-pager hidden-xs hidden-sm"></div>
</div>
<div class="clear40"></div>
<div class="clear10"></div>

<div class="container-fluid">
    <div class="atomwrapper">
        <h3 class="al-center centertable fontsize2 text-bold">NOSSAS ESPECIALIDADES</h3>
        <div class="clear10"></div>
        <div class="row" style="max-width: 90%; margin: 0px auto ">
            <div class=" MainCarousel">
                <?php
                $especialidades = Site::getEspecialidades();
                foreach ($especialidades as $esp):
                    extract($esp)
                    ?>
                    <div class="col-lg-2">
                        <article class="SvItem especialidade-item">
                            <div class="anchorSv onfade4 zd100" href="" title="">
                                <a class="brdContentSv zd45 centralizar font12px al-center animate200"
                                   style="height:80px; min-height: 80px;"
                                   href="<?= BASE ?>/especialidade/<?= $especialidade_name ?>">
                                    <div style="padding-top:0px;">
                                        <?= $especialidade_title ?>
                                    </div>
                                </a>
                            </div>
                        </article>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>

<div class="clear40 hidden-xs  hidden-sm"></div>
<div class="clear10"></div>

<div class="container-fluid bg-light-gray-100">
    <div class="atomwrapper content-page">
        <h3 class="al-center centertable fontsize2 text-bold">NOTÍCIAS</h3>
        <div class="clear10"></div>

        <div class="clear40"></div>

        <?php
        $noticiasDestaque = Site::getNoticias(1);
        $noticia = $noticiasDestaque[0];
        extract($noticia);
        $post_not_id[] = $noticia['post_id'];
        $noticiaData = new Date($post_date);

        $image = empty($noticia['post_image']) ? "image.jpg" : $noticia['post_image'];


        if (file_exists("uploads/{$image}")):
            ?>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="on-thumb-news"
                         style="background-image: url('<?= BASE ?>/uploads/<?= $noticia['post_image'] ?>');"></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"
                     style="height:300px; display: table; position:relative">
                    <div class="set-txt-desc">
                        <span><?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></span>
                    </div>
                    <div class="clear30"></div>
                    <h3 class="nomargin nopadding"><?= $noticia['post_title'] ?></h3>
                    <div class="clear10"></div>
                    <p class="font17px">
                        <?= $noticia['post_chamada'] ?>
                    </p>
                    <div class="clear20"></div>
                    <div style="position:absolute; bottom:10px;">
                        <a href="<?= BASE ?>/artigo/<?= $noticia['post_name'] ?>" title="<?= $noticia['post_title'] ?>"
                           class="onfade4 btn-default-hsm bg-red-300 no-decoration">CONTINUAR LENDO&nbsp;<i
                                    class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
            <?php
        else:
            ?>
            <div class="row">
                <div class="col-xs-12" style="height:300px; display: table; position:relative">
                    <div class="set-txt-desc">
                        <span><?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></span>
                    </div>
                    <div class="clear30"></div>
                    <h3 class="nomargin nopadding"
                        style="font-size: 40px; font-weight: bold;"><?= $noticia['post_title'] ?></h3>
                    <div class="clear10"></div>
                    <p style="font-size: 20px">
                        <?= $noticia['post_chamada'] ?>
                    </p>
                    <div class="clear20"></div>
                    <div style="position:absolute; bottom:10px;">
                        <a href="<?= BASE ?>/artigo/<?= $noticia['post_name'] ?>" title="<?= $noticia['post_title'] ?>"
                           class="onfade4 btn-default-hsm bg-red-300 no-decoration">CONTINUAR LENDO&nbsp;<i
                                    class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
            <?php
        endif;
        ?>


        <div class="clear40"></div>
        <div class="row MainNews">
            <?php
            $noticias = Site::getNoticias(9, $post_not_id);
            foreach ($noticias as $news):
                extract($news);
                $data = new Date($post_date);
                ?>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                    <div class="bg-other-n">
                        <div class="centralizar nopadding">
                            <div class="atomwrapper">
                                <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $post_image ?>&w=600&h=300"
                                     alt="<?= $post_title ?>" title="<?= $post_title ?>"/>
                                <div class="set-txt-desc">
                                    <span class="font18px"><?= "{$data->getDia()} de {$data->getMesName()} de {$data->getAno()} " ?></span>
                                </div>
                                <div class="clear10"></div>
                                <h3 class="nomargin nopadding font18px"><a
                                            href="<?= BASE ?>/artigo/<?= $post_name ?>"><?= $post_title ?></a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
        </div>
    </div>
</div>
<div class="clear40  hidden-xs  hidden-sm"></div>
<div class="clear10"></div>

<div class="about-hsm container-fluid">


    <div class="atomwrapper" onclick="location.href = '<?= BASE ?>/sobre'" style="cursor:pointer">
        <?php
        $page = Site::getPage(1);
        $galery = Site::getGalley(5, 1);
        foreach ($galery as $imgs) {
            $sobreImages[] = $imgs['file_src'];
        }
        shuffle($sobreImages);
        ?>
        <h3 class="al-center centertable fontsize2 text-bold"><?= $page['page_subtitle'] ?></h3>
        <div class="clear10"></div>

        <div class="clear40"></div>

        <div class="row no-gutters">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="item-grid-second"
                     style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[0] ?>') no-repeat;background-size:cover;"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 centralizar">
                <div class="atomwrapper">
                    <h1 class="al-center font17px text-bold">EQUIPAMENTOS MODERNOS</h1>
                    <p class="al-center">
                        <?= $page['page_chamada'] ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="item-grid-second"
                     style="background: url('<?= BASE; ?>/uploads/<?= $sobreImages[1] ?>') no-repeat;background-size:cover;"></div>
            </div>
        </div>

    </div>
</div>

<div class="clear40"></div>
<!--

<section class="MainAtendimento nycPhone hero" data-parallax-speed="1">
    <div class="hero-content">
        <div class="atomwrapper content-page">
            <h1 class="al-center color-white titleBorderW fontzero">TELEFONES<br>PARA CONTATO</h1>
            <div class="centralizar">
                <img style="max-width: 200px" src="<?= BASE; ?>/uploads/midias/slider/bg-shapePhone.png" alt="" title=""/>
            </div>
        </div>
    </div>
</section>
-->
<section class="ligue">
    <div>
        <span>(86)</span>
        <p>2106-8000</p>
    </div>
</section>

</div>

<?php include "inc/footer.php" ?>
        