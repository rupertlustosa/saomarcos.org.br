<?php
//Tratamento de Erros   ####################
//CSS Constantes :: Mensagens de Erro


//AdxErro :: Exibe erros lançados :: Front
function AdxErro($ErrMsg, $ErrNo, $ErrDie = null)
{
    $CssClass = ($ErrNo == E_USER_NOTICE ? DX_INFOR : ($ErrNo == E_USER_WARNING ? DX_ALERT : ($ErrNo == E_USER_ERROR ? DX_ERROR : $ErrNo)));
    echo "<div class=\"alert trigger {$CssClass} alert-dismissible fade in\" role=\"alert\">
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"><p style=\"margin-right:20px;color:#000;\" class=\"fa fa-close fa-lg\"></p></span></button>
          <p style=\"margin:0px\">{$ErrMsg}</p>
    </div>";
    if ($ErrDie):
        die;
    endif;
}

function AdxAlert($MSG, $Class)
{
    /*
    <div class="alert alert-success" role="alert">...</div>
    <div class="alert alert-info" role="alert">...</div>
    <div class="alert alert-warning" role="alert">...</div>
    <div class="alert alert-danger" role="alert">...</div>
     * 
     */
    $trigger = '<div class="alert ' . $Class . ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $MSG . '</div>';
    return $trigger;
}

//PHPErro :: Personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine)
{
    $CssClass = ($ErrNo == E_USER_NOTICE ? DX_INFOR : ($ErrNo == E_USER_WARNING ? DX_ALERT : ($ErrNo == E_USER_ERROR ? DX_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">";
    echo "<strong>Erro na linha: {$ErrLine} :: </strong> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}


function getUserLevels($Level = null, $return = 'key')
{
    $UserLevel = [
        1 => [1, 'Cliente (user)'],
        2 => [2, 'Assinante (user)'],
        3 => [3, 'Autor (user)'],
        9 => [9, 'Editor (adm)'],
        10 => [10, 'Super Admin (adm)']
    ];

    if (!empty($Level)):
        if ($return == 'key'):
            return $UserLevel[$Level][0];
        else:
            return $UserLevel[$Level][1];
        endif;

    else:
        return $UserLevel;
    endif;
}

function getTableList($view = null)
{
    $tabelas = array(
        'error' => ["VIEW" => "error", 'KEY' => 0],
        'login' => ["VIEW" => "login", 'KEY' => 0],
        'users' => ['KEY_NAME' => 'user_id', 'KEY' => 1, "DB" => DB_USERS, "TITLE" => "Usuários", "TITLE_SINGLE" => "Usuário", "CALLBACK" => "Users", "VIEW" => "users"],
        'posts' => ['KEY_NAME' => 'post_id', 'KEY' => 2, "DB" => DB_POSTS, "TITLE" => "Posts", "TITLE_SINGLE" => "Post", "CALLBACK" => "Posts", "VIEW" => "posts"],
        'midias' => ['KEY_NAME' => 'file_id', 'KEY' => 3, "DB" => DB_MIDIAS, "TITLE" => "Mídias", "TITLE_SINGLE" => "Mídia", "CALLBACK" => "Midias", "VIEW" => "midias"],
        /*'menus' => ['KEY_NAME'=>'menu_id','KEY'=>4,"DB"=>DB_MENUS,"TITLE"=>"Menus","TITLE_SINGLE"=>"Menu","CALLBACK"=>"Menus","VIEW"=>"menus"],*/
        'pages' => ['KEY_NAME' => 'page_id', 'KEY' => 5, "DB" => DB_PAGES, "TITLE" => "Páginas", "TITLE_SINGLE" => "Página", "CALLBACK" => "Pages", "VIEW" => "pages"],
        'banners' => ['KEY_NAME' => 'banner_id', 'KEY' => 6, "DB" => DB_BANNERS, "TITLE" => "Banners", "TITLE_SINGLE" => "Banner", "CALLBACK" => "Banners", "VIEW" => "banners"],
        'config' => ['KEY_NAME' => 'config_id', 'KEY' => 7, "DB" => DB_CONFIGS, "TITLE" => "Configurações", "TITLE_SINGLE" => "Configuração", "CALLBACK" => "Config", "VIEW" => "config"],
        'especialidades' => ['KEY_NAME' => 'especialidade_id', 'KEY' => 8, "DB" => DB_ESPECIALIDADES, "TITLE" => "Especialidades", "TITLE_SINGLE" => "Especialidade", "CALLBACK" => "Especialidades", "VIEW" => "especialidades"],
        'manuais' => ['KEY_NAME' => 'manual_id', 'KEY' => 9, "DB" => DB_MANUAIS, "TITLE" => "Manuais/Artigos", "TITLE_SINGLE" => "Manual/Artigo", "CALLBACK" => "Manuais", "VIEW" => "manuais"],
        'profissionais' => ['KEY_NAME' => 'profissional_id', 'KEY' => 10, "DB" => DB_PROFISSIONAIS, "TITLE" => "Profissionais", "TITLE_SINGLE" => "Profissional", "CALLBACK" => "Profissionais", "VIEW" => "profissionais"],
        'diretores' => ['KEY_NAME' => 'diretoria_id', 'KEY' => 11, "DB" => DB_DIRETORES, "TITLE" => "Diretores", "TITLE_SINGLE" => "Diretoria", "CALLBACK" => "Diretores", "VIEW" => "diretores"],
        'convenios' => ['KEY_NAME' => 'convenio_id', 'KEY' => 12, "DB" => DB_CONVENIOS, "TITLE" => "Convenios", "TITLE_SINGLE" => "Convenio", "CALLBACK" => "Convenios", "VIEW" => "convenios"],
        'clinicas' => ['KEY_NAME' => 'clinica_id', 'KEY' => 13, "DB" => DB_CLINICAS, "TITLE" => "Clinicas", "TITLE_SINGLE" => "Clinica", "CALLBACK" => "Clinicas", "VIEW" => "clinicas"],
        'lixeira' => ['KEY' => 14, "TITLE" => "Lixeira", "TITLE_SINGLE" => "lixeira"],
        'videos' => ['KEY_NAME' => 'video_id', 'KEY' => 15, "DB" => DB_VIDEOS, "TITLE" => "Vídeos", "TITLE_SINGLE" => "Vídeo", "CALLBACK" => "Videos", "VIEW" => "Videos"],
        'sociais' => ['KEY_NAME' => 'social_id', 'KEY' => 16, "DB" => DB_SOCIAIS, "TITLE" => "Responsabilidade Social", "TITLE_SINGLE" => "Responsabilidade Social", "CALLBACK" => "Sociais", "VIEW" => "sociais"],
        'revista' => ['KEY_NAME' => 'revista_id', 'KEY' => 17, "DB" => 'dx_revista', "TITLE" => "Revista", "TITLE_SINGLE" => "Revista", "CALLBACK" => "Revista", "VIEW" => "revista"],
        'cursos' => ['KEY_NAME' => 'curso_id', 'KEY' => 18, "DB" => 'dx_cursos', "TITLE" => "Cursos", "TITLE_SINGLE" => "Curso", "CALLBACK" => "Cursos", "VIEW" => "cursos"],
        'trabalhe-conosco' => ['KEY_NAME' => 'id', 'KEY' => 19, "DB" => 'rl_vagas', "TITLE" => "Trabalhe Conosco", "TITLE_SINGLE" => "Trabalhe Conosco", "CALLBACK" => "trabalhe-conosco", "VIEW" => "trabalhe-conosco"],
        'tipos-cursos' => ['KEY_NAME' => 'id', 'KEY' => 20, "DB" => DB_TIPOS_CURSOS, "TITLE" => "Tipos de Cursos", "TITLE_SINGLE" => "Tipo de curso", "CALLBACK" => "tipos-cursos", "VIEW" => "tipo-curso"],
        'corpo-docente' => ['KEY_NAME' => 'id', 'KEY' => 21, "DB" => DB_CORPO_DOCENTE, "TITLE" => "Corpo Docente", "TITLE_SINGLE" => "Corpo docente", "CALLBACK" => "corpo-docente", "VIEW" => "corpo-docente"],
        'artigos' => ['KEY_NAME' => 'id', 'KEY' => 22, "DB" => DB_ARTIGOS, "TITLE" => "Artigos", "TITLE_SINGLE" => "Artigo", "CALLBACK" => "artigos", "VIEW" => "artigos"],
        'rl_anais' => ['KEY_NAME' => 'id', 'KEY' => 23, "DB" => DB_ANAIS, "TITLE" => "Anais", "TITLE_SINGLE" => "Anais", "CALLBACK" => "rl_anais", "VIEW" => "rl_anais"],
        'rl_comite_etica' => ['KEY_NAME' => 'id', 'KEY' => 24, "DB" => DB_COMITE_ETICA, "TITLE" => "Comitê de ética", "TITLE_SINGLE" => "Comitê de ética", "CALLBACK" => "rl_comite_etica", "VIEW" => "rl_comite_etica"],
        'rl_revista_edicoes' => ['KEY_NAME' => 'id', 'KEY' => 25, "DB" => DB_REVISTA_EDICOES, "TITLE" => "Edições das revistas", "TITLE_SINGLE" => "Edições das revistas", "CALLBACK" => "rl_revista_edicoes", "VIEW" => "rl_revista_edicoes"],
###__REPLACE__###



    );

    if ($view) {

        if (array_key_exists($view, $tabelas)) {

            return $tabelas[$view];
        } else {
            null;
        }

    } else {
        return $tabelas;
    }
}

function get_header()
{
    include '_inc/header.php';
}

function get_nav($view)
{
    include '_inc/nav.php';
}


function set_css($link, $rel = "stylesheet", $media = "all")
{
    echo '<link href="' . $link . '" rel="' . $rel . '" media="' . $media . '">';
}

function set_script($link)
{
    echo '<script type="text/javascript" src="' . $link . '"></script>';
}

function dd($array)
{
    echo '<pre style="width:100%">';
    var_dump($array);
    echo '</pre>';
}

function getBannersLocal($local = null)
{
    $locais = array(
        1 => array('local' => 'Destaque', 'largura' => '1200', 'altura' => '600'),
    );
    if ($local) {
        return $locais[$local];
    } else {
        return $locais;
    }
}

function getEspecialidadesSecao($n = null)
{
    $array = array(
        1 => ["Medicina", "medicina"],
        2 => ["Odontologia", "odontologia"],
        3 => ["Outras especialidades", "outras"]
    );

    if ($n) {
        return $array[$n];
    } else {
        return $array;
    }
}


function getManuaisCat($n = null)
{
    $array = array(
        1 => "Artigos",
        2 => "Jornal da mama",
        3 => "Orientações",
        4 => "Cursos",
        5 => "Reunião Ginecologicas",
        6 => "Inscrição Residencia Médica",
        7 => "Dicas",
    );

    if ($n) {
        return $array[$n];
    } else {
        return $array;
    }
}

function getListUF($n = null)
{
    $array = array(
        "AC" => "AC",
        "AL" => "AL",
        "AM" => "AM",
        "AP" => "AP",
        "BA" => "BA",
        "CE" => "CE",
        "DF" => "DF",
        "ES" => "ES",
        "FN" => "FN",
        "GO" => "GO",
        "MA" => "MA",
        "MG" => "MG",
        "MS" => "MS",
        "MT" => "MT",
        "PA" => "PA",
        "PB" => "PB",
        "PE" => "PE",
        "PI" => "PI",
        "PR" => "PR",
        "RJ" => "RJ",
        "RN" => "RN",
        "RO" => "RO",
        "RR" => "RR",
        "RS" => "RS",
        "SC" => "SC",
        "SE" => "SE",
        "SP" => "SP",
        "TO" => "TO",
        "OE" => "OE",
    );

    if ($n) {
        return $array[$n];
    } else {
        return $array;
    }
}

function numToString($number = '')
{
    $rtn = '';
    if (!isset($number) || $number == '' || !is_numeric($number)) {
        $rtn = "<b>Error: </b> an interger 'number' must be passed";
    } elseif ($number > 62 || $number < 1) {
        $rtn = "<b>Error: </b> passed 'number' must be between 1 to 62 (a-zA-Z0-9)";
    } else {
        switch ($number) {
            case 1:
                $rtn = 'a';
                break;
            case 2:
                $rtn = 'b';
                break;
            case 3:
                $rtn = 'c';
                break;
            case 4:
                $rtn = 'd';
                break;
            case 5:
                $rtn = 'e';
                break;
            case 6:
                $rtn = 'f';
                break;
            case 7:
                $rtn = 'g';
                break;
            case 8:
                $rtn = 'h';
                break;
            case 9:
                $rtn = 'i';
                break;
            case 10:
                $rtn = 'j';
                break;
            case 11:
                $rtn = 'k';
                break;
            case 12:
                $rtn = 'l';
                break;
            case 13:
                $rtn = 'm';
                break;
            case 14:
                $rtn = 'n';
                break;
            case 15:
                $rtn = 'o';
                break;
            case 16:
                $rtn = 'p';
                break;
            case 17:
                $rtn = 'q';
                break;
            case 18:
                $rtn = 'r';
                break;
            case 19:
                $rtn = 's';
                break;
            case 20:
                $rtn = 't';
                break;
            case 21:
                $rtn = 'u';
                break;
            case 22:
                $rtn = 'v';
                break;
            case 23:
                $rtn = 'w';
                break;
            case 24:
                $rtn = 'x';
                break;
            case 25:
                $rtn = 'y';
                break;
            case 26:
                $rtn = 'z';
                break;
            case 27:
                $rtn = 'A';
                break;
            case 28:
                $rtn = 'B';
                break;
            case 29:
                $rtn = 'C';
                break;
            case 30:
                $rtn = 'D';
                break;
            case 31:
                $rtn = 'E';
                break;
            case 32:
                $rtn = 'F';
                break;
            case 33:
                $rtn = 'G';
                break;
            case 34:
                $rtn = 'H';
                break;
            case 35:
                $rtn = 'I';
                break;
            case 36:
                $rtn = 'J';
                break;
            case 37:
                $rtn = 'K';
                break;
            case 38:
                $rtn = 'L';
                break;
            case 39:
                $rtn = 'M';
                break;
            case 40:
                $rtn = 'N';
                break;
            case 41:
                $rtn = 'O';
                break;
            case 42:
                $rtn = 'P';
                break;
            case 43:
                $rtn = 'Q';
                break;
            case 44:
                $rtn = 'R';
                break;
            case 45:
                $rtn = 'S';
                break;
            case 46:
                $rtn = 'T';
                break;
            case 47:
                $rtn = 'U';
                break;
            case 48:
                $rtn = 'V';
                break;
            case 49:
                $rtn = 'W';
                break;
            case 50:
                $rtn = 'X';
                break;
            case 51:
                $rtn = 'Y';
                break;
            case 52:
                $rtn = 'Z';
                break;
            case 53:
                $rtn = '0';
                break;
            case 54:
                $rtn = '1';
                break;
            case 55:
                $rtn = '2';
                break;
            case 56:
                $rtn = '3';
                break;
            case 57:
                $rtn = '4';
                break;
            case 58:
                $rtn = '5';
                break;
            case 59:
                $rtn = '6';
                break;
            case 60:
                $rtn = '7';
                break;
            case 61:
                $rtn = '8';
                break;
            case 62:
                $rtn = '9';
                break;
        }
    }
    return $rtn;
}

function cc_rand($length = 6)
{
    $rtn = '';

    if (!is_numeric($length)) {
        $rtn = "<b>Error: </b>'length' parameter expects integer numbers only";
    } else {

        for ($x = 0; $x < $length; $x++) {
            $rtn .= numToString(rand(1, 62));
        }
    }

    return $rtn;
}