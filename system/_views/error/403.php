<?php
if (empty($Read)) {
    $Read = new Read;
}
$front->set_css(ADM_FILE . '/_css/folha.css');
include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div style="margin-top:75px">
            <div class="alert alert-danger" role="alert">403 - Permissão Negada!</div>
        </div>
    </div>
    <!-- /page content -->
<?php
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/footer.php';
?>