<!DOCTYPE html>
<?php

if (empty($URL[1])) {
    header(BASE . "/manuais/1");
}
$cat = $URL[1];
include "inc/header.php"
?>
<!-- Our team Section -->
<section class="content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2>Manuais & Artigos</h2>
            </div><!-- /.col-md-12 -->
        </div>
        <div class="clear40"></div>
        <section class="row">
            <div class="col-md-3" style="height:535px; background-color:#fff ">
                <div class="manuaisCategorias">
                    <ul>
                        <?php
                        $titleSessao = null;
                        foreach (getManuaisCat() as $key => $val) {
                            if ($cat == $key) {
                                $class = "currentManual";
                                $titleSessao = $val;
                            } else {
                                $class = "";
                            }

                            echo "<li ><a class='onfade {$class}' href='" . BASE . "/manuais/{$key}' title='{$val}'>{$val}</a></li>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="margin-top: 0px;"><?= $titleSessao ?></h3>
                    </div>
                    <div class="clear15"></div>
                    <?php
                    $getPage = (!empty($URL[3])) ? $URL[3] : null;
                    $Page = ($getPage ? $getPage : 1);
                    $BaseUrl = BASE . "/manuais/{$cat}/pg/";
                    $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                    $Paginator->ExePager($Page, 12);
                    $where = Admx::Trash();

                    $Read->ExeRead(DB_MANUAIS, " WHERE status=:st AND manual_category =:mc  {$where} ORDER BY created DESC LIMIT :limit OFFSET :offset", "mc={$cat}&st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                    foreach ($Read->getResult() as $ROW):
                        extract($ROW);
                        ?>
                        <div class="col-lg-4">
                            <a href="<?= BASE . "/uploads/" . $manual_file ?>" target="_blank"
                               class="post-it-note onfade4 bg-red-300 fontsize1 text-bold"
                               style="font-size:14px; height: 80px !important">
                                <?= $manual_title ?>
                            </a>
                        </div>
                        <?php
                    endforeach;
                    ?>
                </div>
                <div class="clear40"></div>
                <div style="display:table; margin: 20px auto">
                    <?php
                    $Paginator->ExePaginator(DB_MANUAIS, " WHERE status=:st AND manual_category =:mc  {$where} ", "mc={$cat}&st=1");
                    echo $Paginator->getPaginator();
                    ?>
                </div>

                <div class="clear40"></div>
            </div>
        </section>

    </div>

</section>

<?php include "inc/footer.php" ?>