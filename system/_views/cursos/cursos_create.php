<?php
$TABLE = getTableList($view);

if (empty($Read)) {
    $Read = new Read;
}

    $Read2 = new Read;
$Read2->FullRead("SELECT * FROM rl_tipos_cursos order by tipo");

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['status'] = 0;
    $dados['publico_alvo'] = '';
    $dados['periodo_curso'] = '';
    $dados['coordenacao'] = '';
    $dados['curso_limite'] = 170;
    $create->ExeCreate($TABLE['DB'], $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=cursos/create&id=" . $newId);

} else {
    $Read->FullRead("SELECT * FROM " . $TABLE['DB'] . " WHERE curso_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        header("Location: " . ADM_URL . "?dx=error/404");
    }
}

$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/cursos/cursos.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
    <script src="<?= BASE ?>/system/_js/jquery.maskMoney.min.js"></script>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <a title="Criar <?= $TABLE['TITLE_SINGLE'] ?>"
                           href="<?= ADM_URL ?>/?dx=<?= $TABLE['VIEW'] ?>/create" class="btn btn-info "> <i
                                    class="fa fa-plus"></i></a>
                        <?= $TABLE['TITLE_SINGLE'] ?>
                        <small> <?= $Post['curso_title'] ?></small>
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <?= Admx::formSearch($TABLE['VIEW']) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                <?php Admx::setFormToken() ?>
                                <div class="col-md-8">

                                    <div>
                                        <?php
                                        if (file_exists("../uploads/" . $Post['image']) && !is_dir("../uploads/" . $Post['image'])) {
                                            echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $Post['image'] . '&w=900" />';
                                        } else {
                                            echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=600&h=190" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                        }
                                        ?>

                                    </div>
                                    <div class="clear10"></div>

                                    <div class="form-group col-md-12">
                                        <label>Tipo</label>
                                        <select name="tipo_curso_id" class="form-control">
                                            <option></option>
                                            <?php
                                            foreach ($Read2->getResult() as $tipo) {
                                                $selected = $Post['tipo_curso_id'] == $tipo['id'] ? "SELECTED='SELECTED'" : "";
                                                echo "<option value='{$tipo['id']}' {$selected} >{$tipo['tipo']}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Título</label>
                                        <input type="text" name="curso_title" value="<?= $Post['curso_title'] ?>"
                                               placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                               class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Conteudo</label>
                                        <textarea class="dx_editor"
                                                  name="curso_content"><?= $Post['curso_content'] ?></textarea>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Público alvo</label>
                                        <input type="text" name="publico_alvo" value="<?= $Post['publico_alvo'] ?>"
                                               placeholder=""
                                               class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Período do curso</label>
                                        <input type="text" name="periodo_curso" value="<?= $Post['periodo_curso'] ?>"
                                               placeholder=""
                                               class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Carga Horária</label>
                                        <input type="text" name="carga_horaria" value="<?= $Post['carga_horaria'] ?>"
                                               placeholder=""
                                               class="form-control">
                                    </div>


                                    <div class="form-group col-md-12">
                                        <label>Coordenação</label>
                                        <textarea class="dx_editor"
                                                  name="coordenacao"><?= $Post['coordenacao'] ?></textarea>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Corpo Docente</label>
                                    </div>

                                    <div class="form-group col-md-12">

                                        <?php

                                        $ja_adicionados = [];
                                        $Read->FullRead("SELECT * FROM " . DB_CURSOS_CORPO_DOCENTE . " WHERE curso_id=:curso_id ", "curso_id={$KeyId}");
                                        foreach ($Read->getResult() as $item) {

                                            $ja_adicionados[] =  $item['corpo_docente_id'];
                                        }

                                        $Read->FullRead("SELECT * FROM " . DB_CORPO_DOCENTE . " ORDER BY nome ASC");
                                        foreach ($Read->getResult() as $corpo_docente) {

                                            ?>
                                            <div class="form-group col-xs-6">
                                                <label>
                                                    <input type="checkbox" name="corpo_docente_id[]" value="<?= $corpo_docente['id']?>" <?php echo in_array($corpo_docente['id'], $ja_adicionados) ? 'checked' : '' ?>  > <?= $corpo_docente['nome']?>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="col-md-4" style="padding-left:20px">
                                    <div class="form-group col-md-12">
                                        <label>Imagem</label>
                                        <div class="upload_file" style="border:1px solid #ccc">
                                            <input class="loadimage" type="file" name="image"/>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Valor (R$)</label>
                                        <input type="text" name="curso_valor" id="curso_valor" value="<?= number_format($Post['curso_valor'], 2, ',', '.') ?>"
                                               placeholder="Digite o valor do curso" class="form-control">
                                        <small>Formato monetário, ex: 150,00</small>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Valor por extenso</label>
                                        <input type="text" name="valor_extenso" value="<?= $Post['valor_extenso'] ?>"
                                               placeholder="" class="form-control">
                                        <small>Utilize esse campo se preferir exibir o valor em outro formato, ex: 3 x R$ 50,00</small>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Aceita Pagseguro?</label>
                                        <div class="clear"></div>
                                        <label class="radio-inline">
                                            <input type="radio" name="aceita_pagseguro" id="aceita_pagseguro_sim"
                                                   value="S" <?php echo($Post['aceita_pagseguro'] == 'S' ? 'checked="checked"' : '') ?>>
                                            Sim
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="aceita_pagseguro" id="aceita_pagseguro_nao"
                                                   value="N" <?php echo($Post['aceita_pagseguro'] == 'N' ? 'checked="checked"' : '') ?>>
                                            Não
                                        </label>
                                    </div>
                                    <!--
                                    <div class="form-group col-md-12">
                                        <label>Enviar Comprovante?</label>
                                        <div class="clear"></div>
                                        <label class="radio-inline">
                                            <input type="radio" name="curso_comprovante"  id="envia_sim" value="sim"> sim
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="curso_comprovante" checked="checked" id="envia_nao" value="nao"> Não
                                        </label>            
                                    </div>
                                    -->

                                    <div class="form-group col-md-12">
                                        <label>Limite de Vagas</label>
                                        <input type="text" name="curso_limite" value="<?= $Post['curso_limite'] ?>"
                                               placeholder="Digite o limite" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label>Link Externo</label>
                                        <input type="text" name="link_externo" value="<?= $Post['link_externo'] ?>"
                                               placeholder="URL com http://" class="form-control">
                                        <small>Utilize esse campo para linkar com um site externo</small>
                                    </div>

                                    <div class="clear20"></div>
                                    <div class="form-group">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                            </label>
                                        </div>
                                        <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                        <input type="hidden" name="callback_action" value="manager">
                                        <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                        <img src="_img/load.gif" class="loading_form pull-right">

                                        <button class="btn btn-success pull-right" type="submit">
                                            Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                    </div>

                                </div>


                            </form>
                            <div class="clearfix"></div>
                        </div><!-- content do formulario -->
                    </div><!-- painel do formulario -->
                    <div class="clear20"></div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <script>
        $(function() {
            $("#curso_valor").maskMoney({thousands:'.', decimal:','});
        })
    </script>

<?php
//include './_inc/gallery.php';
include './_inc/footer.php';
?>