<!-- footer content -->
<footer>
    <div class="pull-right">
        By Uapcode - Template by <a target="_blank" href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>

<!-- /footer content -->
</div>
</div>


<!-- FastClick -->
<script src="<?= CDN_FILE ?>/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?= CDN_FILE ?>/vendors/nprogress/nprogress.js"></script>

<!-- bootstrap-progressbar -->
<script src="<?= CDN_FILE ?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?= CDN_FILE ?>/vendors/iCheck/icheck.min.js"></script>


<!-- PNotify -->
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?= CDN_FILE ?>/build/js/custom.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="<?= CDN_FILE ?>/js/moment/moment.min.js"></script>
<script src="<?= CDN_FILE ?>/js/datepicker/daterangepicker.js"></script>


</body>
</html>
