<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>TRABALHE CONOSCO</h2>
                </div><!-- /.col-md-12 -->
            </div>
            <section id="postIndex" class="widthWrapper">

                <?php
                $getPage = (!empty($URL[2])) ? $URL[2] : null;
                $Page = ($getPage ? $getPage : 1);
                $BaseUrl = BASE . "/trabalhe-conosco/pg/";
                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                $Paginator->ExePager($Page, 10);
                $where = Admx::Trash();

                $Read->ExeRead(DB_VAGAS, " WHERE status=:st {$where} ORDER BY created DESC LIMIT :limit OFFSET :offset", "st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                foreach ($Read->getResult() as $ROW):
                    extract($ROW);
                    ?>
                    <article class="postInner">
                        <a href="<?= BASE ?>/vaga/<?= $id ?>" title="<?= $vaga ?>">
                            <div class="postImg">
                                <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $image ?>&w=500"/>
                            </div>
                            <h2 class="color-red-300 fontsize1b"><?= $vaga ?></h2>
                            <p><?= substr(strip_tags(html_entity_decode($informacoes)), 0, 280) ?>...</p>

                            <input type="button" class="btn-default-hsm bg-red-300" value="Candidatar a vaga">
                        </a>
                    </article>
                    <?php
                endforeach;
                ?>

            </section>
            <div style="display:table; margin: 20px auto">
                <?php
                $Paginator->ExePaginator(DB_VAGAS, " WHERE status=:st {$where} ", "&st=1");
                echo $Paginator->getPaginator();
                ?>
            </div>


            <article class="postInner" id="bloco_curriculo" style="display: none">

                <section class="content-section bg-light-brown" style="background-color:#fff; border:solid 1px #ccc;">
                    <div class="container-fluid">
                        <div class="atomwrapper">
                            <div class="section2" style="width:auto">
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <h3>CADASTRO PARA VAGAS GERAIS</h3>
                                    </div>

                                    <div class="clear40"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="sec2contactform">
                                            <form action="javascript:;" method="post" id="formContato">
                                                <div class="clearfix">
                                                    <input type="text" required="required" name="nome" maxlength="100"
                                                           class="col2 first" placeholder="Nome">
                                                    <input type="email" required="required" name="email" maxlength="100"
                                                           class="col2 last" placeholder="Email">
                                                </div>

                                                <div class="clear10"></div>

                                                <div class="clearfix">
                                                    <input type="text" name="linkedin" maxlength="255" class="col-xs-12"
                                                           placeholder="Linkedin (Endereço completo)">
                                                </div>

                                                <div class="clear10"></div>

                                                <div class="clearfix">
                                                    <input type="text" class="col2 first phone" maxlength="20"
                                                           name="telefone" placeholder="Telefone Residencial">
                                                    <div class="col2 last ">
                                                        Arquivo (pdf/zip)
                                                        <input type="file" required="required" name="curriculo">
                                                    </div>

                                                </div>

                                                <div class="clearfix">
                                                    <textarea id="" cols="30" required="required" maxlength="500"
                                                              name="mensagem" rows="3"
                                                              placeholder="Mensagem"></textarea>
                                                </div>
                                                <div class="clearfix">
                                                    <div class="response" style="display:none">
                                                        <div class="alert alert-success" role="alert">Enviando...</div>
                                                    </div>
                                                    <input type="hidden" value="" required="required" name="vaga_id">
                                                    <input type="submit" class="btn-default-hsm bg-red-300"
                                                           value="Enviar">
                                                </div>
                                            </form>
                                        </div>
                                        <script>
                                            $(function () {
                                                $(".phone").mask("(99) 9.9999-9999");
                                                $(".response").click(function () {
                                                    $(this).html('');
                                                })
                                                base = $('link[rel="base"]').attr('href');
                                                $("#formContato").submit(function () {
                                                    $(".response").show();
                                                    var form = $(this);
                                                    form.ajaxSubmit({
                                                        url: base + '/sendcurriculo.php',
                                                        dataType: 'json',
                                                        success: function (data) {
                                                            if (data == 1) {
                                                                $(".response .alert-success").html('Formulário enviado com sucesso!</div>');
                                                            } else {
                                                                $(".response .alert-success").html(response);
                                                            }
                                                        }
                                                    });
                                                    $("#formContato")[0].reset();
                                                    return false;

                                                });

                                                $("#click_bloco_curriculo").click(function () {
                                                    $("#bloco_curriculo").toggle("slow", function () {
                                                        // Animation complete.
                                                    });

                                                    $(this).hide('slow');
                                                });

                                            });


                                        </script>
                                        <br/>
                                        <!--<p style="font-weight: bold">ou clique em alguma vaga abaixo para se candidatar a vaga específica</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>

            <article class="postInner text-center">
                <centrer>
                    <input type="button" class="btn-default-hsm bg-red-300" value="Candidatar as vagas gerais"
                           id="click_bloco_curriculo">
                </centrer>
            </article>

        </div>
        <div class="clear40"></div>
    </section>

<?php include "inc/footer.php" ?>