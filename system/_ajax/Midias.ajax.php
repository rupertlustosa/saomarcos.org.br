<?php


require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'midias';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];

//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "edit_name":
            $Read->ExeRead(DB_MIDIAS, "WHERE file_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):

            else:
                $post = $Read->getResult()[0];
                $file_name = !empty($PostData['file_name']) ? $PostData['file_name'] : $post['file_id'];
                $slug = Check::Name($file_name);
                $src_old = $post['file_src'];
                $ext = substr($src_old, -4);
                $src = preg_replace("/(.*)\/(.*)/", "$1/" . $slug . $ext, $src_old);

                if (file_exists("../../uploads/" . $src_old) && !is_dir("../../uploads/" . $src_old)) {

                    if (file_exists("../../uploads/" . $src) && !is_dir("../../uploads/" . $src)) {
                        $slug = $slug . time();
                    }

                    $src = preg_replace("/(.*)\/(.*)/", "$1/" . $slug . $ext, $src_old);
                    $PostData['file_name'] = $file_name;
                    $PostData['file_slug'] = $slug;
                    $PostData['file_src'] = $src;
                    rename("../../uploads/" . $src_old, "../../uploads/" . $src);
                    $Update->ExeUpdate(DB_MIDIAS, $PostData, "WHERE file_id = :id", "id={$KeyId}");
                    $jSON["result"] = "success";
                    $jSON["src"] = '../tim.php?src=uploads/' . $src . '&w=250&h=150';
                    $jSON["text"] = $file_name;
                } else {

                    $jSON["result"] = "error";
                }

            endif;
            break;

        case 'delete':

            $Read->FullRead("SELECT * FROM " . DB_MIDIAS . " WHERE file_id=:id", "id={$KeyId}");
            if ($Read->getResult()):
                $src = $Read->getResult()[0]['file_src'];
                if (file_exists("../../uploads/" . $src) && !is_dir("../../uploads/" . $src)) {
                    unlink("../../uploads/" . $src);
                    $Delete->ExeDelete(DB_MIDIAS, " WHERE file_id = :id ", "id={$KeyId}");
                    $jSON["result"] = "success";
                } else {
                    $Delete->ExeDelete(DB_MIDIAS, " WHERE file_id = :id ", "id={$KeyId}");
                    $jSON["result"] = "success";
                }
            else:
                $jSON["result"] = "error";
            endif;
            break;

        case "gbGalery":

            if (!empty($_FILES['galery'])):
                $File = $_FILES['galery'];
            endif;
            $gbFile = array();
            $gbCount = count($File['type']);
            $gbKeys = array_keys($File);
            $gbLoop = 0;

            for ($gb = 0; $gb < $gbCount; $gb++):
                foreach ($gbKeys as $Keys):
                    $gbFiles[$gb][$Keys] = $File[$Keys][$gb];
                endforeach;
            endfor;
            $PARENT = getTableList($PostData['view']);
            $table_parent = $PARENT["DB"];
            $campo_id = $PARENT["KEY_NAME"];
            $jSON['gallery'] = null;
            $Read->ExeRead($table_parent, "WHERE {$campo_id} = :id", "id={$KeyId}");
            if (!$Read->getResult()):
                $jSON['error'] = 'ok';
            else:
                $Post = $Read->getResult()[0];
                $tit = preg_match("/([a-z]+)[_id]/", $campo_id, $match);
                $prefix = $match[1];
                $post_title = !empty($Post[$prefix . '_title']) ? $Post[$prefix . '_title'] : $Post[$prefix . '_id'];

                foreach ($gbFiles as $UploadFile):
                    $gbLoop++;
                    $name = $post_title . "-{$gbLoop}-" . time();
                    $image_name = Check::Name($name);
                    $Upload->Image($UploadFile, $image_name, IMAGE_W, $TABLE['VIEW'] . "/" . $PARENT['VIEW']);
                    if ($Upload->getResult()):
                        $image_src = $Upload->getResult();

                        $PostMidias['file_table_id'] = $PARENT['KEY'];
                        $PostMidias['file_key_id'] = $KeyId;
                        $PostMidias['file_name'] = $name;
                        $PostMidias['file_slug'] = $image_name;
                        $PostMidias['file_src'] = $image_src;
                        $PostMidias['file_type'] = $UploadFile['type'];
                        $Create->ExeCreate(DB_MIDIAS, $PostMidias);
                        $image_key = $Create->getResult();
                        $galery = ["key" => $image_key, "name" => $name, "src" => $image_src];
                        $jSON['gallery'][] = $galery;
                    endif;
                    $name = null;
                endforeach;

            endif;

            break;
    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
