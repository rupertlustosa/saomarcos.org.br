<?php
extract($DataResult);
if (isset($URL[2])) {
    $token = $URL[2];
    $Read->FullRead("SELECT hash,inscricao_id FROM dx_cursos_inscricao WHERE hash=:hs", "hs={$token}");
    if ($Read->getResult()) {
        $inscricao = $Read->getResult()[0];
        $inscricao_id = $inscricao['inscricao_id'];
        $Update = new Update;
        $dados['status'] = 1;
        $Update->ExeUpdate("dx_cursos_inscricao", $dados, " WHERE inscricao_id =:id ", "id={$inscricao_id}");
        if ($Update->getResult()) {
            $msg = '<div class="alert alert-success">
                <strong>Sucesso!</strong>
                <br>Inscrição realizada com sucesso!
                <br>Seu número de inscrição é <strong>' . str_pad($inscricao['inscricao_id'], 5, 0, STR_PAD_LEFT) . '</strong></div>';
        } else {
            $msg = '<div class="alert alert-error">Token inválido</div>';
        }
    }

} else {
    header("location: " . BASE);
}

include "inc/header.php";
?>
    <style>
        .curso-info {
            width: 100%;
            height: 200px;
            overflow: auto;
            display: none;
            border: solid 1px #ccc;
            margin-bottom: 20px;
        }

        .curso-info .curso-content {
            padding: 20px;
        }
    </style>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="clear40"></div>
                    <h2>INSCRIÇÃO</h2>
                    <h3 class="caption color-black-100 centertable" style="font-weight:normal; color:#999; width:auto">
                        <?= $curso_title ?>
                    </h3>
                    <div class="clear40"></div>
                    <div style="display:table;margin:20px auto; width:400px">
                        <?= $msg ?>
                    </div>

                </div><!-- /.col-md-12 -->
                <div class="clear30"></div>

                <div class="atomwrapper">

                </div>
                <div class="clear40"></div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="clear20"></div>

    </section><!-- /.our-team -->

<?php include "inc/footer.php" ?>