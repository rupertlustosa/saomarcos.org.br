$(function () {
    var base = $('link[rel="base"]').attr('href');
    var redirect = window.location.href;
    var url_login = base + "/?dx=login";

    $('form').submit(function () {
        var form = $(this);

        var callback = form.find('input[name="callback"]').val();
        var callback_action = form.find('input[name="callback_action"]').val();
        var dados = form.serialize();
        $.post(base + "/_ajax/Login.ajax.php", dados, function (data) {
            if (data.success === 1) {
                new PNotify({
                    title: 'Sucesso!',
                    text: 'Login realizado com sucesso!',
                    type: 'info',
                    styling: 'bootstrap3',
                    addclass: 'dark'
                });
                if (redirect === base || redirect === url_login) {
                    setTimeout(function () {
                        location.href = base
                    }, 1000);
                } else {
                    setTimeout(function () {
                        location.href = redirect
                    }, 1000);
                }

            } else {
                new PNotify({
                    title: 'Error!',
                    text: data.error,
                    type: 'error',
                    styling: 'bootstrap3',
                });
            }
        }, 'json');
        return false;
    });
});