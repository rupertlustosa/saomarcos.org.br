<?php
/**
 * Created by PhpStorm.
 * User: rupert
 * Date: 22/04/17
 * Time: 19:19
 */

namespace Pagseguro;


class Documents
{
    public $number;
    public $type;

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this;
    }

    /**
     * @param mixed $number
     * @return Documents
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @param mixed $type
     * @return Documents
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}