<?php
ob_start();
include "./_app/autoload.php";
if (empty($Read)) {
    $Read = new Read;
}
$admx = new Admx;
$getURL = strip_tags(trim(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setURL = (empty($getURL) ? 'index' : $getURL);
$URL = explode('/', $setURL);
$SEO = new Seo($setURL);
$DataResult = $SEO->getDataResult();

?>
<!DOCTYPE html>
<html lang="pt-br" itemscope itemtype="https://schema.org/<?= $SEO->getSchema(); ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title><?= $SEO->getTitle(); ?></title>
        <meta name="description" content="<?= $SEO->getDescription(); ?>"/>

        <meta name="robots" content="index, follow"/>

        <link rel="base" href="<?= BASE; ?>"/>
        <link rel="canonical" href="<?= BASE; ?>/<?= $getURL; ?>"/>
        <link rel="sitemap" type="application/xml" href="<?= BASE; ?>/sitemap.xml" />

        <meta itemprop="name" content="<?= $SEO->getTitle(); ?>"/>
        <meta itemprop="description" content="<?= $SEO->getDescription(); ?>"/>
        <meta itemprop="image" content="<?= $SEO->getImage(); ?>"/>
        <meta itemprop="url" content="<?= BASE; ?>/<?= $getURL; ?>"/>

        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?= $SEO->getTitle(); ?>" />
        <meta property="og:description" content="<?= $SEO->getDescription(); ?>" />
        <meta property="og:image" content="<?= $SEO->getImage(); ?>" />
        <meta property="og:url" content="<?= BASE; ?>/<?= $getURL; ?>" />
        <meta property="og:site_name" content="<?= SITE_NAME; ?>" />
        <meta property="og:locale" content="pt_BR" />

        <meta property="twitter:card" content="summary_large_image" />

        <meta property="twitter:domain" content="<?= BASE; ?>" />
        <meta property="twitter:title" content="<?= $SEO->getTitle(); ?>" />
        <meta property="twitter:description" content="<?= $SEO->getDescription(); ?>" />
        <meta property="twitter:image" content="<?= $SEO->getImage(); ?>" />
        <meta property="twitter:url" content="<?= BASE; ?>/<?= $getURL; ?>" />           

        <link href="<?= FILE; ?>/_cdn/_front/_libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_libraries/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick-theme.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/default.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/hospitalsm-core.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/responsive.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_assets/_fonts/dosis/dosis.css" rel="stylesheet">
        <link href="<?= FILE; ?>/_cdn/_front/_assets/_img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <link href="<?= FILE; ?>/_cdn/_front/_libraries/lightbox2-master/lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
        
        
        <!--[if lt IE 9]>
        <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/html5shiv.min.js"></script>
        <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/respond.min.js"></script>
        <![endif]-->
        <link href="<?= FILE; ?>/style.css?v=<?=time()?>" rel="stylesheet">

        <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.min.js"></script>
        <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.form.js"></script>
        <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/maskinput.js"></script>
        
        <script src="<?= FILE; ?>/_cdn/_front/_libraries/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= FILE; ?>/script.js"></script>
        
        <script>
          (function() {
            var cx = '006353469993727996294:g0js6myewea';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
          })();
        </script>
        
        

    </head>
    <body>
        <?php
        //var_dump($URL);
        //CONTENT
        $URL[1] = (empty($URL[1]) ? null : $URL[1]);

        $Pages = array();
        $Read->FullRead("SELECT page_name FROM " . DB_PAGES . " WHERE status = 1");
        if ($Read->getResult()):
            foreach ($Read->getResult() as $SinglePage):
                $Pages[] = $SinglePage['page_name'];
            endforeach;
        endif;

        if (in_array($URL[0], $Pages) && file_exists(PATH . '/page.php')):

            if (file_exists(PATH . "/page-{$URL[0]}.php")):

                require PATH . "/page-{$URL[0]}.php";
            else:
                require PATH . '/page.php';
            endif;
        elseif (file_exists(PATH . '/' . $URL[0] . '.php')):
            if ($URL[0] == 'artigos' && file_exists(PATH . "/cat-{$URL[1]}.php")):
                require PATH . "/cat-{$URL[1]}.php";
            else:
                require PATH . '/' . $URL[0] . '.php';
            endif;
        elseif (file_exists(PATH . '/' . $URL[0] . '/' . $URL[1] . '.php')):
            require PATH . '/' . $URL[0] . '/' . $URL[1] . '.php';
        else:
            if (file_exists(PATH . "/404.php")):
                require PATH . '/404.php';
            else:
                require PATH . '/home.php';
            endif;
        endif;
        ?>
    </body>
</html>
<?php
ob_end_flush();
?>