<?php

require('./_app/Library/PHPMailer/class.phpmailer.php');

$params = array();
parse_str($_POST["data"], $params);
function clean_string($string)
{
    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
    return str_replace($bad, "", $string);
}

if (isset($params['contactSOU'])) {

    $contactSou = $params['contactSOU'];
    unset($params['contactSOU']);

    foreach ($contactSou as $key => $val) {
        $contactSou[$key] = clean_string($val);
    }
} else {

    $contactSou = [];
}

foreach ($params as $key => $value) {
    $params[$key] = clean_string($value);
}

$nome = $params['contactNome'];
$email = $params['contactMail'];
$telResidencial = $params['contactTelCasa'];
$telComercial = $params['contactTelComercial'];
$mensagem = $params['contactMensagem'];
$sou = implode(";", $contactSou);


$HTML = 'Contato Site';
$HTML .= "Nome: {$nome} <br>";
$HTML .= "Email: {$email} <br>";
$HTML .= "Sou: {$sou} <br>";
$HTML .= "Telefone Residencial: {$telResidencial} <br>";
$HTML .= "Telefone Comercial: {$telComercial} <br>";
$HTML .= "Mensagem: <br>";
$HTML .= "{$mensagem} <br>";

$mail = new PHPMailer();
$mail->isMail(); // Define que a mensagem será SMTP

$mail->FromName = "WebMaster";
$mail->SetFrom('avisos@saomarcos.org.br');
$mail->AddAddress('faleconosco@saomarcos.org.br', 'Faleconosco HSM');
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->Subject = "|| CONTATO SITE HSM||"; // Assunto da mensagem
$mail->Body = $HTML;

$enviado = $mail->Send();
if ($enviado) {
    echo 1;
} else {
    echo "Não foi possível enviar o e-mail.";
    echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
}

