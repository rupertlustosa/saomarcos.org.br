<?php
/**
 * Created by PhpStorm.
 * User: rupertlustosa
 * Date: 30/11/17
 * Time: 20:42
 */

$__table__ = 'rl_revista_edicoes';
$__DB_TABLE__ = 'DB_REVISTA_EDICOES';
$__TITLE_SINGLE__ = 'Edições das revistas';
$__TITLE__ = 'Edições das revistas';
$__PERMISSION__ = 25;

$_FIELDS = [
    [
        'name' => 'nome',
        'label' => 'Nome',
        'index' => 1,
    ],
    [
        'name' => 'url',
        'label' => 'url',
        'index' => 1,
    ],
    /*[
        'name' => 'conteudo',
        'label' => 'Conteúdo',
        'index' => 1,
    ],*/
    /*[
        'name' => '',
        'index' => '',
    ],
    [
        'name' => '',
        'index' => '',
    ],
    [
        'name' => '',
        'index' => '',
    ],
    [
        'name' => '',
        'index' => '',
    ],*/
];


$replaces[] = [
    'codigo' => 'define(\'' . $__DB_TABLE__ . '\', \'' . $__table__ . '\'); //Tabela de ' . $__table__.'
###__REPLACE__###
',
    'local' => '_app/Config/define.inc.php',
];


$replaces[] = [
    'codigo' => '        \'' . $__table__ . '\' => [\'KEY_NAME\' => \'id\', \'KEY\' => ' . $__PERMISSION__ . ', "DB" => ' . $__DB_TABLE__ . ', "TITLE" => "' . $__TITLE__ . '", "TITLE_SINGLE" => "' . $__TITLE_SINGLE__ . '", "CALLBACK" => "' . $__table__ . '", "VIEW" => "' . $__table__ . '"],
###__REPLACE__###
',
    'local' => '_app/Config/functions.inc.php',
];

$replaces[] = [

    'codigo' => '                    if (Session::checkPermission(' . $__PERMISSION__ . ')) { ?>
                        <li class="<?= (!empty($view) && $view == \'' . $__DB_TABLE__ . '\') ? \'current_section active\' : \'\' ?>">
                            <a>
                                <i class="fa fa-sticky-note"></i> ' . $__TITLE__ . '
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=<?=' . $__DB_TABLE__ . '?>/home">Listar ' . $__TITLE__ . '</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=<?=' . $__DB_TABLE__ . '?>/create">Adicionar ' . $__TITLE__ . '</a>
                                </li>
                            </ul>
                        </li>
                    <?php } 

###__REPLACE__###

',
    'local' => 'system/_inc/nav.php',
];

/*$replaces[] = [
    'codigo' => '',
    'local' => '',
];*/


$pasta_views = '/var/www/html/cj/hospitalsaomarcos/system/_views';

$view_path = $pasta_views . DIRECTORY_SEPARATOR . $__table__;
if(!file_exists($view_path)){

    if (mkdir($view_path)) {

        echo '<br />Pasta Criada: ' . $view_path;
    } else echo '<br />Erro ao criar pasta: ' . $view_path;
}


echo '<hr />';

$file_js = $view_path . '/' . $__table__ . '.js';
if (!file_exists($file_js)) {

    $content = file_get_contents('_views/__table__/__table__.js');

    if (file_put_contents($file_js, $content)) {

        echo $file_js . ' Criado com sucesso<hr />';
    } else {

        echo $file_js . ' ERRO AO CRIAR ESSE ARQUIVO!!<hr />';
    }
}

foreach ($_FIELDS as $FIELD) {

    $__CREATE_VAZIO__[] = '    $dados[\'' . $FIELD['name'] . '\'] = \'\';';
    $__DELETE_VAZIO__[] = '' . $FIELD['name'] . ' = \'\'';

    $__FORM_CREATE__[] = '                                <div class="form-group">
                                    <label>' . $FIELD['label'] . '</label>
                                    <input type="text" name="' . $FIELD['name'] . '" id="' . $FIELD['name'] . '" value="<?= $itemDb[\'' . $FIELD['name'] . '\'] ?>"
                                           class="form-control">
                                </div>';

    if ($FIELD['index']) {

        $__LINHA_INDEX_TH__[] = '                                    <th>#' . $FIELD['label'] . '</th>';
        $__LINHA_INDEX_TD__[] = '                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=<?= ' . $__DB_TABLE__ . '?>/create&id=<?= $id ?>"><?= $' . $FIELD['name'] . ' ?></a>
                                            <?= $' . $FIELD['name'] . ' ?>
                                        </td>';
    }
    $__SEARCH__[] = '\'' . $FIELD['name'] . '\'';

}

$file_create = $view_path . '/' . $__table__ . '_create.php';
if (!file_exists($file_create)) {

    $content = file_get_contents('_views/__table__/__table___create.php');

    $content = str_replace('__CREATE_VAZIO__', implode("\r\n", $__CREATE_VAZIO__), $content);
    $content = str_replace('__FORM_CREATE__', implode("\r\n", $__FORM_CREATE__), $content);
    $content = str_replace('__DB_TABLE__', $__DB_TABLE__, $content);
    $content = str_replace('__TITLE_SINGLE__', $__TITLE_SINGLE__, $content);
    $content = str_replace('__TITLE__', $__TITLE__, $content);

    if (file_put_contents($file_create, $content)) {

        echo $file_create . ' Criado com sucesso<hr />';
    } else {

        echo $file_create . ' ERRO AO CRIAR ESSE ARQUIVO!!<hr />';
    }
}


$file_index = $view_path . '/' . $__table__ . '_home.php';
if (!file_exists($file_index)) {

    $content = file_get_contents('_views/__table__/__table___home.php');

    $content = str_replace('__LINHA_INDEX_TH__', implode("\r\n", $__LINHA_INDEX_TH__), $content);
    $content = str_replace('__LINHA_INDEX_TD__', implode("\r\n", $__LINHA_INDEX_TD__), $content);
    $content = str_replace('__DELETE_VAZIO__', implode(" AND ", $__DELETE_VAZIO__), $content);
    $content = str_replace('__DB_TABLE__', $__DB_TABLE__, $content);
    $content = str_replace('__TITLE_SINGLE__', $__TITLE_SINGLE__, $content);
    $content = str_replace('__TITLE__', $__TITLE__, $content);
    $content = str_replace('__SEARCH__', implode(", ", $__SEARCH__), $content);

    if (file_put_contents($file_index, $content)) {

        echo $file_index . ' Criado com sucesso<hr />';
    } else {

        echo $file_index . ' ERRO AO CRIAR ESSE ARQUIVO!!<hr />';
    }
}

$file_ajax = __DIR__ . '/../../system/_ajax/' . $__table__ . '.ajax.php';
if (!file_exists($file_ajax)) {

    $content = file_get_contents('_views/__table__/__table__.ajax.php');

    $content = str_replace('__DB_TABLE__', $__DB_TABLE__, $content);
    $content = str_replace('__TITLE_SINGLE__', $__TITLE_SINGLE__, $content);
    $content = str_replace('__TITLE__', $__TITLE__, $content);

    if (file_put_contents($file_ajax, $content)) {

        echo $file_ajax . ' Criado com sucesso<hr />';
    } else {

        echo $file_ajax . ' ERRO AO CRIAR ESSE ARQUIVO!!<hr />';
    }
}

foreach ($replaces as $replace){

    $file_replace = __DIR__.'/../../'.$replace['local'];

    $content = file_get_contents($file_replace);
    $content = str_replace('###__REPLACE__###', $replace['codigo'], $content);

    if (file_put_contents($file_replace, $content)) {

        echo $file_replace . ' Atualizado com sucesso<hr />';
    } else {

        echo $file_replace . ' ERRO AO ATUALIZAR ESSE ARQUIVO!!<hr />';
    }

}