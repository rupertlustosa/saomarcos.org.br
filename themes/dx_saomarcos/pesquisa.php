<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <?php
                    $uri = $_SERVER['REQUEST_URI'];
                    preg_match('/\?q=(.*)/', $uri, $matches);
                    $q = $matches[1];
                    ?>
                    <h2>BUSCA POR '<?= strtoupper($q) ?>'</h2>
                    <!--<h3 class="caption color-black-100 width50 centertable"> xx </h3>-->
                </div><!-- /.col-md-12 -->

                <div class="clear40"></div>

                <div class="container">

                    <div class="row">
                        <style type="text/css">
                            .gsc-webResult {
                                margin-bottom: 20px !important;
                                border-bottom: solid 1px #ccc !important;
                                padding: 10px !important;
                                padding-bottom: 20px !important;
                            }

                            .gsc-webResult:nth-child(odd) {
                                background-color: #f1f1f1;
                            }

                            .gsc-webResult a, .gsc-webResult a b {
                                color: #b10d1d !important;
                            }

                            .gsc-thumbnail-inside {
                                padding-left: 0px !important;
                            }

                            .gsc-url-top {
                                margin-bottom: 10px !important;
                                padding-left: 0px !important;
                            }

                            .gs-title {
                                text-align: left !important;
                                display: block;
                                color: #b10d1d !important;
                                text-decoration: none !important
                            }

                            .gs-title:hover {
                                text-decoration: underline;
                            }

                            .gsc-cursor-page {
                                width: 20px;
                                color: #b10d1d !important;
                                display: inline-block !important;
                                height: 20px;
                                text-align: center;
                                padding-top: 2px;
                                background-color: #fff !important;
                            }

                            .gsc-cursor-current-page {
                                background-color: #b10d1d !important;
                                color: #fff !important;
                            }


                        </style>
                        <gcse:searchresults-only></gcse:searchresults-only>
                    </div>

                </div><!-- /.container -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="clear20"></div>

    </section><!-- /.our-team -->

<?php include "inc/footer.php" ?>