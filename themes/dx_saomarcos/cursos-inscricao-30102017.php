<?php
include "inc/header.php";
$read = new Read();
$token = (isset($URL[1])) ? $URL[1] : null;
?>
    <style>
        .curso-info {
            width: 100%;
            height: 200px;
            overflow: auto;
            display: none;
            border: solid 1px #ccc;
            margin-bottom: 20px;
        }

        .curso-info .curso-content {
            padding: 20px;
        }
    </style>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="clear40"></div>
                    <h2>CURSOS</h2>
                </div><!-- /.col-md-12 -->
                <div class="clear30"></div>

                <div class="atomwrapper">
                    <form id="form_inscricao" enctype="multipart/form-data" style="text-align: left !important"
                          method="post">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="inscricao_curso_id">Curso</label>
                                <select id="inscricao_curso_id" name="inscricao_curso_id" class="form-control"
                                        onchange="cursoInfo(this.value)">
                                    <option val="0"></option>
                                    <?php

                                    $read->FullRead("SELECT curso_id,curso_title,curso_valor, curso_limite, curso_id, COUNT(distinct ci.inscricao_id) as total FROM dx_cursos c 
LEFT JOIN dx_cursos_inscricao ci ON c.curso_id = ci.inscricao_curso_id WHERE c.status = 1 GROUP BY c.curso_id ");
                                    foreach ($read->getResult() as $row) {
                                        $valor = ($row['curso_valor'] == 0.00) ? "Grátis" : "R$" . $row['curso_valor'];
                                        if ($row['total'] < $row['curso_limite']) {
                                            echo "<option value='{$row['curso_id']}'>{$row['curso_title']} </option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="curso-info">
                                <div class="curso-content"></div>
                            </div>
                        </div>

                        <div class="response col-md-12">
                            <div class="alert alert-warning " style="display:none" role="alert">Enviando ...</div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inscricao_cpf">CPF:</label>
                                <div class="clear5"></div>
                                <input class="form-control cpf " style="max-width: 200px;float: left;"
                                       name="inscricao_cpf" required="required">
                                <button type="button" class="btn btn-default" onclick="getUser()">Verificar</button>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inscricao_nome">Nome:</label>
                                <input class="form-control" id="nome" name="inscricao_nome" disabled="disabled"
                                       required="required">
                            </div>
                        </div>

                        <div class="col-md-3" id="comprovante" style="display:none">
                            <div class="form-group">
                                <label for="inscricao_nome">Comprovante:</label>
                                <input name="arquivo" type="file">
                                <small>O comprovante deve ser comprimido no formato zip</small>
                            </div>
                        </div>

                        <div class="clear5"></div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inscricao_email">E-Mail:</label>
                                <input class="form-control" id="email" name="inscricao_email" disabled="disabled"
                                       required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inscricao_fone ">Telefone:</label>
                                <input class="form-control phone" id="fone" name="inscricao_fone"
                                       disabled="disabled" required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inscricao_graduacao">Graduação:</label>
                                <select id="inscricao_graduacao" name="inscricao_graduacao" disabled="disabled"
                                        class="form-control" required="required">
                                    <option value=""></option>
                                    <option value="Graduando">Graduando</option>
                                    <option value="Graduado">Graduado</option>
                                    <option value="Residente">Residente</option>
                                    <option value="Pós-graduando">Pós-graduando</option>
                                    <option value="Pós-graduado">Pós-graduado</option>
                                </select>
                            </div>
                        </div>

                        <div class="clear5"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" checked="checked" value="1" name="inscricao_aceito">
                                        Desejo receber informações do Hospital São Marcos
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="clear5"></div>


                        <div class="col-md-offset-4 col-md-4">
                            <div class="form-group">
                                <input type="hidden" name="action" value="inscricao">
                                <button type="submit" class="btn btn-danger btn-large"
                                        style="width: 100%; display: table; margin: 0px auto;">ENVIAR
                                </button>
                            </div>
                        </div>

                    </form>
                    <script>
                        function cursoInfo(id) {
                            $.post("<?=BASE?>/ajax/curso.php", {
                                key_id: id,
                                action: 'listar-cursos'
                            }, function (response) {
                                result = response.result;
                                if (result.status == 'success') {
                                    $(".curso-info").fadeIn('fast');
                                    $(".curso-content").html(result.data.curso_content);
                                    if (result.data.curso_valor == 0.00) {
                                        $("#comprovante").hide();
                                    } else {
                                        $("#comprovante").show();
                                    }
                                } else {
                                    console.log("error");
                                }
                                console.log(result.status);
                            }, 'json');
                        }

                        function getUser() {
                            cpf = $('.cpf').val();
                            if (cpf.length > 10) {
                                $.post("<?=BASE?>/ajax/curso.php", {cpf: cpf, action: 'get-user'}, function (response) {
                                    result = response.result;
                                    $("#nome").removeAttr('disabled');
                                    $("#email").removeAttr('disabled');
                                    $("#fone").removeAttr('disabled');
                                    $("#inscricao_graduacao").removeAttr('disabled');


                                    if (result.status == 'success') {
                                        $("#nome").val(result.data.nome);
                                        $("#email").val(result.data.email);
                                        $("#fone").val(result.data.telefone);
                                        $('#inscricao_graduacao option[value=' + result.data.graduacao + ']').attr('selected', 'selected');

                                    } else {
                                        alert(result.data);
                                        $("#nome").focus();
                                    }
                                    console.log(result.data);
                                    console.log(result.status);
                                }, 'json');
                            } else {
                                alert("Digite seu CPF");
                            }
                        }

                        $(function () {
                            $(".phone").mask("(99) 9.9999-9999");
                            $(".cpf").mask("999.999.999-99");
                            $(".response").click(function () {
                                $(this).html('');
                            })
                            base = $('link[rel="base"]').attr('href');

                            $("#form_inscricao").submit(function () {
                                var form = $(this);
                                var form_this = this;

                                form.ajaxSubmit({
                                    url: base + "/ajax/curso.php",
                                    beforeSubmit: function () {
                                        enviando();
                                    },
                                    dataType: 'json',
                                    success: function (rsp) {
                                        console.log(rsp);
                                        console.log(rsp.msg);
                                        if (rsp.status == 'success') {
                                            $(".curso-content").html('');
                                            $(".curso-info").hide();
                                            $(".response .alert").removeClass("alert-warning");
                                            $(".response .alert").removeClass("alert-danger");
                                            $(".response .alert").addClass("alert-success");
                                            $(".response .alert").html(rsp.msg);
                                            //form_this.reset();
                                        } else {
                                            $(".response .alert").removeClass("alert-warning");
                                            $(".response .alert").addClass("alert-danger");
                                            $(".response .alert").html(rsp.msg);
                                        }
                                    }
                                })


                                return false;
                            })
                        })


                    </script>
                </div>
                <div class="clear40"></div>


                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="clear20"></div>

    </section><!-- /.our-team -->

<?php include "inc/footer.php" ?>