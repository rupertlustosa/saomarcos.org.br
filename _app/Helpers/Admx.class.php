<?php

class Admx
{

    var $TABLE;
    var $levelUserPost = 9;
    var $Read;
    var $Page;
    var $_paginator = 0;
    var $QntPage = 0;

    function __construct()
    {
        $Page = filter_input(INPUT_GET, "page");
        $this->Page = $Page;
    }

    public static function GetCampo($tabela, $campo, $cond)
    {
        $read = new Read();
        $array = array();
        $sql = "SELECT $campo FROM $tabela WHERE 1=1 AND " . $cond;
        $read->FullRead($sql);
        if ($read->getRowCount()):
            $r = $read->getResult();
            return $r[0][$campo];
        else:
            return null;
            //die;
        endif;
    }

    public static function BtnStatus($table, $key_name, $key_id, $value)
    {
        //echo '<button type="button" class="btn btn-success btn-xs">Success</button>';
        if ($value == 0) {
            echo '<button title="Ativar" '
                . 'table="' . $table . '"  '
                . 'key_name="' . $key_name . '" '
                . 'key_id="' . $key_id . '" '
                . 'value="1" '
                . 'class="actions_status btn btn-default btn-xs"><i class="fa fa-exclamation-triangle"></i> Ativar</button>';
        } else {
            echo '<button title="Desativar" '
                . 'table="' . $table . '"  '
                . 'key_name="' . $key_name . '" '
                . 'key_id="' . $key_id . '" '
                . 'value="0" '
                . 'class="actions_status btn btn-primary btn-xs"><i class="fa fa-check"></i></button>';
        }
    }

    public static function BtnTrash($table, $key_name, $key_id, $parent, $value)
    {
        echo '<button title="Enviar para lixeira" '
            . 'table="' . $table . '"  '
            . 'key_name="' . $key_name . '" '
            . 'key_id="' . $key_id . '" '
            . 'value="' . $value . '" '
            . 'parent = "' . $parent . '"'
            . 'class="actions_trash btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Lixeira</button>';
    }

    public static function BtnDelete($table, $key_name, $key_id, $parent, $formToken)
    {
        echo '<button title="Enviar para lixeira" '
            . 'table="' . $table . '"  '
            . 'key_name="' . $key_name . '" '
            . 'key_id="' . $key_id . '" '
            . 'parent = "' . $parent . '"'
            . 'formToken = "' . $formToken . '"'
            . 'class="actions_delete btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Deletar</button>';
    }

    public static function getAutorPost($post_user_id)
    {
        $read = new Read();
        $array = array();
        $read->FullRead("SELECT user_id,user_name,user_email FROM " . DB_USERS . " WHERE user_id=:usr  ", "usr={$post_user_id}");
        if ($read->getRowCount()):
            $r = $read->getResult();
            return $r[0];
        else:
            return null;
        endif;
    }

    public static function Trash($trash = false)
    {
        if ($trash == false) {
            return " AND trash = 0 ";
        } else {
            return " AND trash = 1 ";
        }
    }

    public static function Search($campos)
    {
        $getSearch = filter_input(INPUT_GET, 's', FILTER_DEFAULT);
        if (!empty($getSearch)) {
            $where = " AND ( ";
            $i = 0;
            foreach ($campos as $c) {
                $i++;
                $where .= "{$c} LIKE '%{$getSearch}%' " . ($i == count($campos) || count($campos) == 1 ? "  " : " or ");
            }
            $where .= " ) ";
            return $where;
        } else {
            return null;
        }
    }

    public static function formSearch($view)
    {
        $urlSearch = ADM_URL . "?dx=" . $view . "/home";
        echo '<input type="text" name="s" onKeyPress="enterpressSearch(event)" id="campo_search_top" class="form-control" placeholder="Buscar por...">
            <span class="input-group-btn ">
                <button rel="' . $urlSearch . '" class="btn btn-default btn_search_top" type="button">Ok!</button>
            </span>';
    }

    public static function setFormToken($print = true)
    {
        $token = md5(time());
        $_SESSION['formToken'] = $token;
        if ($print == false) {
            return $token;
        } else {
            echo '<input name="formToken" type="hidden" value="' . $token . '"/>';
        }
    }

    public static function getFormToken()
    {
        if (isset($_SESSION['formToken'])) {
            return $_SESSION['formToken'];
        }
    }

    public static function getResult($table, $where, $order)
    {
        $read = new Read();
        $orderby = (!empty($order)) ? " ORDER BY " . $order : null;
        $where = (!empty($where)) ? " AND " . $where : null;
        $sql = "SELECT * FROM {$table} WHERE 1=1 {$where} {$orderby} ";
        $read->FullRead($sql);
        return $read->getResult();
    }

    /*
     * @params:
     * key => Retorna o nome dos campos como chave;
     * val => Retorna o nome dos campos como valor;
     */

    public static function getResultFields($table, $return = 'key')
    {
        $read = new Read();
        $read->FullRead("SHOW COLUMNS FROM " . $table);
        $fields = array();
        $result = array();
        foreach ($read->getResult() as $f) {
            $fields[] = $f['Field'];
        }
        if ($return == 'key') {
            foreach ($fields as $key => $val) {
                $result[$val] = null;
            }
            return $result;
        } else {
            return $fields;
        }
    }

    public static function setMetas($table, $parent_id, $textarea = true)
    {
        $ReadMetas = new Read();
        $ReadMetas->ExeRead("dx_campos", " WHERE campo_table = :tab AND campo_parent_id = :ParentId ", "tab={$table}&ParentId={$parent_id}");
        foreach ($ReadMetas->getResult() as $meta) {
            echo '<div class="col-md-4">
                <div class="form-group">
                    <label>' . $meta['campo_name'] . '</label>
                    ';
            if ($textarea):
                echo '<textarea class="form-control" name="' . $meta['campo_key'] . '">' . $meta['campo_value'] . '</textarea>';
            else:
                echo '<input class="form-control" type="text" name="' . $meta['campo_key'] . '" value="' . $meta['campo_value'] . '" >';
            endif;
            echo '</div>
            </div>';
        }
    }

    public static function getMeta($table, $parent_id, $meta_key, $print = false)
    {
        $ReadMetas = new Read();
        $ReadMetas->ExeRead("dx_campos", " WHERE campo_table = :tab AND campo_parent_id = :ParentId AND campo_key =:key ", "tab={$table}&ParentId={$parent_id}&key={$meta_key}");
        if ($ReadMetas->getResult()):
            $meta = $ReadMetas->getResult()[0]['campo_value'];
            if ($print) {
                echo $meta;
            } else {
                return $meta;
            }
        endif;
    }

    public static function encripta($string)
    {
        $hash = "hsm@17";
        return md5($hash . $string);
    }

}
