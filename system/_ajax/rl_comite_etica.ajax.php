<?php
require '../../_app/autoload.php';

usleep(40000);

$json = null;
$json['error'] = null;

$TABLE = getTableList(DB_COMITE_ETICA);

$dadosEnviados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$chavePrimaria = $dadosEnviados['key_id'];
$rascunho = (!empty($dadosEnviados['rascunho'])) ? $dadosEnviados['rascunho'] : null;

$file = null;

$callback = $TABLE['CALLBACK'];
$formToken = Admx::getFormToken();

//VALIDA AÇÃO
if ($dadosEnviados && $dadosEnviados['callback_action'] && $dadosEnviados['callback'] = $callback && $dadosEnviados['formToken'] == $formToken) {

    $callback_action = $dadosEnviados['callback_action'];

    if (!empty($_FILES['image'])) {

        $file = $_FILES['image'];
    }
    unset($dadosEnviados['callback'], $dadosEnviados['formToken'], $dadosEnviados['callback_action'], $dadosEnviados['key_id'], $dadosEnviados['image'], $dadosEnviados['rascunho']);


    $read = new Read;
    $create = new Create;
    $update = new Update;
    $delete = new Delete;
    $upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($callback_action) {
        case "manager":
            $read->ExeRead(DB_COMITE_ETICA, "WHERE id = :id", "id={$chavePrimaria}");
            if (!$read->getResult()) {


            } else {

                if (!empty($file)) {

                    $name = !empty($dadosEnviados['nome']) ? Check::Name($dadosEnviados['nome']) : $chavePrimaria;

                    $upload->Image($file, "{$name}-" . time(), 600);

                    if ($upload->getResult()) {

                        $image = $upload->getResult();
                        $dadosEnviados['image'] = $image;
                        $json['image'] = ["src" => "../tim.php?src=uploads/{$image}&w=600"];
                    }
                }

                $dadosEnviados['status'] = ($rascunho) ? 0 : 1;

                $update->ExeUpdate($TABLE['DB'], $dadosEnviados, "WHERE id = :id", "id={$chavePrimaria}");
                $json["success"] = ["title" => "Sucesso", "text" => "O registro oi atualizado com sucesso!", "type" => "info", "style" => "dark"];

            }
            break;

    }

    if (isset($json)) {

        echo json_encode($json);
    }

} else {
    die('<center><h1>Permissão Negada!</h1></center>');
}
