<?php
$subpage = empty($URL[1]) ? 'login' : $URL[1];
if ($subpage == 'login') {
    unset($_SESSION['user']);
} else {
    if ($subpage != 'recuperar' && $subpage != 'cadastre-se') {
        if (!Users::CheckLoginAutor()) {
            header("location: " . BASE . "/{$URL[0]}/login");
        }
    }
}


include "inc/header.php";
?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>REVISTA CIENTÍFICA </h2>

            </div><!-- /.col-md-12 -->
            <div class="clear30"></div>

            <div class="atomwrapper" style="width:100%">
                <div class="row" style="margin: 0px; padding: 0px; text-align:left">
                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <style>
                                .nav > li.disabled > a a {
                                    color: #b10d1d
                                }
                            </style>
                            <ul class="nav nav-pills">
                                <?php
                                if ($subpage != 'login' && $subpage != 'recuperar' && $subpage != 'cadastre-se') {
                                    ?>
                                    <li <?= ($subpage == 'perfil') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/perfil" ?>">Perfil</a></li>
                                    <li <?= ($subpage == 'meus-artigos') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/meus-artigos" ?>">Meus artigos</a>
                                    </li>
                                    <li <?= ($subpage == 'enviar-artigo') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/enviar-artigo" ?>">Enviar artigo</a>
                                    </li>
                                    <li><a href="<?= BASE . "/" . $URL[0] ?>">Sair</a></li>
                                    <?php
                                } else {
                                    ?>
                                    <li <?= ($subpage == 'login') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/login" ?>">Login</a></li>
                                    <li <?= ($subpage == 'cadastre-se') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/cadastre-se" ?>">Cadastre-se</a></li>
                                    <li <?= ($subpage == 'recuperar') ? 'class="disabled"' : '' ?> ><a
                                                href="<?= BASE . "/" . $URL[0] . "/recuperar" ?>">Recuperar Senha</a>
                                    </li>

                                    <?php
                                }
                                ?>
                            </ul>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12" style="padding:0px 10px">
                                    <?php
                                    include "revista/{$subpage}.php";
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>

</section><!-- /.our-team -->
<script>
    $(function () {
        $(".cpf").mask("999.999.999-99");
        $(".cel").mask("(99) 9.9999-9999");
        $(".fone").mask("(99) 9999-9999");
    })
</script>
<?php include "inc/footer.php" ?>
