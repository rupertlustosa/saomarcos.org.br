<?php

class Anti
{
    static function String($str)
    {
        $str = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '', $str);
        $str = trim($str);//limpa espaços vazio
        $str = strip_tags($str);//tira tags html e php
        $str = addslashes($str);//Adiciona barras invertidas a uma string
        return $str;
    }
}

?>