<div class="x_panel">
    <div class="x_title">
        <h2>Campos:</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form class="form-horizontal form-label-left auto_save" method="post">
            <?php Admx::setMetas($TABLE['DB'], $KeyId, $metaTextArea) ?>

            <input type="hidden" name="callback" value="Actions">
            <input type="hidden" name="callback_action" value="campos">
            <input type="hidden" name="table" value="<?= $TABLE['DB'] ?>">
            <input type="hidden" name="key_name" value="<?= $TABLE['KEY_NAME'] ?>">
            <input type="hidden" name="value" value="0">
            <input type="hidden" name="key_id" value="<?= $KeyId ?>">
        </form>
    </div>
</div>
