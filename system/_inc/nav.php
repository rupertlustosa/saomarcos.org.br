<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title">
            <a href="<?= ADM_URL ?>" class="site_title">
                <i class="fa fa-code" aria-hidden="true" style="border:none"></i> <span><?= ADM_NAME ?></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="../tim.php?src=<?= Users::GetUserImage($user_id) ?>&w=100&h=100"
                     alt="<?= Session::getUserName() ?>" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Bem-vindo,</span>
                <h2><?= Session::getUserName() ?></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <div class="clearfix"></div>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3 style="margin-top: 20px;">MENU</h3>
                <ul class="nav side-menu">


                    <li class=" <?= (empty($view)) ? 'current_section active' : '' ?>">
                        <a href="<?= ADM_URL ?>/"><i class="fa fa-home"></i> Home </a>
                    </li>

                    <?php if (Session::checkPermission(1)): ?>
                        <li class="<?= (!empty($view) && $view == 'users') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-users"></i> Usuários <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=users/home">Todos os Usuários</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=users/create">Adicionar Usuários</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(2)): ?>
                        <li class="<?= (!empty($view) && $view == 'posts') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=posts/home">Todos os Posts</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=posts/create">Adicionar Posts</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(18)): ?>
                        <li class="<?= (!empty($view) && ($view == 'cursos' || $view == 'tipos-cursos' || $view == 'corpo-docente')) ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-envira"></i> Cursos <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">

                                <li><a href="<?= ADM_URL ?>/?dx=tipos-cursos/home">Tipos de curso</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=tipos-cursos/create">Adicionar tipo de curso</a></li>

                                <li><a href="<?= ADM_URL ?>/?dx=corpo-docente/home">Corpo docente</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=corpo-docente/create">Adicionar corpo docente</a></li>

                                <li><a href="<?= ADM_URL ?>/?dx=cursos/home">Todos os cursos</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=cursos/create">Adicionar cursos</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(8)): ?>
                        <li class="<?= (!empty($view) && $view == 'especialidades') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-heart"></i> Especialidades <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=especialidades/home">Todas as Especialidades</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=especialidades/create">Adicionar Especialidade</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(13)): ?>
                        <li class="<?= (!empty($view) && $view == 'clinicas') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-medkit" aria-hidden="true"></i> Clinicas <span
                                        class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=clinicas/home">Todas as Clinicas</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=clinicas/create">Adicionar Clinica</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <!--
                    <li class="<?= (!empty($view) && $view == 'profissionais') ? 'current_section active' : '' ?>" >
                        <a><i class="fa fa-user-md" aria-hidden="true"></i> Profissionais <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= ADM_URL ?>/?dx=profissionais/home">Todos os Profissionais</a></li>
                            <li><a href="<?= ADM_URL ?>/?dx=profissionais/create">Adicionar Profissional</a></li>
                        </ul>
                    </li>
                    -->

                    <?php if (Session::checkPermission(11)): ?>
                        <li class="<?= (!empty($view) && $view == 'diretores') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-universal-access" aria-hidden="true"></i> Diretores <span
                                        class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=diretores/home">Todos os Diretores</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=diretores/create">Adicionar Diretoria</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(11)): ?>
                        <li class="<?= (!empty($view) && $view == 'convenios') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-retweet" aria-hidden="true"></i> Convênios <span
                                        class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=convenios/home">Todos os Convênios</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=convenios/create">Adicionar Convênio</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>


                    <!--
                    <li class="<?= (!empty($view) && $view == 'menus') ? 'current_section active' : '' ?>" >
                        <a href="<?= ADM_URL ?>/?dx=menus/home"><i class="fa fa-cogs"></i> Menus </a>
                    </li>
                    -->

                    <?php if (Session::checkPermission(6)): ?>
                        <li class="<?= (!empty($view) && $view == 'banners') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-exchange"></i> Banners <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=banners/home">Todos os Banners</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=banners/create">Adicionar Banner</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(15)): ?>
                        <li class="<?= (!empty($view) && $view == 'videos') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-youtube-play"></i> Vídeos <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=videos/home">Todos os Vídeos</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=videos/create">Adicionar Vídeo</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(18)): ?>
                        <li class="<?= (!empty($view) && $view == 'trabalhe-conosco') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-envira"></i> Trabalhe conosco <span
                                        class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=trabalhe-conosco/home">Todas as vagas</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=trabalhe-conosco/create">Adicionar vaga</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=trabalhe-conosco/inscricoes-avulsas">Currículos
                                        avulsos</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <!--
                    <?php if (Session::checkPermission(9)): ?>
                    <li class="<?= (!empty($view) && $view == 'manuais') ? 'current_section active' : '' ?>" >
                        <a><i class="fa fa-cloud-download"></i> Manuais/Artigos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= ADM_URL ?>/?dx=manuais/home">Todos os Manuais</a></li>
                            <li><a href="<?= ADM_URL ?>/?dx=manuais/create">Adicionar Manual</a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                  -->

                    <?php if (Session::checkPermission(5)): ?>
                        <li class="<?= (!empty($view) && $view == 'pages') ? 'current_section active' : '' ?>">
                            <a><i class="fa fa-sticky-note"></i> Páginas <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=pages/home">Todas as Páginas</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=pages/create">Adicionar Página</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(16)) { ?>
                        <li class="<?= (!empty($view) && $view == 'sociais') ? 'current_section active' : '' ?>">
                            <a>
                                <i class="fa fa-sticky-note"></i> Responsabilidade Social
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=sociais/home">Listar Responsabilidade Social</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=sociais/create">Adicionar Responsabilidade Social</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if (Session::checkPermission(17)): ?>
                        <li class="<?= (!empty($view) && $view == 'config') ? 'current_section active' : '' ?>">
                            <a href="<?= ADM_URL ?>/?dx=revista/create" title="Revista Médica"><i
                                        class="fa fa-wrench"></i> Revista Médica </a>
                        </li>
                    <?php endif;
                    if (Session::checkPermission(25)) { ?>
                        <li class="<?= (!empty($view) && $view == 'DB_REVISTA_EDICOES') ? 'current_section active' : '' ?>">
                            <a>
                                <i class="fa fa-sticky-note"></i> Edições das revistas
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=<?= DB_REVISTA_EDICOES ?>/home">Listar Edições das
                                        revistas</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=<?= DB_REVISTA_EDICOES ?>/create">Adicionar Edições das
                                        revistas</a>
                                </li>
                            </ul>
                        </li>
                    <?php }

                    if (Session::checkPermission(22)): ?>
                        <li class="<?= (!empty($view) && $view == 'config') ? 'current_section active' : '' ?>">
                            <a href="<?= ADM_URL ?>/?dx=artigos/home" title="Artigos"><i
                                        class="fa fa-edit"></i> Artigos </a>
                        </li>
                    <?php endif; ?>

                    <?php if (Session::checkPermission(7)): ?>
                        <li class="<?= (!empty($view) && $view == 'config') ? 'current_section active' : '' ?>">
                            <a href="<?= ADM_URL ?>/?dx=config/home" title="Configurações do site"><i
                                        class="fa fa-wrench"></i> Configurações </a>
                        </li>
                    <?php endif;

                    if (Session::checkPermission(23)) { ?>
                        <li class="<?= (!empty($view) && $view == 'DB_ANAIS') ? 'current_section active' : '' ?>">
                            <a>
                                <i class="fa fa-sticky-note"></i> Anais
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu">
                                <li><a href="<?= ADM_URL ?>/?dx=<?= DB_ANAIS ?>/home">Listar Anais</a></li>
                                <li><a href="<?= ADM_URL ?>/?dx=<?= DB_ANAIS ?>/create">Adicionar Anais</a>
                                </li>
                            </ul>
                        </li>
                    <?php }

                    if (Session::checkPermission(24)) { ?>
                        <li class="<?= (!empty($view) && $view == 'DB_COMITE_ETICA') ? 'current_section active' : '' ?>">
                            <a href="<?= ADM_URL ?>/?dx=<?= DB_COMITE_ETICA ?>/create&id=1">
                                <i class="fa fa-sticky-note"></i> Comitê de ética
                            </a>
                        </li>
                    <?php }

                    ###__REPLACE__###


                    ?>


                    <!--
                    <li class="<?= (!empty($view) && $view == 'categorias') ? 'current_section active' : '' ?>" >
                        <a><i class="fa fa-clone"></i> Categorias <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?= ADM_URL ?>/?dx=categorias/home">Todos as Categorias</a></li>
                            <li><a href="<?= ADM_URL ?>/?dx=categorias/create">Adicionar Categorias</a></li>
                        </ul>
                    </li>
                    -->

                </ul>
            </div>


        </div>
        <!-- /sidebar menu -->

    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <img src="../tim.php?src=<?= Users::GetUserImage($user_id) ?>&w=100&h=100"
                             alt="<?= Session::getUserName() ?>"><?= Session::getUserName() ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="<?= ADM_URL . "/?dx=users/create&id={$user_id}" ?>"> Profile</a></li>
                        <li><a href="<?= ADM_URL ?>/logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
