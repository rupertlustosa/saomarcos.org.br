<?php

require '../_app/autoload.php';

$admx = new Admx;

$getExe = filter_input(INPUT_GET, "dx");
$exe = explode("/", $getExe);
$view = (!empty($exe[0])) ? $exe[0] : null;
$front = new Front();
$TABLE = getTableList($view);
$getId = filter_input(INPUT_GET, "id");
if (!empty($exe[1])) {
    $parans = explode("&", $exe[1]);
    $file = (!empty($parans[0])) ? $parans[0] : null;
} else {
    $file = null;
}

if (!Session::chekLogin()) {
    include_once './login.php';
} else {


    $user_id = Session::getUserId();

    $read = new Read();

    if ($view) {
        if ($view != 'login') {
            $include = "./_views/" . $view . "/" . $view . "_" . $file . ".php";
            if (file_exists($include)) {
                if (Session::checkPermission($TABLE['KEY'])) {
                    include_once "$include";
                } else {

                    if ($view == 'users') {
                        if ($getId == Session::getUserId()) {
                            include_once "$include";
                        } else {
                            include_once '';
                        }
                    } else {
                        include_once './_views/error/403.php';
                    }

                }

            } else {
                include_once './_views/error/404.php';
            }
        } else {
            include_once './login.php';
        }
    } else {
        include_once './dashboard.php';
    }
}
