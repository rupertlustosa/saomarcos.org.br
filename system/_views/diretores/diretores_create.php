<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['status'] = 0;
    $create->ExeCreate(DB_DIRETORES, $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=diretores/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM " . DB_DIRETORES . " WHERE diretoria_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/diretores/diretores.js');

include '_inc/header.php';
include '_inc/nav.php';
?>


    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <a title="Criar <?= $TABLE['TITLE_SINGLE'] ?>"
                           href="<?= ADM_URL ?>/?dx=<?= $TABLE['VIEW'] ?>/create" class="btn btn-info "> <i
                                    class="fa fa-plus"></i></a>
                        <?= $TABLE['TITLE_SINGLE'] ?>
                        <small> <?= $Post['diretoria_title'] ?></small>
                    </h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                <?php Admx::setFormToken() ?>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group title">
                                                <label>Título </label>
                                                <input type="text" name="diretoria_title"
                                                       value="<?= $Post['diretoria_title'] ?>"
                                                       placeholder="Digite o nome" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Cargo</label>
                                                <input type="text" name="diretoria_cargo"
                                                       value="<?= $Post['diretoria_cargo'] ?>"
                                                       placeholder="Digite o cargo" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>Posição</label>
                                                <input type="text" name="diretoria_posicao"
                                                       value="<?= $Post['diretoria_posicao'] ?>"
                                                       placeholder="Digite a posição" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descrição</label>
                                                <textarea class="dx_editor"
                                                          name="diretoria_content"><?= $Post['diretoria_content'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-4 sidebar" style="padding-left:20px">
                                    <div style="overflow: auto; max-height: 400px;">
                                        <?php
                                        if (file_exists("../uploads/" . $Post['diretoria_image']) && !is_dir("../uploads/" . $Post['diretoria_image'])) {
                                            echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $Post['diretoria_image'] . '&w=350&h=350" />';
                                        } else {
                                            echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=350&h=350" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                        }
                                        ?>

                                    </div>
                                    <div class="upload_file"><input class="loadimage" type="file" name="image"/></div>
                                    <div class="clear20"></div>
                                    <div class="ln_solid"></div>
                                    <div class="clear25"></div>
                                    <div class="form-group">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                            </label>
                                        </div>
                                        <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                        <input type="hidden" name="callback_action" value="manager">
                                        <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                        <img src="_img/load.gif" class="loading_form pull-right">

                                        <button class="btn btn-success pull-right" type="submit">
                                            Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                    </div>

                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div><!-- content do formulario -->
                    </div><!-- painel do formulario -->
                    <div class="clear20"></div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php
include './_inc/footer.php';
?>