<?php
$TABLE = getTableList($view);
$TITLE = $TABLE['TITLE'];
$VIEW = $TABLE['VIEW'];
$CALLBACK = $TABLE['CALLBACK'];

if (empty($Read)) {
    $Read = new Read;
}

$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');


include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE'] ?></h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Gerenciar <?= $TABLE['TITLE'] ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <!-- start list -->
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>

                                <div class="clearfix"></div>

                                <?php

                                $Read->ExeRead(DB_CATEGORIAS, " ORDER BY categoria_status DESC, categoria_title ");
                                if (!$Read->getResult()):
                                    echo '<div role="alert" class="alert alert-warning alert-dismissible fade in">
                                        <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span>
                                        </button>
                                        Nenhum registro foi encontrado
                                    </div>';
                                else:
                                    foreach ($Read->getResult() as $post):
                                        $parent = (!empty($post['categoria_parent'])) ? Check::GetCampo(DB_CATEGORIAS, "categoria_title", "categoria_title") : null;
                                        ?>
                                        <div class="col-md-3 col-sm-3 col-xs-12 profile_details default_single <?= ($post['categoria_status'] == 0) ? 'rascunho' : '' ?>"
                                             rel="<?= $post['categoria_id'] ?>">
                                            <div class="well profile_view col-md-12">
                                                <div class="col-md-12">
                                                    <?= ($parent) ? '<h4 class="brief"><i>' . $parent . '</i></h4>' : '' ?>
                                                    <div class="left col-xs-12">
                                                        <h2><?= $post['categoria_title'] ?></h2>
                                                    </div>

                                                </div>
                                                <div class="col-md-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-12 emphasis">
                                                        <?php
                                                        if ($post['categoria_status'] == 0) {
                                                            echo '<button title="Ativar" callback="' . $CALLBACK . '" rel="default_single" callback_action="status"  rel="1" class="status btn btn-default btn-xs"><i class="fa fa-exclamation-triangle"></i> Ativar</button>';
                                                        } else {
                                                            echo '<button title="desativar" callback="' . $CALLBACK . '" rel="default_single"  callback_action="status"  rel="0" class="status btn btn-primary btn-xs"><i class="fa fa-check"></i></button>';
                                                        }
                                                        ?>
                                                        <a href="<?= ADM_URL ?>/?dx=<?= $VIEW ?>/create&id=<?= $post['categoria_id'] ?>"
                                                           class="btn btn-success btn-xs ">
                                                            <i class="fa fa-edit"></i> Editar
                                                        </a>
                                                        <button type="button" callback="<?= $VIEW ?>"
                                                                callback_action="delete"
                                                                class="delete btn btn-danger btn-xs"
                                                                rel="default_single"><i class="fa fa-trash"></i> Excluir
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <!-- end project list -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php
$front->set_js(ADM_FILE . '/_js/admx.js');
include './_inc/footer.php';
