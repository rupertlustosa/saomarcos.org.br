<?php

$jSON = null;
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$msg = null;
$cpf = $PostData['login_cpf'];

if ($PostData) {
    $senha = Admx::encripta($PostData['login_senha']);
    $Read->FullRead("SELECT id FROM dx_clientes WHERE cpf=:cpf AND senha =:pwd ", "cpf={$cpf}&pwd={$senha}");
    if ($Read->getResult()) {
        $id = $Read->getResult()[0]['id'];
        $time = time();
        $token = md5($time);
        $Update = new Update;
        $data['token'] = $token;
        $data['time'] = $time;
        $Update->ExeUpdate("dx_clientes", $data, "WHERE id = :id", "id={$id}");
        $_SESSION['user'] = $token;
        $msg = '<div class="alert alert-success " role="alert">Login realizado com sucesso!</div>
        <script>window.location.href="' . BASE . '/' . $URL[0] . '/perfil"</script>';
    } else {
        $msg = '<div class="alert alert-danger " role="alert">Dados inválidos!</div>';
    }
}

?>


<div class="clear20"></div>

<div class="well" style="max-width:300px; margin:0px auto;">
    <form id="envia_artigo" method="post">
        <div style="display:table; width:100%; text-align:left;">
            <div class="response">
                <?php
                if ($msg) {
                    echo $msg;
                }
                ?>
            </div>
            <div class="form-group">
                <label for="cpf">CPF</label>
                <input type="text" name="login_cpf" class="form-control cpf" id="cpf" placeholder="CPF">
            </div>
            <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" name="login_senha" class="form-control" id="senha" placeholder="Senha">
            </div>
            <button type="submit" class="btn btn-default">Logar</button>
            <div class="clear"></div>
            <hr>
            <div class="clear"></div>
            <a href="<?= BASE . "/{$URL[0]}/cadastre-se" ?>" class="pull-left">Cadastre-se</a><a
                    href="<?= BASE . "/{$URL[0]}/recuperar" ?>" class="pull-right">Recuperar senha</a>
            <div class="clear10"></div>
        </div>
    </form>
</div>
<div class="clear20"></div>