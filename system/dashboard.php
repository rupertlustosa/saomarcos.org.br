<?php
if (empty($Read)) {
    $Read = new Read;
}

$front = new Front();
$front->set_css(CDN_FILE . '/css/folha.css');

include '_inc/header.php';
include '_inc/nav.php';
?>

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dashboard</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div data-example-id="simple-jumbotron" class="bs-example">
                                <div class="jumbotron">
                                    <h1>Bem vindo!</h1>
                                    <p>Olá, <?= Session::getUserName() ?>! Gerencie as sessões do site usando o CMS
                                        Admx. </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!-- helow -->
            </div><!-- row -->

        </div>
    </div>
    <!-- /page content -->

<?php
$front->set_js(ADM_FILE . '/_js/admx.js');
include './_inc/footer.php';
?>