<?php

require '../../_app/autoload.php';

usleep(50000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$CallBack = 'Login';
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):


    $Case = $PostData['callback_action'];
    unset($PostData['callback'], $PostData['callback_action']);


    $Read = new Read;

    //SELECIONA AÇÃO
    switch ($Case):
        case 'logar':
            $Read->FullRead("SELECT user_id FROM " . DB_USERS . " WHERE user_login = :login", "login={$PostData['user_login']}");

            if (!$Read->getResult()):
                $jSON['error'] = "{$PostData['user_login']} inválido!";
                $jSON['success'] = false;
            else:
                $PostData['user_password'] = md5($PostData['user_password'] . HASH_KEY);
                $Read->ExeRead(DB_USERS, "WHERE user_login = :login AND user_password = :pass ", "login={$PostData['user_login']}&pass={$PostData['user_password']}");
                if (!$Read->getResult()):
                    $jSON['error'] = '<b>ERRO:</b> Senha inválida!';
                    $jSON['success'] = false;
                else:
                    if ($Read->getResult()[0]['user_status'] == 1):
                        Session::Login($Read->getResult()[0]);
                        $jSON['success'] = 1;
                    else:
                        Session::Logount();
                        $jSON['error'] = "<b>ERRO:</b> Usuário inátivo!";
                        $jSON['success'] = false;
                    endif;

                endif;
            endif;
            break;
    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    }
endif;
