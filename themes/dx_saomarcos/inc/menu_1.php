<nav class="navbar navbar-default nomargin nopadding">
    <div class="container-fluid">
        <div class="atomwrapper nopadding">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<?= BASE ?>">HOME <span class="sr-only">(atual)</span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down"
                                                               aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="<?= BASE ?>/apcc">SOBRE O HOSPITAL</a></li>
                            <li><a href="<?= BASE ?>/localizacao">LOCALIZAÇÃO</a></li>
                            <li><a href="<?= BASE ?>/diretoria">DIRETORIA</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">ESPECIALIDADES <i class="fa fa-angle-double-down"
                                                                   aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <?php
                            foreach (getEspecialidadesSecao() as $sec) {
                                echo '<li><a href="' . BASE . '/especialidades/' . $sec[1] . '">' . $sec[0] . '</a></li>';
                            }
                            ?>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">PACIENTES E VISITANTES <i class="fa fa-angle-double-down"
                                                                           aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="<?= BASE ?>/convenios">Convênios do Hospital</a></li>
                            <li><a href="<?= BASE ?>/apcc">ENCONTRE SEU MÉDICO</a></li>
                            <li><a href="<?= BASE ?>/apcc">RESULTADOS DE EXAMES</a></li>
                            <li><a href="<?= BASE ?>/apcc">ACOMODAÇÕES</a></li>
                            <li><a href="<?= BASE ?>/apcc">AGENDE SUA CONSULTA</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">SERVIÇOS <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="<?= BASE ?>/apcc">APPEM</a></li>
                            <li><a href="<?= BASE ?>/apcc">FISIOTERAPIA</a></li>
                            <li><a href="<?= BASE ?>/apcc">LAR DE MARIA</a></li>
                            <li><a href="<?= BASE ?>/apcc">NADIPO</a></li>
                            <li><a href="<?= BASE ?>/apcc">NUTTEM</a></li>
                            <li><a href="<?= BASE ?>/apcc">PSICOLOGIA</a></li>
                            <li><a href="<?= BASE ?>/apcc">SERVIÇO SOCIAL</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">PROFISSIONAIS DE SAÚDE <i class="fa fa-angle-double-down"
                                                                           aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">

                            <li><a href="http://ojs.saomarcos.org.br/ojs/index.php/cientifica" target="_blank">Revista
                                    Multiprofissional</a></li>
                            <li><a href="http://www.saomarcos.org.br/web/xhtml/artigo/inicio.xhtml" target="_blank">Artigo
                                    de Revista</a></li>
                            <li role="separator" class="divider"></li>
                            <?php
                            foreach (getManuaisCat() as $key => $val) {
                                echo "<li ><a href='" . BASE . "/manuais/{$key}' title='{$val}'>{$val}</a></li>";
                            }
                            ?>

                        </ul>
                    </li>
                    <li><a href="<?= BASE ?>/noticias">NOTÍCIAS </a></li>
                    <li><a href="<?= BASE ?>/contato">FALE CONOSCO</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
