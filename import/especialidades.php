<?php 
include "../_app/autoload.php";
?>
<style type="text/css">
	pre{background-color: #ccc; padding: 20px; margin-bottom: 30px;}
</style>
<?php 

$link = "../MATERIAS/especialidades.xml"; //link do arquivo xml
$xml = simplexml_load_file($link)->Worksheet->Table;
$table = $xml = simplexml_load_file($link)->Worksheet->Table;

$print = $table->Row->Cell;


$i=0;
foreach ($table->Row as $row) {
	if($i >= 1){
		$cell = $row->Cell;
		$id =  (int) $cell[0]->Data;
		$title = trim((string) $cell[1]->Data);
                $name = Check::Name($title);
                $post['especialidade_id'] = $id;
                $post['especialidade_secao'] = 1;
                $post['especialidade_title'] = $title;
                $post['especialidade_name'] = $name;
                $post['status'] = 1;
		$sql = inserir($post, 'dx_especialidades').";";
		echo "$sql \n";		
	}
	
	$i++;
}

function inserir($post,$tabela,$encode = false){

	$campos = array_keys($post);
	$sql = "insert into $tabela (";
	for($i=0;$i<count($campos);$i++){
		if($i!=count($campos)-1)
			$sql = $sql.$campos[$i].", ";
		else
			$sql = $sql.$campos[$i];
	}
	$sql = $sql.") values (";
	for($i=0;$i<count($campos);$i++){
		if($encode)
			$post[$campos[$i]] = utf8_decode($post[$campos[$i]]);
		if($i!=count($campos)-1)
			$sql = $sql."'".($post[$campos[$i]])."', ";
		else
			$sql = $sql."'".($post[$campos[$i]])."'";
	}
	$sql = $sql.")";
	return $sql;
}
?>