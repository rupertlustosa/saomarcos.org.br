<?php

$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {

    $create = new Create();
    $dados['nome'] = '';
    $dados['url'] = '';
    $dados['image'] = '';
    $dados['data_lancamento'] = '';
    $dados['trash'] = 0;
    $dados['status'] = 0;
    $create->ExeCreate(DB_REVISTA_EDICOES, $dados);

    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=".DB_REVISTA_EDICOES."/create&id=" . $newId);
} else {

    $Read->FullRead("SELECT * FROM " . DB_REVISTA_EDICOES . " WHERE id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {

        $itemDb = $Read->getResult()[0];

    } else {

        die('REGISTRO NÃO ENCONTRADO!');
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/banners/banners.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>

                            <div class="col-md-8">

                                <div>
                                    <?php
                                    if (file_exists("../uploads/" . $itemDb['image']) && !is_dir("../uploads/" . $itemDb['image'])) {
                                        echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $itemDb['image'] . '&w=400" />';
                                    } else {
                                        echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=400&h=190" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                    }
                                    ?>

                                </div>
                                <div class="clear10"></div>

                                <div class="form-group">
                                    <label>Nome</label>
                                    <input type="text" name="nome" id="nome" value="<?= $itemDb['nome'] ?>"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Imagem</label>
                                    <input type="file" name="image" id="image" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>URL</label>
                                    <input type="text" name="url" id="url" value="<?= $itemDb['url'] ?>"
                                           class="form-control"><br />
                                    <small>Endereço completo</small>
                                </div>
                                <div class="form-group">
                                    <label>Data de lançamento</label>
                                    <input type="text" name="data_lancamento" id="data_lancamento" value="<?= $itemDb['data_lancamento'] ?>"
                                           class="form-control"><br />
                                    <small>Ex: <?=date('d')?> de Janeiro de <?=date('Y')?>
                                </div>

                            </div>


                            <div class="col-md-4" style="padding-left:20px">
                                <div class="clear20"></div>
                                <div class="form-group">
                                    <div class="checkbox pull-left" style="display: none">
                                        <label>
                                            <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                        </label>
                                    </div>

                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                    <img src="_img/load.gif" class="loading_form pull-right">

                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar
                                    </button>&nbsp;

                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>
