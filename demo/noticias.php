<!DOCTYPE html>
<?php
define('BASE', 'http://localhost/hsm');
define('SITETITLE', ':: | :. Hospital São Marcos .: | ::');
?>
<html lang="pt-br" itemscope itemtype="https://schema.org/WebSite">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITETITLE; ?></title>
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick-theme.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/default.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/hospitalsm-core.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/responsive.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_fonts/dosis/dosis.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <!--[if lt IE 9]>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/html5shiv.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <header class="container-fluid">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h1 class="fl-left fadezero fontzero nomargin nopadding">
                            <?= SITETITLE; ?>
                        </h1>
                        <a href="" class="header-logo">
                            <img src="<?= BASE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITETITLE; ?>" title="<?= SITETITLE; ?>">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="hintxt">
                            <div class="hinfo-icon">
                                <span class="fa fa-map-marker"></span>
                            </div>
                            <div class="hinfo-text">
                                <h2 class="nomargin nopadding text-bold color-red-300">NOSSA LOCALIZAÇÃO</h2>
                                <p>Rua Olavo Bilac, 2300. Teresina-PI</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="hintxt">
                            <div class="hinfo-icon">
                                <span class="fa fa-coffee"></span>
                            </div>
                            <div class="hinfo-text">
                                <h2 class="nomargin nopadding text-bold color-red-300">HORÁRIOS DE ATENDIMENTO</h2>
                                <p>Aberto 24 horas</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="h-actions fl-right zd100">
                            <a href="#search" title="" class="h-act-search onfade4">
                                <i class="fa fa-search"></i>
                                <span>BUSCAR</span>
                            </a>
                            <div id="search">
                                <button type="button" class="close">x</button>
                                <form action="">
                                    <input type="search" value="" placeholder="O que você está procurando?" />
                                    <button type="submit" class="btn-default-hsm btn bg-red-300 onfade4">Buscar no Hospital</button>
                                </form>
                            </div>
                            <div class="toggle-wrap">
                                <button data-menu="links" class="h-act-links onfade4">
                                    <i class="fa fa-link"></i>
                                    <span>LINKS</span>
                                </button>
                                <ul id="links" class="menu menu--toggle">
                                    <li class="menu__item">Link #01</li>
                                    <li class="menu__item">Link #02</li>
                                    <li class="menu__item">Link #03</li>
                                    <li class="menu__item">Link #04</li>
                                    <li class="menu__item">Link #05</li>
                                    <li class="menu__item">Link #06</li>
                                    <li class="menu__item">Link #07</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <nav class="navbar navbar-default nomargin nopadding">
            <div class="container-fluid">
                <div class="atomwrapper nopadding">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">HOME <span class="sr-only">(atual)</span></a></li>
                            <li><a href="#">A PCC</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li><a href="#">LOCALIZAÇÃO</a></li>
                                    <li><a href="#">ESPECIALIDADES</a></li>
                                    <li><a href="#">DIRETORIA</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">SERVIÇOS</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">APPEM</a></li>
                                            <li><a href="#">Fisioterapia</a></li>
                                            <li><a href="#">Lar de Maria</a></li>
                                            <li><a href="#">Nadipo</a></li>
                                            <li><a href="#">Nuttem</a></li>
                                            <li><a href="#">Psicologia</a></li>
                                            <li><a href="#">Serviço Social</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ÁREA GERAL<i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li><a href="#">Convênios do Hospital</a></li>
                                    <li><a href="#">Buscar Profissionais</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Internação</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Acomodações</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Dicas</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Jornal da MAMA</a></li>
                                            <li><a href="#">Mastectomizadas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROFISSIONAIS <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li class="dropdown-submenu">
                                        <a href="#">MANUAIS E ARTIGOS</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Artigos</a></li>
                                            <li><a href="#">Orientações</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Cursos / Reuniões</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Cursos</a></li>
                                            <li><a href="#">Reunião Ginecologia</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Revista Multiprofissional</a></li>
                                    <li><a href="#">Artigo de Revista</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Residência Médica</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Inscrição</a></li>
                                            <li><a href="#">Apresentação</a></li>
                                        </ul>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Inscrições de Cursos</a></li>
                                </ul>
                            </li>
                            <li><a href="#">IMPRENSA</a></li>
                            <li><a href="#">FALE CONOSCO</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" >
                            <li><a class="font12px text-bold color-red-300" href="#">RESULTADOS DE EXAMES</a></li>
                            <li><a class="font12px text-bold color-red-300" href="#">ACESSO RESTRITO</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <!-- Our team Section -->
        <section class="content-section bg-light-brown">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h2>NOTÍCIAS</h2>
                        <h3 class="caption color-black-100 width50 centertable">unc quis turpis eu est tincidunt rutrum. Cras a purus quis sem tincidunt egestas vel id lacus.</h3>
                    </div><!-- /.col-md-12 -->
                </div>
                <section id="postIndex" class="widthWrapper">
                    <?php
                    for ($ln = 1; $ln <= 4; $ln++):
                        ?>
                        <article class="postInner">
                            <a target="_blank" href="#">
                                <div class="postImg">
                                    <img src="<?= BASE; ?>/tim.php?src=<?= BASE; ?>/uploads/midias/slider/thumb-grid-05.jpg&w=800&h600" alt="" title="" />
                                </div>
                                <h2 class="color-red-300 fontsize1b">Causas e consequências do tabagismo</h2>
                                <span class="tuppercase">Postada em: 22 de Setembro de 2016</span>
                                <p>A nicotina é a principal causa da dependência do tabaco. É encontrada em todos os derivados do tabaco (charuto, cachimbo, cigarro de palha, cigarros comuns, etc). Esta substância é psicoativa, isto é, produz a sensação de prazer, o que pode induzir ao abuso e à dependência. Ao ser ingerida, produz alterações no cérebro, modificando assim o estado emocional e comportamental do indivíduo, da mesma forma como ocorre com a cocaína, heroína e o álcool.</p>
                            </a>
                        </article>
                        <?php
                    endfor;
                    ?>
                </section>
            </div>
            <ul class="pagination centertable">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>
            <div class="clear20"></div>
        </section>

        <footer class="bg-light-gray-100 container-fluid fl-left footer-enable">
            <div class="atomwrapper">
                <div class="centralizar">
                    <a href="" class="header-logo">
                        <img src="<?= BASE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITETITLE; ?>" title="<?= SITETITLE; ?>">
                    </a>
                </div>
                <div class="clear10"></div>
                <p class="al-center centertable font18px">
                    Hospital São Marcos Para toda vida - Todos os direitos reservados 2016
                </p>
                <div class="clear10"></div>
                <ul class="main-footer-social">
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-feed"></i>
                        </a>
                    </li>
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </footer>



        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/ios6fix.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.swipe.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/functions.js"></script>
    </body>
</html>
