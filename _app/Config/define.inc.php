<?php
//include "conexao_inc.php";
//HACK PHP5
define('__DIR__', dirname(__FILE__));

define('HASH_KEY', 'admx2016');
define('SESSION_KEY', 'userLogin');

require_once "database.php";
//HELPERS DE INCLUDES

define('BASE', "http://" . $_SERVER["SERVER_NAME"] . SUB_PATH);
define('CDN_FILE', BASE . "/_cdn");
define('UPLOAD_FILE', BASE . "/uploads/");

//VARIAVEIS ADM
define('ADM_NAME', 'Admx');
define('ADM_DESC', 'Gerenciador de Conteúdo');
define('ADM_PATH', "system");
define('ADM_FILE', BASE . "/" . ADM_PATH);
define('ADM_URL', BASE . "/" . ADM_PATH);


// CLASS DE ALETAS
define('DX_SUCCESS', 'alert-success');
define('DX_INFOR', 'alert-info');
define('DX_ALERT', 'alert-warning');
define('DX_ERROR', 'alert-danger');

//VARIAVEIS DO SITE
define('SITE_NAME', 'Hospital São Marcos');
define('SITE_SUBNAME', 'Para toda vida');
define('SITE_DESC', '');
define('THEMES', 'dx_saomarcos');
define('PATH', 'themes/' . THEMES);
define('FILE', BASE . '/themes/' . THEMES);

//TABELAS DO SISTEMAS
define('DB_CIDADES', 'dx_app_cidades'); //Tabela das cidades
define('DB_ESTADOS', 'dx_app_estados'); //Tabela dos estados
define('DB_PROFISSOES', 'dx_app_profissoes'); //Tabela das profissoes
define('DB_USERS', 'dx_users'); //Tabela de usuários
define('DB_PERMISSION', 'dx_permission'); //Tabela das permissoes
define('DB_POSTS', 'dx_posts'); //Tabela das posts
define('DB_MIDIAS', 'dx_midias'); //Tabela das midias
define('DB_CATEGORIAS', 'dx_categorias'); //Tabela das midias
define('DB_TAGS', 'dx_tags'); //Tabela das midias
define('DB_MENUS', 'dx_menus'); //Tabela dos menus
define('DB_PAGES', 'dx_pages'); //Tabela das paginas
define('DB_BANNERS', 'dx_banners'); //Tabela dos banners
define('DB_CONFIGS', 'dx_configs'); //Tabela de config
define('DB_ESPECIALIDADES', 'dx_especialidades'); //Tabela de especialidades
define('DB_MANUAIS', 'dx_manuais'); //Tabela de manuais
define('DB_CLINICAS', 'dx_clinicas'); //Tabela das clinicas
define('DB_PROFISSIONAIS', 'dx_profissionais'); //Tabela de profissionais
define('DB_DIRETORES', 'dx_diretores'); //Tabela dde diretoria
define('DB_CONVENIOS', 'dx_convenios'); //Tabela dde convenios
define('DB_VIDEOS', 'dx_videos'); //Tabela dde videos
define('DB_SOCIAIS', 'dx_responsabilidade_social'); //Tabela dde videos
define('DB_VAGAS', 'rl_vagas'); //Tabela de DB_VAGAS
define('DB_CURRICULOS', 'rl_curriculos'); //Tabela de rl_curriculos
define('DB_TIPOS_CURSOS', 'rl_tipos_cursos'); //Tabela de tipos_cursos
define('DB_CORPO_DOCENTE', 'rl_corpo_docente'); //Tabela de rl_corpo_docente
define('DB_CURSOS_CORPO_DOCENTE', 'rl_curso_corpo_docente'); //Tabela de tipos_cursos
define('DB_ARTIGOS', 'rl_artigos'); //Tabela de tipos_cursos
define('DB_CURSOS', 'dx_cursos'); //Tabela de cursos

define('DB_ANAIS', 'rl_anais'); //Tabela de rl_anais
define('DB_COMITE_ETICA', 'rl_comite_etica'); //Tabela de rl_comite_etica
define('DB_REVISTA_EDICOES', 'rl_revista_edicoes'); //Tabela de rl_revista_edicoes
###__REPLACE__###




//TAMANHO DE IMAGENS DO SISTEMA

define('IMAGE_W', 1600); //Tamanho da imagem (WIDTH)
define('IMAGE_H', 800); //Tamanho da imagem (HEIGHT)

?>