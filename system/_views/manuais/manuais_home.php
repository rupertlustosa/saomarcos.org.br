<?php
if (empty($Read)) {
    $Read = new Read;
}

//Auto delete
$Delete = new Delete;
$Delete->ExeDelete(DB_MANUAIS, "WHERE manual_title IS NULL AND status = :st", "st=0");


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <a title="Criar <?= $TABLE['TITLE_SINGLE'] ?>"
                           href="<?= ADM_URL ?>/?dx=<?= $TABLE['VIEW'] ?>/create" class="btn btn-info "> <i
                                    class="fa fa-plus"></i></a>
                        <?= $TABLE['TITLE'] ?>
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <?= Admx::formSearch($TABLE['VIEW']) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= (!empty($_GET['s'])) ? "Buscar por '{$_GET['s']}'" : "Gerenciar {$TABLE['TITLE']}" ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th>#Título</th>
                                    <th style="width: 150px">#Secao</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                $getPage = filter_input(INPUT_GET, 'pg', FILTER_VALIDATE_INT);
                                $Page = ($getPage ? $getPage : 1);
                                $BaseUrl = "?dx={$view}/home&pg=";
                                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                                $Paginator->ExePager($Page, 10);


                                $where = Admx::Search(array('manual_title'));
                                $where .= Admx::Trash();
                                $Read->ExeRead(DB_MANUAIS, " WHERE 1=1 {$where} ORDER BY status DESC, created DESC LIMIT :limit OFFSET :offset", "limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <tr id="row_<?= $ROW['manual_id'] ?>" class="class_<?= $ROW['manual_id'] ?>">
                                        <td><?= str_pad($manual_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=manuais/create&id=<?= $manual_id ?>"
                                               title="Editar <?= $manual_title ?>"><?= $manual_title ?></a>
                                        </td>
                                        <td><?= getManuaisCat($manual_category) ?></td>
                                        <td>
                                            <?= Admx::BtnStatus(DB_MANUAIS, 'manual_id', $manual_id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=manuais/create&id=<?= $manual_id ?>"
                                               title="Editar <?= $manual_title ?>" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Edit </a>
                                            <?= Admx::BtnTrash(DB_MANUAIS, 'manual_id', $manual_id, "row_{$ROW['manual_id']}", 1) ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <?php
                            $Paginator->ExePaginator(DB_MANUAIS, " WHERE 1=1 {$where} ");
                            echo $Paginator->getPaginator();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php


include './_inc/footer.php';
?>