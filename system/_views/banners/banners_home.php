<?php
if (empty($Read)) {
    $Read = new Read;
}

//Auto delete
$Delete = new Delete;
$Delete->ExeDelete(DB_BANNERS, "WHERE banner_title IS NULL AND banner_dataentrada IS NULL and status = :st", "st=0");


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><?= $TABLE['TITLE'] ?></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Gerenciar <?= $TABLE['TITLE'] ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th style="width: 300px">#Image</th>
                                    <th>#Banner</th>
                                    <th style="width: 220px">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $where = Admx::Trash();
                                $Read->ExeRead(DB_BANNERS, " WHERE 1=1 {$where} ORDER BY status ASC, banner_dataentrada DESC ");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <tr id="row_<?= $ROW['banner_id'] ?>" class="class_<?= $ROW['banner_id'] ?>">
                                        <td><?= str_pad($banner_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td><img src="<?= BASE ?>/tim.php?src=uploads/<?= $banner_image ?>&w=300"
                                                 style="width: 100%; max-width: 300px;"/></td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=banners/create&id=<?= $banner_id ?>"
                                               title="Editar<?= $ROW['banner_title'] ?>"><?= $banner_title ?></a>
                                            <br>
                                            <small><?= $ROW['banner_subtitle'] ?></small>
                                            <p>
                                                <small class="label label-primary" title="Banner Entrada">
                                                    Entrada: <?= Check::DateDecode($banner_dataentrada) ?></small>
                                                <small class="label label-default" title="Banner Saída">
                                                    Saída: <?= Check::DateDecode($banner_datasaida) ?></small>
                                            <p>
                                                <small class="label label-info"><?= (int)$views ?> Views</small>
                                        </td>

                                        <td style="text-align:center;vertical-align:middle">
                                            <?= Admx::BtnStatus(DB_BANNERS, 'banner_id', $banner_id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=banners/create&id=<?= $banner_id ?>"
                                               title="Editar<?= $ROW['banner_title'] ?>" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Edit </a>
                                            <?= Admx::BtnTrash(DB_BANNERS, 'banner_id', $banner_id, "row_{$ROW['banner_id']}", 1) ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php


include './_inc/footer.php';
?>