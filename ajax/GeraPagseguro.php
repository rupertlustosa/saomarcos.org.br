<?php

require __DIR__ . '/../vendor/autoload.php';
require_once '../_app/autoload.php';

/*$course = new stdClass();
$course->id = 1;
$course->name = 'Curso Teste';
$course->price = 10.00;*/

$description = 'HSM :: Inscrição no curso ' . $course->name . ' - ' . $inscricao_numero;
if (strlen($description) >= 95)
    $description = substr($description, 0, 95) . '...';

$item_a = new \Pagseguro\Item();
$item_a
    ->setId($inscricao_numero . '/' . $course->id)
    ->setDescription($description)
    ->setQuantity(1)
    ->setAmount($course->price)
    ->setWeight(0)
    ->setShippingCost(0.00)
    ->setWidth(0)
    ->setHeight(0)
    ->setLength(0);

/*
 * Criando o endereço
 * */
$address = new \Pagseguro\Address();
$address->setPostalCode($_POST['postal_code'])
    ->setStreet($_POST['street'])
    ->setNumber($_POST['number'])
    ->setDistrict($_POST['district'])
    ->setCity($_POST['city'])
    ->setState($_POST['state'])
    ->setCountry('BRA');

/*
 * Criando o documento
 * */
$documents = new \Pagseguro\Documents();
//$documents->setNumber('00110115309')
$documents->setNumber('')
    ->setType('CPF');

/*
 * Criando a lista de itens
 * */
$itens = new \Pagseguro\ArrayOfItens();
$itens->setItem($item_a);

/*
 * Criando shipping
 * */
$shipping = new \Pagseguro\Shipping();
$shipping->setAddress($address)
    ->setType(2)
    ->setCost(0);

/*
 * Criando o sender
 * */
$sender = new \Pagseguro\Sender();
$sender->setEmail('daniel.financeiro@saomarcos.org.br')
    ->setName('ASSOCIAÇÃO PIAUIENSE DE COMBANTE AO CÂNCER')
    //->setDocuments($documents)
    ->setPhone('8621068000')
    ->setBornDate('1984-05-09');

$checkout = new \Pagseguro\Checkout();
$checkout->setItems($itens)
    ->setShipping($shipping)
    ->setSender($sender);

$retorno = $checkout->realizaCheckout();

$pagamento_inscricao = [
    'cursos_inscricao_id' => $cursos_inscricao_id,
    'code' => $retorno['code'],
    'date' => $retorno['date'],
    'link' => $retorno['link'],
    'data' => $retorno['data'],
    //'return' => 'b'
];
//


$Create = new Create();
$Create->ExeCreate("payments", $pagamento_inscricao);


//dd("payments", $pagamento_inscricao);

/*$enrollment = Enrollment::find($enrollment_id);

$enrollment->code = $hash;
$enrollment->status = 'PAGAMENTO_GERADO';
$enrollment->save();

Payment::create([
    'enrollment_id' => $enrollment->id,
    'code' => $retorno['code'],
    'date' => $retorno['date'],
    'link' => $retorno['link'],
    'data' => $retorno['data'],
    'return' => '',
]);*/

//print_r($retorno);