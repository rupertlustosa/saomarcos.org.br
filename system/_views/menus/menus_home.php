<?php
if (empty($Read)) {
    $Read = new Read;
}


$Read->ExeRead(DB_MENUS, " WHERE menu_parent =  '0' ORDER BY menu_order ASC ");
$Menus = $Read->getResult();


$KeyId = filter_input(INPUT_GET, "id");
if (!empty($KeyId)) {
    $Read->FullRead("SELECT * FROM " . DB_MENUS . " WHERE menu_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Menu = $Read->getResult()[0];
    } else {
        $Menu = Admx::getResultFields(DB_MENUS, 'key');
    }
} else {
    $Menu = Admx::getResultFields(DB_MENUS, 'key');
}
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE'] ?></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">

                    <div class="x_content">
                        <div class="clear10"></div>
                        <div class="col-md-4">
                            <div class="x_panel" style="background-color:#f8f8f8; ">
                                <form method="post">
                                    <?php
                                    $token = Admx::setFormToken(false);
                                    ?>
                                    <input name="formToken" type="hidden" value="<?= $token ?>"/>
                                    <input type="hidden" name="menu_last" value="<?= count($Menus) + 1 ?>">
                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">


                                    <div class="form-group">
                                        <label>Nome</label>
                                        <input type="text" name="menu_title" value="<?= $Menu['menu_title'] ?>"
                                               placeholder="Digite o nome do <?= $TABLE['TITLE_SINGLE'] ?>"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Ordem:</label>
                                        <select name="menu_order" class="form-control">
                                            <?php
                                            for ($i = 1; $i <= (count($Menus) + 1); $i++) {
                                                $selected = ($Menu['menu_order'] == $i) ? " SELECTED='SELECTED' " : "";
                                                echo "<option value='{$i}' {$selected} >{$i}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Submenu de:</label>
                                        <select name="menu_parent" class="form-control">
                                            <option value="0">(este é um menu)</option>
                                            <?php
                                            $MenusSelect = new $Read();
                                            $MenusSelect->ExeRead(DB_MENUS, " WHERE status = 1");
                                            foreach ($MenusSelect->getResult() as $mn) {
                                                $selectedMn = ($Menu['menu_parent'] == $mn['menu_id']) ? " SELECTED='SELECTED' " : "";
                                                echo "<option value='{$mn['menu_id']}' {$selectedMn} >{$mn['menu_title']}</option>";
                                                $parent = $mn['menu_id'];
                                                $children = Admx::getResult(DB_MENUS, " menu_parent = {$parent} ", " menu_order ASC ");
                                                foreach ($children as $ch) {
                                                    $selectedCh = ($Menu['menu_parent'] == $ch['menu_id']) ? " SELECTED='SELECTED' " : "";
                                                    echo "<option value='{$ch['menu_id']}' {$selectedCh} > -- {$ch['menu_title']}</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="status" type="checkbox"
                                                   value="1" <?= $Menu['status'] == 1 ? 'checked' : '' ?>> Ativo
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="clear25"></div>
                                        <button name="btn" title="<?= $TABLE['TITLE_SINGLE'] ?>" type="submit"
                                                class="btn btn-success  pull-left">Salvar
                                        </button>
                                        <?php
                                        if ($KeyId):
                                            ?>
                                            <button type="button" callback="Menus" key_id="<?= $KeyId ?>"
                                                    formToken="<?= $token ?>" callback_action="delete"
                                                    class="categories_delete btn btn-danger  pull-right"> Excluir
                                            </button>
                                            <?php
                                        endif;
                                        ?>
                                    </div>

                                </form>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-cogs"></i> <?= $TABLE['TITLE'] ?></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start accordion -->
                                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php
                                        foreach ($Menus as $ROW):
                                            $parent = $ROW['menu_id'];
                                            $children = Admx::getResult(DB_MENUS, " menu_parent = {$parent} ", " menu_order ASC ");
                                            ?>
                                            <div class="panel">
                                                <a class="panel-heading collapsed" role="tab"
                                                   title="Editar <?= $ROW['menu_title'] ?>"
                                                   id="heade_<?= $ROW['menu_id'] ?>" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapse_<?= $ROW['menu_id'] ?>"
                                                   aria-expanded="false"
                                                   aria-controls="collapse_<?= $ROW['menu_id'] ?>">
                                                    <h4 class="panel-title"><?= $ROW['menu_order'] ?>&#176;
                                                        - <?= $ROW['menu_title'] ?></h4>
                                                </a>
                                                <div id="collapse_<?= $ROW['menu_id'] ?>"
                                                     class="panel-collapse collapse" role="tabpanel"
                                                     aria-labelledby="headingThree">
                                                    <div class="panel-body" style="background-color:#fff">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>#Menu</th>
                                                                <th style="width:150px">#Edit</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><?= $ROW['menu_title'] ?></td>
                                                                <td align="center">
                                                                    <?= Admx::BtnStatus(DB_MENUS, 'menu_id', $ROW['menu_id'], $ROW['status']) ?>
                                                                    <a href="<?= ADM_URL . "/?dx={$TABLE['VIEW']}/home&id={$ROW[$TABLE['KEY_NAME']]}" ?>"
                                                                       title="Editar" class="btn btn-info btn-xs"><i
                                                                                class="fa fa-pencil"></i> Editar</a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <?php
                                                        if ($children):
                                                            ?>
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th style="width:20px">#Order</th>
                                                                    <th>#Submenu</th>
                                                                    <th style="width:150px">#Edit</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                foreach ($children as $child):
                                                                    $child_id = $child['menu_id'];
                                                                    $childOf = Admx::getResult(DB_MENUS, " menu_parent = {$child_id} ", " menu_order ASC ");
                                                                    ?>
                                                                    <tr>
                                                                        <th scope="row"><?= $child['menu_order'] ?></th>
                                                                        <td>
                                                                            <p style="font-weight:bold"><?= $child['menu_title'] ?></p>
                                                                            <?php
                                                                            if ($childOf):
                                                                                ?>
                                                                                <table class="table"
                                                                                       style="background-color:#e8e8e8">
                                                                                    <?php
                                                                                    foreach ($childOf as $ch):
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td><?= $ch['menu_title'] ?></td>
                                                                                            <td align="center">
                                                                                                <?= Admx::BtnStatus(DB_MENUS, 'menu_id', $ch['menu_title'], $ch['status']) ?>
                                                                                                <a href="<?= ADM_URL . "/?dx={$TABLE['VIEW']}/home&id={$ch['menu_id']}" ?>"
                                                                                                   title="Editar"
                                                                                                   class="btn btn-info btn-xs"><i
                                                                                                            class="fa fa-pencil"></i></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    endforeach;
                                                                                    ?>
                                                                                </table>
                                                                                <?php
                                                                            endif;
                                                                            ?>
                                                                        </td>
                                                                        <td align="center">
                                                                            <?= Admx::BtnStatus(DB_MENUS, 'menu_id', $child['menu_id'], $child['status']) ?>
                                                                            <a href="<?= ADM_URL . "/?dx={$TABLE['VIEW']}/home&id={$child['menu_id']}" ?>"
                                                                               title="Editar"
                                                                               class="btn btn-info btn-xs"><i
                                                                                        class="fa fa-pencil"></i> Editar</a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                endforeach;
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                            <?php
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <?php
                                        endforeach;
                                        ?>
                                    </div>
                                    <!-- end of accordion -->


                                </div>
                            </div><!-- col-md-12 -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php
include './_inc/footer.php';
?>