<?php 
include "./_app/autoload.php";
$token = filter_input(INPUT_GET, 'token');
$Read = new Read;
$Read->FullRead("SELECT inscricao_id,inscricao_certificado_url FROM dx_cursos_inscricao WHERE hash=:h","h={$token}");
if($Read->getResult()){
    $Update = new Update;
    $inscricao_id = $Read->getResult()[0]['inscricao_id'];
    $url = BASE."/uploads/files/".$Read->getResult()[0]['inscricao_certificado_url'];
    $Update->ExeUpdate("dx_cursos_inscricao",['inscricao_certificado'=> 1],"WHERE inscricao_id=:id","id={$inscricao_id}");
    header("LOCATION: {$url}");
}else{
    echo "Token inválido!";
    echo '<META http-equiv="refresh" content="1;URL='.BASE.'">';
}
?>