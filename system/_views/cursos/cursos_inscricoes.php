<?php
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (!empty($KeyId)):
    $Read->FullRead("SELECT * FROM dx_cursos_inscricao WHERE inscricao_curso_id =:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];

    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
else:
    //header("Location: " . ADM_URL . "?dx=error/404");
endif;


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Inscrições</h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Curso: <?= Admx::GetCampo(DB_CURSOS, "curso_title", " curso_id = {$KeyId}") ?></h2>
                        <a target="print" class="pull-right btn btn-success btn-xs"
                           href="<?= ADM_URL ?>/imprimir-cursos.php?id=<?= $KeyId ?>"><i class="fa fa-print"></i>
                            imprimir</a>
                        <a class="btn btn-info btn-xs pull-right "
                           href="<?= ADM_URL ?>/imprimir-cursos.php?id=<?= $KeyId ?>&view=xls"><i class="fa fa-download"
                                                                                                  aria-hidden="true"></i>
                            xls</a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- start project list -->
                        <table id="datatable" class="table table-striped table-bordered">
                            <!--<table class="table table-striped projects">-->
                            <thead>
                            <tr>
                                <th style="width: 100px">#Inscrição</th>
                                <th style="width: 150px">#Status</th>
                                <th>#Dados</th>
                                <th>#Certificado</th>
                                <th>#Action</th>
                            </tr>
                            </thead>
                            <style>
                                .status_0 {
                                    background-color: #ec971f;
                                    border: solid 1px #985f0d;
                                    color: #fff;
                                }

                                .status_1 {
                                    background-color: #d9534f;
                                    border: solid 1px #d43f3a;
                                    color: #fff;
                                }

                                .status_2 {
                                    background-color: #26B99A;
                                    border: solid 1px #169F85;
                                    color: #fff;
                                }
                            </style>
                            <tbody>
                            <?php

                            $formToken = Admx::setFormToken(false);
                            $Read->FullRead("
                                    SELECT i.inscricao_certificado, i.inscricao_id,i.inscricao_numero,i.inscricao_curso_id,i.status,i.inscricao_comprovante, c.*
                                    FROM dx_cursos_inscricao as i
                                    INNER JOIN dx_clientes as c ON c.cpf = i.inscricao_cliente_cpf
                                    WHERE inscricao_curso_id = :curso_id
                                    ORDER BY nome ASC, status ASC, created DESC ", "curso_id={$KeyId}");

                            //$Read->ExeRead("dx_cursos_inscricao", " WHERE 1=1 ", "limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                            foreach ($Read->getResult() as $ROW):
                                extract($ROW);

                                ?>
                                <tr id="row_<?= $ROW['inscricao_id'] ?>" class="item_<?= $ROW['inscricao_id'] ?>">
                                    <td><?= $inscricao_numero ?></td>
                                    <td>
                                        <select class="form-control select-status status_<?= $status ?>"
                                                id="select_status_<?= $inscricao_id ?>"
                                                onchange="statusInscricao(<?= $inscricao_id ?>,this.value)">
                                            <option class="label label-warning"
                                                    style="font-size:14px; padding:5px; display:block"
                                                    value="0" <?= ($status == 0) ? 'selected="selected"' : '' ?> >
                                                Pendente
                                            </option>
                                            <option class="label label-danger"
                                                    style="font-size:14px; padding:5px; display:block"
                                                    value="1" <?= ($status == 1) ? 'selected="selected"' : '' ?> >Negado
                                            </option>
                                            <option class="label label-success"
                                                    style="font-size:14px; padding:5px; display:block"
                                                    value="2" <?= ($status == 2) ? 'selected="selected"' : '' ?> >
                                                Confirmado
                                            </option>
                                        </select>
                                    </td>
                                    <td>
                                        <strong>Nome:</strong><?= $nome ?><br>
                                        <strong>Email:</strong><span
                                                id="inscricao_email_<?= $inscricao_id ?>"><?= $email ?></span> -
                                        <strong>CPF:</strong><?= $cpf ?> <br>
                                        <strong>Telefone:</strong><?= $telefone ?> -
                                        <strong>Graduação:</strong><?= $graduacao ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($inscricao_certificado == 0) {
                                            $certificadoClass = "btn-primary ";
                                            $certificadoValue = '<i class="fa fa-share"></i>&nbsp;Enviar';
                                        } else {
                                            $certificadoClass = "btn-default ";
                                            $certificadoValue = '<i class="fa fa-check"></i>&nbsp;Recebido';
                                        }
                                        ?>

                                        <button onclick="enviarCertificado(<?= $inscricao_id ?>)" type="button"
                                                class="btn <?= $certificadoClass ?> btn-xs"><?= $certificadoValue ?></button>

                                    </td>
                                    <td>

                                        <?= Admx::BtnDelete("dx_cursos_inscricao", 'inscricao_id', $inscricao_id, "row_{$ROW['inscricao_id']}", $formToken) ?>
                                        <?php
                                        if ($inscricao_comprovante) {
                                            ?>
                                            <a target="comprovante" id="comprovante_<?= $inscricao_id ?>"
                                               class="btn btn-info btn-xs"
                                               href="<?= BASE ?>/uploads/files/<?= $inscricao_comprovante ?>">comprovante</a>
                                            <?php
                                        }
                                        ?>

                                    </td>
                                </tr>
                                <?php
                            endforeach;

                            ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<div class="modal fade" id="modalInscricaoNegada" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Enviar E-Mail</h4>
            </div>
            <div class="modal-body">
                <p>Digite a mensagem que será enviada!</p>
                <input type="hidden" id="inscricao_campo_id">
                <input type="hidden" id="inscricao_campo_status">
                <textarea rows="4" class="form-control" id="inscricao_msg"></textarea>
            </div>
            <div class="modal-footer">
                <div style="display:table; width:100%; ">
                    <div class="pull-left">
                        <div id="label_response"></div>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" onclick="inscricaoNegada()" class="btn btn-primary">Enviar</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal inscricao status -->

<div class="modal fade" id="modalInscricaoCertificado" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" class="ajax_off" enctype="multipart/form-data" id="formInscricaoCertificado">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Enviar E-Mail</h4>
                </div><!-- header -->
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="certificado_inscricao_id" name="key_id">
                        <input type="hidden" name="formToken" value="<?= $formToken ?>">
                    </div>
                    <div class="clear20"></div>
                    <div class="form-group">
                        <label for="certificado">Certificado(
                            <small>PDF</small>
                            )</label>
                        <input type="file" name="arquivo" id="certificado">
                    </div>
                </div><!-- body -->
                <div class="modal-footer">
                    <div style="display:table; width:100%; ">
                        <div class="pull-left">
                            <div id="label_certificado_response" style="font-size:15px; letter-spacing:1px"></div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div><!-- footer -->
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal envio do certificado -->

<script>

    $(function () {
        $("#formInscricaoCertificado").submit(function () {
            var form = $(this);
            $("#label_certificado_response").html("Aguarde...");
            form.ajaxSubmit({
                url: base + '/_ajax/Cursos.ajax.php',
                data: {callback_action: "enviar_certificado"},
                dataType: 'json',
                success: function (data) {
                    form.find('input[type="file"]').val('');
                    if (data.status) {
                        $("#label_certificado_response").html(`<span class="label label-success">${data.msg}</span>`);
                    } else {
                        $("#label_certificado_response").html(`<span class="label label-danger">${data.msg}</span>`);
                    }
                }
            });
            this.reset();
            return false;
        })
    })

    function enviarCertificado(id) {
        $("#certificado_inscricao_id").val(id);
        $('#modalInscricaoCertificado').modal("show");
    }

    function statusInscricao(id, n) {
        $("#select_status_" + id).removeClass('status_0');
        $("#select_status_" + id).removeClass('status_1');
        $("#select_status_" + id).removeClass('status_2');
        $("#select_status_" + id).addClass('status_' + n);

        $("#inscricao_campo_id").val(id);
        $("#inscricao_campo_status").val(n);
        if (n == 1) {
            $('#modalInscricaoNegada').modal();
        } else {
            upInscricao(id, n);
        }
    }

    function upInscricao(id, n) {
        CALLBACK = 'cursos';
        callback_action = 'manager_inscricao';
        formToken = '<?=$formToken?>';
        key_id = id;
        $.post(base + '/_ajax/Cursos.ajax.php', {
            CALLBACK,
            callback_action,
            formToken,
            key_id,
            status: n
        }, function (data) {
            if (data.response === 'success') {
                trigger('info', 'Sucesso!', 'Status alterado com sucesso!', 'dark');
            } else {
                trigger('error', 'Error!', 'Tente novamente mais tarde!');
            }
        }, 'json');
    }

    function inscricaoNegada() {
        id = $("#inscricao_campo_id").val();
        status = $("#inscricao_campo_status").val();
        texto = $("#inscricao_msg").val();
        CALLBACK = 'cursos';
        callback_action = 'manager_inscricao';
        formToken = '<?=$formToken?>';
        key_id = id;
        if (texto) {
            $.post(base + '/_ajax/Cursos.ajax.php', {
                CALLBACK,
                callback_action,
                formToken,
                key_id,
                status,
                texto
            }, function (data) {
                if (data.response === 'success') {
                    $("#comprovante_" + id).remove();
                    $("#label_response").html("<span class='label label-success'>Email enviado com sucesso!</span>")
                    $("#inscricao_msg").val('');
                } else {
                    $("#label_response").html("<span class='label label-danger'>Error: tente novamente mais tarde!</span>")
                }
            }, 'json');
        } else {
            $("#label_response").html("<span class='label label-danger'>Error: Digite o texto que deve ser enviado!</span>")
        }

    }

</script>
<?php


include './_inc/footer.php';
?>
