<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'manuais';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
$PostFormToken = (!empty($PostData['formToken'])) ? $PostData['formToken'] : null;
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['file'])):
        $File = $_FILES['file'];
    endif;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            if ($PostFormToken == $formToke):
                $Read->ExeRead(DB_MANUAIS, "WHERE manual_id = :id", "id={$KeyId}");
                if (!$Read->getResult()):

                else:
                    $name = !empty($PostData['manual_title']) ? Check::Name($PostData['manual_title']) : $post['manual_id'];
                    if (!empty($File)):
                        $Upload->File($File, $name . "-" . time(), "manuais");
                        if ($Upload->getResult()):
                            $_file = $Upload->getResult();
                            $PostData['manual_file'] = $_file;
                        endif;
                    endif;
                    $ReadName = new $Read;
                    $ReadName->FullRead("SELECT manual_name FROM " . DB_MANUAIS . " WHERE manual_name= :nm and manual_id != :id ", "nm={$name}&id={$KeyId}");
                    if ($ReadName->getResult()):
                        $PostData['manual_name'] = $name . time();
                    else:
                        $PostData['manual_name'] = $name;
                    endif;

                    $PostData['status'] = ($rascunho) ? 0 : 1;

                    $post = $Read->getResult()[0];
                    $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE manual_id = :id", "id={$KeyId}");
                    $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
                endif;
            else:
                die('<center><h1>Permissão Negada!</h1></center>');
            endif;

            break;


    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
