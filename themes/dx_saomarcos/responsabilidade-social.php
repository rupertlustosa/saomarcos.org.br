<?php
extract($DataResult);
include "inc/header.php"
?>
    <style type="text/css">
        .cover-social {
            max-width: 400px;
            height: auto;
            padding: 5px;
            background-color: #ccc;
            display: table;
        }

    </style>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="clear40"></div>
        <div class="container">
            <div class="clear20"></div>
            <div class="row">
                <h1 class="al-center centertable fontsize2 text-bold">RESPONSABILIDADE SOCIAL</h1>
            </div>
            <div class="clear20"></div>
            <div class="clear20 hidden-xs"></div>
            <div class="row text-center">
                <div class="col-md-5">
                    <div class="cover-social">
                        <?php
                        if ($social_image) {
                            echo '<img src="' . BASE . '/uploads/' . $social_image . '">';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-7">
                    <h2 style="margin-top: 0px; text-align: left;"><?= $social_title ?></h2>
                    <div class="clear20"></div>
                    <div class="texto">
                        <?= $social_content ?>
                    </div>
                </div><!-- /.col-md-12 -->
            </div>
        </div>
        <div class="clear40"></div>
    </section><!-- /.our-team -->
<?php include "inc/footer.php" ?>