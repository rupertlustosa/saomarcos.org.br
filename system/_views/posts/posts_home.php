<?php
if (empty($Read)) {
    $Read = new Read;
}

//Auto delete
$Delete = new Delete;
$Delete->ExeDelete(DB_POSTS, "WHERE post_title IS NULL AND post_content IS NULL and status = :st", "st=0");


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><?= $TABLE['TITLE'] ?></h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <?= Admx::formSearch($TABLE['VIEW']) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= (!empty($_GET['s'])) ? "Buscar por '{$_GET['s']}'" : "Gerenciar {$TABLE['TITLE']}" ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th style="width: 150px">#Image</th>
                                    <th>#Título</th>
                                    <th style="width: 150px">#Autor</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                $getPage = filter_input(INPUT_GET, 'pg', FILTER_VALIDATE_INT);
                                $Page = ($getPage ? $getPage : 1);
                                $BaseUrl = "?dx={$view}/home&pg=";
                                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                                $Paginator->ExePager($Page, 10);
                                $formToken = Admx::setFormToken(false);
                                $where = Session::checkAutorPost();
                                $where .= Admx::Search(array('post_title', 'post_content'));
                                $where .= Admx::Trash();
                                $Read->ExeRead(DB_POSTS, " WHERE 1=1 {$where} ORDER BY status ASC, post_date DESC LIMIT :limit OFFSET :offset", "limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    $autor = Admx::getAutorPost($post_user_id);
                                    ?>
                                    <tr id="row_<?= $ROW['post_id'] ?>" class="item_<?= $ROW['post_id'] ?>">
                                        <td><?= str_pad($post_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td><img src="<?= BASE ?>/tim.php?src=uploads/<?= $post_image ?>&w=90"
                                                 style="width: 100%; max-width: 90px;"/></td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=posts/create&id=<?= $post_id ?>"
                                               title="Editar <?= $post_title ?>"><?= $post_title ?></a>
                                            <br/>
                                            <small class="label label-default"><?= Check::DateDecode($post_date) ?></small>
                                        </td>
                                        <td>
                                            <?= $autor['user_name'] ?>
                                        </td>
                                        <td>
                                            <?= Admx::BtnStatus(DB_POSTS, 'post_id', $post_id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=posts/create&id=<?= $post_id ?>"
                                               title="Editar <?= $post_title ?>" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Edit </a>
                                            <?= Admx::BtnDelete(DB_POSTS, 'post_id', $post_id, "row_{$ROW['post_id']}", $formToken) ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <?php
                            $Paginator->ExePaginator(DB_POSTS, " WHERE 1=1 {$where} ");
                            echo $Paginator->getPaginator();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php


include './_inc/footer.php';
?>