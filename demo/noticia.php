<!DOCTYPE html>
<?php
define('BASE', 'http://localhost/hospital-sao-marcos');
define('SITETITLE', ':: | :. Hospital São Marcos .: | ::');
?>
<html lang="pt-br" itemscope itemtype="https://schema.org/WebSite">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITETITLE; ?></title>
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick-theme.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/justified-gallery/justifiedGallery.min.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_libraries/lightbox2-master/lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/default.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/hospitalsm-core.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_css/responsive.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_fonts/dosis/dosis.css" rel="stylesheet">
        <link href="<?= BASE; ?>/_cdn/_front/_assets/_img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <!--[if lt IE 9]>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/html5shiv.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <header class="container-fluid">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <h1 class="fl-left fadezero fontzero nomargin nopadding">
                            <?= SITETITLE; ?>
                        </h1>
                        <a href="" class="header-logo">
                            <img src="<?= BASE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITETITLE; ?>" title="<?= SITETITLE; ?>">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="hintxt">
                            <div class="hinfo-icon">
                                <span class="fa fa-map-marker"></span>
                            </div>
                            <div class="hinfo-text">
                                <h2 class="nomargin nopadding text-bold color-red-300">NOSSA LOCALIZAÇÃO</h2>
                                <p>Rua Olavo Bilac, 2300. Teresina-PI</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="hintxt">
                            <div class="hinfo-icon">
                                <span class="fa fa-coffee"></span>
                            </div>
                            <div class="hinfo-text">
                                <h2 class="nomargin nopadding text-bold color-red-300">HORÁRIOS DE ATENDIMENTO</h2>
                                <p>Aberto 24 horas</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="h-actions fl-right zd100">
                            <a href="#search" title="" class="h-act-search onfade4">
                                <i class="fa fa-search"></i>
                                <span>BUSCAR</span>
                            </a>
                            <div id="search">
                                <button type="button" class="close">x</button>
                                <form action="">
                                    <input type="search" value="" placeholder="O que você está procurando?" />
                                    <button type="submit" class="btn-default-hsm btn bg-red-300 onfade4">Buscar no Hospital</button>
                                </form>
                            </div>
                            <div class="toggle-wrap">
                                <button data-menu="links" class="h-act-links onfade4">
                                    <i class="fa fa-link"></i>
                                    <span>LINKS</span>
                                </button>
                                <ul id="links" class="menu menu--toggle">
                                    <li class="menu__item">Link #01</li>
                                    <li class="menu__item">Link #02</li>
                                    <li class="menu__item">Link #03</li>
                                    <li class="menu__item">Link #04</li>
                                    <li class="menu__item">Link #05</li>
                                    <li class="menu__item">Link #06</li>
                                    <li class="menu__item">Link #07</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <nav class="navbar navbar-default nomargin nopadding">
            <div class="container-fluid">
                <div class="atomwrapper nopadding">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">HOME <span class="sr-only">(atual)</span></a></li>
                            <li><a href="#">A PCC</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li><a href="#">LOCALIZAÇÃO</a></li>
                                    <li><a href="#">ESPECIALIDADES</a></li>
                                    <li><a href="#">DIRETORIA</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">SERVIÇOS</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">APPEM</a></li>
                                            <li><a href="#">Fisioterapia</a></li>
                                            <li><a href="#">Lar de Maria</a></li>
                                            <li><a href="#">Nadipo</a></li>
                                            <li><a href="#">Nuttem</a></li>
                                            <li><a href="#">Psicologia</a></li>
                                            <li><a href="#">Serviço Social</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ÁREA GERAL<i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li><a href="#">Convênios do Hospital</a></li>
                                    <li><a href="#">Buscar Profissionais</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Internação</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Acomodações</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Dicas</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Jornal da MAMA</a></li>
                                            <li><a href="#">Mastectomizadas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROFISSIONAIS <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu tuppercase">
                                    <li class="dropdown-submenu">
                                        <a href="#">MANUAIS E ARTIGOS</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Artigos</a></li>
                                            <li><a href="#">Orientações</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Cursos / Reuniões</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Cursos</a></li>
                                            <li><a href="#">Reunião Ginecologia</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Revista Multiprofissional</a></li>
                                    <li><a href="#">Artigo de Revista</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-submenu">
                                        <a href="#">Residência Médica</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Inscrição</a></li>
                                            <li><a href="#">Apresentação</a></li>
                                        </ul>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Inscrições de Cursos</a></li>
                                </ul>
                            </li>
                            <li><a href="#">IMPRENSA</a></li>
                            <li><a href="#">FALE CONOSCO</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" >
                            <li><a class="font12px text-bold color-red-300" href="#">RESULTADOS DE EXAMES</a></li>
                            <li><a class="font12px text-bold color-red-300" href="#">ACESSO RESTRITO</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>


        <section class="content-section bg-light-brown">
            <div class="container-fluid">
                <div class="atomwrapper">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="contentInfoImv">
                                <img src="<?= BASE; ?>/tim.php?src=<?= BASE; ?>/uploads/midias/slider/001.jpg&w=800&h=500" alt="" title=""/>
                                <div class="clear10"></div>
                                <h1 class="fontsize2 cavbold al-center tuppercase centertable">
                                    TÍTULO DA NOTÍCIA
                                </h1>
                                <span class="al-center centertable nolanbook">
                                    Publicada em: <time class="" pubdate>22 de Setembro de 2016</time>
                                </span>
                                <div class="clear10"></div>
                                <div class="container-line">
                                    <div class="msn">
                                        <ul class="main_share_command_news">
                                            <li class="facebook">
                                                <a href="" title="Compartilhe no Facebook" class="radius1b fb_h">
                                                    <i class="fa fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li class="google">
                                                <a href="" title="Recomende no Google+" class="radius1b gp_h">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li class="twitter">
                                                <a href="" title="" rel="" class="radius1b tt_h">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="gps" href="whatsapp://send?text=<?= BASE; ?>/noticia/link-da-noticia" data-action="share/whatsapp/share">
                                                    <span class="fa fa-whatsapp"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="contentMsg width90">
                                    <em class="explanation">This top paragraph is where I introduce you and explain how this guest post came about. I will write it, but you can provide links that you want used. It's an `<em>` for various syndication reasons.</em>

                                        This is <em>your</em> intro paragraph. This is where you explain briefly what this article is going to be about. Hopefully it's quite enticing and interesting.

                                        Probably best to use Markdown here for formatting.

                                        <strong>Also note</strong> there are no super hard-and-fast rules for post formatting. If you want to do something usual or fancy or whatever, I'm typically down.


                                        <h3>Subheads are &lt;h3> tags</h3>

                                        They probably should be <code>&lt;h2></code> tags but again for legacy reasons they are not. h3's have a bunch of margin above them to set them apart. In the article code itself, put two line breaks above an h3.

                                        Breaking articles into sections with headers is a good idea. It just flows better and is easier to scan. It's probably best to put ID's on headers, but make them pretty obviously unique, like <code>id="article-namespace-section-2"</code>.

                                        <h4>Sub-subheads</h4>

                                        Sub-sections using <code>&lt;h4></code> tags. Like if you were going going to make a section all about the font property and then sub-heads for each of the sub-properties. Sub-sections should be related to the larger section they are a part of. Only one line break above an h4.

                                        <h4>Another sub-subhead</h4>

                                        These don't have as much space above or below them as the h3's. We only use h3's and h4's in articles (legacy reasons).

                                        <h3>The HTML (Example Section Title)</h3>A common thing when demonstrating a front end technique is to break into sections by language.html
                                        You can use Markdown code fences if you like.

                                        But then remove my JavaScript here on this Pen
                                        that escapes it all.
                                        ```

                                        Things to note about posting code blocks:

                                        <ul>
                                            <li>Do not escape HTML</li>
                                            <li>Don't worry about & ampersands. They will show up encoded in this preview but not in the article.</li>
                                            <li>Use two spaces for indenting</li>
                                            <li>HTML uses &lt;pre rel="HTML">&lt;code markup="tt" class="language-markup"></li>
                                            <li>CSS uses &lt;pre rel="CSS">&lt;code class="language-css"></li>
                                            <li>JS uses &lt;pre rel="jQuery">&lt;code class="language-javascript"></li>
                                            <li>For any JS library, replace the rel="" with that library name, like "jQuery"</li>
                                            <li>No extra lines at top or bottom of the code block</li>
                                        </ul>

                                        Use `backticks` for inline code. That way you won't have to escape them.

                                        <h3>The CSS (Example Header)</h3>

                                        <blockquote>To live is to love or something.</blockquote>

                                        <blockquote><p>This is the first paragraph of a multi-paragraph quote.</p>
                                            <p>And this is the second.</p>
                                        </blockquote>
                                    </em>
                                </div>
                                <div class="clear20"></div>
                                <div id="mcsc-gallery-history">
                                    <a data-lightbox="roadtrip" href="uploads/midias/slider/thumb-grid-01.jpg" title="Galeria de ">
                                        <img alt="" src = "uploads/midias/slider/thumb-grid-01.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/thumb-grid-02.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/001.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/thumb-grid-03.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/thumb-grid-04.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/thumb-grid-05.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/doctor.jpg" title=""/>
                                    </a>
                                    <a data-lightbox="roadtrip" href="" title="Galeria de ">
                                        <img alt="" src = "<?= BASE; ?>/uploads/midias/slider/002.jpg" title=""/>
                                    </a>
                                </div>
                            </div>
                            <div class="clear20"></div>
                            <div class="fb-comments" data-href="" data-width="100%" data-numposts="100"></div>
                        </div>
                        <div class="col-lg-4">
                            <div class="sideInfoImv">

                                <div class="clear20"></div>

                                <h1 class="font13px al-center centertable">ÚLTIMAS NOTÍCIAS</h1>
                                <hr class=""/>
                                <article class="col-lg-12 mAnd nomargin nopadding">
                                    <a href="" class="no-decoration onfade3 color-black-100">
                                        <img src="<?= BASE; ?>/tim.php?src=<?= BASE; ?>/uploads/midias/slider/001.jpg&w=600&h=300" alt="" title=""/>
                                        <span class="al-center centertable">
                                            Publicada em: <time class="" pubdate>22 de Setembro de 2016</time>
                                        </span>
                                        <h1 class="fontsize1 al-center color-dark-gray nomargin nopadding">
                                            TÍTULO DA NOTÍCIA
                                        </h1>
                                    </a>
                                    <div class="clear20"></div>
                                </article>
                                <article class="col-lg-12 mAnd nomargin nopadding">
                                    <a href="" class="no-decoration onfade3 color-black-100">
                                        <img src="<?= BASE; ?>/tim.php?src=<?= BASE; ?>/uploads/midias/slider/001.jpg&w=600&h=300" alt="" title=""/>
                                        <span class="al-center centertable">
                                            Publicada em: <time class="" pubdate>22 de Setembro de 2016</time>
                                        </span>
                                        <h1 class="fontsize1 al-center color-dark-gray nomargin nopadding">
                                            TÍTULO DA NOTÍCIA
                                        </h1>
                                    </a>
                                    <div class="clear20"></div>
                                </article>
                                <article class="col-lg-12 mAnd nomargin nopadding">
                                    <a href="" class="no-decoration onfade3 color-black-100">
                                        <img src="<?= BASE; ?>/tim.php?src=<?= BASE; ?>/uploads/midias/slider/001.jpg&w=600&h=300" alt="" title=""/>
                                        <span class="al-center centertable">
                                            Publicada em: <time class="" pubdate>22 de Setembro de 2016</time>
                                        </span>
                                        <h1 class="fontsize1 al-center color-dark-gray nomargin nopadding">
                                            TÍTULO DA NOTÍCIA
                                        </h1>
                                    </a>
                                    <div class="clear20"></div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="bg-light-gray-100 container-fluid fl-left footer-enable">
            <div class="atomwrapper">
                <div class="centralizar">
                    <a href="" class="header-logo">
                        <img src="<?= BASE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITETITLE; ?>" title="<?= SITETITLE; ?>">
                    </a>
                </div>
                <div class="clear10"></div>
                <p class="al-center centertable font18px">
                    Hospital São Marcos Para toda vida - Todos os direitos reservados 2016
                </p>
                <div class="clear10"></div>
                <ul class="main-footer-social">
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-feed"></i>
                        </a>
                    </li>
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a class="onfade4" href="" title="">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </footer>



        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/slick/slick.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/justified-gallery/jquery.justifiedGallery.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_libraries/lightbox2-master/lightbox2-master/dist/js/lightbox.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/ios6fix.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.swipe.min.js"></script>
        <script src="<?= BASE; ?>/_cdn/_front/_assets/_js/functions.js"></script>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
