<?php

class Medico
{
    private $baseApi = 'http://www.saomarcos.org.br/api/response.php';
    private $idClinica;
    private $idEspecialidades;
    private $action;
    private $params;
    private $result;

    function __construct($action)
    {
        $this->action = $action;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function exe()
    {
        $this->getJson();
    }

    public function getResult()
    {
        return $this->result;
    }

    private function getJson()
    {
        $data = array('api' => 'get_medicos', 'Content-Type: application/json');
        $url = "{$this->baseApi}?action={$this->action}&{$this->params}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);
        $this->result = $json;
    }

}