<?php
require '../_app/autoload.php';
require('../_app/Library/PHPMailer/class.phpmailer.php');


function clean_string($string)
{
    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
    return str_replace($bad, "", $string);
}


usleep(2000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);


$KeyId = (!empty($PostData['key_id'])) ? $PostData['key_id'] : null;

if ($PostData && $PostData['action']):
    $Case = $PostData['action'];
    $Read = new Read;
    $Create = new Create;

    //SELECIONA AÇÃO
    switch ($Case):
        case "listar-cursos":
            $Read->ExeRead('dx_cursos', "WHERE curso_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):
                $jSON["result"] = array('status' => 'error', 'data' => $post);
            else:
                $post = $Read->getResult()[0];
                $jSON["result"] = array('status' => 'success', 'data' => $post);
            endif;
            break;

        case "add-usuario":
            $Read->ExeCreate(DB_CURSOS, "WHERE curso_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):
                $jSON["result"] = array('status' => 'error', 'data' => $post);
            else:
                $post = $Read->getResult()[0];
                $jSON["result"] = array('status' => 'success', 'data' => $post);
            endif;
            break;

        case "get-user":
            $cpf = $PostData['cpf'];
            if (Check::CheckCPF($cpf)):

                $Read->ExeRead('dx_clientes', "WHERE cpf = :cpf", "cpf={$cpf}");
                if ($Read->getResult()):
                    $post = $Read->getResult()[0];
                    $jSON["result"] = array('status' => 'success', 'data' => $post);
                else:
                    $jSON["result"] = array('status' => 'error', 'data' => 'Você não está resgistrado, cadastre-se!');
                endif;
            else:
                $jSON["result"] = array('status' => 'error', 'data' => 'Digite um CPF válido!');
            endif;
            break;

        case "inscricao":


            $params = $PostData;

            /*print_r($params);
            die;*/

            $cpf = $params['inscricao_cpf'];

            if (Check::CheckCPF($cpf)):
                $Read->ExeRead('dx_clientes', "WHERE cpf = :cpf", "cpf={$cpf}");
                if (!$Read->getResult()):
                    $insert['nome'] = $params['inscricao_nome'];
                    $insert['cpf'] = $params['inscricao_cpf'];
                    $insert['email'] = $params['inscricao_email'];
                    $insert['telefone'] = $params['inscricao_fone'];
                    $insert['graduacao'] = $params['inscricao_graduacao'];
                    if (!empty($params['inscricao_aceito'])) {
                        $insert['aceito'] = $params['inscricao_aceito'];
                    } else {
                        $insert['aceito'] = 0;
                    }
                    unset($insert['inscricao_curso_id']);
                    $Create = new Create();
                    $Create->ExeCreate("dx_clientes", $insert);
                else:
                    $cliente_id = $Read->getResult()[0]['id'];
                    $DataUpdate['nome'] = $params['inscricao_nome'];
                    $DataUpdate['cpf'] = $params['inscricao_cpf'];
                    $DataUpdate['email'] = $params['inscricao_email'];
                    $DataUpdate['telefone'] = $params['inscricao_fone'];
                    $DataUpdate['graduacao'] = $params['inscricao_graduacao'];
                    if (!empty($params['inscricao_aceito'])) {
                        $DataUpdate['aceito'] = $params['inscricao_aceito'];
                    } else {
                        $DataUpdate['aceito'] = 0;
                    }

                    $Update = new Update();
                    $Update->ExeUpdate("dx_clientes", $DataUpdate, " WHERE id = :id ", "id={$cliente_id}");

                endif;

                $curso_id = $params['inscricao_curso_id'];
                $Read->ExeRead("dx_cursos", " WHERE curso_id=:id", "id={$curso_id}");
                $curso = $Read->getResult()[0];

                if ($curso['curso_valor'] > 0.00) {

                    $types = array(
                        'application/x-compressed',
                        'application/x-zip-compressed',
                        'application/zip',
                        'multipart/x-zip',
                        'application/octet-stream',
                        'image/jpeg',
                        'image/png',
                        'application/pdf',
                        'application/x-7z-compressed',

                    );
                    $maxSize = 1024 * 1024 * 2; // 2Mb
                    $File = null;

                    if (!empty($_FILES['arquivo'])):
                        $File = $_FILES['arquivo'];
                    endif;

                    $ext = substr($File['name'], -4);
                    $nome_final = md5(time()) . $ext;

                    if ($curso['aceita_pagseguro'] == 'S') {

                    } else if ($File) {
                        if (in_array($File['type'], $types)) {
                            if ($File['size'] <= $maxSize) {
                                if (move_uploaded_file($File['tmp_name'], '../uploads/files/' . $nome_final)) {
                                    $inscricao['inscricao_comprovante'] = $nome_final;
                                }
                            } else {
                                $jSON['status'] = 'error';
                                $jSON['msg'] = "Arquivo muito grande máximo 2mb";
                                echo json_encode($jSON);
                                die();
                            }
                        } else {
                            $jSON['status'] = 'error';
                            $jSON['msg'] = "arquivo inválido";
                            echo json_encode($jSON);
                            die();
                        }
                    } elseif ($curso['aceita_pagseguro'] == 'N') {

                        $jSON['status'] = 'error';
                        $jSON['msg'] = "Envie o comprovante de pagamento!";
                        echo json_encode($jSON);
                        die();
                    }
                }//verifica se o curso é pago

                // $curso = Admx::GetCampo("dx_cursos", "curso_title", " curso_id = '{$curso_id}'");
                // $curso_limite = Admx::GetCampo("dx_cursos", "curso_limite", " curso_id = '{$curso_id}'");

                $Read->FullRead("SELECT inscricao_cliente_cpf FROM dx_cursos_inscricao WHERE inscricao_cliente_cpf =:cpf and inscricao_curso_id =:curso LIMIT 1", "cpf={$cpf}&curso={$curso_id}");
                if ($Read->getResult()) {

                    $jSON['status'] = 'error';
                    $jSON['msg'] = "Esse CPF:{$cpf} já esta cadastrado nesse curso!";

                } else {

                    $Create = new Create();
                    $cliente_cpf = $cpf;
                    $inscricao['inscricao_cliente_cpf'] = $cliente_cpf;
                    $inscricao['inscricao_curso_id'] = $params['inscricao_curso_id'];


                    $Read->FullRead("SELECT count(inscricao_id) as total FROM dx_cursos_inscricao WHERE inscricao_curso_id =:id LIMIT 1", "id={$curso_id}");
                    $total_inscricao = 0;

                    if ($Read->getResult()) {
                        $total_inscricao = $Read->getResult()[0]['total'];
                    }

                    if ($total_inscricao < $curso['curso_limite']) {

                        $inscricao_numero = $curso_id . ($total_inscricao + 1);
                        $hash = md5(uniqid(rand(), true));
                        $inscricao['hash'] = $hash;
                        $inscricao['inscricao_numero'] = $inscricao_numero;
                        $Create->ExeCreate("dx_cursos_inscricao", $inscricao);

                        $cursos_inscricao_id = $Create->getResult();

                        $nome = $params['inscricao_nome'];
                        $cpf = $params['inscricao_cpf'];
                        $email = $params['inscricao_email'];
                        $telefone = $params['inscricao_fone'];
                        $graduacao = $params['inscricao_graduacao'];

                        $HTML = 'Inscrição para o curso ' . $curso['curso_title'] . "<br><br>";
                        $HTML .= "Nome: {$nome} <br>";
                        $HTML .= "CPF: {$cpf} <br>";
                        $HTML .= "Email: {$email} <br>";
                        $HTML .= "Telefone: {$telefone} <br>";
                        $HTML .= "Graduação: {$graduacao} <br><br>";

                        $link_pagamento = null;

                        if ($curso['aceita_pagseguro'] == 'S' && $curso['curso_valor'] > 0.00) {

                            $course = new stdClass();
                            $course->id = $curso_id;
                            $course->name = $curso['curso_title'];
                            $course->price = $curso['curso_valor'];

                            //require_once "./GeraPagseguro.php";

                            $link_pagamento = true;
                            $HTML .= $link_pagamento . "<br>";
                        }


                        if (AMBIENTE == 'cliente') {

                            $mail = new PHPMailer();
                            $mail->isSMTP();
                            $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
                            $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
                            $mail->Password = 'apcchsm$69';                           // SMTP password
                            $mail->Port = 25;
                            $mail->FromName = "Hospital São Marcos";
                            $mail->AddAddress('centrodepesquisa@saomarcos.org.br', 'Centro de Ensino e Pesquisa');
                            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                            $mail->Subject = "|| INSCRICAO DE CURSO HSM ||"; // Assunto da mensagem
                            $mail->Body = $HTML;

                            $enviado = $mail->Send();


                            $mail->ClearAddresses();

                            $mail = new PHPMailer();
                            $mail->isSMTP();
                            $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
                            $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
                            $mail->Password = 'apcchsm$69';                           // SMTP password
                            $mail->Port = 25;
                            $mail->FromName = 'Hospital Sao Marcos';
                            $mail->AddAddress(trim($email), $nome);
                            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                            $mail->Subject = "|| PRE-INSCRIÇÃO ||"; // Assunto da mensagem
                            $title = "PRÉ-INSCRICAO";
                            $texto = $nome . ", sua pré-inscrição foi realizada com sucesso! <br>Em breve, você receberá um e-mail confirmando sua inscrição.";
                            include("../templates/email-inscricao.php");
                            $mail->Body = $msg;
                            $mail->Send();
                        }

                        $jSON['status'] = 'success';
                        if (empty($link_pagamento))
                            $jSON['msg'] = '<strong>Sucesso!</strong> <br>Sua pré-inscrição foi realizada com sucesso! <br>Em breve, você receberá um e-mail confirmando sua inscrição.';
                        else $jSON['msg'] = '<strong>Sucesso!</strong> <br>Sua pré-inscrição foi realizada com sucesso! <br>Em breve, você receberá um link para pagamento da sua inscrição.';

                    } else {

                        $jSON['status'] = 'error';
                        $jSON['msg'] = "Este curso não tem mais vaga!";
                    }

                }
            else:
                $jSON['status'] = 'error';
                $jSON['msg'] = "Digite um CPF válido";
            endif;


            break;

        case "anexo":
            $read = new Read;
            $token = $PostData['token'];
            $read->ExeRead("dx_cursos_inscricao", "WHERE hash =:t", "t={$token}");
            if ($read->getResult()) {
                $types = array(
                    'application/x-compressed',
                    'application/x-zip-compressed',
                    'application/zip',
                    'multipart/x-zip',
                    'application/octet-stream',
                    'image/jpeg',
                    'image/png',
                    'application/pdf',
                    'application/x-7z-compressed',

                );
                $maxSize = 1024 * 1024 * 2; // 2Mb
                $File = null;

                if (!empty($_FILES['arquivo'])):
                    $File = $_FILES['arquivo'];
                endif;
                $ext = substr($File['name'], -4);
                $nome_final = md5(time()) . $ext;
                if ($File) {
                    if (in_array($File['type'], $types)) {
                        if ($File['size'] <= $maxSize) {
                            if (move_uploaded_file($File['tmp_name'], '../uploads/files/' . $nome_final)) {
                                $Data['inscricao_comprovante'] = $nome_final;
                            }
                        } else {
                            $jSON['status'] = 'error';
                            $jSON['msg'] = "Arquivo muito grande máximo 2mb";
                            echo json_encode($jSON);
                            die();
                        }
                    } else {
                        $jSON['status'] = 'error';
                        $jSON['msg'] = "arquivo inválido";
                        echo json_encode($jSON);
                        die();
                    }
                } else {
                    $jSON['status'] = 'error';
                    $jSON['msg'] = "Envie o comprovante de pagamento!";
                    echo json_encode($jSON);
                    die();
                }

                $inscricao = $read->getResult()[0];
                $KeyId = $inscricao['inscricao_id'];
                $Update = new Update;
                $Data['status'] = 0;
                $Update->ExeUpdate("dx_cursos_inscricao", $Data, " WHERE inscricao_id = :id ", "id={$KeyId}");
                $jSON['status'] = 'success';
                $jSON['msg'] = '<strong>Sucesso!</strong> <br>Sua pré-inscrição foi realizada! Em breve você recebera um email confirmando sua inscrição.';

            } else {
                $jSON['status'] = 'error';
                $jSON['msg'] = "Error: token inválido!";
                echo json_encode($jSON);
                die();
            }


            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;