<header class="container-fluid">
    <div class="atomwrapper">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h1 class="fl-left fadezero fontzero nomargin nopadding">
                    <?= SITE_NAME; ?>
                </h1>
                <a href="" class="header-logo">
                    <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>"
                         title="<?= SITE_NAME; ?>">
                </a>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="hintxt">
                    <div class="hinfo-icon">
                        <span class="fa fa-map-marker"></span>
                    </div>
                    <div class="hinfo-text">
                        <h2 class="nomargin nopadding text-bold color-red-300">NOSSA LOCALIZAÇÃO</h2>
                        <p>Rua Olavo Bilac, 2300. Teresina-PI</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="hintxt">
                    <div class="hinfo-icon">
                        <span class="fa fa-coffee"></span>
                    </div>
                    <div class="hinfo-text">
                        <h2 class="nomargin nopadding text-bold color-red-300">HORÁRIOS DE ATENDIMENTO</h2>
                        <p>Aberto 24 horas</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">

                <div class="h-actions fl-right zd100">
                    <a href="#search" title="" class="h-act-search onfade4">
                        <i class="fa fa-search"></i>
                        <span>BUSCAR</span>
                    </a>
                    <div id="search">
                        <button type="button" class="close">x</button>
                        <form action="javascript:;">
                            <input type="search" autocomplete="off" id="campoBusca" value=""
                                   placeholder="O que você está procurando?"/>
                            <button type="button" onclick="search()" class="btn-default-hsm btn bg-red-300 onfade4">
                                Buscar no Hospital
                            </button>
                        </form>
                        <script>
                            function search() {
                                s = $("#campoBusca").val().replace(/[\\"]/g, '');
                                s = s.replace(/[\\"]/g, '');
                                s = s.replace(/[\\']/g, '');

                                if (s != null) {
                                    location.href = '<?=BASE?>/pesquisa/' + s;
                                }
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php include "menu.php";