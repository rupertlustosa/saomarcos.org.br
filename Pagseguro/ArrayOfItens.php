<?php
/**
 * Created by PhpStorm.
 * User: rupert
 * Date: 22/04/17
 * Time: 19:33
 */

namespace Pagseguro;


class ArrayOfItens extends \ArrayObject
{
    /**
     * @return array
     */
    public $itens;

    /**
     * @return mixed
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item)
    {
        $this->itens[] = $item;
    }
}