<?php
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (!empty($KeyId)):
    $Read->FullRead("SELECT * FROM " . DB_CURRICULOS . " WHERE vaga_id =:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];

    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
else:
    //header("Location: " . ADM_URL . "?dx=error/404");
endif;


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Currículos</h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Vaga: <?= Admx::GetCampo(DB_VAGAS, "vaga", " id = {$KeyId}") ?></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- start project list -->
                        <table id="datatable" class="table table-striped table-bordered">
                            <!--<table class="table table-striped projects">-->
                            <thead>
                            <tr>
                                <th>#Candidato</th>
                                <!--<th>#Email</th>-->
                                <th style="width: 150px">#Telefone</th>
                                <th style="width: 150px">#Data</th>
                                <th style="width: 120px">#Ações</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php

                            $formToken = Admx::setFormToken(false);
                            $Read->FullRead("
                                    SELECT *
                                    FROM " . DB_CURRICULOS . "
                                    WHERE vaga_id = :id
                                    ORDER BY created DESC ", "id={$KeyId}");
                            foreach ($Read->getResult() as $ROW):
                                extract($ROW);

                                ?>
                                <tr>
                                    <td>
                                        <em><strong>Candidato:</strong> <?= $nome ?></em><br/>
                                        <em><strong>E-mail:</strong> <?= $email ?></em><br/>
                                        <em><strong>Mensagem:</strong> <?= $mensagem ?></em><br/>
                                    </td>
                                    <!--<td><?/*= $email */
                                    ?></td>-->
                                    <td><?= $telefone ?></td>
                                    <td><?php echo (new DateTime($created))->format('d/m/Y H:i:s'); ?></td>
                                    <td>
                                        <a target="_blank" class="btn btn-info btn-xs"
                                           href="<?= BASE ?>/uploads/<?= $curriculo ?>">Visualizar</a>
                                        <?php
                                        if (!empty($linkedin)) {
                                            ?>
                                            <a target="_blank" class="btn btn-info btn-xs" href="<?= $linkedin ?>">Linkedin</a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td
                                    <td colspan="4"><?/*= $mensagem */
                                ?></td>
                                </tr>-->
                                <?php
                            endforeach;

                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php


include './_inc/footer.php';
?>
