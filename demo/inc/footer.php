<footer class="bg-light-gray-100 container-fluid fl-left footer-enable">
        <div class="atomwrapper">
            <div class="centralizar">
                <a href="" class="header-logo">
                    <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>" title="<?= SITE_NAME; ?>">
                </a>
            </div>
            <div class="clear10"></div>
            <p class="al-center centertable font18px">
                <?=SITE_NAME ." " .SITE_SUBNAME?> - Todos os direitos reservados <?=date('Y')?>
            </p>
            <div class="clear10"></div>
            <ul class="main-footer-social">
                <li>
                    <a class="onfade4" href="" title="">
                        <i class="fa fa-feed"></i>
                    </a>
                </li>
                <li>
                    <a class="onfade4" href="" title="">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="onfade4" href="" title="">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>
        </div>

    </footer>


<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/ios6fix.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.swipe.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/functions.js"></script>
