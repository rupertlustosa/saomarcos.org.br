var roxyFileman = '../_cdn/ckeditor/plugins/fileman/index.html';
$(function () {
    $('.dx_editor').ckeditor({
        filebrowserBrowseUrl: roxyFileman,
        filebrowserImageBrowseUrl: roxyFileman + '?type=image',
        removeDialogTabs: 'link:upload;image:upload',
        toolbarGroups: [
            {"name": "document", "groups": ["mode"]},
            {"name": "basicstyles", "groups": ["basicstyles"]},
            {"name": "links", "groups": ["links"]},
            {"name": "paragraph", "groups": ["list", "blocks"]},
            {"name": "insert", "groups": ["insert"]},
            {"name": "styles", "groups": ["styles"]},
        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    })
});