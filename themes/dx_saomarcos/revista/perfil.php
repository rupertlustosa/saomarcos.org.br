<form method="post">
    <?php
    $msg = null;
    $PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $autor = Users::GetAutorArtigo();
    if ($autor) {
        if ($PostData) {
            $id = $autor['cliente_id'];
            $Update = new Update;
            $data = null;
            $error = 0;
            $confirme_senha = $PostData['cliente_confirm_senha'];
            unset($PostData['cliente_confirm_senha']);
            foreach ($PostData as $key => $value) {
                $fil = str_replace("cliente_", "", $key);
                $data[$fil] = $value;
            }
            if (empty($data['senha'])) {
                unset($data['senha']);
            } else {
                if ($data['senha'] != $confirme_senha) {
                    $error++;
                    $msg = '<div class="alert alert-danger " role="alert">As Senhas não são iguais!</div>';
                } else {
                    $data['senha'] = Admx::encripta($data['senha']);
                }
            }
            if ($error == 0) {
                $Update->ExeUpdate("dx_clientes", $data, "WHERE id = :id", "id={$id}");
                $msg = '<div class="alert alert-success " role="alert">Perfil atualizado com sucesso!</div>';
            }

            extract($PostData);

        } else {
            extract($autor);
        }
        include("form_cliente.php");
    }
    ?>
</form>