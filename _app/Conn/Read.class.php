<?php
ini_set("memory_limit", "1024M");

/**
 * <b>Read.class</b>
 * Classe responsável pelas leituras genéricos no banco de dados
 * @copyright (c) 2015, João Manoel
 */
class Read extends Conn
{

    private $Select;
    private $Places;
    private $Result;

    /** @var PDOStatement */
    private $Read;

    /** @var PDO */
    private $Conn;

    public function ExeRead($Tabela, $Termos = null, $ParseString = null)
    {
        if (!empty($ParseString)):
            parse_str($ParseString, $this->Places);
        endif;

        $this->Select = "SELECT * FROM {$Tabela} {$Termos}";
        $this->Execute();
    }

    public function ExeCampo($Tabela, $Campo, $Termos = null)
    {
        $this->Select = "SELECT {$Campo} FROM {$Tabela} {$Termos}";
        $this->Connect();
        try {
            $this->getSyntax();
            $this->Read->execute();
            $fetAll = $this->Read->fetchAll();

            if (!empty($fetAll)) {
                return $fetAll[0][$Campo];
            } else {
                return null;
            }
        } catch (PDOException $e) {
            return null;
            AdxErro("<strong>Erro ao ler:</strong> {$e->getMessage()}", $e->getCode());
        }
    }

    public function getResult()
    {
        return $this->Result;
    }

    public function getRowCount()
    {
        return $this->Read->rowCount();
    }

    public function FullRead($Query, $ParseString = null)
    {

        $this->Select = (string)$Query;
        if (!empty($ParseString)):
            parse_str($ParseString, $this->Places);
        endif;
        $this->Execute();
    }

    public function setPlaces($ParseString)
    {
        parse_str($ParseString, $this->Places);
        $this->Execute();
    }

    /*
     *  ****************************************
     *  *********** PRIVATE MÉTODOS ************
     *  ****************************************
     */

    //Obtém o PDO e prepara a query
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Read = $this->Conn->prepare($this->Select);
        $this->Read->setFetchMode(PDO::FETCH_ASSOC);
    }

    //Cria uma sintaxe da query para Prepared Statements
    private function getSyntax()
    {
        if ($this->Places):
            foreach ($this->Places as $Vinculo => $Valor):
                if ($Vinculo == 'limit' || $Vinculo == 'offset'):
                    $Valor = (int)$Valor;
                endif;
                $this->Read->bindValue(":{$Vinculo}", $Valor, (is_int($Valor) ? PDO::PARAM_INT : PDO::PARAM_STR));
            endforeach;
        endif;
    }

    //Obtém a Conexão e a Sintaxe, e executa a query
    private function Execute()
    {
        $this->Connect();
        try {
            $this->getSyntax();
            $this->Read->execute();
            $this->Result = $this->Read->fetchAll();
        } catch (PDOException $e) {
            $this->Result = null;
            AdxErro("<strong>Erro ao ler:</strong> {$e->getMessage()}", $e->getCode());
        }
    }

}
