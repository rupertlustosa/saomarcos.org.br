<?php

$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {

    $create = new Create();
    $dados['titulo'] = '';
    $dados['image'] = '';
    $dados['conteudo'] = '';
    $dados['trash'] = 0;
    $dados['status'] = 0;
    $create->ExeCreate(DB_COMITE_ETICA, $dados);

    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=".DB_COMITE_ETICA."/create&id=" . $newId);
} else {

    $Read->FullRead("SELECT * FROM " . DB_COMITE_ETICA . " WHERE id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {

        $itemDb = $Read->getResult()[0];

    } else {

        die('REGISTRO NÃO ENCONTRADO!');
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/banners/banners.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar</h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>

                            <div class="col-md-8">

                                <div>
                                    <?php
                                    if (file_exists("../uploads/" . $itemDb['image']) && !is_dir("../uploads/" . $itemDb['image'])) {
                                        echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $itemDb['image'] . '&w=900" />';
                                    } else {
                                        echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=600" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                    }
                                    ?>

                                </div>
                                <div class="clear10"></div>

                                <div class="form-group">
                                    <label>Imagem</label>
                                    <div class="upload_file" style="border:1px solid #ccc">
                                        <input class="loadimage" type="file" name="image"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="titulo" id="titulo" value="<?= $itemDb['titulo'] ?>"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Conteúdo</label>
                                    <textarea class="dx_editor" id="conteudo"
                                              name="conteudo"><?= $itemDb['conteudo'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <div style="display: none">
                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">
                                    <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                    <img src="_img/load.gif" class="loading_form pull-right">
                                    </div>
                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar
                                    </button>&nbsp;
                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>
