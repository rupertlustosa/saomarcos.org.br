$(function () {
    $('a[href="#search"]').on('click', function (event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function (event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });

    $('.h-act-links').click(function () {
        var target = $(this).data('menu');

        $('#' + target).toggleClass('menu--open');
    });

    var progress = $('#progress'),
            slideshow = $('.cycle-slideshow');

    slideshow.on('cycle-initialized cycle-before', function (e, opts) {
        progress.stop(true).css('width', 0);
    });

    slideshow.on('cycle-initialized cycle-after', function (e, opts) {
        if (!slideshow.is('.cycle-paused'))
            progress.animate({width: '100%'}, opts.timeout, 'linear');
    });

    slideshow.on('cycle-paused', function (e, opts) {
        progress.stop();
    });

    slideshow.on('cycle-resumed', function (e, opts, timeoutRemaining) {
        progress.animate({width: '100%'}, timeoutRemaining, 'linear');
    });

    $('.MainCarousel').slick({
        infinite: true,
        slidesToShow: 6,
        dots: true,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.MainNews').slick({
        infinite: true,
        slidesToShow: 3,
        dots: true,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $("#mcsc-gallery-history").justifiedGallery({
        rowHeight: 200,
        lastRow: 'justify',
        captions: false
    });
    $('a[data-rel^=lightcase]').lightcase({
        swipe: true
    });



});