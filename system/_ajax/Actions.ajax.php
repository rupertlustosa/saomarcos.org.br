<?php

require '../../_app/autoload.php';

usleep(20000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$CallBack = 'Actions';
$Table = $PostData['table'];
$key_name = $PostData['key_name'];
$KeyId = $PostData['key_id'];
$Value = !empty($PostData['value']) ? $PostData['value'] : 0;

$jSON['error'] = null;
$formToken = Admx::getFormToken();
$postToken = !empty($PostData['formToken']) ? $PostData['formToken'] : null;
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];
    unset(
        $PostData['callback'], $PostData['callback_action'], $PostData['table'], $PostData['key_id'], $PostData['key_name']
    );
    if (!empty($PostData['status'])) {
        unset($PostData['status']);
    }
    if (!empty($PostData['trash'])) {
        unset($PostData['trash']);
    }
    if (!empty($PostData['value'])) {
        unset($PostData['value']);
    }


    $Read = new Read;

    $Update = new Update;
    $Delete = new Delete;

    //SELECIONA AÇÃO
    switch ($Case):
        case 'status':
            $Read->FullRead("SELECT * FROM {$Table} WHERE {$key_name}=:id", "id={$KeyId}");
            if ($Read->getResult()):
                $Dados['status'] = $Value;
                $Update->ExeUpdate($Table, $Dados, " WHERE {$key_name}=:id", "id={$KeyId}");
                if ($Value == 1) {
                    $success = 'ativo';
                } else {
                    $success = 'inativo';
                }
                $jSON["success"] = $success;
            else:
                $jSON['error'] = 'ok';
            endif;
            break;

        case 'trash':
            $Read->FullRead("SELECT * FROM {$Table} WHERE {$key_name}=:id", "id={$KeyId}");
            if ($Read->getResult()):
                $Dados['trash'] = $Value;
                $Update->ExeUpdate($Table, $Dados, " WHERE {$key_name}=:id", "id={$KeyId}");
                $jSON["success"] = 'ok';
            else:
                $jSON['error'] = 'ok';
            endif;
            break;

        case 'delete':
            if ($formToken == $postToken):
                $Read->FullRead("SELECT * FROM  {$Table} WHERE {$key_name}=:id", "id={$KeyId}");
                if ($Read->getResult()):
                    if (Session::getUserLevel() <= 6) {
                        if (Session::checkPermission(18)) {
                            $Delete->ExeDelete($Table, " WHERE {$key_name}=:id", "id={$KeyId}");
                            $jSON["success"] = 'ok';
                        } else {
                            $jSON["result"] = "error";
                        }
                    } else {
                        $Delete->ExeDelete($Table, " WHERE {$key_name}=:id", "id={$KeyId}");
                        $jSON["success"] = 'ok';
                    }

                else:
                    $jSON["result"] = "error";
                endif;
            else:
                $jSON["result"] = "error";
            endif;
            break;

        case 'campos':
            //$PostData
            foreach ($PostData as $meta => $val) {
                $Dados['campo_value'] = $val;
                $where = " WHERE campo_table='{$Table}' AND campo_parent_id ='{$KeyId}' AND  campo_key=:key ";
                $Update->ExeUpdate('dx_campos', $Dados, $where, "key={$meta}");
                unset($Dados);
            }
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
