$(function () {

});

function get_profissionais() {
    var base = $('link[rel="base"]').attr('href');
    clinica = $("#clinica option:selected").val();
    especialidade = $("#especialidade option:selected").val();

    $.post(base + "/processador.php", {
        action: 'get_profissionais',
        especialidade: especialidade,
        clinica: clinica
    }, function (res) {
        $("#profissionais").html(res);
    });
}

function goToProfissional(link) {
    if (link != 0) {
        location.href = link;
    } else {
        alert("Escolha um profissional");
    }
}

function enviando() {
    removeClassAlert();
    $(".alert")
        .addClass('alert-warning')
        .html('Enviando...')
        .show();
}

function success() {
    removeClassAlert();
    $(".alert")
        .addClass('alert-success')
        .html('Formulário enviado com sucesso!')
        .show();
}

function error() {
    removeClassAlert();
    $(".alert")
        .addClass('alert-danger')
        .html('Erro ao enviar tente novamente mais tarde!')
        .show();
}

function removeClassAlert() {
    $(".alert").removeClass("alert-success");
    $(".alert").removeClass("alert-info");
    $(".alert").removeClass("alert-warning");
    $(".alert").removeClass("alert-danger");
}
