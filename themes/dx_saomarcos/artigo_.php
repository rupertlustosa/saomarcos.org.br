<?php
include "inc/header.php";
extract($DataResult);
?>

    <section class="content-section bg-light-brown">
        <div class="container-fluid">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="contentInfoImv">
                            <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $post_image ?>&w=800&h=500" alt="" title=""/>
                            <div class="clear10"></div>
                            <h1 class="fontsize2 cavbold al-center tuppercase centertable">
                                <?= $post_title ?>
                            </h1>
                            <span class="al-center centertable nolanbook">
                                    <?php
                                    $noticiaData = new Date($post_date);
                                    ?>
                                Publicada em: <time class=""
                                                    pubdate><?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></time>
                                <?php
                                $noticiaData = null;
                                ?>
                                </span>
                            <div class="clear10"></div>
                            <div class="container-line">
                                <div class="sharethis-inline-share-buttons"></div>
                                <!--<div class="msn">
                                        <ul class="main_share_command_news">
                                            <li class="facebook">
                                                <a href="" title="Compartilhe no Facebook" class="radius1b fb_h">
                                                    <i class="fa fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li class="google">
                                                <a href="" title="Recomende no Google+" class="radius1b gp_h">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li class="twitter">
                                                <a href="" title="" rel="" class="radius1b tt_h">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="gps" href="whatsapp://send?text=<?= BASE; ?>/artigo/<?= $post_name ?>" data-action="share/whatsapp/share">
                                                    <span class="fa fa-whatsapp"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->

                            </div>
                            <div class="contentMsg width90">
                                <?= $post_content ?>
                            </div>
                            <div class="clear20"></div>

                            <div class="atomwrapper" style="width: 90%;">
                                <div class="artigo-gallery">
                                    <?php
                                    $galery = Site::getGalley(2, 3);
                                    if ($galery):
                                        foreach ($galery as $img) {
                                            ?>
                                            <a data-lightbox="roadtrip"
                                               href="<?= BASE . "/uploads/" . $img['file_src'] ?>"
                                               title="<?= $img['file_name'] ?> ">
                                                <img alt="<?= $img['file_name'] ?>"
                                                     src="<?= BASE . "/tim.php?src=/uploads/" . $img['file_src'] . "&w=220&h=140" ?>"
                                                     title="<?= $img['file_name'] ?>"/>
                                            </a>
                                            <?php
                                        }
                                    endif;
                                    ?>
                                </div>
                                <script>
                                    $(function () {
                                        $('.artigo-gallery').slick({
                                            infinite: true,
                                            slidesToShow: 3,
                                            slidesToScroll: 3
                                        });
                                    })
                                </script>
                            </div>

                        </div>
                        <div class="clear20"></div>

                    </div>
                    <div class="col-lg-4">
                        <div class="sideInfoImv">

                            <div class="clear20"></div>

                            <h1 class="font13px al-center centertable">ÚLTIMAS NOTÍCIAS</h1>
                            <hr class=""/>

                            <?php
                            $outrasNoticias = new Read();
                            $where = Admx::Trash();
                            $outrasNoticias->ExeRead(DB_POSTS . " WHERE 1=1 {$where} AND status = 1 AND  post_id != '$post_id' ORDER BY post_date DESC LIMIT 5");
                            foreach ($outrasNoticias->getResult() as $row):
                                $noticiaData = new Date($row['post_date']);
                                ?>
                                <article class="col-lg-12 mAnd nomargin nopadding">
                                    <a href="<?= BASE . "/" . $row['post_name'] ?>" title="<?= $row['post_title'] ?>"
                                       class="no-decoration onfade3 color-black-100">
                                        <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $row['post_image'] ?>&w=600&h=200"
                                             alt="<?= $row['post_title'] ?>" title="<?= $row['post_title'] ?>"/>
                                        <span class="al-center centertable">
                                            Publicada em: <time class=""
                                                                pubdate><?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></time>
                                        </span>
                                        <h1 class="fontsize1 al-center color-dark-gray nomargin nopadding">
                                            <?= $row['post_title'] ?>
                                        </h1>
                                    </a>
                                    <div class="clear20"></div>
                                </article>
                                <?php
                            endforeach;
                            ?>
                            <div class="clear"></div>
                            <hr class=""/>
                            <a href="<?= BASE ?>/noticias" class="link_mais_noticias">Mais Notícias</a>
                            <div class="clear10"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include "inc/footer.php";
?>