<?php
extract($DataResult);
include "inc/header.php" ?>
    <!-- Our team Section -->
    <style type="text/css">
        .revista {
            list-style: none;
            padding: 0px;
            margin: 20px
        }

        .revista li {
            display: table;
            width: 100%;
        }

        .revista li h3 {
            border-bottom: solid 1px #fff;
            background-color: #b10d1d;
            padding: 10px;
            margin: 0px;
            color: #fff;
            font-size: 20px;
            cursor: pointer;
        }

        .revista-content {
            padding: 20px;
            text-align: left;
            background-color: #f2f2f2;
            font-size: 15px;
            display: none
        }

        .revista-content * {
            text-align: left;
        }

        .padding-20 {
            padding: 20px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".revista li h3").click(function () {
                li = $(this).parents('li');
                content = $(li).find('div');
                $('.revista-content').hide();
                $(content).show();
                $('.response').hide();
                $('html,body').animate({scrollTop: $(li).offset().top}, 200);
            })
        })

    </script>
    <section class="team content-section bg-light-brown">
        <div class="clear40"></div>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2><?= $revista_title ?></h2>
                    <div class="clear20"></div>
                    <?php
                    if ($revista_image) {
                        echo '<img src="' . BASE . '/uploads/' . $revista_image . '">';
                    }
                    ?>
                    <div class="clear20"></div>


                    <div class="clear20"></div>
                </div><!-- /.col-md-12 -->
                <div class="col-md-12">
                    <ul class="revista">
                        <li>
                            <h3>Guia do Autor</h3>
                            <div class="revista-content">
                                <?= $revista_autor ?>
                            </div>
                        </li>
                        <li>
                            <h3>Editorial</h3>
                            <div class="revista-content">
                                <?= $revista_editorial ?>
                            </div>
                        </li>
                        <li>
                            <h3>Submissão dos Artigos</h3>
                            <div class="revista-content">
                                <?= $revista_submissao ?>
                            </div>

                            <div class="revista-content">
                                <?php include __DIR__ . "/revista-medica-envio-arquivo.php"; ?>
                            </div>
                        </li>
                        <li>
                            <h3>Aceite dos Artigos</h3>
                            <div class="revista-content">
                                <?= $revista_aceite ?>
                            </div>
                        </li>
                        <li>
                            <h3>Estrutura do Artigo</h3>
                            <div class="revista-content">
                                <?= $revista_estrutura ?>
                            </div>
                        </li>
                        <li>
                            <h3>Edições Lançadas</h3>
                            <div class="revista-content">

                                <?php

                                $read3 = new Read();
                                $read3->FullRead("SELECT * FROM rl_revista_edicoes WHERE rl_revista_edicoes.`status` = 1 AND rl_revista_edicoes.trash = 0;");

                                if ($read3->getResult()) {

                                    foreach ($read3->getResult() as $indice => $item) {

                                        $noticiaData = new Date($item['created']);
                                        ?>
                                        <article class="postInner">
                                            <a href="<?= $item['url'] ?>" target="_blank">
                                                <div class="postImg">
                                                    <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $item['image'] ?>&w=400"/>
                                                </div>
                                                <h2 class="color-red-300 fontsize1b"><?= $item['nome']?></h2>
                                                <span class="tuppercase">Lançada em: <?= $item['data_lancamento']?></span>
                                                <p><?= $item['url'] ?></p>
                                            </a>
                                        </article>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear40"></div>
    </section><!-- /.our-team -->
<?php include "inc/footer.php" ?>