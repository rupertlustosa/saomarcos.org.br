<?php

/**
 * <b>Create.class</b>
 * Classe responsável pelos cadastrados genéricos no banco de dados
 * @copyright (c) 2015, João Manoel
 */
class Create extends Conn {

    private $Tabela;
    private $Dados;
    private $Result;

    /** @var PDOStatement */
    private $Create;

    /** @var PDO */
    private $Conn;

    /**
     * <b>ExeCreate: </b> Executa um cadastro simplificado no banco de dados utilizando o prepared statements
     * Basta informar o nome da tabela e um array atribuitivo com o nome da coluna e o seu valor.
     * @param STRING $Tabela = Informe o nome da tabela no banco
     * @param ARRAY $Dados = Informe um array atribuitivo. (Nome da coluna => Valor)
     */
    public function ExeCreate($Tabela, array $Dados) {
        $this->Tabela = (string) $Tabela;
        $this->Dados = $Dados;

        $this->getSyntax();
        $this->Execute();

    }

    /**
     * <b>getResult: </b> Retorna no resultado um false ou o último resultado inserido na tabela
     * Caso seja executado com sucesso
     * @return Resultado
     */
    public function getResult() {
        return $this->Result;
    }

    /*
     *  ****************************************
     *  *********** PRIVATE MÉTODOS ************
     *  ****************************************
     */

    //Obtém o PDO e prepara a query
    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Create = $this->Conn->prepare($this->Create);
    }

    //Cria uma sintaxe da query para Prepared Statements
    private function getSyntax() {
        $Fields = implode(', ', array_keys($this->Dados));
        $Places = ':' . implode(', :', array_keys($this->Dados));
        $this->Create = "INSERT INTO {$this->Tabela} ({$Fields}) VALUES ({$Places})";

    }

    //Obtém a Conexão e a Sintaxe, e executa a query
    private function Execute() {
        $this->Connect();
        try {
            $this->Create->execute($this->Dados);
            $this->Result = $this->Conn->lastInsertId();
        } catch (PDOException $e) {
            $this->Result = null;
            AdxErro("<strong>Erro ao cadastrar:</strong> {$e->getMessage()}", $e->getCode());
        }
    }


}
