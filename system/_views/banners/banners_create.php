<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['status'] = 0;
    $dados['views'] = 0;
    $dados['trash'] = 0;
    $create->ExeCreate(DB_BANNERS, $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=banners/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM " . DB_BANNERS . " WHERE banner_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/banners/banners.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE_SINGLE'] ?>
                    <small> <?= $Post['banner_title'] ?></small>
                </h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                            <?php Admx::setFormToken() ?>
                            <div class="col-md-8">
                                <div>
                                    <?php
                                    if (file_exists("../uploads/" . $Post['banner_image']) && !is_dir("../uploads/" . $Post['banner_image'])) {
                                        echo '<img class="image image_default pull-right" alt="Imagem de Destaque" title="Imagem de Destaque" src="../tim.php?src=uploads/' . $Post['banner_image'] . '&w=600&h=190" />';
                                    } else {
                                        echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=600&h=190" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                    }
                                    ?>

                                </div>
                                <div class="clear10"></div>

                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="banner_title" value="<?= $Post['banner_title'] ?>"
                                           placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Subtitle</label>
                                    <input type="text" name="banner_subtitle" value="<?= $Post['banner_subtitle'] ?>"
                                           placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Link</label>
                                    <input type="text" name="banner_link" value="<?= $Post['banner_link'] ?>"
                                           placeholder="Digite o link do <?= $TABLE['TITLE_SINGLE'] ?> "
                                           class="form-control">
                                </div>

                            </div>
                            <div class="col-md-4" style="padding-left:20px">
                                <div class="form-group">
                                    <label>Imagem</label>
                                    <div class="upload_file" style="border:1px solid #ccc"><input class="loadimage"
                                                                                                  type="file"
                                                                                                  name="image"/></div>
                                </div>
                                <div class="form-group">
                                    <div class="xdisplay_inputx form-group has-feedback" style="width:100%">
                                        <label>Data Entrada</label>
                                        <input type="text" name="banner_dataentrada"
                                               value="<?= ($Post['banner_dataentrada'] == '0000-00-00 00:00:00' || empty($Post['banner_dataentrada'])) ? date('d/m/Y') : Check::DateDecode($Post['banner_dataentrada']) ?>"
                                               style="padding-left:55px;" placeholder="Data" id="single_cal3"
                                               class="inputDatepicker form-control has-feedback-left active">
                                        <span aria-hidden="true" style="margin-top:5px;"
                                              class="fa fa-calendar-o form-control-feedback left"></span>
                                        <span class="sr-only" id="inputSuccess2Status3">(success)</span>
                                    </div>
                                    <div class="xdisplay_inputx form-group has-feedback" style="width:100%">
                                        <label>Data Saída</label>
                                        <input type="text" name="banner_datasaida"
                                               value="<?= ($Post['banner_datasaida'] == '0000-00-00 00:00:00' || empty($Post['banner_datasaida'])) ? date('d/m/Y') : Check::DateDecode($Post['banner_datasaida']) ?>"
                                               style="padding-left:55px;" placeholder="Data" id="single_cal3"
                                               class="inputDatepicker form-control has-feedback-left active">
                                        <span aria-hidden="true" style="margin-top:5px;"
                                              class="fa fa-calendar-o form-control-feedback left"></span>
                                        <span class="sr-only" id="inputSuccess2Status3">(success)</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Local</label>
                                    <select name="banner_local" class="form-control">
                                        <?php
                                        foreach (getBannersLocal() as $key => $bnr) {
                                            echo "<option value='{$key}'>{$bnr['local']} - {$bnr['largura']}/{$bnr['altura']}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="clear20"></div>
                                <div class="form-group">
                                    <div class="checkbox pull-left">
                                        <label>
                                            <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                        </label>
                                    </div>
                                    <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                    <input type="hidden" name="callback_action" value="manager">
                                    <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                    <img src="_img/load.gif" class="loading_form pull-right">

                                    <button class="btn btn-success pull-right" type="submit">
                                        Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                </div>

                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div><!-- content do formulario -->
                </div><!-- painel do formulario -->

            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>
