<?php

require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'menus';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$jSON['error'] = null;
$jSON["redirect"] = null;
$CallBack = $TABLE['CALLBACK'];

$formToke = Admx::getFormToken();
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack && $PostData['formToken'] == $formToke):
    $Case = $PostData['callback_action'];
    $menu_last = (!empty($PostData['menu_last'])) ? $PostData['menu_last'] : null;
    $Status = (!empty($PostData['status'])) ? $PostData['status'] : 0;
    $parent = (!empty($PostData['menu_parent'])) ? $PostData['menu_parent'] : null;
    $KeyId = (!empty($PostData['key_id'])) ? $PostData['key_id'] : null;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['menu_last']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            $menu_order = $PostData['menu_order'];
            if ($parent) {
                $condParent = " AND menu_parent = '$parent' ";
            } else {
                $PostData['menu_parent'] == null;
                $condParent = null;
            }

            $Read->ExeRead(DB_MENUS, "WHERE menu_order = :ord {$condParent} AND menu_id != :mn ", "ord={$menu_order}&mn={$KeyId}");
            if ($Read->getResult()):
                $menu_up_id = $Read->getResult()[0]['menu_id'];
                $UpMenu['menu_order'] = $menu_last;
                $Update->ExeUpdate(DB_MENUS, $UpMenu, "WHERE menu_id = :id", "id={$menu_up_id}");
            endif;

            $name = !empty($PostData['menu_title']) ? Check::Name($PostData['menu_title']) : $KeyId;

            $ReadName = new $Read;
            $ReadName->FullRead("SELECT menu_name FROM " . DB_MENUS . " WHERE menu_name= :nm and menu_id != :id ", "nm={$name}&id={$KeyId}");
            if ($ReadName->getResult()):
                $PostData['menu_name'] = $name . time();
            else:
                $PostData['menu_name'] = $name;
            endif;
            $PostData['status'] = $Status;
            if ($KeyId) {
                $Update->ExeUpdate(DB_MENUS, $PostData, "WHERE menu_id = :id", "id={$KeyId}");
            } else {
                $Create->ExeCreate(DB_MENUS, $PostData);
            }

            $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
            $jSON["redirect"] = ADM_URL . "/?dx=menus/home";
            break;
        case "delete":
            $Read->ExeRead(DB_MENUS, "WHERE menu_id = :id", "id={$KeyId}");
            if ($Read->getResult()) {
                $UpMenu['menu_parent'] = null;
                $Update->ExeUpdate(DB_MENUS, $UpMenu, "WHERE menu_parent = :id", "id={$KeyId}");

                $UpPage['page_menu_id'] = null;
                $Update->ExeUpdate(DB_PAGES, $UpPage, "WHERE page_menu_id= :id", "id={$KeyId}");
                $Delete->ExeDelete(DB_MENUS, "WHERE menu_id = :id", "id={$KeyId}");
                $jSON["success"] = "ok";
                $jSON["redirect"] = ADM_URL . "/?dx=menus/home";
            }
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
