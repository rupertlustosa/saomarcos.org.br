<form method="post">
    <?php
    $msg = null;
    $PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $autor = Users::GetAutorArtigo();
    if ($autor) {
        ?>
        <div style="max-width:700px; padding:20px; margin:0px auto;">
            <table class="table table-bordered">
                <tr>
                    <td width="40">##</td>
                    <td>Título</td>

                    <td width="200">Data de envio</td>
                    <td width="150">Estatus</td>
                    <td width="40">baixa</td>
                </tr>
                <?php
                $Read->ExeRead("dx_artigos_revista", " WHERE artigo_autor_id=:id ORDER BY artigo_status", "id={$autor['cliente_id']}");
                if ($Read->getResult()):
                    foreach ($Read->getResult() as $row):
                        list($date, $hour) = explode(" ", $row['artigo_create']);
                        $data = implode("/", array_reverse(explode("-", $date)));
                        switch ($row['artigo_status']) {
                            case 0:
                                $status = '<span class="label label-warning"><i class="fa fa-exclamation-triangle"></i> Aguardando</span>';
                                break;

                            case 1:
                                $status = '<span class="label label-success"><i class="fa fa-check"></i> Aceito</span>';
                                break;

                            case 2:
                                $status = '<span class="label label-danger"><i class="fa fa-remove"></i> Rejeitado</span>';
                                break;
                        }
                        ?>
                        <tr>
                            <td><?= $row['artigo_id'] ?></td>
                            <td><?= $row['artigo_title'] ?></td>
                            <td><?= $data . " " . $hour ?></td>
                            <td><?= $status ?></td>
                            <td><a href="<?= BASE . "/" . $row['artigo_src'] ?>"><i class="fa fa-download"></i></a></td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>

            </table>
        </div>

        <?php
    }
    ?>
</form>