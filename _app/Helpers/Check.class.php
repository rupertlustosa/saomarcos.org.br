<?php

/**
 * Check.class [HELPER]
 * Classe responsável por manipular e validar dados do sistema
 *
 * @copyright (c) 2016, Ax de Sousa
 */
class Check
{

    private static $Data;
    private static $Format;

    public static function Email($Email)
    {
        self::$Data = (string)$Email;
        self::$Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

        if (preg_match(self::$Format, self::$Data)):
            return true;
        else:
            return false;
        endif;
    }

    public static function Name($Name)
    {
        self::$Format = array();
        self::$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        self::$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), self::$Format['b']);
        self::$Data = strip_tags(trim(self::$Data));
        self::$Data = str_replace(' ', '-', self::$Data);
        self::$Data = str_replace(array('-----', '----', '---', '--', '--'), '-', self::$Data);

        return strtolower(utf8_encode(self::$Data));
    }

    public static function Data($Data)
    {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('/', self::$Format[0]);

        if (empty(self::$Format[1])):
            self::$Format[1] = date('H:i:s');
        endif;

        self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
        return self::$Data;
    }

    public static function Resumo($String, $QntLetras, $More = '')
    {
        $mensagem = array();
        $msg = strip_tags(trim($String));
        $palavras = explode(" ", $msg);
        for ($i = 0; $i < count($palavras); $i++) {
            $cont = implode(" ", $mensagem);
            if ((strlen($cont) + strlen($palavras[$i]) + 1) <= $QntLetras) {
                $mensagem[] = $palavras[$i];
            }
        }

        if (strlen($msg) > $QntLetras) {
            $texto = implode(" ", $mensagem);
            $texto = $texto . $More;
        } else {
            $texto = implode(" ", $mensagem);
        }
        return $texto;
    }

    public static function codificacao($string)
    {
        return mb_detect_encoding($string . 'x', 'UTF-8, ISO-8859-1');
    }

    public static function DateEncode($data = '01/01/2017 00:00:00')
    {
        $dateTime = explode(" ", $data);
        $date = $dateTime[0];
        $date = explode("/", $date);
        $date = implode("-", array_reverse($date));
        if (!empty($dateTime[1])) {
            $hora = $dateTime[1];
        } else {
            $hora = '00:00:00';
        }
        return $date . " " . $hora;
    }

    public static function DateDecode($data = '2017-01-01 00:00:00', $hora = false)
    {
        $dateTime = explode(" ", $data);
        $date = $dateTime[0];
        $date = explode("-", $date);
        $date = implode("/", array_reverse($date));
        if (!empty($dateTime[1])) {
            $hora = $dateTime[1];
        } else {
            $hora = '00:00:00';
        }
        if ($hora) {
            return $date;
        } else {
            return $date . " " . $hora;
        }
    }

    public static function formatDate($date, $format)
    {
        //d/m/Y => 21/09/2016
        //w, d/m/Y => Quarta-feira, 21/09/2016
        //d \d\e F \d\e Y => 21 de Setembro de 2016
        $newDate = new DateTime($date);
        return $newDate->format($format);
    }

    public static function Status($table, $campo_id, $key_id)
    {
        $read = new Read;
        $read->FullRead("SELECT {$campo_id},status FROM {$table} WHERE {$campo_id}=:id", "id={$key_id}");
        $status = $read->getResult()[0]['status'];
        if ($status == '1') {
            return true;
        } else {
            return false;
        }
    }

    public static function CheckCPF($cpf = null)
    {
        $cpf = preg_replace('/[^0-9]/', '', (string)$cpf);
        // Valida tamanho
        if (strlen($cpf) != 11)
            return false;
        // Calcula e confere primeiro dígito verificador
        for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
            $soma += $cpf{$i} * $j;
        $resto = $soma % 11;
        if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Calcula e confere segundo dígito verificador
        for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
            $soma += $cpf{$i} * $j;
        $resto = $soma % 11;
        return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
    }


}
