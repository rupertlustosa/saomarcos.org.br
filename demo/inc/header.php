<header class="container-fluid">
    <div class="atomwrapper">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h1 class="fl-left fadezero fontzero nomargin nopadding">
                    <?= SITE_NAME; ?>
                </h1>
                <a href="" class="header-logo">
                    <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>" title="<?= SITE_NAME; ?>">
                </a>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="hintxt">
                    <div class="hinfo-icon">
                        <span class="fa fa-map-marker"></span>
                    </div>
                    <div class="hinfo-text">
                        <h2 class="nomargin nopadding text-bold color-red-300">NOSSA LOCALIZAÇÃO</h2>
                        <p>Rua Olavo Bilac, 2300. Teresina-PI</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="hintxt">
                    <div class="hinfo-icon">
                        <span class="fa fa-coffee"></span>
                    </div>
                    <div class="hinfo-text">
                        <h2 class="nomargin nopadding text-bold color-red-300">HORÁRIOS DE ATENDIMENTO</h2>
                        <p>Aberto 24 horas</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="h-actions fl-right zd100">
                    <a href="#search" title="" class="h-act-search onfade4">
                        <i class="fa fa-search"></i>
                        <span>BUSCAR</span>
                    </a>
                    <div id="search">
                        <button type="button" class="close">x</button>
                        <form action="">
                            <input type="search" value="" placeholder="O que você está procurando?" />
                            <button type="submit" class="btn-default-hsm btn bg-red-300 onfade4">Buscar no Hospital</button>
                        </form>
                    </div>
                    <div class="toggle-wrap">
                        <button data-menu="links" class="h-act-links onfade4">
                            <i class="fa fa-link"></i>
                            <span>LINKS</span>
                        </button>
                        <ul id="links" class="menu menu--toggle">
                            <li class="menu__item">Link #01</li>
                            <li class="menu__item">Link #02</li>
                            <li class="menu__item">Link #03</li>
                            <li class="menu__item">Link #04</li>
                            <li class="menu__item">Link #05</li>
                            <li class="menu__item">Link #06</li>
                            <li class="menu__item">Link #07</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<nav class="navbar navbar-default nomargin nopadding">
    <div class="container-fluid">
        <div class="atomwrapper nopadding">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">HOME <span class="sr-only">(atual)</span></a></li>
                    <li><a href="#">A PCC</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="#">LOCALIZAÇÃO</a></li>
                            <li><a href="#">ESPECIALIDADES</a></li>
                            <li><a href="#">DIRETORIA</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#">SERVIÇOS</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">APPEM</a></li>
                                    <li><a href="#">Fisioterapia</a></li>
                                    <li><a href="#">Lar de Maria</a></li>
                                    <li><a href="#">Nadipo</a></li>
                                    <li><a href="#">Nuttem</a></li>
                                    <li><a href="#">Psicologia</a></li>
                                    <li><a href="#">Serviço Social</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ÁREA GERAL<i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li><a href="#">Convênios do Hospital</a></li>
                            <li><a href="#">Buscar Profissionais</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#">Internação</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Acomodações</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#">Dicas</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Jornal da MAMA</a></li>
                                    <li><a href="#">Mastectomizadas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROFISSIONAIS <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu tuppercase">
                            <li class="dropdown-submenu">
                                <a href="#">MANUAIS E ARTIGOS</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Artigos</a></li>
                                    <li><a href="#">Orientações</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#">Cursos / Reuniões</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Cursos</a></li>
                                    <li><a href="#">Reunião Ginecologia</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Revista Multiprofissional</a></li>
                            <li><a href="#">Artigo de Revista</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="#">Residência Médica</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Inscrição</a></li>
                                    <li><a href="#">Apresentação</a></li>
                                </ul>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Inscrições de Cursos</a></li>
                        </ul>
                    </li>
                    <li><a href="#">IMPRENSA</a></li>
                    <li><a href="#">FALE CONOSCO</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right" >
                    <li><a class="font12px text-bold color-red-300" href="#">RESULTADOS DE EXAMES</a></li>
                    <li><a class="font12px text-bold color-red-300" href="#">ACESSO RESTRITO</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
