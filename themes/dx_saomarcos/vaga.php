<?php
include "inc/header.php";
extract($DataResult);
?>

    <section class="content-section ">
        <div class="container-fluid" style="background-color: #fff">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div>
                            <div class="contentInfoImv">
                                <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $image ?>&w=900" alt="" title=""/>
                                <div class="clear10"></div>
                                <h1 class="fontsize2 cavbold al-center tuppercase centertable">
                                    <?= $vaga ?>
                                </h1>
                                <span class="al-center centertable nolanbook">
                                    <?php
                                    $noticiaData = new Date($created);
                                    ?>
                                    Publicada em: <time class=""
                                                        pubdate><?= "{$noticiaData->getDia()} de {$noticiaData->getMesName()} de {$noticiaData->getAno()}" ?></time>
                                    <?php
                                    $noticiaData = null;
                                    ?>
                                </span>
                                <div class="contentMsg width90">
                                    <?= $informacoes ?>
                                </div>
                                <div class="clear20"></div>
                            </div>
                        </div>
                        <div class="clear20"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="section2" style="width:auto">
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <h3>ENVIE SEU CURRÍCULO</h3>
                                </div>

                                <div class="clear40"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sec2contactform">
                                        <form action="javascript:;" method="post" id="formContato">
                                            <div class="clearfix">
                                                <input type="text" required="required" name="nome" maxlength="100"
                                                       class="col2 first" placeholder="Nome">
                                                <input type="email" required="required" name="email" maxlength="100"
                                                       class="col2 last" placeholder="Email">
                                            </div>

                                            <div class="clear10"></div>

                                            <div class="clearfix">
                                                <input type="text" name="linkedin" maxlength="255" class="col-xs-12"
                                                       placeholder="Linkedin (Endereço completo)">
                                            </div>

                                            <div class="clear10"></div>

                                            <div class="clearfix">
                                                <input type="text" class="col2 first phone" maxlength="20"
                                                       name="telefone" placeholder="Telefone Residencial">
                                                <div class="col2 last ">
                                                    Arquivo (pdf/zip)
                                                    <input type="file" required="required" name="curriculo">
                                                </div>

                                            </div>

                                            <div class="clearfix">
                                                <textarea id="" cols="30" required="required" maxlength="500"
                                                          name="mensagem" rows="3" placeholder="Mensagem"></textarea>
                                            </div>
                                            <div class="clearfix">
                                                <div class="response" style="display:none">
                                                    <div class="alert alert-success" role="alert">Enviando...</div>
                                                </div>
                                                <input type="hidden" value="<?= $id ?>" required="required"
                                                       name="vaga_id">
                                                <input type="submit" class="btn-default-hsm bg-red-300" value="Enviar">
                                            </div>
                                        </form>
                                    </div>
                                    <script>
                                        $(function () {
                                            $(".phone").mask("(99) 9.9999-9999");
                                            $(".response").click(function () {
                                                $(this).html('');
                                            })
                                            base = $('link[rel="base"]').attr('href');
                                            $("#formContato").submit(function () {
                                                $(".response").show();
                                                var form = $(this);
                                                form.ajaxSubmit({
                                                    url: base + '/sendcurriculo.php',
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        if (data == 1) {
                                                            $(".response .alert-success").html('Formulário enviado com sucesso!</div>');
                                                        } else {
                                                            $(".response .alert-success").html(response);
                                                        }
                                                    }
                                                });
                                                $("#formContato")[0].reset();
                                                return false;

                                            });

                                        });
                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include "inc/footer.php";
?>