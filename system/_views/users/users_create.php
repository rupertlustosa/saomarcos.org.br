<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['user_status'] = 0;
    $create->ExeCreate(DB_USERS, $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=users/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM " . DB_USERS . " WHERE user_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/users/users.js');

include '_inc/header.php';
include '_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><?= $TABLE['TITLE_SINGLE'] ?>
                        <small> <?= $Post['user_name'] ?></small>
                    </h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Editar <?= $TABLE['TITLE_SINGLE'] ?></h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input type="text" riquered name="user_name"
                                                       value="<?= $Post['user_name'] ?>"
                                                       placeholder="Digite o nome do <?= $TABLE['TITLE_SINGLE'] ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Login</label>
                                                <input type="text" riquered name="user_login"
                                                       value="<?= $Post['user_login'] ?>"
                                                       placeholder="Digite o Login do <?= $TABLE['TITLE_SINGLE'] ?> "
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" riquered name="user_email"
                                                       value="<?= $Post['user_email'] ?>"
                                                       placeholder="Digite o E-Mail do <?= $TABLE['TITLE_SINGLE'] ?> "
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Senha</label>
                                                <input type="password" riquered name="user_password" value=""
                                                       placeholder="Digite a senha do <?= $TABLE['TITLE_SINGLE'] ?> "
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Genero</label>
                                                <p>
                                                    Masculino:
                                                    <input type="radio" class="flat" name="user_genero"
                                                           id="user_generoM"
                                                           value="M" <?= ($Post['user_genero'] === 'M') ? 'checked="checked"' : '' ?> />
                                                    &nbsp;&nbsp;Feminino:
                                                    <input type="radio" class="flat" name="user_genero"
                                                           id="user_generoF"
                                                           value="F" <?= ($Post['user_genero'] === 'F') ? 'checked="checked"' : '' ?> />
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Sobre</label>
                                                <textarea name="user_bio" class="form-control"
                                                          cols="4"><?= $Post['user_bio'] ?></textarea>
                                            </div>
                                        </div>

                                        <?php
                                        if (Session::getUserLevel() >= 6):
                                            ?>
                                            <div class="col-lg-12">
                                                <div class="well well-permissoes">
                                                    <h3>Permissões</h3>
                                                    <div style="width: 100%; display: table">
                                                        <?php
                                                        $permissoes = array();

                                                        $Read->FullRead("SELECT * FROM " . DB_PERMISSION . " WHERE user_id=:id ", "id={$Post['user_id']}");
                                                        if ($Read->getResult()) {
                                                            foreach ($Read->getResult() as $p) {
                                                                $permissoes[] = $p['table_id'];
                                                            }
                                                        }

                                                        $table_exclude = array(0);

                                                        foreach (getTableList() as $table) {
                                                            $check = (in_array($table['KEY'], $permissoes)) ? 'checked' : '';
                                                            if (!in_array($table['KEY'], $table_exclude)):
                                                                if ($table['KEY'] != 0):
                                                                    if ($table['KEY'] == 1):
                                                                        if ($Post['user_level'] > 9):
                                                                            echo '<div class="col-sm-3 col-md-3 col-lg-3">';
                                                                            echo '<div class="checkbox">
                                        <label><input ' . $check . ' type="checkbox" class="permissoes_item" name="permissoes[]" value="' . $table['KEY'] . '"> ' . $table['TITLE'] . '</label>
                                    </div>';
                                                                            echo '</div>';
                                                                        endif;
                                                                    else:
                                                                        echo '<div class="col-sm-3 col-md-3 col-lg-3">';
                                                                        echo '<div class="checkbox">
                                        <label><input ' . $check . ' type="checkbox" class="permissoes_item" name="permissoes[]" value="' . $table['KEY'] . '"> ' . $table['TITLE'] . '</label>
                                    </div>';
                                                                        echo '</div>';
                                                                    endif;

                                                                endif;
                                                            endif;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endif;
                                        ?>
                                    </div>

                                </div>
                                <div class="col-md-4" style="padding-left:20px">
                                    <?php
                                    if (Session::getUserLevel() >= 6):
                                        ?>
                                        <div class="form-group">
                                            <label style="display: block">Level</label>
                                            <select name="user_level">
                                                <?php
                                                foreach (getUserLevels() as $usr):
                                                    $selected = ($Post['user_level'] == $usr['0']) ? 'selected="selected"' : '';
                                                    echo "<option {$selected} value='{$usr[0]}'>{$usr[1]}</option>";
                                                endforeach;
                                                ?>

                                            </select>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                    <div class="upload_progress none"
                                         style="padding: 5px; background: #00B594; color: #fff; width: 0%; text-align: center; max-width: 100%;">
                                        0%
                                    </div>
                                    <div style="overflow: auto; max-height: 400px;">
                                        <?php
                                        if (file_exists("../uploads/" . $Post['user_image']) && !is_dir("../uploads/" . $Post['user_image'])) {
                                            echo '<img class="image image_default pull-right" alt="Avatar" title="Avatar" src="../tim.php?src=uploads/' . $Post['user_image'] . '&w=350&h=350" />';
                                        } else {
                                            echo '<img class="image image_default pull-right" alt="Nova Imagem" title="Nova Imagem" src="../tim.php?src=system/_img/no_image.jpg&w=350&h=350" default="../tim.php?src=system/_img/no_image.jpg&w=350&h=350"/>';
                                        }
                                        ?>
                                    </div>
                                    <div class="upload_file"><input class="loadimage" type="file" name="image"/></div>
                                    <div class="clear20"></div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox"
                                                       name="rascunho" <?= ($Post['user_status'] == 0) ? 'checked' : '' ?>
                                                       value="1">Desativado
                                            </label>
                                        </div>
                                        <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                        <input type="hidden" name="callback_action" value="manager">
                                        <input type="hidden" name="key_id" value="<?= $KeyId ?>">
                                        <img src="_img/load.gif" class="loading_form pull-right">

                                        <button class="btn btn-success pull-right" type="submit">
                                            Salvar <?= $TABLE['TITLE_SINGLE'] ?></button>&nbsp;

                                    </div>

                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div><!-- content do formulario -->
                    </div><!-- painel do formulario -->

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php

//include './_inc/gallery.php';
include './_inc/footer.php';
?>