<?php
/**
 * Created by PhpStorm.
 * User: rupert
 * Date: 22/04/17
 * Time: 19:19
 */

namespace Pagseguro;


class Sender
{
    public $email;
    public $name;
    public $documents;
    public $phone;
    public $bornDate;

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this;
    }

    /**
     * @param mixed $email
     * @return Sender
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param mixed $name
     * @return Sender
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param Documents $documents
     * @return Sender
     */
    public function setDocuments(Documents $documents)
    {
        $this->documents = $documents;
        return $this;
    }

    /**
     * @param mixed $phone
     * @return Sender
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @param mixed $bornDate
     * @return Sender
     */
    public function setBornDate($bornDate)
    {
        $this->bornDate = $bornDate;
        return $this;
    }
}