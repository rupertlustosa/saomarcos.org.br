<?php
/**
 * Created by PhpStorm.
 * User: rupertlustosa
 * Date: 21/06/17
 * Time: 18:19
 */

namespace Pagseguro;


class LinkProtegido
{
    public static function montaLink($action, $id)
    {

        $key = base64_encode(\Hash::make($id));
        $id = base64_encode($id);
        $action = base64_encode($action);

        return route('linkProtegido', [$id, $action, $key]);
    }
}