<?php
extract($DataResult);
include "inc/header.php";
?>

<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="clear40"></div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <?php
                if ($page_image) {
                    echo '<img src="' . BASE . '/uploads/' . $page_image . '">';
                }
                ?>
                <div class="clear20"></div>
                <h2><?= $page_title ?></h2>
                <h3 class="caption color-black-100 width50 centertable">
                    <?= $page_chamada ?>
                </h3>
                <div class="clear20"></div>
            </div><!-- /.col-md-12 -->
            <div class="col-md-6">
                <div class="texto">
                    <?= $page_content ?>
                </div>
                <div class="page-galeria">
                    <?php
                    $galery = Site::getGalley(5, $page_id);
                    if ($galery):
                        foreach ($galery as $img) {
                            ?>
                            <a data-lightbox="roadtrip" href="<?= BASE . "/uploads/" . $img['file_src'] ?>"
                               title="<?= $img['file_name'] ?> ">
                                <img alt="<?= $img['file_name'] ?>"
                                     src="<?= BASE . "/tim.php?src=/uploads/" . $img['file_src'] . "&w=220&h=140" ?>"
                                     title="<?= $img['file_name'] ?>"/>
                            </a>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-md-5 pull-right">
                <form id="formVisitaTecnica" style="text-align:left" action="javascript:;">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="nome">Nome</label>
                            <input type="text" required="required" name="nome" class="form-control" id="nome"
                                   placeholder="Nome">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="email">E-Mail</label>
                            <input type="email" required="required" name="email" class="form-control" id="email"
                                   placeholder="E-Mail">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="profissao">Profissão</label>
                            <input type="text" name="profissao" class="form-control" id="profissao"
                                   placeholder="Profissão">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="empresa">Empresa</label>
                            <input type="text" required="required" name="empresa" class="form-control" id="empresa"
                                   placeholder="Empresa">
                        </div>

                        <div class="form-group col-md-8">
                            <label for="enderecocomercial">Endereço Comercial</label>
                            <input type="text" required="required" name="enderecocomercial" class="form-control"
                                   id="enderecocomercial" placeholder="Endereço Comercial">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="estado">Estado</label>
                            <select name="estado" id="estado" class="form-control">
                                <option></option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="RS">RS</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cep">Cep</label>
                            <input type="text" name="cep" class="form-control cep" id="cep" placeholder="cep">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="telefone">Telefone</label>
                            <input type="text" name="telefone" class="form-control fone" id="telefone"
                                   placeholder="Telefone">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="cnpj">CNPJ</label>
                            <input type="text" name="cnpj" class="form-control cnpj" id="cnpj" placeholder="CNPJ">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="unidade_setor">Unidade/Setor</label>
                            <input type="text" name="unidade_setor" class="form-control" id="unidade_setor"
                                   placeholder="Unidade/Setor">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cargo">Cargo</label>
                            <input type="text" name="cargo" class="form-control" id="cargo" placeholder="Cargo">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cep">Numero de Visitante</label>
                            <select name="estado" required="required" id="estado" class="form-control">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="objetivo">Objetivo da Visita</label>
                            <textarea id="objetivo" required="required" name="objetivo" class="form-control"></textarea>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="objetivo">Enviar comprovante de Inscrição</label>
                            <input type="file">
                        </div>

                        <div class="form-group col-md-12">
                            <p>Áreas que deseja visitar (selecione ao menos uma, e no máximo três opções):</p>
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Ambulatório Médico">
                                        Ambulatório Médico
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas"
                                               value="Central de Materiais"> Central de Materiais
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas"
                                               value="Centro de Cardiologia"> Centro de Cardiologia
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas"
                                               value="Centro de Diagnósticos"> Centro de Diagnósticos
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Centro de Oncologia">
                                        Centro de Oncologia
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Enfermagem">
                                        Enfermagem
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Farmácia"> Farmácia
                                    </label>
                                </div>
                            </div>
                            <div class="pull-right">

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Filantropia">
                                        Filantropia
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value"Nutrição"> Nutrição
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Pronto Atendimento">
                                        Pronto Atendimento
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Qualidade"> Qualidade
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="U.T.I."> U.T.I.
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="areas[]" class="areas" value="Outros"> Outros
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="response pull-left" style="display:table;"></div>
                            <button type="submit" class="btn btn-danger pull-right">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
    <div class="clear40"></div>
    <script>
        $(function () {
            $(".cep").mask("99999-999");
            $(".fone").mask("(99)9999-9999");
            $(".cnpj").mask("99.999.999/9999-99");
            $(".response").click(function () {
                $(this).html('');
            })
            base = $('link[rel="base"]').attr('href');
            $("#formVisitaTecnica").submit(function () {
                data = $(this).serialize();
                countCheck = $(".areas:checked").size();
                if (countCheck >= 1 && countCheck <= 3) {
                    $.post(base + "/ajax/sendvisitatecnica.php", {data: data}, function (response) {
                        if (response == 1) {
                            $(".response").html('<div class="alert alert-success" role="alert">Formulário enviado com sucesso!</div>');
                        } else {
                            $(".response").html('<div class="alert alert-danger" role="alert">' + response + '</div>');
                        }
                    });
                    this.reset();
                } else {
                    $(".response").html('<div class="alert alert-danger" role="alert">Escolha entre 1 e 3 áreas para visitar!</div>');
                }


            })
        })
    </script>
</section><!-- /.our-team -->
<?php include "inc/footer.php" ?>
