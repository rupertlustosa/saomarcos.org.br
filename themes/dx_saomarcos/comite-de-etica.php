<?php
include "inc/header.php";
extract($DataResult);
?>

<section class="content-section bg-light-brown">
    <div class="container-fluid">
        <div class="atomwrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contentInfoImv">
                        <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $image ?>&w=800&h=500" alt="" title=""/>
                        <div class="clear10"></div>
                        <h1 class="fontsize2 cavbold al-center tuppercase centertable">
                            <?= $titulo ?>
                        </h1>
                        <div class="clear10"></div>
                        <div class="contentMsg width90">
                            <?= $conteudo ?>
                        </div>
                    </div>
                    <div class="clear20"></div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "inc/footer.php";
?>
