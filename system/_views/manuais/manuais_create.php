<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$KeyId = filter_input(INPUT_GET, "id");
if (empty($KeyId)) {
    $create = new Create();
    $dados['status'] = 0;
    $create->ExeCreate(DB_MANUAIS, $dados);
    $newId = $create->getResult();
    header("Location: " . ADM_URL . "?dx=manuais/create&id=" . $newId);
} else {
    $Read->FullRead("SELECT * FROM " . DB_MANUAIS . " WHERE manual_id=:id ", "id={$KeyId}");
    if ($Read->getResult()) {
        $Post = $Read->getResult()[0];
    } else {
        //header("Location: " . ADM_URL . "?dx=error/404");
    }
}
$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
$front->set_js(ADM_FILE . '/_views/manuais/manuais.js');

include '_inc/header.php';
include '_inc/nav.php';
?>


    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <a title="Criar <?= $TABLE['TITLE_SINGLE'] ?>"
                           href="<?= ADM_URL ?>/?dx=<?= $TABLE['VIEW'] ?>/create" class="btn btn-info "> <i
                                    class="fa fa-plus"></i></a>
                        <?= $TABLE['TITLE_SINGLE'] ?>
                        <small> <?= $Post['manual_title'] ?></small>
                    </h3>

                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                Editar <?= $TABLE['TITLE_SINGLE'] ?>

                            </h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                <?php Admx::setFormToken() ?>

                                <div class="col-md-8">
                                    <div class="form-group title">
                                        <label>Título </label>
                                        <input type="text" name="manual_title" value="<?= $Post['manual_title'] ?>"
                                               placeholder="Digite o título do <?= $TABLE['TITLE_SINGLE'] ?>"
                                               class="form-control">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group title">
                                                <label>Seção </label>
                                                <select name="manual_category" class="form-control">
                                                    <option></option>
                                                    <?php
                                                    foreach (getManuaisCat() as $key => $val) {
                                                        $selected = $Post['manual_category'] == $key ? "SELECTED='SELECTED'" : "";
                                                        echo "<option value='{$key}' {$selected} >{$val}</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="clear30"></div>
                                            <div class="form-group ">
                                                <input class="loadimage" type="file" name="file"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 sidebar" style="padding-left:20px">
                                    <div class="clear20"></div>
                                    <div class="form-group">
                                        <?php
                                        if ($Post['manual_file']) {
                                            echo '<a href="' . BASE . '/uploads/' . $Post['manual_file'] . '" style="background-color:#f8f8f8;color:#000; margin-bottom:25px; display:table;float:right; padding:10px 20px; border:solid 1px #ccc" target="_blank">
                                           <i class="fa fa-file-o" aria-hidden="true"></i>
                                           &nbsp;Visualizar
                                           </a>';
                                        }
                                        ?>
                                        <div class="clear"></div>
                                        <input type="hidden" name="callback" value="<?= $TABLE['CALLBACK'] ?>">
                                        <input type="hidden" name="callback_action" value="manager">
                                        <input type="hidden" name="key_id" value="<?= $KeyId ?>">

                                        <img src="_img/load.gif" class="loading_form pull-right">

                                        <button class="btn btn-success pull-right" type="submit"
                                                style="margin: 0px 5px;">Salvar
                                        </button>&nbsp;
                                        <div class="checkbox pull-right">
                                            <label>
                                                <input type="checkbox" name="rascunho" value="1">Salvar como rascunho
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div><!-- content do formulario -->
                    </div><!-- painel do formulario -->
                    <div class="clear20"></div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php
include './_inc/footer.php';
?>