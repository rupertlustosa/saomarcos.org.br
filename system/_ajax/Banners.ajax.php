<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'banners';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack && $PostData['formToken'] == $formToke):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            $Read->ExeRead(DB_BANNERS, "WHERE banner_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):


            else:
                $local = $PostData['banner_local'];
                $getLocal = getBannersLocal($local);
                $banner_largura = $getLocal['largura'];
                $banner_altura = $getLocal['altura'];

                $name = !empty($PostData['banner_title']) ? Check::Name($PostData['banner_title']) : $post['banner_id'];

                if (!empty($File)):
                    $Upload->Image($File, "{$name}-" . time(), $banner_largura);
                    if ($Upload->getResult()):
                        $image = $Upload->getResult();
                        $PostData['banner_image'] = $image;
                        $jSON['image'] = ["src" => "../tim.php?src=uploads/{$image}&w={$banner_largura}&h={$banner_altura}"];
                    endif;
                endif;
                $ReadName = new $Read;
                $ReadName->FullRead("SELECT banner_name FROM " . DB_BANNERS . " WHERE banner_name= :nm and banner_id != :id ", "nm={$name}&id={$KeyId}");
                if ($ReadName->getResult()):
                    $PostData['banner_name'] = $name . time();
                else:
                    $PostData['banner_name'] = $name;
                endif;

                $PostData['status'] = ($rascunho) ? 0 : 1;
                $PostData['banner_dataentrada'] = Check::DateEncode($PostData['banner_dataentrada']);
                $PostData['banner_datasaida'] = Check::DateEncode($PostData['banner_datasaida']);
                $post = $Read->getResult()[0];
                $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE banner_id = :id", "id={$KeyId}");
                $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
            endif;
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
