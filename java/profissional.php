<?php 
include "inc/header.php";
$prof_espec_id = $DataResult['profissional_especialidade_id'];
$especialidade = Admx::GetCampo(DB_ESPECIALIDADES, "especialidade_title", "especialidade_id={$prof_espec_id}");
$clinica = Admx::GetCampo(DB_CLINICAS, 'clinica_title', "clinica_id = {$DataResult['profissional_clinica_id']}");
?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>NOME DO PROFISSIONAL</h2>
                <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">
                    especialidade do proficional<br>
                </h3>
            </div><!-- /.col-md-12 -->

            <div class="clear40"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">   
                        <div class="atomwrapper nopadding" style="text-align: left">
                            <h4 style="text-align: left">Dados Pessoais:</h4>
                            <p><strong>Nome: </strong>profissional_title</p>
                            <p><strong>CRM: </strong>profissional_crm</p>
                            <p><strong>Email: </strong>profissional_email'</p>
                            <p><strong>Especialidade: </strong>especialidade</p>
                            <p><strong>Clinica: </strong>clinica</p>
                            <p>&nbsp;</p>
                            Descrição do profisional.
                           
                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>
    
</section><!-- /.our-team -->

<?php include "footer.html" ?>