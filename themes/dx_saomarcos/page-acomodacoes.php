<?php
extract($DataResult);
include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="clear40"></div>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2><?= $page_title ?></h2>
                    <h3 class="caption color-black-100 width50 centertable">
                        <?= $page_chamada ?>
                    </h3>
                    <div class="clear20"></div>
                </div><!-- /.col-md-12 -->
                <div class="col-md-12">
                    <div class="texto">
                        <?= $page_content ?>
                    </div>
                    <div class="galeria">
                        <div class="row">
                            <?php
                            $galery = Site::getGalley(5, $page_id);
                            foreach ($galery as $key => $val) {
                                echo "<div class='galeria-item'>";
                                echo "<a href='" . BASE . "/uploads/{$val['file_src']}' data-lightbox='roadtrip'><img src='" . BASE . "/tim.php?src=uploads/{$val['file_src']}&w=190&h=120'></a>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear40"></div>
    </section><!-- /.our-team -->
<?php include "inc/footer.php" ?>