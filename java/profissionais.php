<?php
include "header.html";
?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>PROFISSIONAIS</h2>
                <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">
                    Encontre um profissional
                </h3>
            </div><!-- /.col-md-12 -->
            <div class="clear30"></div>

            <div class="atomwrapper">
                <form style="text-align: left !important">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputName2">Especialidade</label>
                            <select id="especialidade" class="form-control">
                                <option val="0"></option>                                        
                                                             
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail2">Clinica</label>
                            <select id="clinica"  class="form-control">
                                <option val="0"></option>                                        
                                                                   
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="profissionais">Profissional</label>
                            <select id="profissionais" class="form-control">
                                <option val="0"></option>                                        
                            </select>
                        </div>
                    </div>
                   
                </form>
            </div>
            <div class="clear40"></div>



            <div class="container">
                <div class="row">
                    <div class="col-md-12">   

                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>

</section><!-- /.our-team -->

<?php include "footer.html" ?>