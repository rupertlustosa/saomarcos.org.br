<div class="col-md-12">
    <div class="section2" style="width:auto">
        <div class="row text-center">


            <div class="clear40"></div>

            <div class="col-md-12">
                <h3 style="font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: inherit;
font-size: 24px;border: none; color: #000 !important; background-color: #F9F7F2; text-align: center"
                    class="text-center">Envie seu Artigo</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">


                <div class="atomwrapper">
                    <form id="form_inscricao" enctype="multipart/form-data"
                          style="text-align: left !important"
                          method="post">

                        <div class="response col-md-12">
                            <div class="alert alert-warning " style="display:none" role="alert">
                                Enviando ...
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Nome:</label>
                                <input class="form-control" id="nome" name="nome"
                                       required="required">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-Mail:</label>
                                <input class="form-control" id="email" name="email"
                                       required="required">
                            </div>
                        </div>

                        <div class="clear5"></div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cpf">CPF:</label>
                                <input class="form-control cpf"
                                       style="max-width: 159px;float: left;"
                                       name="cpf" required="required">
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="telefone ">Telefone:</label>
                                <input class="form-control phone" id="fone"
                                       name="telefone"
                                       required="required">
                            </div>
                        </div>

                        <div class="col-md-6"
                             id="comprovante">
                            <div class="form-group">
                                <label for="arquivo">Arquivo:</label>
                                <input name="arquivo" type="file" required="required">
                                <small>O arquivo deve ser um PDF</small>
                            </div>
                        </div>

                        <div class="clear5"></div>

                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger btn-large text-center"
                                        style="width: 100%; display: table; margin: 0px auto; text-align: center">
                                    ENVIAR
                                </button>
                            </div>
                        </div>

                    </form>
                    <script>


                        $(function () {

                            $(".phone").mask("(99) 9.9999-9999");
                            $(".cpf").mask("999.999.999-99");

                            $(".response").click(function () {
                                $(this).html('');
                            });

                            base = $('link[rel="base"]').attr('href');

                            $("#form_inscricao").submit(function () {

                                var form = $(this);
                                var form_this = this;

                                form.ajaxSubmit({
                                    url: base + "/ajax/revista-medica-envio-arquivo.php",
                                    beforeSubmit: function () {
                                        enviando();
                                        $('.response').show();
                                    },
                                    dataType: 'json',
                                    success: function (rsp) {

                                        console.log(">>" + rsp);
                                        console.log(rsp.msg);

                                        if (rsp.status == 'success') {

                                            $(".response .alert").removeClass("alert-warning");
                                            $(".response .alert").removeClass("alert-danger");
                                            $(".response .alert").addClass("alert-success");
                                            $(".response .alert").html(rsp.msg);
                                            form_this.reset();

                                        } else {

                                            $(".response .alert").removeClass("alert-warning");
                                            $(".response .alert").addClass("alert-danger");
                                            $(".response .alert").html(rsp.msg);
                                        }
                                    }
                                })


                                return false;
                            });
                        })


                    </script>
                </div>

            </div>
        </div>
    </div>
</div>