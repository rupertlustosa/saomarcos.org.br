<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="icon" href="<?= ADM_FILE ?>/_img/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= ADM_NAME ?></title>

    <link rel="base" href="<?= ADM_URL ?>"/>
    <!-- Bootstrap -->
    <link href="<?= CDN_FILE ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= CDN_FILE ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= CDN_FILE ?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= CDN_FILE ?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?= CDN_FILE ?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?= CDN_FILE ?>/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= CDN_FILE ?>/build/css/custom.min.css" rel="stylesheet">
    <?php
    $front->CSS();
    ?>
    <!-- jQuery -->
    <script src="<?= CDN_FILE ?>/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?= CDN_FILE ?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= ADM_FILE ?>/_js/jquery.form.js"></script>

    <!--
        <script src="<?= ADM_FILE ?>/_js/tinymce/tinymce.min.js"></script>
        
        -->

    <!-- CKEditor -->
    <script src="<?= BASE ?>/_cdn/ckeditor/ckeditor.js"></script>
    <script src="<?= BASE ?>/_cdn/ckeditor/adapters/jquery.js"></script>

    <?php
    $front->JS();
    ?>
    <script type="text/javascript" src="<?= ADM_FILE ?>/_js/plugins.js"></script>


</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">