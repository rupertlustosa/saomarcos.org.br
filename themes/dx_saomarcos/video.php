<?php
include "inc/header.php";
extract($DataResult);
?>

    <section class="content-section ">
        <div class="container-fluid" style="background-color: #fff">
            <div class="atomwrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="width: 100%; max-width: 670px; margin: 0px auto; display: table;">
                            <div class="contentInfoImv">
                                <?= Site::getFrameYoutube($video_url) ?>
                                <div class="clear10"></div>
                                <h1 class="fontsize2 cavbold al-center tuppercase centertable">
                                    <?= $video_title ?>
                                </h1>

                                <div class="contentMsg width90">
                                    <?= $video_desc ?>
                                </div>
                                <div class="clear20"></div>
                            </div>
                        </div>
                        <div class="clear20"></div>
                    </div>
                    <div class="col-lg-12">
                        <div class="sideInfoImv">

                            <div class="clear20"></div>

                            <h1 class="font13px al-center centertable">Mais VÍDEOS</h1>
                            <hr class=""/>

                            <?php
                            $outrasNoticias = new Read();
                            $where = Admx::Trash();

                            $outrasNoticias->ExeRead(DB_VIDEOS . " WHERE 1=1 {$where} AND status = 1 AND  video_id != '$video_id' ORDER BY video_date DESC LIMIT 4");
                            if ($outrasNoticias->getResult()):
                                foreach ($outrasNoticias->getResult() as $row):
                                    extract($row);
                                    ?>
                                    <article class="col-md-3 mAnd">
                                        <a href="<?= BASE ?>/video/<?= $video_name ?>"
                                           class="no-decoration onfade3 color-black-100 ">
                                            <img src="<?= Site::getImageYoutube($video_url) ?>"/>
                                            <h1 class="fontsize1 al-center color-dark-gray nomargin nopadding">
                                                <?= $row['video_title'] ?>
                                            </h1>
                                        </a>
                                        <div class="clear20"></div>
                                    </article>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include "inc/footer.php";
?>