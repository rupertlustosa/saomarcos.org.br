<?php
require '../../_app/autoload.php';

usleep(40000);

$json = null;
$json['error'] = null;

$TABLE = getTableList(DB_ANAIS);

$dadosEnviados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$chavePrimaria = $dadosEnviados['key_id'];
$rascunho = (!empty($dadosEnviados['rascunho'])) ? $dadosEnviados['rascunho'] : null;

$file = null;

$callback = $TABLE['CALLBACK'];
$formToken = Admx::getFormToken();

//VALIDA AÇÃO
if ($dadosEnviados && $dadosEnviados['callback_action'] && $dadosEnviados['callback'] = $callback && $dadosEnviados['formToken'] == $formToken) {

    $callback_action = $dadosEnviados['callback_action'];

    if (!empty($_FILES['image'])) {

        $file = $_FILES['image'];
    }
    unset($dadosEnviados['callback'], $dadosEnviados['formToken'], $dadosEnviados['callback_action'], $dadosEnviados['key_id'], $dadosEnviados['arquivo'], $dadosEnviados['rascunho']);


    $read = new Read;
    $create = new Create;
    $update = new Update;
    $delete = new Delete;
    $upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($callback_action) {
        case "manager":
            $read->ExeRead(DB_ANAIS, "WHERE id = :id", "id={$chavePrimaria}");
            if (!$read->getResult()) {


            } else {
                $types = ['application/pdf'];

                $maxSize = 1024 * 1024 * 8; // 8Mb

                $File = null;

                if (!empty($_FILES['arquivo'])):
                    $File = $_FILES['arquivo'];
                endif;

                $ext = substr($File['name'], -4);
                $nome_final = 'anais-' . md5(time()) . $ext;

                if ($File) {

                    if (in_array($File['type'], $types)) {

                        if ($File['size'] <= $maxSize) {

                            if (move_uploaded_file($File['tmp_name'], '../uploads/files/' . $nome_final)) {

                                $dadosEnviados['arquivo'] = $nome_final;
                            }

                        } else {

                            $json['error'] = true;
                            $json['msg'] = "Arquivo muito grande";
                        }
                    } else {

                        $json['error'] = true;
                        $json['msg'] = "Arquivo inválido";
                    }


                }

                if ($json['error'] != true) {

                    $dadosEnviados['status'] = 1;

                    $update->ExeUpdate($TABLE['DB'], $dadosEnviados, "WHERE id = :id", "id={$chavePrimaria}");
                    $json["success"] = ["title" => "Sucesso", "text" => "O registro oi atualizado com sucesso!", "type" => "info", "style" => "dark"];
                }


            }
            break;

    }

    if (isset($json)) {

        echo json_encode($json);
    }

} else {
    die('<center><h1>Permissão Negada!</h1></center>');
}
