<?php
/*
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://carlosmonte.com.br/hospitalsaomarcos/api/clinica/17');
$data = array('api' => 'get_medicos');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$output = curl_exec($ch);
var_dump($output);
*/
?>
<form>
<p>
  <label>
  URL BASE <input type="text" style="width:300px" id="host" placeholder="http://carlosmonte.com.br/hospitalsaomarcos/">
</label><br>
<small>Url onde a pasta api esta.</small>
</p>

<p>
  Pesquisar por clinica:<br>
  <label>
  Clinica ID <input type="text" id="idClinica">
  </label>
  <button type="button" onclick="Pesquisar(1)">Pesquisar</button>
</p>

<p>
  Pesquisar por especialidade:<br>
  <label>
  Especialidade ID <input type="text" id="especialidade">
  </label>
  <button type="button" onclick="Pesquisar(2)">Pesquisar</button>
</p>

<h2>Médicos</h2>

<span id="pesquisando" style="display:none; font-size:11px;">Pesquisando...</span>
<ul id="medicos">

</ul>

</form>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>

<script>
function Pesquisar(tipo){
  $("#pesquisando").show();
  $("#medicos").html('');
  host = $("#host").val();
  if(tipo == 1){
    id = $("#idClinica").val();
    $.post(host + 'api/clinica/'+id,{api:'get_medicos'},function(data){
      if(data){
        $("#pesquisando").hide();
        for(var i=0; i<data.length; i++){
            nome = data[i].nm_medico;
            console.log(nome);
            $( "#medicos" ).append("<li>"+nome+"</li>");

        }
      }
    },'json');

  }else{
    id = $("#especialidade").val();
    $.post(host + 'api/especialidade/'+id,{api:'get_medicos'},function(data){
      if(data){
        $("#pesquisando").hide();
        for(var i=0; i<data.length; i++){
            nome = data[i].nm_medico;
            console.log(nome);
            $( "#medicos" ).append("<li>"+nome+"</li>");

        }
      }
    },'json');
  }

}

</script>
