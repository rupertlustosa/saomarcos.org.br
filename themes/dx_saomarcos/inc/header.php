<header class="container-fluid" style="border-bottom: solid 1px #8a0e0b; border-top: solid 5px #b10d1d">
    <script>
        $(function () {
            console.log("Teste de unload");
            /*
            $(window).on('beforeunload', function (){
                alert("ax");
            });
            */
        })
    </script>


    <div class="atomwrapper" style="padding: 5px 0px 0px ; position: relative">

        <div class="h-actions busca-top fl-right zd100" style="display: table; width:auto;">
            <div class="login_busca">
                <a href="<?= Admx::getMeta('dx_configs', 1, 'url_login') ?>" title="Acesso Restrito"
                   class="h-act-login "><i class="fa fa-lock"></i> Acesso Restrito</a>

                <a href="#search" title="" class="h-act-search onfade4">

                    <i class="fa fa-search"></i>

                    <!--<span>BUSCAR</span> -->

                </a>
            </div>

            <div id="search">

                <button type="button" class="close">x</button>


                <form action="javascript:;">

                    <input type="search" autocomplete="off" id="campoBusca" value=""
                           placeholder="O que você está procurando?"/>

                    <button type="button" onclick="search()" class="btn-default-hsm btn bg-red-300 onfade4">Buscar no
                        Hospital
                    </button>

                </form>

                <script>

                    function search() {

                        s = $("#campoBusca").val().replace(/[\\"]/g, '');

                        s = s.replace(/[\\"]/g, '');

                        s = s.replace(/[\\']/g, '');


                        if (s != null) {

                            location.href = '<?= BASE ?>/pesquisa/?q=' + s;

                        }

                    }

                </script>

            </div>

            <div class="redes-topo">

                <ul class="main-footer-social">

                    <li>

                        <a class="onfade4" href="https://www.youtube.com/channel/UCVlxRFU-J-nxFWgKM0Kwi8g"
                           title="Nosso canal no youtube">

                            <i class="fa fa-youtube-play"></i>

                        </a>

                    </li>

                    <li>

                        <a class="onfade4" href="https://www.facebook.com/saomarcospi/?fref=ts"
                           title="Hospital São Marcos">

                            <i class="fa fa-facebook"></i>

                        </a>

                    </li>

                    <li>

                        <a class="onfade4" href="https://www.instagram.com/hospitalsaomarcos/"
                           title="Hospital São Marcos">

                            <i class="fa fa-instagram"></i>

                        </a>

                    </li>
                    <li>

                        <a class="onfade4" href="http://webmail.saomarcos.org.br/"
                           style="margin-top:-2px; height:42px; font-size:18px;" title="Webmail Hospital São Marcos">
                            @
                        </a>

                    </li>

                </ul>

            </div>

        </div><!-- busca -->

        <div class="row">

            <div class="col-lg-2 col-md-2">

                <h1 class="fl-left fadezero fontzero nomargin nopadding">

                    <?= SITE_NAME; ?>

                </h1>

                <a href="<?= BASE ?>" class="header-logo">

                    <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>"
                         title="<?= SITE_NAME; ?>">

                </a>

            </div>

            <div class="col-lg-10 col-md-10">

                <?php include "menu.php"; ?>

            </div>


        </div>

    </div>

</header>



