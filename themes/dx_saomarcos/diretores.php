<?php include "inc/header.php" ?>
<!-- Our team Section -->
<section id="team" class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2>DIRETORIA</h2>
                <!--<h3 class="caption color-black-100 width50 centertable"> </h3>-->
            </div><!-- /.col-md-12 -->

            <div class="clear20"></div>

            <div class="container">
                <div class="row">

                    <?php
                    $read = new Read;
                    $where = Admx::Trash();
                    $read->ExeRead(DB_DIRETORES, "WHERE status =:st {$where} order by diretoria_posicao asc ", "st=1");
                    foreach ($read->getResult() as $row):
                        extract($row);
                        ?>
                        <div class="col-lg-4">
                            <h3 class="post-it-note onfade4 bg-red-300 text-bold"
                                style="font-size:23px; height: 145px !important">
                                <a href="<?= BASE ?>/diretoria/<?= $diretoria_name ?>" title="<?= $diretoria_title ?>">
                                    <?= $diretoria_cargo ?>
                                </a>
                                <div class="clear10"></div>
                                <a href="<?= BASE ?>/diretoria/<?= $diretoria_name ?>" title="<?= $diretoria_title ?>">
                                    <span style="font-weight: normal; font-size: 16px;"><?= $diretoria_title ?></span>
                                </a>
                            </h3>

                            <div class="clear50"></div>
                        </div>
                        <?php
                    endforeach;
                    ?>


                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear40"></div>

</section><!-- /.our-team -->
<?php include "inc/footer.php" ?>
