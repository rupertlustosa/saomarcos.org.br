<?php

include "inc/header.php";

?>

    <!-- Our team Section -->

    <section class="team content-section bg-light-brown">

        <div class="container">

            <div class="row text-center">

                <div class="col-md-12">

                    <div class="clear40"></div>

                    <h2>CURSOS</h2>

                    <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">

                        Escolha um curso e faça sua inscrição!

                    </h3>

                </div><!-- /.col-md-12 -->

                <div class="clear30"></div>


                <div class="atomwrapper">

                    <form id="form_inscricao" method="javascript:;" style="text-align: left !important" method="post">

                        <div class="response col-md-12">

                            <div class="alert alert-warning " style="display:none" role="alert">Enviando ...</div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_curso_id">Curso</label>

                                <select id="inscricao_curso_id" name="inscricao_curso_id" class="form-control">

                                    <option val="0"></option>

                                    <?php

                                    $read = new Read();

                                    $read->FullRead("SELECT * FROM dx_cursos WHERE status = 1 ");

                                    foreach ($read->getResult() as $row) {

                                        echo "<option value='{$row['curso_id']}'>{$row['curso_title']}</option>";

                                    }

                                    ?>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_cpf">CPF:</label>

                                <input class="form-control" name="inscricao_cpf">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_nome">Nome:</label>

                                <input class="form-control" name="inscricao_nome">

                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_email">E-Mail:</label>

                                <input class="form-control" name="inscricao_email">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_fone">Telefone:</label>

                                <input class="form-control" name="inscricao_fone">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="inscricao_graduacao">Graduação:</label>

                                <select id="inscricao_graduacao" name="inscricao_graduacao" class="form-control">

                                    <option value="Graduando">Graduando</option>

                                    <option value="Graduado">Graduado</option>

                                    <option value="Residente">Residente</option>

                                    <option value="Pós-graduando">Pós-graduando</option>

                                    <option value="Pós-graduado">Pós-graduado</option>

                                </select>

                            </div>

                        </div>


                        <div class="col-md-offset-4 col-md-4">

                            <div class="form-group">

                                <button type="submit" class="btn btn-danger btn-large"
                                        style="width: 100%; display: table; margin: 0px auto;">ENVIAR
                                </button>

                            </div>

                        </div>


                    </form>

                    <script>

                        $(function () {

                            $(".phone").mask("(99) 9.9999-9999");

                            $(".cpf").mask("999.999.999-99");

                            $(".response").click(function () {

                                $(this).html('');

                            })

                            base = $('link[rel="base"]').attr('href');

                            $("#form_inscricao").submit(function () {

                                enviando();

                                data = $(this).serialize();

                                $.post(base + "/sendinscricao.php", {data: data}, function (rsp) {

                                    if (rsp == 1) {

                                        success();

                                    } else {

                                        $(".response alert").removeClass("alert-warning");
                                        $(".response alert").addClass("alert-danger");
                                        $(".response alert").html(rsp);

                                    }

                                });

                                this.reset();

                                return false;

                            })

                        })


                    </script>

                </div>

                <div class="clear40"></div>


                <div class="container">

                    <div class="row">

                        <div class="col-md-12">


                        </div>

                    </div>

                </div><!-- /.container -->

            </div><!-- /.row -->

        </div><!-- /.container -->

        <div class="clear20"></div>


    </section><!-- /.our-team -->


<?php include "inc/footer.php" ?>