<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="robots" content="index, follow"/>


    <link href="<?= FILE; ?>/_cdn/_front/_libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_libraries/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick-theme.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/default.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/hospitalsm-core.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_assets/_css/responsive.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_assets/_fonts/dosis/dosis.css" rel="stylesheet">
    <link href="<?= FILE; ?>/_cdn/_front/_assets/_img/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    <link href="<?= FILE; ?>/_cdn/_front/_libraries/lightbox2-master/lightbox2-master/dist/css/lightbox.css"
          rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/html5shiv.min.js"></script>
    <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/respond.min.js"></script>
    <![endif]-->
    <link href="<?= FILE; ?>/style.css?v=<?= time() ?>" rel="stylesheet">

    <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.min.js"></script>
    <script src="<?= FILE; ?>/_cdn/_front/_assets/_js/maskinput.js"></script>

    <script src="<?= FILE; ?>/_cdn/_front/_libraries/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= FILE; ?>/script.js"></script>

</head>
<body>
<header class="container-fluid" style="border-bottom: solid 1px #8a0e0b; border-top: solid 5px #b10d1d">
    <div class="atomwrapper" style="padding: 5px 0px 0px ; position: relative">

        <div class="h-actions busca-top fl-right zd100" style="display: table; width:auto;">
            <div class="login_busca">
                <a href="#" title="Acesso Restrito" class="h-act-login "><i class="fa fa-lock"></i> Acesso Restrito</a>

                <a href="#search" title="" class="h-act-search onfade4">

                    <i class="fa fa-search"></i>

                    <!--<span>BUSCAR</span> -->

                </a>
            </div>

            <div id="search">

                <button type="button" class="close">x</button>


                <form action="javascript:;">

                    <input type="search" autocomplete="off" id="campoBusca" value=""
                           placeholder="O que você está procurando?"/>

                    <button type="button" onclick="search()" class="btn-default-hsm btn bg-red-300 onfade4">Buscar no
                        Hospital
                    </button>

                </form>

                <script>

                    function search() {

                        s = $("#campoBusca").val().replace(/[\\"]/g, '');

                        s = s.replace(/[\\"]/g, '');

                        s = s.replace(/[\\']/g, '');


                        if (s != null) {

                            location.href = '<?= BASE ?>/pesquisa/?q=' + s;

                        }

                    }

                </script>

            </div>

            <div class="redes-topo">

                <ul class="main-footer-social">

                    <li>

                        <a class="onfade4" href="https://www.youtube.com/channel/UCVlxRFU-J-nxFWgKM0Kwi8g"
                           title="Nosso canal no youtube">

                            <i class="fa fa-youtube-play"></i>

                        </a>

                    </li>

                    <li>

                        <a class="onfade4" href="https://www.facebook.com/saomarcospi/?fref=ts"
                           title="Hospital São Marcos">

                            <i class="fa fa-facebook"></i>

                        </a>

                    </li>

                    <li>

                        <a class="onfade4" href="https://www.instagram.com/hospitalsaomarcos/"
                           title="Hospital São Marcos">

                            <i class="fa fa-instagram"></i>

                        </a>

                    </li>
                    <li>

                        <a class="onfade4" href="http://webmail.saomarcos.org.br/"
                           style="margin-top:-2px; height:42px; font-size:18px;" title="Webmail Hospital São Marcos">
                            @
                        </a>

                    </li>

                </ul>

            </div>

        </div><!-- busca -->

        <div class="row">

            <div class="col-lg-2 col-md-2">

                <h1 class="fl-left fadezero fontzero nomargin nopadding">

                    <?= SITE_NAME; ?>

                </h1>

                <a href="<?= BASE ?>" class="header-logo">

                    <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>"
                         title="<?= SITE_NAME; ?>">

                </a>

            </div>

            <div class="col-lg-10 col-md-10">

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?= BASE ?>">HOME <span class="sr-only">(atual)</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">O HOSPITAL <i class="fa fa-angle-double-down"
                                                                                        aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">
                                <li><a href="<?= BASE ?>/sobre">SOBRE O HOSPITAL</a></li>

                                <li><a href="<?= BASE ?>/diretores">DIRETORIA</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">PACIENTES <i class="fa fa-angle-double-down"
                                                                                       aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">
                                <li><a href="<?= BASE ?>/convenios">Convênios do Hospital</a></li>
                                <li><a href="#">ENCONTRE SEU MÉDICO</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">SERVIÇOS <i class="fa fa-angle-double-down"
                                                                                      aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">
                                <li><a href="<?= BASE ?>/fisioterapia">FISIOTERAPIA</a></li>
                                <li><a href="<?= BASE ?>/nuttem">NUTTEM</a></li>
                                <li><a href="<?= BASE ?>/psicologia">PSICOLOGIA</a></li>
                                <li><a href="<?= BASE ?>/servico-social">SERVIÇO SOCIAL</a></li>
                                <li><a href="<?= BASE ?>/cartao-hsm+">CARTÃO HSM+</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">PROFISSIONAIS <i
                                        class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">

                                <li><a href="<?= BASE ?>/revista-medica" target="_blank">Revista Médica HSM</a></li>
                                <li><a href="<?= BASE ?>/cursos">Cursos</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">NOVIDADES <i class="fa fa-angle-double-down"
                                                                                       aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">
                                <li><a href="<?= BASE ?>/noticias">Notícias</a></li>
                                <li><a href="<?= BASE ?>/videos">Vídeos</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">CONTATO <i class="fa fa-angle-double-down"
                                                                                     aria-hidden="true"></i></a>
                            <ul class="dropdown-menu tuppercase">
                                <li><a href="<?= BASE ?>/contato">FALE CONOSCO</a></li>
                                <li><a href="<?= BASE ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
                            </ul>
                        </li>

                    </ul>
                </div>

                </nav>


            </div>


        </div>

    </div>

</header>

<!-- Our team Section -->
<section class="content-section bg-light-brown">
    <div class="container">

        [ -- COLOCA O CODIGO AQUI -- ]


    </div>
</section>

<footer class="bg-light-gray-100 container-fluid fl-left footer-enable">

    <div class="atomwrapper">

        <div class="centralizar">

            <a href="" class="header-logo">

                <img src="<?= FILE; ?>/_cdn/_front/_assets/_img/header-logo.png" alt="<?= SITE_NAME; ?>"
                     title="<?= SITE_NAME; ?>">

            </a>

        </div>

        <div class="clear10"></div>

        <p class="al-center centertable font18px">

            <?= SITE_NAME . ". " . SITE_SUBNAME ?> - Todos os direitos reservados <?= date('Y') ?>

        </p>

        <div class="clear10"></div>

        <ul class="main-footer-social">

            <li>

                <a class="onfade4" href="https://www.youtube.com/channel/UCVlxRFU-J-nxFWgKM0Kwi8g"
                   title="Nosso canal no youtube">

                    <i class="fa fa-youtube-play"></i>

                </a>

            </li>

            <li>

                <a class="onfade4" href="https://www.facebook.com/saomarcospi/?fref=ts" title="Hospital São Marcos">

                    <i class="fa fa-facebook"></i>

                </a>

            </li>

            <li>

                <a class="onfade4" href="https://www.instagram.com/hospitalsaomarcos/" title="Hospital São Marcos">

                    <i class="fa fa-instagram"></i>

                </a>

            </li>

        </ul>

    </div>


</footer>

<script src="<?= FILE; ?>/_cdn/_front/_libraries/slick/slick.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_libraries/lightbox2-master/lightbox2-master/dist/js/lightbox.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/ios6fix.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/jquery.cycle2.swipe.min.js"></script>
<script src="<?= FILE; ?>/_cdn/_front/_assets/_js/functions.js"></script>
</body>
</html>
