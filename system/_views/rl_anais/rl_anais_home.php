<?php
if (empty($Read)) {
    $Read = new Read;
}

//Auto delete
$Delete = new Delete;
$Delete->ExeDelete(DB_ANAIS, "WHERE nome = '' AND arquivo = '' AND status = :st", "st=0");

$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        <a title="Criar"
                           href="<?= ADM_URL ?>/?dx=<?= DB_ANAIS ?>/create" class="btn btn-info "> <i
                                    class="fa fa-plus"></i></a>
                        <?= $TABLE['TITLE'] ?>
                    </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <?= Admx::formSearch($TABLE['VIEW']) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= (!empty($_GET['s'])) ? "Buscar por '{$_GET['s']}'" : "Gerenciar {$TABLE['TITLE']}" ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th>#Nome</th>
                                    <th>#Arquivo</th>
                                    <th style="width: 20%">#AÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $getPage = filter_input(INPUT_GET, 'pg', FILTER_VALIDATE_INT);
                                $Page = ($getPage ? $getPage : 1);
                                $BaseUrl = "?dx={$view}/home&pg=";
                                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                                $Paginator->ExePager($Page, 10);
                                $formToken = Admx::setFormToken(false);
                                $where = Admx::Trash();
                                $where .= Admx::Search(array('nome', 'arquivo'));

                                $Read->ExeRead(DB_ANAIS, " WHERE 1=1 {$where} ORDER BY created DESC LIMIT :limit OFFSET :offset", "limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <tr id="row_<?= $ROW['id'] ?>" class="item_<?= $ROW['id'] ?>">
                                        <td><?= str_pad($id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=<?= DB_ANAIS?>/create&id=<?= $id ?>"><?= $nome ?></a>
                                            <?= $nome ?>
                                        </td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/uploads/files/<?= $arquivo ?>" target="_blank"><?= $arquivo ?></a>
                                        </td>
                                        <td>
                                            <?= Admx::BtnStatus(DB_ANAIS, 'id', $id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=<?= DB_ANAIS?>/create&id=<?= $id ?>"
                                               title="Editar" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Editar </a>
                                            <?= Admx::BtnDelete(DB_ANAIS, 'id', $id, "row_{$ROW['id']}", $formToken) ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <?php
                            $Paginator->ExePaginator(DB_ANAIS, " WHERE 1=1 {$where} ");
                            echo $Paginator->getPaginator();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php


include './_inc/footer.php';
?>