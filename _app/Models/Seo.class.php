<?php

/**
 * Seo [ MODEL ]
 * Classe de apoio para o modelo LINK. Pode ser utilizada para gerar SSEO para as páginas do sistema!
 *
 * @copyright (c) 2014, Robson V. Leite UPINSIDE TECNOLOGIA
 */
class Seo
{

    private $Pach;
    private $File;
    private $Link;
    private $Key;
    private $Schema;
    private $Title;
    private $Description;
    private $Image;
    public $tipo_curso_id;
    private $DataResult;

    public function __construct($Pach)
    {
        $this->Pach = explode('/', strip_tags(trim($Pach)));
        $this->File = (!empty($this->Pach[0]) ? $this->Pach[0] : null);
        $this->Link = (!empty($this->Pach[1]) ? $this->Pach[1] : null);
        $this->Key = (!empty($this->Pach[2]) ? $this->Pach[2] : null);

        $this->setPach();
        //var_dump($this);
    }

    public function getSchema()
    {
        return $this->Schema;
    }

    public function getTitle()
    {
        return $this->Title;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getImage()
    {
        return $this->Image;
    }

    public function getDataResult()
    {
        return $this->DataResult;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    private function setPach()
    {
        if (empty($Read)):
            $Read = new Read;
        endif;

        $Pages = array();
        $Read->FullRead("SELECT page_name FROM " . DB_PAGES . " WHERE status = 1");
        if ($Read->getResult()):
            foreach ($Read->getResult() as $SinglePage):
                $Pages[] = $SinglePage['page_name'];
            endforeach;
        endif;

        if ($this->File == 'trabalhe-conosco'):
            $this->Schema = 'WebSite';
            $this->Title = "Trabalhe conosco" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'vagas'):
            $this->Schema = 'WebSite';
            $this->Title = "Trabalhe conosco" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif (in_array($this->File, $Pages) && empty($this->Link)):
            //PÁGINAS
            $Read->FullRead("SELECT * FROM " . DB_PAGES . " WHERE page_name = :nm AND status = 1", "nm={$this->File}");
            if ($Read->getResult()):
                $Page = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Page['page_title'] . " - " . SITE_NAME;
                $this->Description = $Page['page_subtitle'];
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Page;
            else:
                $this->set404();
            endif;
        elseif ($this->File == 'index'):
            //INDEX
            $this->Schema = 'WebSite';
            $this->Title = SITE_NAME . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'noticias'):
            $this->Schema = 'WebSite';
            $this->Title = "Notícias" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'cursos'):
            $this->Schema = 'WebSite';
            $this->Title = "Cursos" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';
            $this->tipo_curso_id =  $this->Link;

        elseif ($this->File == 'curso-detalhes'):

            $Read->FullRead("SELECT * FROM dx_cursos WHERE curso_id = :nm ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['curso_title'] . " - " . SITE_NAME;
                $this->Description = SITE_DESC;
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;
        elseif ($this->File == 'cursos-inscricao'):

            $Read->FullRead("SELECT * FROM dx_cursos WHERE curso_id = :nm ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['curso_title'] . " - " . SITE_NAME;
                $this->Description = SITE_DESC;
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'curso'):

            $Read->FullRead("SELECT * FROM dx_cursos WHERE curso_id = :nm ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['curso_title'] . " - " . SITE_NAME;
                $this->Description = SITE_DESC;
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'revista-medica'):
            $Read->FullRead("SELECT * FROM dx_revista WHERE revista_id = :nm ", "nm=1");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['revista_title'] . " - " . SITE_NAME;
                $this->Description = SITE_DESC;
                $this->Image = BASE . "/uploads/{$Post['revista_image']}";
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;


        elseif ($this->File == 'artigo'):
            //ARTIGO
            $where = Admx::Trash();
            $Read->FullRead("SELECT * FROM " . DB_POSTS . " WHERE post_name = :nm AND status = 1 AND post_date <= NOW() {$where} ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];

                $this->Schema = 'WebSite';
                $this->Title = $Post['post_title'] . " - " . SITE_NAME;
                $this->Description = $Post['post_subtitle'];
                $this->Image = BASE . "/uploads/{$Post['post_image']}";
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;
        elseif ($this->File == 'artigos'):
            //ARTIGOS
            $where = Admx::Trash();
            $Read->FullRead("SELECT * FROM " . DB_CATEGORIES . " WHERE category_name = :nm {$where}", "nm={$this->Link}");
            if ($Read->getResult()):
                $Category = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Category['category_title'] . " - " . SITE_NAME;
                $this->Description = $Category['category_content'];
                $this->Image = FILE . '/images/default.jpg';
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'videos'):
            $this->Schema = 'WebSite';
            $this->Title = "Vídeos" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'video'):
            //ARTIGO
            $Read->FullRead("SELECT * FROM " . DB_VIDEOS . " WHERE video_name = :nm AND status = 1 ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];

                $this->Schema = 'WebSite';
                $this->Title = $Post['video_title'] . " - " . SITE_NAME;
                $this->Description = $Post['video_desc'];
                $this->Image = Site::getImageYoutube($Post['video_url']);
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'vaga'):
            //VAGA
            $Read->FullRead("SELECT * FROM " . DB_VAGAS . " WHERE id = :id AND status = 1 ", "id={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];

                $this->Schema = 'WebSite';
                $this->Title = $Post['vaga'] . " - " . SITE_NAME;
                $this->Description = $Post['vaga'];
                $this->Image = BASE . "/uploads/{$Post['image']}";
                $this->DataResult = $Post;

            else:
                $this->set404();
            endif;

        elseif ($this->File == 'responsabilidade-social'):
            //ARTIGO
            $Read->FullRead("SELECT * FROM " . DB_SOCIAIS . " WHERE social_name = :nm AND status = 1 ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['social_title'] . " - " . SITE_NAME;
                $desc = strip_tags($Post['social_content']);
                $this->Description = substr($desc, 0, 100);
                $this->Image = BASE . "/uploads/{$Post['social_image']}";
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'manuais'):
            $this->Schema = 'WebSite';
            $this->Title = "Manuais & Artigos" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'diretoria'):
            $this->Schema = 'WebSite';
            $this->Title = "Diretoria" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'especialidades'):
            $this->Schema = 'WebSite';
            $this->Title = "Diretoria" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'especialidade'):
            //ARTIGO
            $where = Admx::Trash();
            $Read->FullRead("SELECT * FROM " . DB_ESPECIALIDADES . " WHERE especialidade_name = :nm AND status = 1 {$where} ", "nm={$this->Link}");
            if ($Read->getResult()):
                $Post = $Read->getResult()[0];
                $this->Schema = 'WebSite';
                $this->Title = $Post['especialidade_title'] . " - " . SITE_NAME;
                $this->Description = $Post['especialidade_title'] . " , " . SITE_DESC;
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;

        elseif ($this->File == 'convenios'):
            $this->Schema = 'WebSite';
            $this->Title = "Convênios do Hospital" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/convenios.png';

        elseif ($this->File == 'diretores'):
            $this->Schema = 'WebSite';
            $this->Title = "Diretoria" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';


        elseif ($this->File == 'profissionais'):
            $this->Schema = 'WebSite';
            $this->Title = "Profissionais" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'profissional'):
            $idMedico = $this->Key;
            $Read->FullRead('SELECT * FROM hsm_medicos m  WHERE m.Id_Medico =:id  ORDER BY Nm_Medico ', "id={$idMedico}");
            if ($Read->getResult()) {
                $result = $Read->getResult();
                $medico = $result[0];
                $Read->FullRead('SELECT * FROM hsm_horarios  WHERE Id_Medico =:id', "id={$idMedico}");
                $medico['horario'] = $Read->getResult();

                $Read->FullRead('SELECT * FROM hsm_dados  WHERE id_Medico =:id', "id={$idMedico}");
                if ($Read->getResult()) {
                    $medico['dados'] = $Read->getResult();
                }

                $this->Schema = 'WebSite';
                $this->Title = "Médico: {$medico['Nm_Medico']} - CRM: {$medico['CRM_Medico']} - " . SITE_NAME . " - " . SITE_SUBNAME;
                $arr_especialidades = array($medico['nm_especialidade1'], $medico['nm_especialidade2'], $medico['nm_especialidade3']);
                foreach ($arr_especialidades as $key => $value) {
                    if (!empty($value)) {
                        $especialidades[] = $value;
                    }
                }
                $this->Description = "Médico: {$medico['Nm_Medico']} - CRM: {$medico['CRM_Medico']} - Clinica: {$medico['Nm_Clinica']} - Especialidade: " . implode(",", $especialidades);
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $medico;
            } else {
                $this->set404();
            }

        elseif ($this->File == 'contato'):
            $this->Schema = 'WebSite';
            $this->Title = "Contato" . " - " . SITE_SUBNAME;
            $this->Description = "envie sugestões, dúvidas, elogios ou reclamações.";
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'pesquisa'):
            //PESQUISA
            $this->Schema = 'WebSite';
            $this->Title = "Pesquisa por {$this->Link} em " . SITE_NAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';
        elseif ($this->File == 'anais'):
            $this->Schema = 'WebSite';
            $this->Title = "Anais" . " - " . SITE_SUBNAME;
            $this->Description = SITE_DESC;
            $this->Image = FILE . '/images/default.jpg';

        elseif ($this->File == 'comite-de-etica'):
            $Read->FullRead("SELECT * FROM " . DB_COMITE_ETICA . " WHERE id = 1");

            if ($Read->getResult()):
                $Post = $Read->getResult()[0];

                $this->Schema = 'WebSite';
                $this->Title = $Post['titulo'] . " - " . SITE_NAME;
                $this->Description = $Post['titulo'];
                $this->Image = FILE . '/images/default.jpg';
                $this->DataResult = $Post;
            else:
                $this->set404();
            endif;
        else:
            //404
            $this->set404();
        endif;

        //die($this->File);
    }

    private function set404()
    {
        $this->Schema = 'WebSite';
        $this->Title = "Oppsss, nada encontrado! - " . SITE_NAME;
        $this->Description = SITE_DESC;
        $this->Image = FILE . '/images/default.jpg';
    }

    /**
     * @return mixed
     */
    public function getTipoCursoId()
    {
        return $this->tipo_curso_id;
    }

}
