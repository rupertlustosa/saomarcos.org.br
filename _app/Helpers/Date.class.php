<?php

class Date
{
    var $date = null;

    function __construct($date)
    {
        $this->date = $date;
    }

    public function getDia()
    {
        $newDate = new DateTime($this->date);
        return $newDate->format('d');
    }

    public function getDiaName()
    {
        $newDate = new DateTime($this->date);
        $dia = $newDate->format('w');
        $array = array("Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado");
        return $array[$dia];
    }

    public function getMes()
    {
        $newDate = new DateTime($this->date);
        return $newDate->format('m');
    }

    public function getMesName()
    {
        $newDate = new DateTime($this->date);
        $mes = $newDate->format('n');
        $array = array("", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
        return $array[$mes];
    }

    public function getAno($format = 'Y')
    {
        $newDate = new DateTime($this->date);
        return $newDate->format($format);
    }

    static function formatDate($date, $format)
    {
        $newDate = new DateTime($date);
        return $newDate->format($format);
    }

}