<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'profissionais';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
$PostFormToken = (!empty($PostData['formToken'])) ? $PostData['formToken'] : null;
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];

    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            if ($PostFormToken == $formToke):
                $Read->ExeRead(DB_PROFISSIONAIS, "WHERE profissional_id = :id", "id={$KeyId}");
                if (!$Read->getResult()):

                else:
                    $name = !empty($PostData['profissional_title']) ? Check::Name($PostData['profissional_title']) : $post['profissional_id'];

                    $ReadName = new $Read;
                    $ReadName->FullRead("SELECT profissional_name FROM " . DB_PROFISSIONAIS . " WHERE profissional_name= :nm and profissional_id != :id ", "nm={$name}&id={$KeyId}");
                    if ($ReadName->getResult()):
                        $PostData['profissional_name'] = $name . time();
                    else:
                        $PostData['profissional_name'] = $name;
                    endif;

                    $PostData['status'] = ($rascunho) ? 0 : 1;

                    $post = $Read->getResult()[0];
                    $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE profissional_id = :id", "id={$KeyId}");
                    $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
                endif;
            else:
                die('<center><h1>Permissão Negada!</h1></center>');
            endif;

            break;


    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
