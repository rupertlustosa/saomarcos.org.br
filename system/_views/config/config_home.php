<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');

include '_inc/header.php';
include '_inc/nav.php';

$KeyId = 1;
$metaTextArea = false;
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Configurações</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="campos">
                        <?php include './_inc/campos_meta.php'; ?><!-- inclui os campos metas para o post -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php
include './_inc/footer.php';