<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>LOCALIZAÇÃO</h2>
                    <h3 class="caption color-black-100 width50 centertable">Duis non condimentum nunc. Nunc quis turpis
                        eu est tincidunt rutrum. Cras a purus quis sem tincidunt egestas vel id lacus.</h3>
                    <div class="clear20"></div>
                </div><!-- /.col-md-12 -->
                <div class="col-md-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m13!1m11!1m3!1d4062.7748422829886!2d-42.801550841717415!3d-5.090099859487547!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1spt-BR!2sus!4v1474466754972"
                            width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section><!-- /.our-team -->
<?php include "inc/footer.php" ?>