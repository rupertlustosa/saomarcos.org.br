<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'videos';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
//VALIDA AÇÃO

if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack && $PostData['formToken'] == $formToke):
    $Case = $PostData['callback_action'];
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":

            $Read->ExeRead(DB_VIDEOS, "WHERE video_id = :id", "id={$KeyId}");
            $post = $Read->getResult()[0];
            if (!$Read->getResult()):


            else:
                $name = !empty($PostData['video_title']) ? Check::Name($PostData['video_title']) : $post['video_id'];
                $ReadName = new $Read;
                $ReadName->FullRead("SELECT video_name FROM " . DB_VIDEOS . " WHERE video_name= :nm and video_id != :id ", "nm={$name}&id={$KeyId}");
                if ($ReadName->getResult()):
                    $PostData['video_name'] = $name . time();
                else:
                    $PostData['video_name'] = $name;
                endif;

                $PostData['status'] = ($rascunho) ? 0 : 1;
                $PostData["video_title"] = htmlspecialchars($PostData["video_title"]);
                $PostData['video_date'] = Check::DateEncode($PostData['video_date']);
                $post = $Read->getResult()[0];
                $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE video_id = :id", "id={$KeyId}");
                $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
            endif;
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
