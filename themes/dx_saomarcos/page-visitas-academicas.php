<?php
extract($DataResult);
include "inc/header.php" ?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="clear40"></div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <?php
                if ($page_image) {
                    echo '<img src="' . BASE . '/uploads/' . $page_image . '">';
                }
                ?>
                <div class="clear20"></div>
                <h2><?= $page_title ?></h2>
                <h3 class="caption color-black-100 width50 centertable">
                    <?= $page_chamada ?>
                </h3>
                <div class="clear20"></div>
            </div><!-- /.col-md-12 -->
            <div class="col-md-6">
                <div class="texto">
                    <?= $page_content ?>
                </div>
                <div class="page-galeria">
                    <?php
                    $galery = Site::getGalley(5, $page_id);
                    if ($galery):
                        foreach ($galery as $img) {
                            ?>
                            <a data-lightbox="roadtrip" href="<?= BASE . "/uploads/" . $img['file_src'] ?>"
                               title="<?= $img['file_name'] ?> ">
                                <img alt="<?= $img['file_name'] ?>"
                                     src="<?= BASE . "/tim.php?src=/uploads/" . $img['file_src'] . "&w=220&h=140" ?>"
                                     title="<?= $img['file_name'] ?>"/>
                            </a>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-md-6">
                <form style="text-align:left">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" placeholder="Nome">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="profissao">Profissão</label>
                            <input type="text" class="form-control" id="profissao" placeholder="Profissão">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="empresa">Empresa</label>
                            <input type="text" class="form-control" id="empresa" placeholder="Empresa">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="enderecocomercial">Endereço Comercial</label>
                            <input type="text" class="form-control" id="enderecocomercial"
                                   placeholder="Endereço Comercial">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="empresa">Empresa</label>
                            <input type="text" class="form-control" id="empresa" placeholder="Empresa">
                        </div>

                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
    <div class="clear40"></div>
</section><!-- /.our-team -->
<?php include "inc/footer.php" ?>
