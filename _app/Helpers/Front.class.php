<?php

class Front
{

    var $css = array();
    var $script = array();

    public function set_css($link, $rel = "stylesheet", $media = "all")
    {
        $this->css[] = array("link" => $link, "rel" => $rel, "media" => $media);
    }

    public function set_js($link)
    {
        $this->script[] = $link;
    }


    public function CSS()
    {
        $css = $this->css;
        foreach ($css as $c) {
            echo '<link href="' . $c['link'] . '" rel="' . $c['rel'] . '" media="' . $c['media'] . '">' . "\n";
        }
    }

    public function JS()
    {
        $js = $this->script;
        foreach ($js as $j) {
            echo '<script type="text/javascript" src="' . $j . '"></script>' . "\n";
        }
    }


}
