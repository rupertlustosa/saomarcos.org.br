<?php
require '../_app/autoload.php';
require('../_app/Library/PHPMailer/class.phpmailer.php');

$params = array();
parse_str($_POST["data"], $params);
function clean_string($string) {
  $bad = array("content-type","bcc:","to:","cc:","href");
  return str_replace($bad,"",$string);
}

foreach ($params as $key=>$value){
    $params["visita_".$key] = clean_string($value);
}

$visita = $params;
if(isset($params['areas'])){
  $visita['visita_areas'] = implode(", ",$params['visita_areas']);
}


$types = array(
    'application/x-compressed',
    'application/x-zip-compressed',
    'application/zip',
    'multipart/x-zip',
    'application/octet-stream',
    'image/jpeg',
    'image/png',
    'application/pdf',
    'application/x-7z-compressed',

);
$maxSize = 1024 * 1024 * 2; // 2Mb
$File = null;

if (!empty($_FILES['arquivo'])):
    $File = $_FILES['arquivo'];
endif;
$ext = substr($File['name'],-4);
$nome_final = md5(time()).$ext;
if($File){
    if(in_array($File['type'],$types)){
        if($File['size'] <= $maxSize){
            if (move_uploaded_file($File['tmp_name'], '../uploads/files/' . $nome_final)) {
                $visita['visita_comprovante'] = $nome_final;
            }
        }else{
            echo "Arquivo muito grande máximo 2mb";
            die();
        }
    }else{
        echo "arquivo inválido";
        die();
    }
}

$Create = new Create();
$Create->ExeCreate("dx_visitas_tecnicas", $visita);

$HTML = 'Visita Tecnica <br><br>';
$HTML .= "Nome: {$visita['visita_nome']} <br>";
$HTML .= "E-Mail: {$visita['visita_email']} <br>";
$HTML .= "Profissão: {$visita['visita_profissao']} <br>";
$HTML .= "Empresa: {$visita['visita_empresa']} <br>";
$HTML .= "Endereço Comercial: {$visita['visita_enderecocomercial']} <br>";
$HTML .= "Estado: {$visita['visita_estado']} <br>";
$HTML .= "Cep: {$visita['visita_cep']} <br>";
$HTML .= "Telefone: {$visita['visita_telefone']} <br>";
$HTML .= "CNPJ: {$visita['visita_cnpj']} <br>";
$HTML .= "Unidade/Setor: {$visita['visita_unidade_setor']} <br>";
$HTML .= "Objetivo: {$visita['visita_objetivo']} <br>";
$HTML .= "Áreas a ser visitadas: {$visita['visita_areas']} <br>";

$mail = new PHPMailer();
$mail->isSMTP();
$mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
$mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
$mail->Password = 'apcchsm$69';                           // SMTP password
$mail->Port = 25;
$mail->FromName = "WebMaster";

$mail->AddAddress('francisco@cjflash.com.br', 'WebMaster CJFLASH');
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->Subject  = "|| VISITA TECNICA ||"; // Assunto da mensagem
$mail->Body = $HTML;

$enviado = $mail->Send();
if ($enviado) {
  echo 1;
} else {
  echo "Não foi possível enviar o e-mail.";
  echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
}
