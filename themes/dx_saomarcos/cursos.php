<?php include "inc/header.php";

$tipo_curso_id = (int)$URL[1];

$read2 = new Read();
$read2->ExeRead(DB_TIPOS_CURSOS, " WHERE id = $tipo_curso_id");
$tipo_curso = $read2->getResult();


$read3 = new Read();

?>
    <!-- Our team Section -->
    <section class="content-section bg-light-brown">
        <div class="container">


            <section id="postIndex" class="widthWrapper">

                <div class="col-md-12">

                    <ul id="ul-listagem">
                        <li class="no-padding-left no-border">
                            CURSOS >>
                        </li>

                        <li>
                            <a href="<?= BASE . '/cursos/' ?>">Todos</a>
                        </li>

                        <?php
                        $tipo_curso_2_ = Site::getTipoCurso();
                        foreach ($tipo_curso_2_ as $in => $row) {

                            $cl = '';
                            if ($in == (count($tipo_curso_2_) - 1)) {

                                $cl = 'class="no-border"';
                            }
                            echo '<li ' . $cl . '><a href="' . BASE . '/cursos/' . $row['id'] . '">' . $row['tipo'] . '</a></li>';
                        }
                        ?>

                    </ul>

                </div>

                <?php

                $getPage = (!empty($URL[2])) ? $URL[2] : null;
                $Page = ($getPage ? $getPage : 1);
                $BaseUrl = BASE . "/cursos/pg/";
                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                $Paginator->ExePager($Page, 100);
                $where = Admx::Trash();

                if (empty($tipo_curso_id) || !count($tipo_curso)) {

                    $complemento = " ";
                } else {

                    $complemento = "AND tipo_curso_id = $tipo_curso_id ";
                }

                $Read->ExeRead(DB_CURSOS, " WHERE status=:st {$where} {$complemento} ORDER BY created DESC LIMIT :limit OFFSET :offset", "st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");

                $resultados = $Read->getResult();
                if (count($resultados)) {

                    foreach ($resultados as $ROW):

                        extract($ROW);

                        $read3->ExeRead(DB_TIPOS_CURSOS, " WHERE id = $tipo_curso_id");
                        $tipo_curso = $read3->getResult();

                        if (!empty($link_externo)) {

                            $urlCurso = $link_externo;
                            $targetCurso = '_blank';
                        } else {

                            $urlCurso = BASE . '/curso-detalhes/' . $curso_id;
                            $targetCurso = '_self';
                        }
                        ?>

                        <div class="col-md-6">
                            <a href="<?= $urlCurso ?>" target="<?= $targetCurso ?>" title="<?= $curso_title ?>">
                                <div class="col-md-12 no-padding no-margin">

                                    <div id="escopo-imagem">
                                        <img src="<?= BASE; ?>/tim.php?src=uploads/<?= $image ?>&w=500&h=300"/>
                                        <div class="legenda">

                                            <?php
                                            if (isset($image) && !empty($image))
                                                echo mb_strtoupper($tipo_curso[0]['tipo']);
                                            ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 no-padding no-margin">
                                    <h3 class="color-black-300">
                                        <b><?= $curso_title ?></b>
                                    </h3>

                                    <h4 class="color-red-300 fontsize1b">CARGA HORÁRIA: <?= $carga_horaria ?></h4>
                                </div>
                            </a>
                        </div>
                    <?php
                    endforeach;
                } else {
                    echo "<center><h3 class=\"caption color-black-100 width100 centertable\">Não existem cursos para essa categoria!</h3></center>";
                }

                ?>

            </section>
            <div style="display:table; margin: 20px auto">
                <?php
                $Paginator->ExePaginator(DB_CURSOS, " WHERE status=:st {$where} ", "&st=1");
                echo $Paginator->getPaginator();
                ?>
            </div>
        </div>
        <div class="clear40"></div>
    </section>

<?php include "inc/footer.php" ?>