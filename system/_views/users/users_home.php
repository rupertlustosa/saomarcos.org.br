<?php
$TABLE = getTableList($view);
if (empty($Read)) {
    $Read = new Read;
}

$front = new Front();
$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');


include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?= $TABLE['TITLE'] ?></h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Gerenciar <?= $TABLE['TITLE'] ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <!-- start list -->
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                                </div>

                                <div class="clearfix"></div>

                                <?php
                                $Read->ExeRead(DB_USERS, " ORDER BY user_status DESC ");
                                $formToken = Admx::setFormToken(false);
                                if (!$Read->getResult()):
                                    echo '<div role="alert" class="alert alert-warning alert-dismissible fade in">
                                        <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span>
                                        </button>
                                        Nenhum registro foi encontrado
                                    </div>';
                                else:
                                    foreach ($Read->getResult() as $post):
                                        $bio = nl2br($post['user_bio']);
                                        $user_id = $post['user_id'];
                                        ?>
                                        <div class="col-md-4 col-sm-4 col-xs-12 profile_details default_single <?= ($post['user_status'] == 0) ? 'rascunho' : '' ?>"
                                             rel="<?= $post['user_id'] ?>" class="item_<?= $post['user_id'] ?>">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <h4 class="brief">
                                                        <i><?= getUserLevels($post['user_level'], 'name') ?></i></h4>
                                                    <div class="left col-xs-8">
                                                        <h2><?= $post['user_name'] ?></h2>
                                                        <p><?= (!empty($post['user_bio'])) ? "<strong>Sobre: </strong> <br>{$bio}" : "" ?></p>
                                                        <ul class="list-unstyled">
                                                            <!--<li><i class="fa fa-phone"></i>&nbsp;(86) 98877-8384 </li>-->
                                                            <li>
                                                                <i class="fa fa-envelope"></i>&nbsp;<?= $post['user_email'] ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="right col-xs-4 text-center">
                                                        <img class="img-circle img-responsive cover" alt=""
                                                             src="../tim.php?src=<?= Users::GetUserImage($post['user_id']) ?>&w=200&h=200">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-12 emphasis">
                                                        <?php

                                                        if ($post['user_status'] == 0) {
                                                            echo '<button title="Ativar" callback="Users" rel="default_single" callback_action="status"  rel="1" class="status btn btn-default btn-xs"><i class="fa fa-exclamation-triangle"></i> Ativar</button>';
                                                        } else {
                                                            echo '<button title="desativar" callback="Users" rel="default_single"  callback_action="status"  rel="0" class="status btn btn-primary btn-xs"><i class="fa fa-check"></i></button>';
                                                        }
                                                        ?>
                                                        <a href="<?= ADM_URL ?>/?dx=users/create&id=<?= $post['user_id'] ?>"
                                                           class="btn btn-success btn-xs">
                                                            <i class="fa fa-edit"></i> Editar
                                                        </a>
                                                        <!--
                                                        <button type="button" callback="Users" callback_action="delete" class="delete btn btn-danger btn-xs" rel="default_single"><i class="fa fa-trash"></i> Excluir </button>
                                                        -->
                                                        <?= Admx::BtnDelete("dx_users", 'user_id', $user_id, "row_{$user_id}", $formToken) ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php

                                    endforeach;
                                endif;
                                ?>
                                <!-- end project list -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

<?php

include './_inc/footer.php';
    