<?php
ob_start();
include "./_app/autoload.php";
$action = Anti::String($_POST['action']);
$clinica = (!empty($_POST['clinica'])) ? $_POST['clinica'] : null;
$especialidade = (!empty($_POST['especialidade'])) ? $_POST['especialidade'] : null;

if($action == 'get_profissionais'){
    $clinica = Anti::String($clinica);
    $especialidade = Anti::String($especialidade);
    $where = Admx::Trash();
    if($clinica){
        $where .= " AND profissional_clinica_id = '$clinica'";
    }
    if($especialidade){
        $where .= " AND profissional_especialidade_id = '$especialidade'";
    }
    
    $read = new Read();
    $read->ExeRead(DB_PROFISSIONAIS," WHERE status =:st {$where} ","st=1");
    echo "<option></option>";
    if($read->getResult()){
         foreach($read->getResult() as $row){
             echo "<option value='".BASE."/profissional/{$row['profissional_name']}'>{$row['profissional_title']}</option>";
         }
    }
   
}


ob_end_flush();
