<?php include "inc/header.php" ?>
    <!-- Our team Section -->
    <section class="content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>VÍDEOS</h2>
                </div><!-- /.col-md-12 -->
            </div>
            <section id="postIndex" class="widthWrapper">
                <?php

                $getPage = (!empty($URL[2])) ? $URL[2] : null;
                $Page = ($getPage ? $getPage : 1);
                $BaseUrl = BASE . "/videos/pg/";
                $Paginator = new Pager($BaseUrl, '<<', '>>', 5);
                $Paginator->ExePager($Page, 10);
                $where = Admx::Trash();

                $Read->ExeRead(DB_VIDEOS, " WHERE status=:st {$where} ORDER BY video_date DESC LIMIT :limit OFFSET :offset", "st=1&limit={$Paginator->getLimit()}&offset={$Paginator->getOffset()}");
                foreach ($Read->getResult() as $ROW):
                    extract($ROW);
                    ?>
                    <article class="postInner">
                        <a href="<?= BASE ?>/video/<?= $video_name ?>" title="<?= $video_title ?>">
                            <div class="postImg">
                                <img src="<?= Site::getImageYoutube($video_url) ?>"/>
                            </div>
                            <h2 class="color-red-300 fontsize1b"><?= $video_title ?></h2>
                            <p><?= $video_desc ?></p>
                        </a>
                    </article>
                    <?php
                endforeach;
                ?>
            </section>
            <div style="display:table; margin: 20px auto">
                <?php
                $Paginator->ExePaginator(DB_VIDEOS, " WHERE status=:st {$where} ", "&st=1");
                echo $Paginator->getPaginator();
                ?>
            </div>
        </div>
        <div class="clear40"></div>
    </section>

<?php include "inc/footer.php" ?>