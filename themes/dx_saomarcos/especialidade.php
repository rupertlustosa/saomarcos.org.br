<?php

include "inc/header.php";


?>
    <!-- Our team Section -->
    <section class="team content-section bg-light-brown">
        <div class="container">
            <div class="row text-center">

                <div class="col-md-12">

                    <h2><?= $DataResult['especialidade_title'] ?></h2>
                    <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">
                        Profissionais</h3>
                </div><!-- /.col-md-12 -->

                <div class="clear40"></div>

                <div class="container">

                    <div class="row">

                        <?php
                        $especialidade = $DataResult['especialidade_name'];
                        $especialidade_id = $DataResult['especialidade_id'];
                        $medico = new Medico('busca');
                        $medico->setParams("tipo=especialidade&especialidade={$especialidade_id}");
                        $medico->exe();

                        foreach ($medico->getResult() as $medico):
                            ?>
                            <div class="col-lg-3">
                                <article class="SvItem">
                                    <div class="anchorSv onfade4 zd100">
                                        <div class="icoContentSv zd50">
                                            <div class="insetBlk centralizar">
                                                <span class="fa fa-user-md fontsize2"></span>
                                            </div>
                                        </div>
                                        <div class="brdContentSv zd45">
                                            <div class="insetSlk centralizar">
                                                <h1 class="font15px al-center">
                                                    <a style="font-size:15px;"
                                                       href="<?= BASE ?>/profissional/<?= $medico->slug ?>/<?= $medico->id_medico ?>"><?= $medico->Nm_Medico ?></a>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <?php
                        endforeach;
                        ?>
                    </div>

                </div><!-- /.container -->
            </div><!-- /.row -->
        </div><!-- /.container -->

    </section><!-- /.our-team -->

<?php include "inc/footer.php" ?>