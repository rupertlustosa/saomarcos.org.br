<?php
/**
 * Created by PhpStorm.
 * User: rupert
 * Date: 22/04/17
 * Time: 19:16
 */

namespace Pagseguro;


class Shipping
{
    public $address;
    public $type;
    public $cost;

    /**
     * @return mixed
     */
    public function getShipping()
    {
        return $this;
    }


    /**
     * @param Address $address
     * @return Shipping
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @param mixed $type
     * @return Shipping
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param mixed $cost
     * @return Shipping
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }
}