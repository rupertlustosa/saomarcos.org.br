<?php
/**
 * Created by PhpStorm.
 * User: rupert
 * Date: 22/04/17
 * Time: 19:22
 */

namespace Pagseguro;

use laravel\pagseguro\Facades\PagSeguro;

class Checkout
{
    public $items;
    public $shipping;
    public $sender;

    /**
     * @return mixed
     */
    public function getCheckout()
    {
        return $this;
    }

    /**
     * @param ArrayOfItens $items
     * @return Checkout
     */
    public function setItems(ArrayOfItens $items)
    {
        $this->items = $items->getItens();
        return $this;
    }

    /**
     * @param mixed $shipping
     * @return Checkout
     */
    public function setShipping(Shipping $shipping)
    {
        $this->shipping = $shipping;
        return $this;
    }

    /**
     * @param mixed $sender
     * @return Checkout
     */
    public function setSender(Sender $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    function convert($item)
    {
        //dd($item);
        return (array)$item;
    }

    function objectToArray($object)
    {
        if (!is_object($object) && !is_array($object))
            return $object;

        return array_map([$this, 'objectToArray'], (array)$object);
    }


    public function realizaCheckout()
    {
        if ($this->items && $this->shipping && $this->shipping) {

            $object = $this->getCheckout();

            $data = json_decode(json_encode($object), true);

            $pagseguro = new PagSeguro();

            $checkout = $pagseguro->checkout()->createFromArray($data);
            $credentials = $pagseguro->credentials()->get();
            $information = $checkout->send($credentials); // Retorna um objeto de laravel\pagseguro\Checkout\Information\Information

            if ($information) {
                /*print_r($information->getCode());
                print_r($information->getDate());
                print_r($information->getLink());*/

                return [
                    'code' => $information->getCode(),
                    'date' => $information->getDate()->format('Y-m-d H:i:s'),
                    'link' => $information->getLink(),
                    'data' => json_encode($data),
                ];
            }
        } else {
            throw new \Exception('Faltam dados para realizar o Checkout');
        }
    }
}