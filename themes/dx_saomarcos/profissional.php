<?php
include "inc/header.php";

if ($DataResult) {
    $arr_especialidades = array($DataResult['nm_especialidade1'], $DataResult['nm_especialidade2'], $DataResult['nm_especialidade3']);
    foreach ($arr_especialidades as $key => $value) {
        if (!empty($value)) {
            $especialidades[] = $value;
        }
    }
} else {
    //header("location: ".BASE);
}

?>
<!-- Our team Section -->
<section class="team content-section bg-light-brown">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="clear40"></div>
                <h2><?= $DataResult['Nm_Medico'] ?></h2>
                <h3 class="caption color-black-100 width50 centertable" style="font-weight:normal; color:#999">
                    <?= implode(",", $especialidades) ?><br>
                </h3>
            </div><!-- /.col-md-12 -->

            <div class="clear40"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="atomwrapper nopadding" style="text-align: left">
                            <h4 style="text-align: left">Dados Pessoais:</h4>
                            <p><strong>Nome: </strong><?= $DataResult['Nm_Medico'] ?></p>
                            <p><strong>CRM: </strong><?= $DataResult['CRM_Medico'] ?></p>
                            <p><strong>Especialidade: </strong><?= implode(",", $especialidades) ?></p>
                            <p><strong>Clinica: </strong><?= $DataResult['Nm_Clinica'] ?></p>
                            <?php if ($DataResult['horario']): ?>
                                <p><strong>Hor&aacute;rios:</strong><br>
                                    <?php
                                    foreach ($DataResult['horario'] as $hr) {
                                        echo "- {$hr['dia']}: {$hr['hr_inicio']} - {$hr['hr_fim']} <br>";
                                    }
                                    ?>
                                </p>
                            <?php endif ?>

                            <?php
                            if (isset($DataResult['dados'])) {
                                if (isset($DataResult['dados'][0]['graduacao'])) {
                                    echo '<p><strong>Gradua&ccedil&atilde;o</strong><br>';
                                    echo $DataResult['dados'][0]['graduacao'] . '</p>';
                                };

                                if (isset($DataResult['dados'][0]['posgraduacao'])) {
                                    echo '<p><strong>P&oacute;s-gradua&ccedil&atilde;o</strong><br>';
                                    echo nl2br($DataResult['dados'][0]['posgraduacao']) . '</p>';

                                };

                                if (isset($DataResult['dados'][0]['vidaprofissional'])) {
                                    echo '<p><strong>V&iacute;da Profissional</strong><br>';
                                    echo nl2br($DataResult['dados'][0]['vidaprofissional']) . '</p>';
                                };
                            }
                            ?>


                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clear20"></div>

</section><!-- /.our-team -->

<?php include "inc/footer.php" ?>
