<?php

//AdxErro :: Exibe erros lançados :: Front
function AdxErro($ErrMsg, $ErrNo, $ErrDie = null) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? DX_INFOR : ($ErrNo == E_USER_WARNING ? DX_ALERT : ($ErrNo == E_USER_ERROR ? DX_ERROR : $ErrNo)));
    echo "<div class=\"alert trigger {$CssClass} alert-dismissible fade in\" role=\"alert\">
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\"><p style=\"margin-right:20px;color:#000;\" class=\"fa fa-close fa-lg\"></p></span></button>
          <p style=\"margin:0px\">{$ErrMsg}</p>
    </div>";
    if ($ErrDie):
        die;
    endif;
}

function AdxAlert($MSG,$Class){
    /*
    <div class="alert alert-success" role="alert">...</div>
    <div class="alert alert-info" role="alert">...</div>
    <div class="alert alert-warning" role="alert">...</div>
    <div class="alert alert-danger" role="alert">...</div>
     *
     */
    $trigger = '<div class="alert '.$Class.' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$MSG.'</div>';
    return $trigger;
}

//PHPErro :: Personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? DX_INFOR : ($ErrNo == E_USER_WARNING ? DX_ALERT : ($ErrNo == E_USER_ERROR ? DX_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">";
    echo "<strong>Erro na linha: {$ErrLine} :: </strong> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}

 ?>
