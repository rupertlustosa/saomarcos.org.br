<?php

require '../_app/autoload.php';
require('../_app/Library/PHPMailer/class.phpmailer.php');
require_once '../_app/Config/database.php';

$params = [];

foreach ($_POST as $key => $value) {

    $params[$key] = trim($value);
}

$params['status'] = 1;

$jSON['status'] = '';
$jSON['msg'] = "";

if (Check::CheckCPF($params['cpf'])) {

    $types = ['application/pdf'];

    $maxSize = 1024 * 1024 * 8; // 8Mb

    $File = null;

    if (!empty($_FILES['arquivo'])):
        $File = $_FILES['arquivo'];
    endif;

    $ext = substr($File['name'], -4);
    $nome_final = 'artigo-' . md5(time()) . $ext;

    if ($File) {

        if (in_array($File['type'], $types)) {

            if ($File['size'] <= $maxSize) {

                if (move_uploaded_file($File['tmp_name'], '../uploads/files/' . $nome_final)) {

                    $params['arquivo'] = $nome_final;
                }

            } else {

                $jSON['status'] = 'error';
                $jSON['msg'] = "Arquivo muito grande";
            }
        } else {

            $jSON['status'] = 'error';
            $jSON['msg'] = "Arquivo inválido";
        }

        if ($jSON['status'] != 'error') {

            $Create = new Create();
            $Create->ExeCreate("rl_artigos", $params);

            if (AMBIENTE == 'cliente') {

                $mail = new PHPMailer();
                $mail->isSMTP();
                $mail->Host = 'webmail.saomarcos.org.br';  // Specify main and backup SMTP servers
                $mail->Username = 'avisos@saomarcos.org.br';                 // SMTP username
                $mail->Password = 'apcchsm$69';                           // SMTP password
                $mail->Port = 25;
                $mail->FromName = 'Hospital Sao Marcos';
                $mail->AddAddress(trim($params['email']), $params['nome']);
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                $mail->Subject = "|| ENVIO DE ARTIGO ||";

                $msg = '<div style="border: solid 1px #b10d1d;border-top-width: 5px; width: 600px; max-width: 100%; margin: 20px auto; font-family: arial ">
<img src="http://www.saomarcos.org.br/themes/dx_saomarcos/_cdn/_front/_assets/_img/header-logo.png" style="display:table; margin:20px auto">
<div style="background-color: #8a0e0b; padding: 10px; color:#fff; text-align: center;">
    ENVIO DE ARTIGO
</div>
<div style="padding: 40px 20px; text-align: justify; text-align:center ">
	' . $params['nome'] . ', agradecemos pelo envio do arquivo. Em breve entraremos em contato
</div>
</div>';
                $mail->AddAddress('centrodepesquisa@saomarcos.org.br', 'Centro de Ensino e Pesquisa');

                $mail->Body = $msg;
                $mail->Send();
            }

            $jSON['status'] = 'success';
            $jSON['msg'] = "Artigo enviado com sucesso";
        }
    } else {

        $jSON['status'] = 'error';
        $jSON['msg'] = "Falta enviar o artigo";
    }


} else {

    $jSON['status'] = 'error';
    $jSON['msg'] = "Digite um CPF válido";
}

echo json_encode($jSON);
