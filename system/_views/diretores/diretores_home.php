<?php
if (empty($Read)) {
    $Read = new Read;
}


$front->set_css(ADM_FILE . '/_css/folha.css');
$front->set_js(ADM_FILE . '/_js/admx.js');
include './_inc/header.php';
include './_inc/nav.php';
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3><?= $TABLE['TITLE'] ?></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= (!empty($_GET['s'])) ? "Buscar por '{$_GET['s']}'" : "Gerenciar {$TABLE['TITLE']}" ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!-- start project list -->
                            <table id="datatable" class="table table-striped table-bordered">
                                <!--<table class="table table-striped projects">-->
                                <thead>
                                <tr>
                                    <th style="width: 50px">#Cod</th>
                                    <th>#Cargo</th>
                                    <th>#Nome</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $formToken = Admx::setFormToken(false);
                                $where = Admx::Trash();
                                $Read->ExeRead(DB_DIRETORES, " WHERE 1=1 {$where} ORDER BY status DESC, diretoria_title ASC ");
                                foreach ($Read->getResult() as $ROW):
                                    extract($ROW);
                                    ?>
                                    <tr id="row_<?= $ROW['diretoria_id'] ?>" class="class_<?= $ROW['diretoria_id'] ?>">
                                        <td><?= str_pad($diretoria_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td>
                                            <?= $diretoria_cargo ?>
                                        </td>
                                        <td>
                                            <a href="<?= ADM_URL ?>/?dx=diretores/create&id=<?= $diretoria_id ?>"
                                               title="Editar: <?= $diretoria_title ?>"><?= $diretoria_title ?></a>
                                        </td>
                                        <td>
                                            <?= Admx::BtnStatus(DB_DIRETORES, 'diretoria_id', $diretoria_id, $status) ?>
                                            <a href="<?= ADM_URL ?>/?dx=diretores/create&id=<?= $diretoria_id ?>"
                                               title="Editar: <?= $diretoria_title ?>" class="btn btn-info btn-xs"><i
                                                        class="fa fa-pencil"></i> Edit </a>
                                            <?= Admx::BtnDelete(DB_DIRETORES, 'diretoria_id', $diretoria_id, "row_{$ROW['diretoria_id']}", $formToken) ?>

                                        </td>
                                    </tr>
                                    <?php
                                endforeach;

                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


<?php


include './_inc/footer.php';
?>