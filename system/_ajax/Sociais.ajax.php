<?php
require '../../_app/autoload.php';

usleep(40000);

//DEFINE O CALLBACK E RECUPERA O POST
$jSON = null;
$view = 'sociais';
$TABLE = getTableList($view);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$KeyId = $PostData['key_id'];
$rascunho = (!empty($PostData['rascunho'])) ? $PostData['rascunho'] : null;
$File = null;
$jSON['error'] = null;
$CallBack = $TABLE['CALLBACK'];
$formToke = Admx::getFormToken();
$PostFormToken = (!empty($PostData['formToken'])) ? $PostData['formToken'] : null;
//VALIDA AÇÃO
if ($PostData && $PostData['callback_action'] && $PostData['callback'] = $CallBack):
    $Case = $PostData['callback_action'];
    if (!empty($_FILES['image'])):
        $File = $_FILES['image'];
    endif;
    unset($PostData['callback'], $PostData['formToken'], $PostData['callback_action'], $PostData['key_id'], $PostData['image'], $PostData['rascunho']);


    $Read = new Read;
    $Create = new Create;
    $Update = new Update;
    $Delete = new Delete;
    $Upload = new Upload('../../uploads/');

    //SELECIONA AÇÃO
    switch ($Case):
        case "manager":
            if ($PostFormToken == $formToke):
                $Read->ExeRead(DB_SOCIAIS, "WHERE social_id = :id", "id={$KeyId}");
                if (!$Read->getResult()):

                else:
                    $name = !empty($PostData['social_title']) ? Check::Name($PostData['social_title']) : $post['social_id'];
                    if (!empty($File)):
                        $Upload->Image($File, "{$name}-" . time(), 1000);
                        if ($Upload->getResult()):
                            $image = $Upload->getResult();
                            $PostData['social_image'] = $image;
                            $jSON['image'] = ["src" => "../tim.php?src=uploads/{$image}&w=350&h=350"];
                        endif;
                    endif;
                    $ReadName = new $Read;
                    $ReadName->FullRead("SELECT social_name FROM " . DB_SOCIAIS . " WHERE social_name= :nm and social_id != :id ", "nm={$name}&id={$KeyId}");
                    if ($ReadName->getResult()):
                        $PostData['social_name'] = $name . time();
                    else:
                        $PostData['social_name'] = $name;
                    endif;

                    $PostData['status'] = ($rascunho) ? 0 : 1;

                    $post = $Read->getResult()[0];
                    $Update->ExeUpdate($TABLE['DB'], $PostData, "WHERE social_id = :id", "id={$KeyId}");
                    $jSON["success"] = ["title" => "Sucesso", "text" => "O registro {$name}, foi atualizado com sucesso!", "type" => "info", "style" => "dark"];
                endif;
            else:
                die('<center><h1>Permissão Negada!</h1></center>');
            endif;

            break;

        case 'sendimage':
            $NewImage = $_FILES['image'];
            $Read->FullRead("SELECT social_title, social_name FROM " . DB_SOCIAIS . " WHERE social_id = :id", "id={$KeyId}");
            if (!$Read->getResult()):
                $jSON['error'] = "ok";
            else:
                $Upload = new Upload('../../uploads/');
                $Upload->Image($NewImage, $KeyId . '-' . time(), IMAGE_W);
                if ($Upload->getResult()):
                    $PostData['image'] = $Upload->getResult();

                    $jSON['tinyMCE'] = "<img title='{$Read->getResult()[0]['social_title']}' alt='{$Read->getResult()[0]['social_title']}' src='../uploads/{$PostData['image']}'/>";
                else:
                    $jSON['success'] = AjaxErro("<b class='icon-image'>ERRO AO ENVIAR IMAGEM:</b> Olá {$_SESSION['userLogin']['user_name']}, selecione uma imagem JPG ou PNG para inserir no post!", E_USER_WARNING);
                endif;
            endif;
            break;

    endswitch;

    if (isset($jSON)) {
        echo json_encode($jSON);
    };
else:
    die('<center><h1>Permissão Negada!</h1></center>');
endif;
